/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.service;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.log4j.Logger;
import org.hibernate.Query;

import com.fiveamsolutions.nci.commons.data.persistent.Deletable;
import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.nci.commons.service.AbstractBaseSearchBean;
import com.fiveamsolutions.nci.commons.util.UsernameHolder;
import com.fiveamsolutions.tissuelocator.data.AccessRestricted;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.InstitutionRestricted;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.annotation.OnSave;
import com.fiveamsolutions.tissuelocator.service.user.TissueLocatorUserServiceLocal;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;
import com.google.inject.Inject;

/**
 * Implementation of the abstract base class.
 * @param <T> type to manipulate
 * @author smiller
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class GenericServiceBean<T extends PersistentObject> extends AbstractBaseSearchBean<T>
    implements GenericServiceLocal<T> {

    private static final Logger LOG = Logger.getLogger(GenericServiceBean.class);
    
    /**
     * Exception message if an object is in a state that does not permit deletion.
     */
    public static final String CURRENTLY_NOT_DELETABLE = "Object currently is not deletable.";

    private TissueLocatorUserServiceLocal userService;
    
    
    /**
     * @return the userService
     */
    public TissueLocatorUserServiceLocal getUserService() {
        return userService;
    }

    /**
     * @param userService the userService to set
     */
    @Inject
    public void setUserService(TissueLocatorUserServiceLocal userService) {
        this.userService = userService;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public <TYPE extends PersistentObject> TYPE getPersistentObject(Class<TYPE> clazz, Long id) {
        TYPE object = (TYPE) TissueLocatorHibernateUtil.getCurrentSession().get(clazz, id);
        enforceAccessRestrictions(object);
        return object;
    }

    /**
     * {@inheritDoc}
     */
    public Long savePersistentObject(T o) {
        enforceAccessRestrictions(o);
        enforceInstitutionRestrictions(o);
        onSave(o);
        TissueLocatorHibernateUtil.getCurrentSession().saveOrUpdate(o);
        return o.getId();
    }
    
    /**
     * Perform any operations annotated with {@link OnSave}.
     * @param o Persistent object.
     */
    protected void onSave(T o) {
        Map<Method, Integer> onSaveMap = new HashMap<Method, Integer>();
        Method[] methods = o.getClass().getMethods();
        for (Method m : methods) {
            OnSave a = m.getAnnotation(OnSave.class);
            if (a != null) {
                onSaveMap.put(m, a.index());
            }
        }
        List<Method> onSaveMethods = new ArrayList<Method>(onSaveMap.size());
        for (Map.Entry<Method, Integer> entry : onSaveMap.entrySet()) {
            onSaveMethods.add(entry.getValue(), entry.getKey());
        }
        invokeOnSave(o, onSaveMethods);
    }
    
    private void invokeOnSave(T o, List<Method> onSaveMethods) {
        for (Method m : onSaveMethods) {
            try {
                m.invoke(o, new Object[0]);
            } catch (IllegalArgumentException e) {
                LOG.error("Exception invoking onSave method: " + m.getName(), e);
            } catch (IllegalAccessException e) {
                LOG.error("Exception invoking onSave method: " + m.getName(), e);
            } catch (InvocationTargetException e) {
                LOG.error("Exception invoking onSave method: " + m.getName(), e);
            }
        }
    }
    
    /**
     * {@inheritDoc}
     */
    public void deletePersistentObject(T o) {
        enforceAccessRestrictions(o);
        enforceInstitutionRestrictions(o);
        enforceDeletionRestrictions(o);
        TissueLocatorHibernateUtil.getCurrentSession().delete(o);
    }

    private <TYPE extends PersistentObject> void enforceAccessRestrictions(TYPE object) {
        if (!(object instanceof AccessRestricted)) {
            return;
        }
        if (!checkAccessRestrictions((AccessRestricted) object)) {
            throw new IllegalArgumentException("User does not have access to this object.");
        }
    }

    private boolean checkAccessRestrictions(AccessRestricted ar) {
        TissueLocatorUser u = getUserService().getByUsername(UsernameHolder.getUser());
        if (u == null) {
            LOG.debug("Unknown current user: " + UsernameHolder.getUser());
            return false;
        }
        return ar.isAccessible(u);
    }
    
    
    private void enforceInstitutionRestrictions(T o) {
        if (!(o instanceof InstitutionRestricted)) {
            return;
        }
        if (!checkInstitutionRestrictions((InstitutionRestricted) o)) {
            throw new IllegalArgumentException("User does not belong to all related institutions.");
        }
    }

    private boolean checkInstitutionRestrictions(InstitutionRestricted ir) {
        // Some copy-paste from the user service, but we can't use an @EJB here
        // because we'd end up with circular dependencies.
        Query q = TissueLocatorHibernateUtil.getCurrentSession()
                      .createQuery("FROM " + TissueLocatorUser.class.getName() + " u WHERE u.username = :name");
        q.setString("name", UsernameHolder.getUser());
        TissueLocatorUser u = (TissueLocatorUser) q.uniqueResult();
        Set<Institution> userInstitutions = new HashSet<Institution>();
        if (u != null) {
            if (u.isCrossInstitution()) {
                return true;
            }
            userInstitutions.add(u.getInstitution());
        } else {
            LOG.debug("Unknown current user: " + UsernameHolder.getUser());
        }
        // need a copy of related institutions, so we can remove from it as we find one thas pass
        Set<Institution> tmp = new HashSet<Institution>(ir.getRelatedInstitutions());

        for (Institution i : userInstitutions) {
            // remove institutions that the user is related to
            tmp.remove(i);
        }

        // empty -> user's Institutions >= ir.relatedInstitutions
        boolean passes = tmp.isEmpty();

        if (!passes) {
            passes = performAlternateChecks(ir, u);
        }
        return passes;
    }

    /**
     * Perform alternate security checks.  Default implementation returns false.  Subclasses should override this
     * implementation with custom logic.
     * @param ir the object being operated on
     * @param u the current user
     * @return true if the object passes the secondary checks, false otherwise.
     */
    protected boolean performAlternateChecks(InstitutionRestricted ir, TissueLocatorUser u) {
        return false;
    }

    private void enforceDeletionRestrictions(T o) {
        if (!(o instanceof Deletable)) {
            throw new IllegalArgumentException("Object may not be deleted");
        }
        if (!((Deletable) o).isDeletable()) {
            throw new IllegalArgumentException(CURRENTLY_NOT_DELETABLE);
        }
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public <TYPE extends PersistentObject> List<TYPE> getAll(Class<TYPE> type) {
        return TissueLocatorHibernateUtil.getCurrentSession().createCriteria(type).list();
    }
}
