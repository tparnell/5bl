/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.service.config.search;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.fiveamsolutions.tissuelocator.data.config.search.AbstractSearchFieldConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.SearchSetType;

/**
 * Encapsulates an entire search configuration, including
 * search field configurations and any additional configuration.
 * @author gvaughn
 *
 */
public class SearchConfig {

    private static final Map<SearchSetType, Collection<AbstractSearchFieldConfig>> SEARCH_CONFIGS 
        = new HashMap<SearchSetType, Collection<AbstractSearchFieldConfig>>();
    
    private final Collection<AbstractSearchFieldConfig> searchFieldConfigs = new ArrayList<AbstractSearchFieldConfig>();
    private final  Map<Class<? extends AbstractSearchFieldConfig>, Map<Long, AbstractSearchFieldConfig>> configMap = 
        new HashMap<Class<? extends AbstractSearchFieldConfig>, Map<Long, AbstractSearchFieldConfig>>();
    
    
    /**
     * @param searchSetType Set of search fields this configuration will contain.
     * @param searchFieldConfigService search feild config service.
     * 
     */
    public SearchConfig(SearchSetType searchSetType, SearchFieldConfigServiceLocal searchFieldConfigService) {
        initSearchFieldConfigs(searchSetType, searchFieldConfigService);
    }
    
    private void initSearchFieldConfigs(SearchSetType searchSetType, 
            SearchFieldConfigServiceLocal searchFieldConfigService) {
        searchFieldConfigs.addAll(getSearchConfigs(searchSetType, searchFieldConfigService));
        for (AbstractSearchFieldConfig config : searchFieldConfigs) {
            if (!configMap.containsKey(config.getClass())) {
                configMap.put(config.getClass(), new HashMap<Long, AbstractSearchFieldConfig>());
            }
            configMap.get(config.getClass()).put(config.getId(), config);
        }
    }
    
    private static synchronized Collection<AbstractSearchFieldConfig> getSearchConfigs(SearchSetType searchSetType, 
            SearchFieldConfigServiceLocal configService) {
        if (!SEARCH_CONFIGS.containsKey(searchSetType)) {
            Collection<AbstractSearchFieldConfig> configs = configService.getSearchFieldConfigs(searchSetType);
            SEARCH_CONFIGS.put(searchSetType, configs);
        }
        return SEARCH_CONFIGS.get(searchSetType);
    }

    /**
     * Returns the collection of all search field configurations.
     * @return the collection of all search field configurations.
     */
    public Collection<AbstractSearchFieldConfig> getSearchFieldConfigs() {
        return searchFieldConfigs;
    }
    
    /**
     * Returns the search field configuration of the given type with the given id.
     * @param <TYPE> Type of the configuration to be returned.
     * @param type Class type of the configuration.
     * @param id Id of the configuration.
     * @return the search field configuration of the given type with the given id.
     */
    @SuppressWarnings("unchecked")
    public <TYPE extends AbstractSearchFieldConfig> TYPE getConfig(Class<TYPE> type, Long id) {
        return (TYPE) configMap.get(type).get(id);
    }
    
    /**
     * Clears all cached search configuration data for testing purposes.
     */
    public static void reset() {
        SEARCH_CONFIGS.clear();
    }
}
