/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.data.preferences;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.validator.NotNull;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.config.category.DisplayOption;
import com.fiveamsolutions.tissuelocator.data.config.category.SearchResultFieldDisplaySetting;

/**
 * @author cgoina
 *
 */
@Entity
@Table(name = "search_result_field_display_user_preference")
public class SearchResultFieldDisplayUserPreference implements PersistentObject {

    private static final long serialVersionUID = -3125162137225035233L;

    private Long id;
    private TissueLocatorUser user;
    private SearchResultFieldDisplaySetting fieldDisplaySetting;
    private DisplayOption displayFlag;

    /**
     * Default constructor.
     */
    public SearchResultFieldDisplayUserPreference() {
        displayFlag = DisplayOption.DISABLED;
    }

    /**
     * @return the id
     */
    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the user
     */
    @ManyToOne
    @JoinColumn(name = "user_id")
    @NotNull
    @ForeignKey(name = "TISSUE_LOCATOR_USER_FK")
    public TissueLocatorUser getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(TissueLocatorUser user) {
        this.user = user;
    }

    /**
     * @return the fieldDisplaySetting
     */
    @ManyToOne
    @JoinColumn(name = "field_display_setting_id")
    @NotNull
    @ForeignKey(name = "FIELD_DISPLAY_SETTING_FK")
    public SearchResultFieldDisplaySetting getFieldDisplaySetting() {
        return fieldDisplaySetting;
    }

    /**
     * @param fieldDisplaySetting the fieldDisplaySetting to set
     */
    public void setFieldDisplaySetting(SearchResultFieldDisplaySetting fieldDisplaySetting) {
        this.fieldDisplaySetting = fieldDisplaySetting;
    }

    /**
     * @return the field name
     */
    @Transient
    public String getFieldName() {
        return fieldDisplaySetting != null ? fieldDisplaySetting.getFieldConfig().getSearchFieldName() : null;
    }

    /**
     * User's display preference for the field referenced by fieldDisplaySeting.
     * @return the displayFlag
     */
    @Column(name = "display_flag")
    @Enumerated(EnumType.STRING)
    @NotNull
    public DisplayOption getDisplayFlag() {
        return displayFlag;
    }

    /**
     * @param displayFlag the displayFlag to set
     */
    public void setDisplayFlag(DisplayOption displayFlag) {
        this.displayFlag = displayFlag;
    }

    /**
     * @see com.fiveamsolutions.tissuelocator.data.config.category.SearchResultFieldDisplaySetting
     *
     * @return true if display flag is ENABLED false otherwise
     */
    @Transient
    public boolean isDisplayFlagAsBoolean() {
        return displayFlag == DisplayOption.ENABLED ? true : false;
    }

    /**
     * @param booleanDisplayFlag if true this.displayFlag is set to ENABLED otherwise DISABLED
     */
    public void setDisplayFlagAsBoolean(boolean booleanDisplayFlag) {
        displayFlag = booleanDisplayFlag ? DisplayOption.ENABLED : DisplayOption.DISABLED;
    }

}
