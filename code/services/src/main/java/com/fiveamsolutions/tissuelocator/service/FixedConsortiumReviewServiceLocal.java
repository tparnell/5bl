/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.service;

import javax.ejb.Local;
import javax.mail.MessagingException;

import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestReviewVote;

/**
 * @author ddasgupta
 *
 */
@Local
public interface FixedConsortiumReviewServiceLocal extends SpecimenRequestServiceLocal {

    /**
     * Save a request with new assignees, and send an email to all new assignees.
     * @param request the request to assign
     * @param votingPeriod the length of the voting period
     * @throws MessagingException on error sending assignment email
     */
    void assignRequest(SpecimenRequest request, int votingPeriod) throws MessagingException;

    /**
     * Decline a review for a request.
     * @param request the request with a declined review
     * @param declinedReview the review to decline
     * @param votingPeriod the length of the voting period
     * @param reviewerAssignmentPeriod the length of the reviewer assignment period
     * @throws MessagingException on error sending assignment email
     */
    void declineReview(SpecimenRequest request, SpecimenRequestReviewVote declinedReview,
            int votingPeriod, int reviewerAssignmentPeriod) throws MessagingException;

    /**
     * Save the final consortium vote and comment, and email the review decision notifiers.
     * @param request the request to update
     * @throws MessagingException on error
     */
    void saveFinalVote(SpecimenRequest request) throws MessagingException;
}
