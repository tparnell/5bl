/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.service;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.log4j.Logger;

import com.fiveamsolutions.nci.commons.search.SearchCriteria;
import com.fiveamsolutions.tissuelocator.data.RequestStatus;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.service.search.TissueLocatorAnnotatedBeanSearchCriteria;
import com.fiveamsolutions.tissuelocator.util.Email;
import com.fiveamsolutions.tissuelocator.util.EmailHelper;
import com.fiveamsolutions.tissuelocator.util.RequestProcessingConfiguration;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;

/**
 * @author ddasgupta
 */
@Stateless
public class SpecimenRequestProcessingServiceBean extends GenericServiceBean<SpecimenRequest> implements
        SpecimenRequestProcessingServiceLocal {

    private static final Logger LOG = Logger.getLogger(SpecimenRequestProcessingServiceBean.class);
    
    private final EmailHelper emailHelper = new EmailHelper();

    @EJB
    private SpecimenRequestServiceLocal specimenRequestService;

    /**
     * {@inheritDoc}
     */
    @TransactionAttribute(TransactionAttributeType.NEVER)
    public void processPendingRequests(RequestProcessingConfiguration config) {
        try {
            // Manually managing session because this method is called from a background thread,
            // so the OSIV filter is not in effect
            TissueLocatorHibernateUtil.getHibernateHelper().openAndBindSession();

            setupConfig(config);

            List<SpecimenRequest> pendingDecisionRequests = getRequestsByStatus(RequestStatus.PENDING_FINAL_DECISION);
            LOG.info("found " + pendingDecisionRequests.size() + " pending decision requests.");
            for (SpecimenRequest request : pendingDecisionRequests) {
                try {
                    SpecimenRequest persistentReq = (SpecimenRequest) TissueLocatorHibernateUtil.getCurrentSession().
                        get(SpecimenRequest.class, request.getId());
                    getSpecimenRequestService().processPendingDecisionRequest(persistentReq, config);
                    TissueLocatorHibernateUtil.getCurrentSession().flush();
                } catch (Exception e) {
                    LOG.error("error processing pending final decision request " + request.getId(), e);
                } finally {
                    TissueLocatorHibernateUtil.getCurrentSession().clear();
                }
            }

            List<SpecimenRequest> pendingRequests = getRequestsByStatus(RequestStatus.PENDING);
            LOG.info("found " + pendingRequests.size() + " pending requests.");
            for (SpecimenRequest request : pendingRequests) {
                try {
                    SpecimenRequest persistentReq = (SpecimenRequest) TissueLocatorHibernateUtil.getCurrentSession().
                        get(SpecimenRequest.class, request.getId());
                    getSpecimenRequestService().processPendingRequest(persistentReq, config);
                    TissueLocatorHibernateUtil.getCurrentSession().flush();
                } catch (Exception e) {
                    LOG.error("error processing pending request " + request.getId(), e);
                } finally {
                    TissueLocatorHibernateUtil.getCurrentSession().clear();                   
                }
            }
        } finally {
            TissueLocatorHibernateUtil.getHibernateHelper().unbindAndCleanupSession();
        }
    }

    private List<SpecimenRequest> getRequestsByStatus(RequestStatus status) {
        SpecimenRequest sr = new SpecimenRequest();
        sr.setStatus(status);
        SearchCriteria<SpecimenRequest> criteria = new TissueLocatorAnnotatedBeanSearchCriteria<SpecimenRequest>(sr);
        return getSpecimenRequestService().search(criteria);
    }

    private void setupConfig(RequestProcessingConfiguration config) {
        config.setReviewLateEmail(getEmail("reviewLate"));
        config.setVoteLateEmail(getEmail("voteLate"));
        config.setDeadlockEmail(getEmail("deadlock"));
        config.setDeadlockInstAdminEmail(getEmail("deadlockInstAdmin"));
        config.setReviewerAssignmentLateEmail(getEmail("reviewerAssignmentLate"));
        config.setInstitutionalAdminEmail(getEmail("instAdmin"));
        config.setVoteFinalizedEmail(getEmail("voteFinalized"));
        config.setVoteFinalizedAssignmentEmail(getEmail("voteFinalizedAssignment"));
        config.setRequestPendingReleaseEmail(getEmail("coordinator"));
     }

    private Email getEmail(String keySuffix) {
        return emailHelper.getEmail("specimenRequest.email." + keySuffix, new String[0]);
    }

    /**
     * @return the specimenRequestService
     */
    public SpecimenRequestServiceLocal getSpecimenRequestService() {
        return this.specimenRequestService;
    }

    /**
     * @param specimenRequestService the specimenRequestService to set
     */
    public void setSpecimenRequestService(SpecimenRequestServiceLocal specimenRequestService) {
        this.specimenRequestService = specimenRequestService;
    }
}
