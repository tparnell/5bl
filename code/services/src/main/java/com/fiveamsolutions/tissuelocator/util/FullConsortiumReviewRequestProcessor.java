/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.mail.MessagingException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.fiveamsolutions.nci.commons.data.security.AbstractUser;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.RequestStatus;
import com.fiveamsolutions.tissuelocator.data.Role;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestReviewVote;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.Vote;
import com.fiveamsolutions.tissuelocator.service.InstitutionServiceLocal;
import com.fiveamsolutions.tissuelocator.service.NotEnoughVotesException;
import com.fiveamsolutions.tissuelocator.service.SpecimenRequestStatusAnalyzer;
import com.fiveamsolutions.tissuelocator.service.SpecimenRequestVoteAnalyzer;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceLocal;
import com.fiveamsolutions.tissuelocator.service.user.TissueLocatorUserServiceLocal;
import com.google.inject.Inject;

/**
 * Implementation of the request processor interface to support the full consortium review process.
 * @author ddasgupta
 */
@SuppressWarnings("PMD.TooManyMethods")
public class FullConsortiumReviewRequestProcessor implements RequestProcessor {

    private static final Logger LOG = Logger.getLogger(FullConsortiumReviewRequestProcessor.class);

    private InstitutionServiceLocal institutionService;
    private TissueLocatorUserServiceLocal userService;

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("PMD.ExcessiveParameterList")
    //CHECKSTYLE:OFF too many parameters
    public Collection<Email> initializeRequest(SpecimenRequest request, Email emailBase, Email leadReviewContent,
            Email institutionalApprovalContent, Email revisionEmail, Email reviewerNotAssignedBase,
            int numReviewers, boolean isNew) {
    //CHECKSTYLE:ON

        setReviewCommittee(request, numReviewers);
        setVotes(request, isNew);
        clearRevisionRequestVotesIfNeeded(request, isNew);

        Collection<Email> emails = new ArrayList<Email>();
        if (!isNew) {
            emails.addAll(getRevisionEmails(request, revisionEmail));
            emails.addAll(getRequestEmails(request, emailBase, leadReviewContent, institutionalApprovalContent,
                    reviewerNotAssignedBase, true));
        } else {
            emails.addAll(getRequestEmails(request, emailBase, leadReviewContent, institutionalApprovalContent,
                    reviewerNotAssignedBase, false));
        }
        return emails;
    }

    private void setReviewCommittee(SpecimenRequest request, int numReviewers) {
        if (request.getReviewers().isEmpty()) {
            List<Institution> reviewers = getInstitutionService().getReviewers(numReviewers);
            for (Institution reviewer : reviewers) {
                reviewer.setLastReviewAssignment(new Date());
                request.getReviewers().add(reviewer);
            }
        }
    }

    private void setVotes(SpecimenRequest r, boolean isNew) {
        if (isNew) {
            // on first save make sure all review objects are created with a null vote
            List<Institution> consortiumReviewers = getInstitutionService().getConsortiumMembers();
            for (Institution inst : consortiumReviewers) {
                SpecimenRequestReviewVote vote = new SpecimenRequestReviewVote();
                vote.setInstitution(inst);
                r.getConsortiumReviews().add(vote);
            }

            for (Institution inst : SpecimenRequestVoteAnalyzer.getInsitutionsNeedingToPerformInstitutionalReview(r)) {
                SpecimenRequestReviewVote vote = new SpecimenRequestReviewVote();
                vote.setInstitution(inst);
                r.getInstitutionalReviews().add(vote);
            }
        }
    }

    private void clearRevisionRequestVotesIfNeeded(SpecimenRequest request, boolean isNew) {
        if (!isNew) {
            for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
                if (Vote.REVISE.equals(vote.getVote())) {
                    vote.setVote(null);
                }
            }
        }
    }

    private Collection<Email> getRevisionEmails(SpecimenRequest request, Email revisionEmail) {
        Collection<Email> emails = new ArrayList<Email>();
        for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
            if (vote.getUser() != null) {
                emails.add(getEmail(revisionEmail, vote.getUser().getEmail(), request, null, null, null, null));
            }
        }
        return emails;
    }

    @SuppressWarnings({"PMD.ExcessiveParameterList" })
    private Collection<Email> getRequestEmails(SpecimenRequest request, Email emailBase, Email leadReviewContent,
            Email institutionalApprovalContent, Email reviewerNotAssignedBase, boolean isRevision) {

        Collection<Email> emails = new ArrayList<Email>();
        Set<Institution> institutions = new HashSet<Institution>();
        for (SpecimenRequestLineItem lineItem : request.getLineItems()) {
            institutions.add(lineItem.getSpecimen().getExternalIdAssigner());
        }

        Set<TissueLocatorUser> reviewAssigners = getReviewAssigners(request);
        for (AbstractUser reviewAssigner : reviewAssigners) {
            StringBuffer textContent = new StringBuffer();
            StringBuffer htmlContent = new StringBuffer();
            String leadReviewerContent = "";

            Institution reviewAssignerInstitution = ((TissueLocatorUser) reviewAssigner).getInstitution();
            if (request.getReviewers().contains(reviewAssignerInstitution)) {
                leadReviewerContent = leadReviewContent.getText();
                textContent.append(leadReviewContent.getText());
                htmlContent.append(leadReviewContent.getHtml());
            }

            //This is a little strange - we're going to send the reviewer assigners an email saying
            //that they have an institutional review to do, but institutional reviews aren't protected
            //by the reviewer assigner role. This is ok because that role is assigned to the same users,
            //and it is better than sending separate emails.
            if (institutions.contains(reviewAssignerInstitution)) {
                textContent.append(institutionalApprovalContent.getText());
                htmlContent.append(institutionalApprovalContent.getHtml());
            }

            if (isRevision) {
                String reviewer = getAssignedScienticReviewer(request, reviewAssignerInstitution);
                Email revisionBase = getBaseEmail(reviewer, emailBase, reviewerNotAssignedBase);

                emails.add(getEmail(revisionBase, reviewAssigner.getEmail(), request, textContent.toString(),
                        htmlContent.toString(), reviewer, leadReviewerContent));
            } else {
                emails.add(getEmail(emailBase, reviewAssigner.getEmail(), request, textContent.toString(),
                        htmlContent.toString(), "", ""));
            }

        }
        return emails;
    }

    private Set<TissueLocatorUser> getReviewAssigners(SpecimenRequest request) {
        Set<TissueLocatorUser> reviewAssigners = new HashSet<TissueLocatorUser>();
        Set<Institution> reviewInstitutions = new HashSet<Institution>();
        for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
            reviewInstitutions.add(vote.getInstitution());
        }
        reviewAssigners.addAll(getUserService().getByRoleAndInstitutions(Role.SCIENTIFIC_REVIEWER_ASSIGNER.getName(),
                reviewInstitutions));
        return reviewAssigners;
    }

    private Email getBaseEmail(String reviewer, Email emailBase, Email baseNoReviewerAssigned) {
        return StringUtils.isBlank(reviewer) ? baseNoReviewerAssigned : emailBase;
    }

    private String getAssignedScienticReviewer(SpecimenRequest request, Institution institution) {
        String assignedScientificReviewer = "";
        for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
            if (vote.getInstitution().equals(institution) && vote.getUser() != null) {
                assignedScientificReviewer = vote.getUser().getFirstName() + " " + vote.getUser().getLastName();
                break;
            }
        }
        return assignedScientificReviewer;
    }

    @SuppressWarnings({"PMD.ExcessiveParameterList" })
    private Email getEmail(Email template, String recipient, SpecimenRequest request, String textReplace,
            String htmlReplace, String assignedUser, String leadReviewer) {
        Email email = new Email();
        email.setRecipient(recipient);
        email.setSubject(template.getSubject());
        email.setHtml(template.getHtml());
        email.setText(template.getText());

        email.setRequest(request);
        email.setTextReplacement(textReplace);
        email.setHtmlReplacement(htmlReplace);
        email.setAssignedUser(assignedUser);
        email.setLeadReviewer(leadReviewer);
        return email;
    }

    /**
     * {@inheritDoc}
     */
    public Collection<Email> processPendingRequest(SpecimenRequest request,
            RequestProcessingConfiguration config) {
        Collection<Email> emails = new ArrayList<Email>();
        try {
            LOG.info("processing request " + request.getId());
            SpecimenRequestVoteAnalyzer voteAnalyzer = new SpecimenRequestVoteAnalyzer(config);
            RequestStatus finalVote = voteAnalyzer.tabulateVotes(request);

            if (finalVote != null && !RequestStatus.PENDING.equals(finalVote)) {
                emails.addAll(finishVote(request, finalVote, config));
            }
        } catch (NotEnoughVotesException e) {
            emails.addAll(emailMissingVotes(request, e, config));
        }

        if (RequestStatus.PENDING.equals(request.getStatus())) {
            SpecimenRequestStatusAnalyzer statusAnalyzer = new SpecimenRequestStatusAnalyzer(config);
            emails.addAll(analyzeRequestStatus(statusAnalyzer, request, config));
        }
        return emails;
    }

    private Collection<Email> emailMissingVotes(SpecimenRequest request, NotEnoughVotesException e,
            RequestProcessingConfiguration config) {
        Collection<Email> emails = new ArrayList<Email>();
        try {
            Set<AbstractUser> usersToEmail = new HashSet<AbstractUser>();
            Set<AbstractUser> copyInstAdmins = new HashSet<AbstractUser>();

            populateUsersToEmail(request, usersToEmail, copyInstAdmins, e);

            for (AbstractUser member : usersToEmail) {
                emails.add(getEmail(e.getEmail(), member.getEmail(), request, null, null, null, null));
            }

            if (!copyInstAdmins.isEmpty()) {
                emails.addAll(notifyInstitutionalAdmins(request, copyInstAdmins, e.getEmail(),
                                e.isVoteDeadLocked() ? config.getDeadlockInstAdminEmail()
                                        : config.getInstitutionalAdminEmail()));
            }
        } catch (Exception e2) {
            LOG.error("error processing NotEnoughVotesException for request " + request.getId(), e2);
        }
        return emails;
    }

    private void populateUsersToEmail(SpecimenRequest request, Set<AbstractUser> usersToEmail,
            Set<AbstractUser> copyInstAdmins, NotEnoughVotesException e) {

        if (!e.isConsortiumReviewComplete() && !e.isVoteDeadLocked()) {
            //send email to all committee members whose institution hasen't voted yet only if the
            //vote is not deadlocked
            Set<AbstractUser> outstandingReviewers = getOutstandingConsortiumReviewers(request);
            usersToEmail.addAll(outstandingReviewers);
            copyInstAdmins.addAll(outstandingReviewers);
        }

        if (!e.isInstitutionalReviewComplete()) {
            // Send mail to all institutional reviewers from institutes who have not voted.
            usersToEmail.addAll(getReviewersToEmail(request.getInstitutionalReviews(),
                    Role.INSTITUTIONAL_REVIEW_VOTER.getName()));
        }

        if (e.isVoteDeadLocked()) {
            //Sent an email to all scientific reviewers and their institutional assigners if the voting
            //is deadlocked
            Set<AbstractUser> notify = getAllScientificReviewers(request);
            usersToEmail.addAll(notify);
            copyInstAdmins.addAll(notify);
        }

    }

    private Set<AbstractUser> getReviewersToEmail(Set<SpecimenRequestReviewVote> votes, String roleName) {
        Set<AbstractUser> usersToEmail = new HashSet<AbstractUser>();
        Set<Institution> needToVote = new HashSet<Institution>();
        for (SpecimenRequestReviewVote vote : votes) {
            if (vote.getVote() == null) {
                needToVote.add(vote.getInstitution());
            }
        }
        for (AbstractUser member : getUserService().getUsersInRole(roleName)) {
            if (needToVote.contains(((TissueLocatorUser) member).getInstitution())) {
                usersToEmail.add(member);
            }
        }
        return usersToEmail;
    }

    private Set<AbstractUser> getAllScientificReviewers(SpecimenRequest request) {
        Set<AbstractUser> leadReviewers = new HashSet<AbstractUser>();
        //create a set of all of the assigned lead reviewers
        for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
            if (vote.getUser() != null) {
                leadReviewers.add(vote.getUser());
            }
        }
        return leadReviewers;
    }

    private Set<AbstractUser> getOutstandingConsortiumReviewers(SpecimenRequest request)  {
        Set<AbstractUser> outstandingReviewers = new HashSet<AbstractUser>();
        for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
            if (vote.getUser() != null && vote.getVote() == null) {
                outstandingReviewers.add(vote.getUser());
            }
        }
        return outstandingReviewers;
    }

    private Collection<Email> notifyInstitutionalAdmins(SpecimenRequest request, Set<AbstractUser> reviewers,
            Email reviewerEmail, Email institutionalAdminEmail) {
        Collection<Email> emails = new ArrayList<Email>();
        Set<AbstractUser> instAdmins = getUserService().getUsersInRole(Role.SCIENTIFIC_REVIEWER_ASSIGNER.getName());
        for (AbstractUser reviewer : reviewers) {
            for (AbstractUser instAdmin : instAdmins) {
                Institution reviewerInst = ((TissueLocatorUser) reviewer).getInstitution();
                Institution instAdminInst = ((TissueLocatorUser) instAdmin).getInstitution();
                if (reviewerInst.equals(instAdminInst)) {
                    String reviewerName = reviewer.getFirstName() + " " + reviewer.getLastName();
                    Email combinedEmail = new Email();
                    combinedEmail.setSubject(institutionalAdminEmail.getSubject());
                    combinedEmail.setText(institutionalAdminEmail.getText() + reviewerEmail.getText());
                    combinedEmail.setHtml(institutionalAdminEmail.getHtml() + reviewerEmail.getHtml());
                    emails.add(getEmail(combinedEmail, instAdmin.getEmail(), request, reviewerName,
                            reviewerName, null, null));
                }
            }
        }
        return emails;
    }

    private Collection<Email> analyzeRequestStatus(SpecimenRequestStatusAnalyzer statusAnalyzer,
            SpecimenRequest request, RequestProcessingConfiguration config) {
        Collection<Email> emails = new ArrayList<Email>();
        try {
            OutstandingSpecimenRequestRequirements issues = statusAnalyzer.analyzeRequestStatus(request);
            emails.addAll(emailOutstandingReviewAssignments(issues, config, request));
            emails.addAll(emailOutstandingLeadReviewers(issues, config, request));
        } catch (Exception e) {
            //catch general exception to ensure that an error while processing one request does not
            //affect all subsequent requests
            LOG.error("error processing request status " + request.getId(), e);
        }
        return emails;
    }

    private Collection<Email> emailOutstandingReviewAssignments(OutstandingSpecimenRequestRequirements issues,
            RequestProcessingConfiguration config, SpecimenRequest request) throws MessagingException {
        Collection<Email> emails = new ArrayList<Email>();
        if (!issues.getOutstandingReviewAssignments().isEmpty()) {
            Set<AbstractUser> reviewerAssigners =
                getUserService().getUsersInRole(Role.SCIENTIFIC_REVIEWER_ASSIGNER.getName());
            Map<Institution, AbstractUser> reviewerAssignerMap = new HashMap<Institution, AbstractUser>();
            for (AbstractUser reviewerAssigner : reviewerAssigners) {
                reviewerAssignerMap.put(((TissueLocatorUser) reviewerAssigner).getInstitution(), reviewerAssigner);
            }
            for (Institution outstanding : issues.getOutstandingReviewAssignments()) {
                if (reviewerAssignerMap.get(outstanding) != null) {
                    emails.add(getEmail(config.getReviewerAssignmentLateEmail(),
                            reviewerAssignerMap.get(outstanding).getEmail(), request, null, null, null, null));
                } else {
                    LOG.warn("No user exists to assign reviewers for " + outstanding.getName() + " : "
                            + outstanding.getId());
                }
            }
        }
        return emails;
    }

    private Collection<Email> emailOutstandingLeadReviewers(OutstandingSpecimenRequestRequirements issues,
            RequestProcessingConfiguration config, SpecimenRequest request) throws MessagingException {
        Collection<Email> emails = new ArrayList<Email>();
        if (!issues.getOutstandingLeadReviewers().isEmpty()) {
            for (AbstractUser member : issues.getOutstandingLeadReviewers()) {
                emails.add(getEmail(config.getReviewLateEmail(), member.getEmail(), request, null, null, null, null));
            }
            emails.addAll(notifyInstitutionalAdmins(request, issues.getOutstandingLeadReviewers(),
                    config.getReviewLateEmail(), config.getInstitutionalAdminEmail()));
        }
        return emails;
    }

    private Collection<Email> finishVote(SpecimenRequest request, RequestStatus finalVote,
            RequestProcessingConfiguration config) {
        LOG.info("finishing vote for request " + request.getId() + " with outcome " + finalVote.name());
        request.setExternalComment(null);
        request.setStatus(RequestStatus.PENDING_FINAL_DECISION);
        request.setFinalVote(finalVote);
        request.setUpdatedDate(new Date());
        return processPendingDecisionRequest(request, config);
    }

    /**
     * {@inheritDoc}
     */
    public Collection<Email> processPendingDecisionRequest(SpecimenRequest request,
            RequestProcessingConfiguration config) {
        Collection<Email> emails = new ArrayList<Email>();
        Set<AbstractUser> scientificReviewers = getAllScientificReviewers(request);
        Set<AbstractUser> leadReviewers = new HashSet<AbstractUser>();
        for (AbstractUser reviewer : scientificReviewers) {
            if (request.getReviewers().contains(((TissueLocatorUser) reviewer).getInstitution())) {
                emails.add(getEmail(config.getVoteFinalizedEmail(), reviewer.getEmail(),
                        request, null, null, null, null));
                leadReviewers.add(reviewer);
            }
        }
        if (!leadReviewers.isEmpty()) {
            emails.addAll(notifyInstitutionalAdmins(request, leadReviewers, config.getVoteFinalizedEmail(),
                    config.getInstitutionalAdminEmail()));
        } else {
            emails.addAll(notifyLeadReviewerAssigners(request, config.getVoteFinalizedAssignmentEmail()));
        }
        return emails;
    }

    private Collection<Email> notifyLeadReviewerAssigners(SpecimenRequest request, Email assignmentEmail) {
        Collection<Email> emails = new ArrayList<Email>();
        Set<AbstractUser> instAdmins = getUserService().getUsersInRole(Role.SCIENTIFIC_REVIEWER_ASSIGNER.getName());
        for (AbstractUser instAdmin : instAdmins) {
            if (request.getReviewers().contains(((TissueLocatorUser) instAdmin).getInstitution())) {
                emails.add(getEmail(assignmentEmail, instAdmin.getEmail(), request, null, null, null, null));
            }
        }
        return emails;
    }

    /**
     * @return the institutionService
     */
    public InstitutionServiceLocal getInstitutionService() {
        return institutionService;
    }

    /**
     * {@inheritDoc}
     */
    public void setInstitutionService(InstitutionServiceLocal institutionService) {
        this.institutionService = institutionService;
    }

    /**
     * @return the userService
     */
    public TissueLocatorUserServiceLocal getUserService() {
        return userService;
    }

    /**
     * {@inheritDoc}
     */
    @Inject
    public void setUserService(TissueLocatorUserServiceLocal userService) {
        this.userService = userService;
    }

    /**
     * {@inheritDoc}
     */
    public void setApplicationSettingService(ApplicationSettingServiceLocal applicationSettingService) {
        //do nothing
    }
}
