/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;
import javax.mail.MessagingException;

import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.util.RequestProcessingConfiguration;

/**
 * @author ddasgupta
 */
@Local
public interface SpecimenRequestServiceLocal extends GenericServiceLocal<SpecimenRequest> {

    /**
     * Count the votes on a given pending request and update the status and send email as appropriate.
     * @param request the request to process
     * @param config the request processing configuration parameters
     * @throws MessagingException on error sending email
     */
    void processPendingRequest(SpecimenRequest request, RequestProcessingConfiguration config)
        throws MessagingException;

    /**
     * Process a pending final decision request and send email as appropriate.
     * @param request the request to process
     * @param config the request processing configuration parameters
     * @throws MessagingException on error sending email
     */
    void processPendingDecisionRequest(SpecimenRequest request, RequestProcessingConfiguration config)
        throws MessagingException;

    /**
     * Save a draft request.
     * @param request the request to save.
     * @return the id of the saved request
     */
    Long saveDraftRequest(SpecimenRequest request);

    /**
     * Save a request, assign reviewers, and email the review committee.
     * @param request the request to save.
     * @param votingPeriod the length of the voting period
     * @param reviewPeriod the length of the review period
     * @param reviewerAssignmentPeriod the length of the reviewer assignment period
     * @param numReviewers the number of reviewers to assign.
     * @return the id of the saved request
     * @throws MessagingException on error sending email
     */
    @SuppressWarnings("PMD.ExcessiveParameterList")
    Long saveRequest(SpecimenRequest request, int votingPeriod, int reviewPeriod, int reviewerAssignmentPeriod,
            int numReviewers) throws MessagingException;

    /**
     * Delete a specimen request.
     * @param request The request to be deleted.
     */
    void deleteRequest(SpecimenRequest request);

    /**
     * Get requests that have not undergone instituional review within the indicated
     * grace period.
     * @param lineItemReviewGracePeriod The period in days to allow for line item review.
     * @return the list of unreviewed requests.
     */
    List<SpecimenRequest> getUnreviewedRequests(int lineItemReviewGracePeriod);

    /**
     * Finalize a vote with a new external comment, create shipments if necessary, and email the requester.
     * @param request the request to save
     * @param createShipments whether to create shipments for the requested biospecimens
     * @throws MessagingException on error sending assignment email
     */
    void finalizeVote(SpecimenRequest request, boolean createShipments) throws MessagingException;

    /**
     * Returns the request containing the given specimen.
     * @param specimen the specimen
     * @return the request containing the given specimen
     */
    SpecimenRequest getRequestWithSpecimen(Specimen specimen);

    /**
     * Get the number of requests that were submitted during a date range, with an optional
     * additional condition restricting the results.
     * @param startDate the start date
     * @param endDate the end date
     * @param additionalCondition an optional additional condition to restrict the results
     * @return a count of new requests submitted between start date and end date
     */
    int getNewRequestCount(Date startDate, Date endDate, String additionalCondition);

    /**
     * Get the number of scientific reviews that were completed late or are still incomplete after the voting
     * deadline for requests that were submitted during a date range.
     * @param startDate the start date
     * @param endDate the end date
     * @param votingPeriod the length of the voting period
     * @return A map of counts of late or outstanding reviews.  The map keys are the institution names,
     * and the map values are the number of late or outstanding votes for each institution.
     */
    Map<String, Integer> getLateScientificReviewCounts(Date startDate, Date endDate, int votingPeriod);

    /**
     * Get the number of institutional reviews that were completed late or are still incomplete after the voting
     * deadline for requests that were submitted during a date range.
     * @param startDate the start date
     * @param endDate the end date
     * @param votingPeriod the length of the voting period
     * @return A map of counts of late or outstanding reviews.  The map keys are the institution names,
     * and the map values are the number of late or outstanding votes for each institution.
     */
    Map<String, Integer> getLateInstitutionalReviewCounts(Date startDate, Date endDate, int votingPeriod);
}
