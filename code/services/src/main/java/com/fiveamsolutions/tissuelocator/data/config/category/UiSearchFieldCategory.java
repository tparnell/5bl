/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.data.config.category;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import com.fiveamsolutions.tissuelocator.data.config.search.AbstractSearchFieldConfig;

/**
 * User interface category used to group search fields.
 *
 * @author jstephens
 */
@Entity
@Table(name = "ui_search_field_category")
public class UiSearchFieldCategory extends AbstractUiCategory {

    private static final long serialVersionUID = -7741729717107152339L;
    private String searchSetType;
    private List<UiSearchCategoryField> uiSearchCategoryFields = new ArrayList<UiSearchCategoryField>();

    /**
     * Returns the search set type of this category.
     *
     * @return search set type
     */
    @Column(name = "search_set_type", length = MAX_STRING_LENGTH)
    public String getSearchSetType() {
        return searchSetType;
    }

    /**
     * Sets the search set type of this category.
     *
     * @param searchSetType search set type
     */
    public void setSearchSetType(String searchSetType) {
        this.searchSetType = searchSetType;
    }

    /**
     * Gets the list of fields that belong to the category.
     *
     * @return the category fields
     */
    @OneToMany(mappedBy = "uiSearchFieldCategory", fetch = FetchType.EAGER)
    @OrderBy(value = "id")
    public List<UiSearchCategoryField> getUiSearchCategoryFields() {
        return uiSearchCategoryFields;
    }

    /**
     * Sets the list of fields that belong to the category.
     *
     * @param uiSearchCategoryFields the category fields
     */
    public void setUiSearchCategoryFields(List<UiSearchCategoryField> uiSearchCategoryFields) {
        this.uiSearchCategoryFields = uiSearchCategoryFields;
    }

    /**
     * Returns true if this category contains the specified search field configuration.
     *
     * @param targetConfiguration search field configuration whose presence in this category is tested
     * @return true if this category contains the specified search field configuration
     */
    public boolean containsSearchFieldConfiguration(AbstractSearchFieldConfig targetConfiguration) {
        for (UiSearchCategoryField field : getUiSearchCategoryFields()) {
            if (field.getFieldConfig().getId().equals(targetConfiguration.getFieldConfig().getId())) {
                return true;
            }
        }
        return false;
    }
}