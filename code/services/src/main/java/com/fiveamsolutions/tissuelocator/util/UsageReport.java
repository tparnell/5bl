/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.util;

import java.util.List;
import java.util.Map;

/**
 * @author ddasgupta
 *
 */
public class UsageReport {

    private List<NameCountPair> topSpecimenTypes;
    private int shippedOrderCount = -1;
    private int newUserCount = -1;

    private int restrictedNewRequestCount = -1;
    private int unrestrictedNewRequestCount = -1;
    
    private int totalReturnsRequested = -1;
    private int totalReturnsFulfilled = -1;
    private int totalUnfulfilledReturnRequests = -1;
    private Map<Long, ParticipantReturnRequestData> participantReturnRequestMap;

    /**
     * @return the topSpecimenTypes
     */
    public List<NameCountPair> getTopSpecimenTypes() {
        return topSpecimenTypes;
    }

    /**
     * @param topSpecimenTypes the topSpecimenTypes to set
     */
    public void setTopSpecimenTypes(List<NameCountPair> topSpecimenTypes) {
        this.topSpecimenTypes = topSpecimenTypes;
    }

    /**
     * @return the shippedOrderCount
     */
    public int getShippedOrderCount() {
        return shippedOrderCount;
    }

    /**
     * @param shippedOrderCount the shippedOrderCount to set
     */
    public void setShippedOrderCount(int shippedOrderCount) {
        this.shippedOrderCount = shippedOrderCount;
    }

    /**
     * @return the newUserCount
     */
    public int getNewUserCount() {
        return newUserCount;
    }

    /**
     * @param newUserCount the newUserCount to set
     */
    public void setNewUserCount(int newUserCount) {
        this.newUserCount = newUserCount;
    }

    /**
     * @return the restrictedNewRequestCount
     */
    public int getRestrictedNewRequestCount() {
        return restrictedNewRequestCount;
    }

    /**
     * @param restrictedNewRequestCount the restrictedNewRequestCount to set
     */
    public void setRestrictedNewRequestCount(int restrictedNewRequestCount) {
        this.restrictedNewRequestCount = restrictedNewRequestCount;
    }

    /**
     * @return the unrestrictedNewRequestCount
     */
    public int getUnrestrictedNewRequestCount() {
        return unrestrictedNewRequestCount;
    }

    /**
     * @param unrestrictedNewRequestCount the unrestrictedNewRequestCount to set
     */
    public void setUnrestrictedNewRequestCount(int unrestrictedNewRequestCount) {
        this.unrestrictedNewRequestCount = unrestrictedNewRequestCount;
    }

    /**
     * @return the number of specimen returns requested by participants.
     */
    public int getTotalReturnsRequested() {
        return totalReturnsRequested;
    }

    /**
     * @param totalReturnsRequested the number of specimen returns requested by participants.
     */
    public void setTotalReturnsRequested(int totalReturnsRequested) {
        this.totalReturnsRequested = totalReturnsRequested;
    }

    /**
     * @return the number of specimens returned to participants.
     */
    public int getTotalReturnsFulfilled() {
        return totalReturnsFulfilled;
    }
    
    /**
     * @return the number of unfulfilled specimen returns requested by participants.
     */
    public int getTotalUnfulfilledReturnRequests() {
        return totalUnfulfilledReturnRequests;
    }

    /**
     * @param totalUnfulfilledReturnRequests the number of unfulfilled specimen returns requested by participants.
     */
    public void setTotalUnfulfilledReturnRequests(int totalUnfulfilledReturnRequests) {
        this.totalUnfulfilledReturnRequests = totalUnfulfilledReturnRequests;
    }

    /**
     * @param totalReturnsFulfilled the number of specimens returned to participants.
     */
    public void setTotalReturnsFulfilled(int totalReturnsFulfilled) {
        this.totalReturnsFulfilled = totalReturnsFulfilled;
    }

    /**
     * @return mapping of participant id to return request data for that participant.
     */
    public Map<Long, ParticipantReturnRequestData> getParticipantReturnRequestMap() {
        return participantReturnRequestMap;
    }

    /**
     * @param participantReturnRequestMap mapping of participant id to return request data for that participant.
     */
    public void setParticipantReturnRequestMap(
            Map<Long, ParticipantReturnRequestData> participantReturnRequestMap) {
        this.participantReturnRequestMap = participantReturnRequestMap;
    }
    
}
