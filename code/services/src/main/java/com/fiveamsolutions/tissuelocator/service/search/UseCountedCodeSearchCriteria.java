/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.service.search;

import java.util.Map;

import org.hibernate.Query;

import com.fiveamsolutions.nci.commons.search.SearchableUtils;
import com.fiveamsolutions.nci.commons.search.SearchableUtils.AfterIterationHelper;
import com.fiveamsolutions.tissuelocator.data.code.Code;
import com.fiveamsolutions.tissuelocator.data.code.UseCountedCode;

/**
 * Enables code searches based on a minimum in-use count.
 * @author gvaughn
 *
 */
public class UseCountedCodeSearchCriteria extends
        TissueLocatorAnnotatedBeanSearchCriteria<Code> {

    private static final long serialVersionUID = -9119752608052247689L;

    private Long minimumUseCount;
    
    /**
     * Standard constructor.
     * @param example Example code.
     */
    public UseCountedCodeSearchCriteria(UseCountedCode example) {
        super(example);
    }

    /**
     * Constructs criteria for searching codes with a minimum use count.
     * @param example Example code.
     * @param minimumUseCount Minimum use count.
     */
    public UseCountedCodeSearchCriteria(UseCountedCode example, long minimumUseCount) {
        super(example);
        this.minimumUseCount = minimumUseCount;
    }

    /**
     * @return the minimumUseCount
     */
    public Long getMinimumUseCount() {
        return minimumUseCount;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Query getQuery(String orderByProperty, boolean isCountOnly) {
        return getQuery(orderByProperty, null, isCountOnly);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Query getQuery(String orderByProperty, String leftJoinClause,
            boolean isCountOnly) {
        return SearchableUtils.getQueryBySearchableFields(getCriteria(), isCountOnly, orderByProperty, 
                leftJoinClause, getSession(), getAfterIterationHelper());
    }
    
    /**
     * exposes the private inner class to subclasses.
     * @return the inner class for the iteration helper.
     */
    public AfterIterationHelper getAfterIterationHelper() {
        return new UseCountedCodeHelper();
    }
    
    /**
     * Helper that adds checks for the additional specimen search options.
     */
    private class UseCountedCodeHelper implements AfterIterationHelper {

        /**
         * {@inheritDoc}
         */
        public void afterIteration(Object obj, boolean isCountOnly,
                                   StringBuffer whereClause, Map<String, Object> params) {
            if (getMinimumUseCount() != null) {
                String condition = String.format("%s %s.useCount >= :useCount",
                        whereOrAnd(whereClause), SearchableUtils.ROOT_OBJ_ALIAS);
                whereClause.append(condition);
                params.put("useCount", getMinimumUseCount());
            }
        }
    }
}
