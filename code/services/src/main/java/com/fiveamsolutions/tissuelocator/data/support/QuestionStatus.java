/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.data.support;

/**
 * @author ddasgupta
 *
 */
public enum QuestionStatus {

    /**
     * Pending (no responses).
     */
    PENDING("questionStatus.administrator.pending", "questionStatus.researcher.pending"),

    /**
     * Partially Responded (at least one response).
     */
    PARTIALLY_RESPONDED("questionStatus.administrator.partiallyResponded",
            "questionStatus.researcher.partiallyResponded"),

    /**
     * Fully Responded (all responses).
     */
    FULLY_RESPONDED("questionStatus.administrator.fullyResponded",
            "questionStatus.researcher.fullyResponded");

    private String administratorResourceKey;
    private String researcherResourceKey;

    /**
     * constructor.
     * @param resourceKey the key of the human readable text
     */
    private QuestionStatus(String administratorResourceKey, String researcherResourceKey) {
        this.administratorResourceKey = administratorResourceKey;
        this.researcherResourceKey = researcherResourceKey;
    }

    /**
     * @return the administratorResourceKey
     */
    public String getAdministratorResourceKey() {
        return administratorResourceKey;
    }

    /**
     * @return the researcherResourceKey
     */
    public String getResearcherResourceKey() {
        return researcherResourceKey;
    }
}
