/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.service;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import org.hibernate.Query;

import com.fiveamsolutions.tissuelocator.data.CollectionProtocol;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.util.EmailHelper;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;

/**
 * @author smiller
 *
 */
@Stateless
@SuppressWarnings("PMD.AvoidDuplicateLiterals")
public class CollectionProtocolServiceBean extends GenericServiceBean<CollectionProtocol> implements
        CollectionProtocolServiceLocal {

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public Map<String, CollectionProtocol> getProtocols(Long institutionId, Collection<String> names) {
        Map<String, CollectionProtocol> protocolMap = new HashMap<String, CollectionProtocol>();
        if (!names.isEmpty()) {
            String queryString = "from " + CollectionProtocol.class.getName()
            + " where name in (:names) and institution.id = :institutionId";
            Query q = TissueLocatorHibernateUtil.getCurrentSession().createQuery(queryString);
            q.setParameterList("names", names);
            q.setParameter("institutionId", institutionId);
            List<CollectionProtocol> protocols = q.list();
            for (CollectionProtocol protocol : protocols) {
                protocolMap.put(protocol.getName(), protocol);
            }
        }
        return protocolMap;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public List<CollectionProtocol> getProtocolsByInstitution(Long institutionId) {
        String queryString = "from " + CollectionProtocol.class.getName()
        + " where institution.id = :institutionId";
        Query q = TissueLocatorHibernateUtil.getCurrentSession().createQuery(queryString);
        q.setParameter("institutionId", institutionId);
        return q.list();
    }

    /**
     * {@inheritDoc}
     */
    public CollectionProtocol getDefaultProtocol(Long institutionId) {
        String queryString = "from " + CollectionProtocol.class.getName()
            + " where institution.id in (select institution.id  from " + CollectionProtocol.class.getName()
            + " where institution.id = :institutionId group by institution.id having count(*) = 1)";
        Query q = TissueLocatorHibernateUtil.getCurrentSession().createQuery(queryString);
        q.setParameter("institutionId", institutionId);
        return (CollectionProtocol) q.uniqueResult();
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public Map<Long, CollectionProtocol> getDefaultProtocols() {
        Map<Long, CollectionProtocol> protocolMap = new HashMap<Long, CollectionProtocol>();
        String queryString = "from " + CollectionProtocol.class.getName()
        + " where institution.id in (select institution.id from "
        + CollectionProtocol.class.getName()
        + " group by institution.id having count(*) = 1)";
        Query q = TissueLocatorHibernateUtil.getCurrentSession().createQuery(queryString);
        List<CollectionProtocol> protocols = q.list();
        for (CollectionProtocol protocol : protocols) {
            protocolMap.put(protocol.getInstitution().getId(), protocol);
        }
        return protocolMap;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CollectionProtocol createDefaultProtocol(Institution institution) {
        CollectionProtocol cp = new CollectionProtocol();
        cp.setName(new EmailHelper().getResourceBundle().getString("protocol.name.default"));
        cp.setInstitution(institution);
        savePersistentObject(cp);
        return cp;
    }

}
