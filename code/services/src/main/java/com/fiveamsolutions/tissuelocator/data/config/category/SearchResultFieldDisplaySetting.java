/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.data.config.category;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.Immutable;
import org.hibernate.validator.NotNull;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.tissuelocator.data.config.GenericFieldConfig;

/**
 * @author cgoina
 *
 */
@Entity
@Table(name = "search_result_field_display_setting")
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@Immutable
public class SearchResultFieldDisplaySetting implements PersistentObject {

    private static final long serialVersionUID = -1992319126315744904L;

    private Long id;
    private GenericFieldConfig fieldConfig;
    private DisplayOption displayFlag;
    private UiSearchFieldCategory fieldCategory;

    /**
     * Default class constructor.
     */
    public SearchResultFieldDisplaySetting() {
        displayFlag = DisplayOption.NOT_APPLICABLE;
    }

    /**
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    @Override
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return The field configuration for this display setting.
     */
    @NotNull
    @ManyToOne
    @JoinColumn(name = "generic_field_config_id")
    @ForeignKey(name = "search_result_display_setting_field_config_fk")
    public GenericFieldConfig getFieldConfig() {
        return fieldConfig;
    }

    /**
     * @param fieldConfig The field configuration for this display setting.
     */
    public void setFieldConfig(GenericFieldConfig fieldConfig) {
        this.fieldConfig = fieldConfig;
    }

    /**
     * @return the displayFlag
     */
    @Column(name = "display_flag")
    @Enumerated(EnumType.STRING)
    @NotNull
    public DisplayOption getDisplayFlag() {
        return displayFlag != null ? displayFlag : DisplayOption.NOT_APPLICABLE;
    }

    /**
     * @param displayFlag the displayFlag to set
     */
    public void setDisplayFlag(DisplayOption displayFlag) {
        this.displayFlag = displayFlag;
    }

    /**
     * A field for which the display option is not applicable will not show at all so when the get/set as
     * boolean is used the only valid values are ENABLED and DISABLED.
     *
     * @return true if display flag is ENABLED false otherwise
     */
    @Transient
    public boolean isDisplayFlagAsBoolean() {
        return displayFlag == DisplayOption.ENABLED ? true : false;
    }

    /**
     * @param booleanDisplayFlag if true this.displayFlag is set to ENABLED otherwise DISABLED
     */
    public void setDisplayFlagAsBoolean(boolean booleanDisplayFlag) {
        displayFlag = booleanDisplayFlag ? DisplayOption.ENABLED : DisplayOption.DISABLED;
    }

    /**
     * @return the fieldCategory
     */
    @NotNull
    @ManyToOne
    @JoinColumn(name = "category_id")
    public UiSearchFieldCategory getFieldCategory() {
        return fieldCategory;
    }

    /**
     * @param fieldCategory the fieldCategory to set
     */
    public void setFieldCategory(UiSearchFieldCategory fieldCategory) {
        this.fieldCategory = fieldCategory;
    }

    /**
     * Return the field category name.
     *
     * @return field category name
     */
    @Transient
    public String getFieldCategoryName() {
        return fieldCategory == null || StringUtils.isEmpty(fieldCategory.getCategoryName())
            ? null : fieldCategory.getCategoryName();
    }

}
