/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.service.config.category;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Stateless;

import org.hibernate.Query;

import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.config.category.SearchResultFieldDisplaySetting;
import com.fiveamsolutions.tissuelocator.data.preferences.SearchResultFieldDisplayUserPreference;
import com.fiveamsolutions.tissuelocator.service.GenericServiceBean;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;

/**
 * @author cgoina
 *
 */
@Stateless
public class SearchResultDisplaySettingsServiceBean extends GenericServiceBean<SearchResultFieldDisplaySetting>
        implements SearchResultDisplaySettingsServiceLocal {

    private static final String UNCHECKED_WARNING = "unchecked";
    private static final String FROM = "from ";

    /**
     * {@inheritDoc}
     */
    @Override
    public List<SearchResultFieldDisplaySetting> getDefaultSearchResultDisplaySettings() {
        return retrieveDefaultSearchResultDisplaySettings();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<SearchResultFieldDisplaySetting> getUserSearchResultDisplaySettings(String user) {
        List<SearchResultFieldDisplayUserPreference> userPreferences = retrieveUserDisplayPreferences(user);
        List<SearchResultFieldDisplaySetting> defaultPreferences =
            retrieveDefaultSearchResultDisplaySettings();
        overlayUserPreferences(defaultPreferences, userPreferences);
        return defaultPreferences;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void storeUserSearchResultDisplaySettings(TissueLocatorUser user,
            Collection<SearchResultFieldDisplaySetting> fieldDisplaySettings) {
        Map<String, SearchResultFieldDisplaySetting> fieldNameDisplaySettingMap =
            new LinkedHashMap<String, SearchResultFieldDisplaySetting>();
        for (SearchResultFieldDisplaySetting fieldDisplaySetting : fieldDisplaySettings) {
            fieldNameDisplaySettingMap.put(fieldDisplaySetting.getFieldConfig()
                    .getSearchFieldName(), fieldDisplaySetting);
        }
        List<SearchResultFieldDisplayUserPreference> userPreferences =
            retrieveUserDisplayPreferencesByFieldNames(user.getUsername(),
                fieldNameDisplaySettingMap.keySet());
        List<SearchResultFieldDisplayUserPreference> newUserPreferences =
            new ArrayList<SearchResultFieldDisplayUserPreference>();
        for (SearchResultFieldDisplayUserPreference userDisplayPreference : userPreferences) {
            filterNewUserPreference(userDisplayPreference, fieldNameDisplaySettingMap, newUserPreferences);
        }
        handleConfiguredSearchResultDisplaySettings(fieldNameDisplaySettingMap, user, newUserPreferences);
        for (SearchResultFieldDisplayUserPreference newUserPreference : newUserPreferences) {
            TissueLocatorHibernateUtil.getCurrentSession().saveOrUpdate(newUserPreference);
        }
    }

    private void filterNewUserPreference(SearchResultFieldDisplayUserPreference currentUserDisplayPreference,
            Map<String, SearchResultFieldDisplaySetting> fieldNameDisplaySettingMap,
            List<SearchResultFieldDisplayUserPreference> newUserPreferences) {
        SearchResultFieldDisplaySetting newDisplaySetting =
            fieldNameDisplaySettingMap.get(currentUserDisplayPreference.getFieldName());
        if (newDisplaySetting == null) {
            return; // don't change anything for this setting
        }
        if (!currentUserDisplayPreference.getDisplayFlag().equals(newDisplaySetting.getDisplayFlag())) {
            currentUserDisplayPreference.setDisplayFlag(newDisplaySetting.getDisplayFlag());
            newUserPreferences.add(currentUserDisplayPreference);
        }
        // mark the field as done by removing it from the name->display setting map
        fieldNameDisplaySettingMap.remove(currentUserDisplayPreference.getFieldName());
    }

    private void handleConfiguredSearchResultDisplaySettings(
            Map<String, SearchResultFieldDisplaySetting> fieldNameDisplaySettingMap,
            TissueLocatorUser user,
            List<SearchResultFieldDisplayUserPreference> newUserPreferences) {
        if (fieldNameDisplaySettingMap.isEmpty()) {
            // nothing to do
            return;
        }
        // for the remaining entries that have corresponding default entries, create a new user display setting
        List<SearchResultFieldDisplaySetting> defaultPreferences =
            retrieveDefaultSearchResultDisplaySettingsByFieldNames(fieldNameDisplaySettingMap.keySet());
        for (SearchResultFieldDisplaySetting fieldSetting : defaultPreferences) {
            SearchResultFieldDisplayUserPreference newUserPreference = new SearchResultFieldDisplayUserPreference();
            newUserPreference.setUser(user);
            newUserPreference.setFieldDisplaySetting(fieldSetting);
            newUserPreference.setDisplayFlag(fieldNameDisplaySettingMap
                    .get(fieldSetting.getFieldConfig().getSearchFieldName()).getDisplayFlag());
            newUserPreferences.add(newUserPreference);
            fieldNameDisplaySettingMap.remove(fieldSetting.getFieldConfig().getSearchFieldName());
        }
    }

    private void overlayUserPreferences(List<SearchResultFieldDisplaySetting> defaultPreferences,
            List<SearchResultFieldDisplayUserPreference> userPreferences) {
        Map<Long, SearchResultFieldDisplayUserPreference> userFieldNameDisplayOptionMap =
            new LinkedHashMap<Long, SearchResultFieldDisplayUserPreference>();
        for (SearchResultFieldDisplayUserPreference userFieldDisplayPreference : userPreferences) {
            userFieldNameDisplayOptionMap.put(userFieldDisplayPreference.getFieldDisplaySetting().getId(),
                    userFieldDisplayPreference);
        }
        for (SearchResultFieldDisplaySetting fieldDisplaySetting : defaultPreferences) {
            SearchResultFieldDisplayUserPreference userFieldNameDisplayOption = userFieldNameDisplayOptionMap
                    .get(fieldDisplaySetting.getId());
            if (userFieldNameDisplayOption != null) {
                fieldDisplaySetting.setDisplayFlag(userFieldNameDisplayOption.getDisplayFlag());
            }
        }
    }

    @SuppressWarnings(UNCHECKED_WARNING)
    private List<SearchResultFieldDisplaySetting> retrieveDefaultSearchResultDisplaySettings() {
        final StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append(FROM)
                .append(SearchResultFieldDisplaySetting.class.getName()).append(' ')
                .append("order by id ");
        final Query query = TissueLocatorHibernateUtil.getCurrentSession().createQuery(queryBuilder.toString());
        return query.list();
    }

    @SuppressWarnings(UNCHECKED_WARNING)
    private List<SearchResultFieldDisplayUserPreference> retrieveUserDisplayPreferences(String userName) {
        final StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append(FROM).append(SearchResultFieldDisplayUserPreference.class.getName())
                .append(" where user.username = :userName order by fieldDisplaySetting.id");
        final Query query = TissueLocatorHibernateUtil.getCurrentSession().createQuery(queryBuilder.toString());
        query.setString("userName", userName);
        return query.list();
    }

    @SuppressWarnings(UNCHECKED_WARNING)
    private List<SearchResultFieldDisplaySetting> retrieveDefaultSearchResultDisplaySettingsByFieldNames(
            Set<String> fieldNames) {
        final StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append(FROM).append(SearchResultFieldDisplaySetting.class.getName());
        if (fieldNames != null && !fieldNames.isEmpty()) {
            queryBuilder.append(" where fieldConfig.searchFieldName in (:fieldNames)");
        }
        queryBuilder.append(" order by id");
        final Query query = TissueLocatorHibernateUtil.getCurrentSession().createQuery(queryBuilder.toString());
        if (fieldNames != null && !fieldNames.isEmpty()) {
            query.setParameterList("fieldNames", fieldNames);
        }
        return query.list();
    }

    @SuppressWarnings(UNCHECKED_WARNING)
    private List<SearchResultFieldDisplayUserPreference> retrieveUserDisplayPreferencesByFieldNames(String userName,
            Set<String> fieldNames) {
        final StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append(FROM).append(SearchResultFieldDisplayUserPreference.class.getName())
                .append(" where user.username = :userName");
        if (fieldNames != null && !fieldNames.isEmpty()) {
            queryBuilder.append(" and fieldDisplaySetting.fieldConfig.searchFieldName in (:fieldNames)");
        }
        queryBuilder.append(" order by fieldDisplaySetting.id ");
        final Query query = TissueLocatorHibernateUtil.getCurrentSession().createQuery(queryBuilder.toString());
        query.setString("userName", userName);
        if (fieldNames != null && !fieldNames.isEmpty()) {
            query.setParameterList("fieldNames", fieldNames);
        }
        return query.list();
    }
}
