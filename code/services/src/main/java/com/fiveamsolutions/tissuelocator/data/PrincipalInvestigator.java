/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.data;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.validator.AssertTrue;
import org.hibernate.validator.Length;
import org.hibernate.validator.Valid;

import com.fiveamsolutions.tissuelocator.data.validation.DisableableNotNull;
import com.fiveamsolutions.tissuelocator.hibernate.interceptor.DeleteOrphans;

/**
 * @author ddasgupta
 *
 */
@Entity
@Table(name = "principal_investigator")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@DeleteOrphans(targetProperties = { "resume" }, idProperties = { "lob.id" }, orphanClasses = { LobHolder.class })
public class PrincipalInvestigator extends Person {

    private static final long serialVersionUID = -6739660648143495272L;
    private static final int MAX_COMMENT_LENGTH = 1000;

    private TissueLocatorFile resume;
    private YesNoResponse certified;
    private YesNoResponse investigated;
    private String investigationComment;

    /**
     * default no-arg constructor.
     */
    public PrincipalInvestigator() {
        super();
    }

    /**
     * copy constructor.
     * @param user the user to copy.
     */
    public PrincipalInvestigator(TissueLocatorUser user) {
        super(user);
        if (user.getResume() != null) {
            resume = new TissueLocatorFile(user.getResume());
        }
    }

    /**
     * @return the resume
     */
    @Valid
    @Embedded
    @AttributeOverrides({
        @AttributeOverride(name = "contentType", column = @Column(name = "resume_content_type")),
        @AttributeOverride(name = "name", column = @Column(name = "resume_name"))
    })
    @AssociationOverrides({
        @AssociationOverride(name = "lob", joinColumns = @JoinColumn(name = "resume_lob_id"))
    })
    public TissueLocatorFile getResume() {
        return resume;
    }

    /**
     * @param resume the resume to set
     */
    public void setResume(TissueLocatorFile resume) {
        this.resume = resume;
    }

    /**
     * @return the certified
     */
    @DisableableNotNull
    @Enumerated(EnumType.STRING)
    public YesNoResponse getCertified() {
        return certified;
    }

    /**
     * @param certified the certified to set
     */
    public void setCertified(YesNoResponse certified) {
        this.certified = certified;
    }

    /**
     * @return the investigated
     */
    @DisableableNotNull
    @Enumerated(EnumType.STRING)
    public YesNoResponse getInvestigated() {
        return investigated;
    }

    /**
     * @param investigated the investigated to set
     */
    public void setInvestigated(YesNoResponse investigated) {
        this.investigated = investigated;
    }

    /**
     * @return the investigationComment
     */
    @Length(max = MAX_COMMENT_LENGTH)
    @Column(name = "investigation_comment")
    public String getInvestigationComment() {
        return investigationComment;
    }

    /**
     * @param investigationComment the investigationComment to set
     */
    public void setInvestigationComment(String investigationComment) {
        this.investigationComment = investigationComment;
    }

    /**
     * tests if investigation comment should be required because the PI has been investigated.
     * @return true if valid.
     */
    @Transient
    @AssertTrue(message = "If the PI has been investigated, a comment must be provided.")
    public boolean isInvestigationCommentValid() {
        if (YesNoResponse.YES.equals(getInvestigated())) {
            return StringUtils.isNotBlank(getInvestigationComment());
        }
        return true;
    }

    /**
     * Tests if an investigation comment has been included despite no investigation having been conducted.
     * @return false if a comment was included without an investigation, true otherwise.
     */
    @Transient
    @AssertTrue(message = "If the PI has not been investigated, a comment should not be included.")
    public boolean isInvestigationCommentAllowed() {
        if (YesNoResponse.NO.equals(getInvestigated())) {
            return StringUtils.isBlank(getInvestigationComment());
        }
        return true;
    }
}
