/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.Index;
import org.hibernate.validator.AssertTrue;
import org.hibernate.validator.Length;
import org.hibernate.validator.NotEmpty;
import org.hibernate.validator.NotNull;
import org.hibernate.validator.Pattern;
import org.hibernate.validator.Valid;

import com.fiveamsolutions.nci.commons.audit.Auditable;
import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.nci.commons.search.Searchable;
import com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreement;
import com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreementStatus;
import com.fiveamsolutions.tissuelocator.data.validation.RequireConsortiumContactInfo;
import com.fiveamsolutions.tissuelocator.data.validation.UniqueConstraint;

/**
 * An institution.
 *
 * @author smiller
 */
@Entity(name = "institution")
@RequireConsortiumContactInfo
@UniqueConstraint(propertyNames = "name", message = "{validator.insitution.duplicate}")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@SuppressWarnings("PMD.TooManyFields")
public class Institution implements PersistentObject, Auditable, InstitutionRestricted {

    private static final long serialVersionUID = 1L;

    private static final String URL_REGEX =
        "([A-Za-z][A-Za-z0-9+.-]{1,120}:[A-Za-z0-9/](([A-Za-z0-9$_.+!*,;/?:@&~=-])|%[A-Fa-f0-9]{2}){1,333}"
        + "(#([a-zA-Z0-9][a-zA-Z0-9$_.+!*,;/?:@&~=%-]{0,1000}))?)";
    private static final String DUNS_EIN_NUMBER_REGEX = "[0-9]{9}";

    private static final int HOMEPAGE_LENGTH = 254;
    private static final int NAME_LENGTH = 254;
    private static final int RESTRICTIONS_LENGTH = 3999;
    private static final int INSTRUCTION_LENGTH = 254;
    private static final int DUNS_NUM_MAX_LENGTH = 9;
    private static final int DUNS_TYPE_MAX_LENGTH = 4;

    private Long id;
    private String name;
    private String homepage;
    private String institutionProfileUrl;
    private InstitutionType type;
    private Contact contact;
    private String specimenUseRestrictions;
    private Boolean consortiumMember;
    private Date lastReviewAssignment;
    private String paymentInstructions;
    private Address billingAddress;
    private DunsEin dunsEINType;
    private String dunsEINNumber;

    private Person mtaContact;
    private List<SignedMaterialTransferAgreement> signedRecipientMtas =
        new ArrayList<SignedMaterialTransferAgreement>();
    private List<SignedMaterialTransferAgreement> signedSenderMtas =
        new ArrayList<SignedMaterialTransferAgreement>();

    /**
     * {@inheritDoc}
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * The name of the institution.  This is part of the CBM model.
     * Note that the case insensitive unique constraint and index for this column in the database
     * are specified in plain sql.
     * @return the name of the institution
     */
    @NotEmpty
    @Length(max = NAME_LENGTH)
    @Searchable(caseSensitive = false, matchMode = Searchable.MATCH_MODE_CONTAINS)
    public String getName() {
        return name;
    }

    /**
     * @param name the institution name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * The type of institution, this is NOT part of the CBM model.
     * @return the type
     */
    @ManyToOne
    @NotNull
    @JoinColumn(name = "institution_type_id")
    @ForeignKey(name = "institution_institution_type_fk")
    @Index(name = "institution_institution_type_idx")
    public InstitutionType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(InstitutionType type) {
        this.type = type;
    }

    /**
     * @return the homepage
     */
    @Pattern(regex = URL_REGEX,
            message = "{validator.url}")
    @Length(max = HOMEPAGE_LENGTH)
    public String getHomepage() {
        return homepage;
    }

    /**
     * @param homepage the homepage to set
     */
    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    /**
     * @return the institutionProfileUrl
     */
    @Pattern(regex = URL_REGEX,
            message = "{validator.url}")
    @Length(max = HOMEPAGE_LENGTH)
    @Column(name = "institution_profile_url")
    public String getInstitutionProfileUrl() {
        return institutionProfileUrl;
    }

    /**
     * @param institutionProfileUrl the institutionProfileUrl to set
     */
    public void setInstitutionProfileUrl(String institutionProfileUrl) {
        this.institutionProfileUrl = institutionProfileUrl;
    }

    /**
     * @return the contact
     */
    @Valid
    public Contact getContact() {
        return contact;
    }

    /**
     * @param contact the contact to set
     */
    public void setContact(Contact contact) {
        this.contact = contact;
    }

    /**
     * @return the specimenUseRestrictions
     */
    @Column(name = "specimen_use_restrictions")
    @Length(max = RESTRICTIONS_LENGTH)
    public String getSpecimenUseRestrictions() {
        return specimenUseRestrictions;
    }

    /**
     * @param specimenUseRestrictions the specimenUseRestrictions to set
     */
    public void setSpecimenUseRestrictions(String specimenUseRestrictions) {
        this.specimenUseRestrictions = specimenUseRestrictions;
    }

    /**
     * @return the specimen use restrictions split on newlines
     */
    @Transient
    public String[] getSpecimenUseRestrictionParts() {
        return StringUtils.split(getSpecimenUseRestrictions(), "\n");
    }

    /**
     * @return the consortiumMember
     */
    @Searchable
    public Boolean getConsortiumMember() {
        return consortiumMember;
    }

    /**
     * @param consortiumMember the consortiumMember to set
     */
    public void setConsortiumMember(Boolean consortiumMember) {
        this.consortiumMember = consortiumMember;
    }

    /**
     * @return the lastReviewAssignment
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_review_assignment")
    public Date getLastReviewAssignment() {
        return lastReviewAssignment;
    }

    /**
     * @param lastReviewAssignment the lastReviewAssignment to set
     */
    public void setLastReviewAssignment(Date lastReviewAssignment) {
        this.lastReviewAssignment = lastReviewAssignment;
    }

    /**
     * @return the paymentInstructions
     */
    @Length(max = INSTRUCTION_LENGTH)
    @Column(name = "payment_instructions")
    public String getPaymentInstructions() {
        return paymentInstructions;
    }

    /**
     * @param paymentInstructions the paymentInstructions to set
     */
    public void setPaymentInstructions(String paymentInstructions) {
        this.paymentInstructions = paymentInstructions;
    }

    /**
     * @return the billingAddress
     */
    @Valid
    @ManyToOne
    @Cascade(CascadeType.ALL)
    @JoinColumn(name = "address_id")
    @ForeignKey(name = "inst_address_fk")
    @Index(name = "inst_address_idx")
    public Address getBillingAddress() {
        return billingAddress;
    }

    /**
     * tests if the billing address is valid.
     * @return true if valid.
     */
    @Transient
    @AssertTrue(message = "{validator.consortiumMembers.billingAddress.required}")
    public boolean isBillingAddressValid() {
        return getConsortiumMember() == null || !getConsortiumMember()
        || getBillingAddress() != null;
    }

    /**
     * @param billingAddress the billingAddress to set
     */
    public void setBillingAddress(Address billingAddress) {
        this.billingAddress = billingAddress;
    }

    /**
     * @return the dunsEINType
     */
    @Column(name = "duns_ein_type", length = DUNS_TYPE_MAX_LENGTH)
    @Enumerated(EnumType.STRING)
    public DunsEin getDunsEINType() {
        return dunsEINType;
    }

    /**
     * tests if the DunsEIN fields are valid.
     * @return true if valid.
     */
    @Transient
    @AssertTrue(message = "If setting DUNS/EIN, both Type and Number must be set.")
    public boolean isDunsEINValid() {
        return !(getDunsEINType() == null ^ getDunsEINNumber() == null);
    }

    /**
     * @param dunsEINType the dunsEINType to set
     */
    public void setDunsEINType(DunsEin dunsEINType) {
        this.dunsEINType = dunsEINType;
    }

    /**
     * @return the dunsEINNumber
     */
    @Length(max = DUNS_NUM_MAX_LENGTH)
    @Column(name = "duns_ein_number")
    @Pattern(regex = DUNS_EIN_NUMBER_REGEX, message = "DUNS/EIN number must be 9 digits.")
    public String getDunsEINNumber() {
        return dunsEINNumber;
    }

    /**
     * @param dunsEINNumber the dunsEINNumber to set
     */
    public void setDunsEINNumber(String dunsEINNumber) {
        this.dunsEINNumber = dunsEINNumber;
    }

    /**
     * @return the mtaContact
     */
    @Valid
    @ManyToOne
    @Cascade(CascadeType.ALL)
    @JoinColumn(name = "mta_contact_id")
    @ForeignKey(name = "inst_mta_contact_fk")
    @Index(name = "inst_mta_contact_idx")
    public Person getMtaContact() {
        return mtaContact;
    }

    /**
     * @param mtaContact the mtaContact to set
     */
    public void setMtaContact(Person mtaContact) {
        this.mtaContact = mtaContact;
    }

    /**
     * @return the signedRecipientMtas
     */
    @OneToMany(mappedBy = "receivingInstitution")
    @OrderBy(value = "uploadTime desc")
    public List<SignedMaterialTransferAgreement> getSignedRecipientMtas() {
        return signedRecipientMtas;
    }

    /**
     * @param signedRecipientMtas the signedRecipientMtas to set
     */
    public void setSignedRecipientMtas(List<SignedMaterialTransferAgreement> signedRecipientMtas) {
        this.signedRecipientMtas = signedRecipientMtas;
    }

    /**
     * @return the most recently approved signed recipient mta
     */
    @Transient
    public SignedMaterialTransferAgreement getCurrentSignedRecipientMta() {
            return getCurrentSignedMta(getSignedRecipientMtas());
    }

    /**
     * @return the signedSenderMtas
     */
    @OneToMany(mappedBy = "sendingInstitution")
    @OrderBy(value = "uploadTime desc")
    public List<SignedMaterialTransferAgreement> getSignedSenderMtas() {
        return signedSenderMtas;
    }

    /**
     * @param signedSenderMtas the signedSenderMtas to set
     */
    public void setSignedSenderMtas(List<SignedMaterialTransferAgreement> signedSenderMtas) {
        this.signedSenderMtas = signedSenderMtas;
    }

    /**
     * @return the most recently approved signed sender mta
     */
    @Transient
    public SignedMaterialTransferAgreement getCurrentSignedSenderMta() {
            return getCurrentSignedMta(getSignedSenderMtas());
    }

    private SignedMaterialTransferAgreement getCurrentSignedMta(List<SignedMaterialTransferAgreement> mtas) {
        SignedMaterialTransferAgreement approved = getLatestSignedMtaInStatus(mtas,
                SignedMaterialTransferAgreementStatus.APPROVED);
        SignedMaterialTransferAgreement pending = getLatestSignedMtaInStatus(mtas,
                SignedMaterialTransferAgreementStatus.PENDING_REVIEW);
        SignedMaterialTransferAgreement outOfDate = getLatestSignedMtaInStatus(mtas,
                SignedMaterialTransferAgreementStatus.OUT_OF_DATE);
        return (SignedMaterialTransferAgreement) ObjectUtils.defaultIfNull(
                ObjectUtils.defaultIfNull(approved, pending), outOfDate);
    }

    private SignedMaterialTransferAgreement getLatestSignedMtaInStatus(List<SignedMaterialTransferAgreement> mtas,
            SignedMaterialTransferAgreementStatus status) {
        for (SignedMaterialTransferAgreement mta : mtas) {
            if (status.equals(mta.getStatus())) {
                return mta;
            }
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (obj == this) {
            return true;
        }

        if (!(obj instanceof Institution)) {
            return false;
        }

        Institution rhs = (Institution) obj;
        return new EqualsBuilder().append(getName(), rhs.getName()).isEquals();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getName()).toHashCode();
    }

    /**
     * {@inheritDoc}
     */
    @Transient
    public Collection<Institution> getRelatedInstitutions() {
        return Collections.singleton(this);
    }

    /**
     * Static inner class used for comparing/sorting institutions alphabetically.
     *
     * @author akong
     *
     */
    public static class InstitutionComparator implements Comparator<Institution> {

        /**
         * Alphabetical comparison of institution names.
         *
         * {@inheritDoc}
         */
        public int compare(Institution inst1, Institution inst2) {

            String instName1 = inst1.getName();
            String instName2 = inst2.getName();

            if (instName1 == null) {
                instName1 = "";
            }
            if (instName2 == null) {
                instName2 = "";
            }

            return instName1.compareTo(instName2);
        }
    }
}
