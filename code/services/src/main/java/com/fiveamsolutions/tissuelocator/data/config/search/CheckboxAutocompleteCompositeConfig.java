/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.data.config.search;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.validator.Length;
import org.hibernate.validator.NotNull;

import com.fiveamsolutions.nci.commons.data.security.ApplicationRole;
import com.fiveamsolutions.tissuelocator.data.config.GenericFieldConfig;

/**
 * @author gvaughn
 *
 */
@Entity
@Table(name = "checkbox_autocomplete_composite_config")
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class CheckboxAutocompleteCompositeConfig extends AbstractSearchFieldConfig {

    private static final long serialVersionUID = 4524064613229295831L;

    private CheckboxConfig checkboxConfig;
    private AutocompleteConfig autocompleteConfig;
    private String checkboxTextValue;

    /**
     * @return the checkboxConfig
     */
    @NotNull
    @ManyToOne
    @JoinColumn(name = "checkbox_config_id")
    @ForeignKey(name = "checkbox_auto_fk")
    public CheckboxConfig getCheckboxConfig() {
        return checkboxConfig;
    }

    /**
     * @param checkboxConfig the checkboxConfig to set
     */
    public void setCheckboxConfig(CheckboxConfig checkboxConfig) {
        this.checkboxConfig = checkboxConfig;
    }

    /**
     * @return the autocompleteConfig
     */
    @NotNull
    @ManyToOne
    @JoinColumn(name = "autocomplete_config_id")
    @ForeignKey(name = "auto_checkbox_fk")
    public AutocompleteConfig getAutocompleteConfig() {
        return autocompleteConfig;
    }

    /**
     * @param autocompleteConfig the autocompleteConfig to set
     */
    public void setAutocompleteConfig(AutocompleteConfig autocompleteConfig) {
        this.autocompleteConfig = autocompleteConfig;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transient
    public String getSearchFieldDisplayName() {
        return getAutocompleteConfig().getSearchFieldDisplayName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transient
    public ApplicationRole getRequiredRole() {
        return getAutocompleteConfig().getRequiredRole();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transient
    public GenericFieldConfig getFieldConfig() {
        return getAutocompleteConfig().getFieldConfig();
    }

    /**
     * @return The value entered in the autocomplete field when the box is checked.
     */
    @NotNull
    @Length(max = MAX_LENGTH)
    @Column(name = "checkbox_text_value")
    public String getCheckboxTextValue() {
        return checkboxTextValue;
    }

    /**
     * @param checkboxTextValue The value entered in the autocomplete field when the box is checked.
     */
    public void setCheckboxTextValue(String checkboxTextValue) {
        this.checkboxTextValue = checkboxTextValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCriteriaString(Object object, String mapName) {
        return getAutocompleteConfig().getCriteriaString(object, mapName);
    }

    /**
     * Returns the name of the property being searched.
     *
     * @return the property name
     */
    @Override
    @Transient
    public String getSearchFieldName() {
        return getAutocompleteConfig().getSearchFieldName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<AbstractSearchFieldConfig, Object> getValues(Object object, String mapName) {
        Map<AbstractSearchFieldConfig, Object> configMap = new HashMap<AbstractSearchFieldConfig, Object>();
        configMap.putAll(getCheckboxConfig().getValues(object, mapName));
        configMap.putAll(getAutocompleteConfig().getValues(object, mapName));
        return configMap;
    }


}
