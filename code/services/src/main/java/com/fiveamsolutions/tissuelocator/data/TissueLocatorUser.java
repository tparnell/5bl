/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OrderBy;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.Index;
import org.hibernate.validator.AssertTrue;
import org.hibernate.validator.Length;
import org.hibernate.validator.NotEmpty;
import org.hibernate.validator.NotNull;
import org.hibernate.validator.Valid;

import com.fiveamsolutions.dynamicextensions.ExtendableEntity;
import com.fiveamsolutions.nci.commons.audit.Auditable;
import com.fiveamsolutions.nci.commons.data.persistent.Deletable;
import com.fiveamsolutions.nci.commons.data.security.AbstractUser;
import com.fiveamsolutions.nci.commons.data.security.AccountStatus;
import com.fiveamsolutions.nci.commons.data.security.ApplicationRole;
import com.fiveamsolutions.nci.commons.data.security.UserGroup;
import com.fiveamsolutions.tissuelocator.data.annotation.OnSave;
import com.fiveamsolutions.tissuelocator.data.validation.AccountStatusChange;
import com.fiveamsolutions.tissuelocator.data.validation.ConsistentNames;
import com.fiveamsolutions.tissuelocator.data.validation.RequiredUserResume;
import com.fiveamsolutions.tissuelocator.data.validation.SinglePropertyUniqueConstraint;
import com.fiveamsolutions.tissuelocator.data.validation.ValidExtensions;
import com.fiveamsolutions.tissuelocator.hibernate.interceptor.DeleteOrphans;
import com.fiveamsolutions.tissuelocator.util.AccountStatusTransitionHistoryHandler;

/**
 * @author ddasgupta
 */
@Entity(name = "tissue_locator_user")
@AccountStatusChange
@ConsistentNames
@SinglePropertyUniqueConstraint(propertyName = "email", message = "{validator.user.register.duplicate}")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@ValidExtensions
@DeleteOrphans(targetProperties = { "resume" }, idProperties = { "lob.id" }, orphanClasses = { LobHolder.class })
public class TissueLocatorUser extends AbstractUser
    implements InstitutionRestricted, Auditable, Deletable, ExtendableEntity,
    StatusTransitionable<AccountStatusTransition, AccountStatus> {

    private static final long serialVersionUID = -3089869011908328910L;
    private static final int LENGTH = 254;

    private Address address = new Address();
    private Date creationDate = new Date();
    private Date updatedDate = new Date();
    private Institution institution;
    private String department;
    private String researchArea;
    private String title;
    private String degree;
    private TissueLocatorFile resume;

    private List<AccountStatusTransition> statusTransitionHistory = new ArrayList<AccountStatusTransition>();
    private Date statusTransitionDate;
    private String statusTransitionComment;

    private Map<String, Object> customProperties = new HashMap<String, Object>();

    /**
     * @return the address
     */
    @Valid
    @NotNull
    @ManyToOne
    @Cascade(CascadeType.ALL)
    @JoinColumn(name = "address_id")
    @ForeignKey(name = "user_address_fk")
    @Index(name = "user_address_idx")
    public Address getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(Address address) {
        this.address = address;
    }

    /**
     * @return the creationDate
     */
    @Temporal(TemporalType.TIMESTAMP)
    @NotNull
    @Column(name = "creation_date")
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * @param creationDate the creationDate to set
     */
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * @return the updatedDate
     */
    @Temporal(TemporalType.TIMESTAMP)
    @NotNull
    public Date getUpdatedDate() {
        return updatedDate;
    }

    /**
     * @param updatedDate the updatedDate to set
     */
    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    /**
     * @return the institution
     */
    @NotNull
    @Valid
    @ManyToOne
    @Cascade(value = CascadeType.SAVE_UPDATE)
    @JoinColumn(name = "institution_id")
    @ForeignKey(name = "user_institution_fk")
    @Index(name = "user_institution_idx")
    public Institution getInstitution() {
        return institution;
    }

    /**
     * @param institution the institution to set
     */
    public void setInstitution(Institution institution) {
        this.institution = institution;
    }

    /**
     * @return the department
     */
    @NotEmpty
    @Length(max = LENGTH)
    public String getDepartment() {
        return department;
    }

    /**
     * @param department the department to set
     */
    public void setDepartment(String department) {
        this.department = department;
    }

    /**
     * @return the researchArea
     */
    @NotEmpty
    @Length(max = LENGTH)
    public String getResearchArea() {
        return researchArea;
    }

    /**
     * @param researchArea the researchArea to set
     */
    public void setResearchArea(String researchArea) {
        this.researchArea = researchArea;
    }

    /**
     * @return the title
     */
    @NotEmpty
    @Length(max = LENGTH)
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the degree
     */
    @NotEmpty
    @Length(max = LENGTH)
    public String getDegree() {
        return degree;
    }

    /**
     * @param degree the degree to set
     */
    public void setDegree(String degree) {
        this.degree = degree;
    }

    /**
     * @return the resume
     */
    @Valid
    @RequiredUserResume
    @Embedded
    @AttributeOverrides({
        @AttributeOverride(name = "contentType", column = @Column(name = "resume_content_type")),
        @AttributeOverride(name = "name", column = @Column(name = "resume_name"))
    })
    @AssociationOverrides({
        @AssociationOverride(name = "lob", joinColumns = @JoinColumn(name = "resume_lob_id"))
    })
    public TissueLocatorFile getResume() {
        return resume;
    }

    /**
     * @param resume the resume to set
     */
    public void setResume(TissueLocatorFile resume) {
        this.resume = resume;
    }

    /**
     * @return the customProperties
     */
    @Transient
    public Map<String, Object> getCustomProperties() {
        return customProperties;
    }

    /**
     * @param customProperties the customProperties to set
     */
    public void setCustomProperties(Map<String, Object> customProperties) {
        this.customProperties = customProperties;
    }

    /**
     * {@inheritDoc}
     */
    @Transient
    public Object getCustomProperty(String name) {
        return getCustomProperties().get(name);
    }

    /**
     * {@inheritDoc}
     */
    public void setCustomProperty(String name, Object value) {
        getCustomProperties().put(name, value);
    }

    /**
     * {@inheritDoc}
     */
    @Transient
    public Collection<Institution> getRelatedInstitutions() {
        return Collections.singleton(getInstitution());
    }

    /**
     * @return whether the user has the cross institution role
     */
    @Transient
    public boolean isCrossInstitution() {
        Set<ApplicationRole> allRoles = new HashSet<ApplicationRole>();
        allRoles.addAll(getRoles());
        for (UserGroup group : getGroups()) {
            allRoles.addAll(group.getRoles());
        }
        for (ApplicationRole role : allRoles) {
            if (Role.CROSS_INSTITUTION.getName().equals(role.getName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return whether the user has a given role
     * @param role the role to check
     */
    @Transient
    public boolean inRole(Role role) {
        return isInRoleName(role.getName());
    }

    /**
     * @return whether the user has a given role of the specified name
     * @param roleName the name of the role to check
     */
    public boolean isInRoleName(String roleName) {
        Set<ApplicationRole> allRoles = new HashSet<ApplicationRole>();
        allRoles.addAll(getRoles());
        for (UserGroup group : getGroups()) {
            allRoles.addAll(group.getRoles());
        }
        for (ApplicationRole r : allRoles) {
            if (r.getName().equals(roleName)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Indicates whether this user can be deleted.
     *
     * @return true if deletable otherwise false.
     */
    @Transient
    public boolean isDeletable() {
        return false;
    }

    /**
     * Whether account deactivation comment is valid.
     * @return true if the comment is valid, false otherwise.
     */
    @Transient
    @AssertTrue(message = "{validator.user.deactivate.comment.required}")
    public boolean isDeactivationStatusTransitionCommentValid() {
        return AccountStatus.ACTIVE.equals(getPreviousStatus()) && AccountStatus.INACTIVE.equals(getStatus())
            ? StringUtils.isNotBlank(getStatusTransitionComment()) : true;
    }

    /**
     * Whether account denial comment is valid.
     * @return true if the comment is valid, false otherwise.
     */
    @Transient
    @AssertTrue(message = "{validator.user.register.denial.required}")
    public boolean isDenialStatusTransitionCommentValid() {
        return AccountStatus.PENDING.equals(getPreviousStatus()) && AccountStatus.INACTIVE.equals(getStatus())
            ? StringUtils.isNotBlank(getStatusTransitionComment()) : true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ManyToMany
    @Valid
    @JoinTable(name = "account_status_transition_history",
                joinColumns = @JoinColumn(name = "tissue_locator_user_id"),
                inverseJoinColumns = @JoinColumn(name = "status_transition_id"))
    @ForeignKey(name = "USER_TRANSITION_FK",
                inverseName = "TRANSITION_USER_FK")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @Cascade(value = CascadeType.ALL)
    @OrderBy(value = "systemTransitionDate")
    public List<AccountStatusTransition> getStatusTransitionHistory() {
        return statusTransitionHistory;
    }

    /**
     * @param statusTransitionHistory the statusTransitionHistory to set
     */
    public void setStatusTransitionHistory(
            List<AccountStatusTransition> statusTransitionHistory) {
        this.statusTransitionHistory = statusTransitionHistory;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transient
    public Date getStatusTransitionDate() {
        return statusTransitionDate;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setStatusTransitionDate(Date statusTransitionDate) {
        this.statusTransitionDate = statusTransitionDate;
    }

    /**
     * @return the statusTransitionComment
     */
    @Transient
    public String getStatusTransitionComment() {
        return statusTransitionComment;
    }

    /**
     * @param statusTransitionComment the statusTransitionComment to set
     */
    public void setStatusTransitionComment(String statusTransitionComment) {
        this.statusTransitionComment = statusTransitionComment;
    }

    /**
     * Update the status transition history based on the current and previous status.
     */
    @OnSave
    public void updateStatusTransitionHistory() {
        new AccountStatusTransitionHistoryHandler().updateStatusTransitionHistory(this);
    }
}
