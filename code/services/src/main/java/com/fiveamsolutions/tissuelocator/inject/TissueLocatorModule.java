/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.inject;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.hibernate.Session;

import com.fiveamsolutions.dynamicextensions.hibernate.DynamicExtensionsConfigurator;
import com.fiveamsolutions.dynamicextensions.hibernate.DynamicExtensionsHibernateHelper;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;
import com.google.inject.Provides;
import com.google.inject.Singleton;

/**
 * The module for the tissue locator application.  Provides access to things needed by all modules.
 * @author smiller
 */
public class TissueLocatorModule extends AbstractModule {

    /**
     * {@inheritDoc}
     */
    @Override
    protected void configure() {
        // do nothing
    }
    
    /**
     * Configures and returns our hibernate helper.
     * 
     * @return the helper.
     */
    @Provides
    @Singleton
    protected DynamicExtensionsHibernateHelper provideHibernateHelper() {
        // TODO - punt the static hibernate util class completely and move
        // initialization of hibernate here.
        return TissueLocatorHibernateUtil.getHibernateHelper();
    }

    /**
     * Extracts the session from a hibernate helper.
     * 
     * @param helper the helper.
     * @return the session.
     */
    @Provides
    Session provideCurrentSession(DynamicExtensionsHibernateHelper helper) {
        return helper.getCurrentSession();
    }
    
    /**
     * Provides the context.
     * 
     * @return the context.
     */
    @Provides
    @Singleton
    Context provideContext() throws NamingException {
        return new InitialContext();
    }
    
    /**
     * @return the configurator.
     */
    @Provides    
    DynamicExtensionsConfigurator provideDynamicExtensionsConfigurator() {
        return TissueLocatorHibernateUtil.getDynamicExtensionConfigurator();
    }
}
