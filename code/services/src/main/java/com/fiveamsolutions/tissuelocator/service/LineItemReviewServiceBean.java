/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.service;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.ejb.Stateless;
import javax.mail.MessagingException;

import com.fiveamsolutions.nci.commons.util.UsernameHolder;
import com.fiveamsolutions.tissuelocator.data.AggregateSpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.ReviewComment;
import com.fiveamsolutions.tissuelocator.data.ReviewProcess;
import com.fiveamsolutions.tissuelocator.data.Role;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestReviewVote;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.Vote;
import com.fiveamsolutions.tissuelocator.util.EmailHelper;

/**
 * @author ddasgupta
 *
 */
@Stateless
public class LineItemReviewServiceBean extends SpecimenRequestServiceBean implements
        LineItemReviewServiceLocal {

    /**
     * {@inheritDoc}
     */
    public void addLineItemReviews(SpecimenRequest request) throws MessagingException {
        request.setUpdatedDate(new Date());
        TissueLocatorUser reviewer = getUserService().getByUsername(UsernameHolder.getUser());
        finalizeLineItemReview(request, reviewer);

        // Ensure the request votes/orders are current
        SpecimenRequest updatedRequest = getPersistentObject(SpecimenRequest.class, request.getId());
        updateStatusForLineItemReview(updatedRequest);
        savePersistentObject(updatedRequest);

        String[] args = new String[] {String.valueOf(request.getId())};
        // notify the requestor
        new EmailHelper().sendEmail("specimenRequest.email.lineItemReview.complete",
                new String[] {request.getRequestor().getEmail()}, args);
        // notify the other state administrators
        sendNotificationToInstitutionAdmins(request, reviewer, Role.LINE_ITEM_REVIEW_VOTER,
                "specimenRequest.email.lineItemReview.completedByOtherStates");
    }

    private void finalizeLineItemReview(SpecimenRequest request, TissueLocatorUser reviewer)
        throws MessagingException {
        Set<AggregateSpecimenRequestLineItem> allLineItems =
            new HashSet<AggregateSpecimenRequestLineItem>(request.getAggregateLineItems());
        Set<AggregateSpecimenRequestLineItem> approvedLineItems = new HashSet<AggregateSpecimenRequestLineItem>();
        for (AggregateSpecimenRequestLineItem lineItem : allLineItems) {
            SpecimenRequestReviewVote vote = lineItem.getVote();
            if (reviewer.getInstitution().getId().equals(vote.getInstitution().getId())) {
                vote.setDate(new Date());
                vote.setUser(reviewer);
                if (Vote.APPROVE.equals(vote.getVote())) {
                    approvedLineItems.add(lineItem);
                    request.getAggregateLineItems().remove(lineItem);
                }
            }
        }

        if (!approvedLineItems.isEmpty()) {
            Institution i = reviewer.getInstitution();
            Long shipmentId = createShipment(request, i, null, approvedLineItems);
            Map<Institution, Long> shipmentIds = new HashMap<Institution, Long>();
            shipmentIds.put(i, shipmentId);
            sendTissueTechEmails(shipmentIds);
        }
        savePersistentObject(request);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addReviewComment(SpecimenRequest request, ReviewComment comment) throws MessagingException {
        request.setUpdatedDate(new Date());
        request.getComments().add(comment);
        // persist the comment
        savePersistentObject(request);
        // notify the other state administrators
        sendNotificationToInstitutionAdmins(request, comment.getUser(), Role.REVIEW_COMMENTER,
                "specimenRequest.email.lineItemReview.comment");
    }

    /**
     * send emails all other state administrators that associated with the request.
     *
     * @param request is the request for which a comment has just been added.
     * @param currentActor is the person who triggers the notification.
     * @param messageBaseKey is the resource bundle key for the notification message
     * @throws MessagingException on error
     */
    private void sendNotificationToInstitutionAdmins(SpecimenRequest request, TissueLocatorUser currentActor,
            Role actorRole, String messageBaseKey) throws MessagingException {
        Map<Institution, Set<AggregateSpecimenRequestLineItem>> aggregateLineItemMap = getAggregateLineItemMap(request);

        Set<Institution> requestedInstitutions = aggregateLineItemMap.keySet();
        if (requestedInstitutions.isEmpty()) {
            return; // nothing to do
        }

        Set<TissueLocatorUser> institutionUsers = getUserService().getByRoleAndInstitutions(
                actorRole.getName(), requestedInstitutions);
        Set<String> userEmailsToBeNotifiedSet = new HashSet<String>();
        for (TissueLocatorUser u : institutionUsers) {
            if (currentActor != null && !currentActor.getId().equals(u.getId())) {
                // no need to notify the person who made the comment
                userEmailsToBeNotifiedSet.add(u.getEmail());
            }
        }
        if (!userEmailsToBeNotifiedSet.isEmpty()) {
            // send email notifications if there's anybody to be notified
            String[] userEmailsToBeNotified = new String[userEmailsToBeNotifiedSet.size()];
            userEmailsToBeNotifiedSet.toArray(userEmailsToBeNotified);

            String[] emailMessageArgs = new String[] {String.valueOf(request.getId())};
            new EmailHelper().sendEmail(messageBaseKey, userEmailsToBeNotified, emailMessageArgs);
        }
    }

    private void updateStatusForLineItemReview(SpecimenRequest request) {
        ReviewProcess process = getApplicationSettingService().getReviewProcess();
        process.getRequestStatusUpdater().updateStatus(request);
    }
}
