/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.fiveamsolutions.tissuelocator.data.AggregateSpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.Role;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestReviewVote;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.service.InstitutionServiceLocal;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceLocal;
import com.fiveamsolutions.tissuelocator.service.user.TissueLocatorUserServiceLocal;
import com.google.inject.Inject;

/**
 * @author bhumphrey
 *
 */
public class LineItemReviewRequestProcessor implements RequestProcessor {

    private TissueLocatorUserServiceLocal userService;

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("PMD.ExcessiveParameterList")
    // CHECKSTYLE:OFF � too many parameters
    public Collection<Email> initializeRequest(SpecimenRequest request, Email emailBase, Email leadReviewContent,
            Email institutionalApprovalContent, Email revisionEmail, Email reviewerNotAssignedBase, int numReviewers,
            boolean isNew) {
        // CHECKSTYLE:OFF
        Collection<Email> emails = new ArrayList<Email>();
        Set<Institution> institutions = new HashSet<Institution>();
        for (AggregateSpecimenRequestLineItem lineItem : request.getAggregateLineItems()) {
            institutions.add(lineItem.getInstitution());
            SpecimenRequestReviewVote vote = new SpecimenRequestReviewVote();
            vote.setInstitution(lineItem.getInstitution());
            lineItem.setVote(vote);
        }

        for (Institution inst : institutions) {
            Set<TissueLocatorUser> users = userService.getByRoleAndInstitution(Role.LINE_ITEM_REVIEW_VOTER
                    .getName(), inst);

            for (TissueLocatorUser user : users) {
                emails.add(getEmail(institutionalApprovalContent, user.getEmail(), request));
            }
        }
        return emails;
    }

    private Email getEmail(Email template, String recipient, SpecimenRequest request) {
        Email email = new Email();
        email.setRecipient(recipient);
        email.setSubject(template.getSubject());
        email.setHtml(template.getHtml());
        email.setText(template.getText());
        email.setRequest(request);
        return email;
    }

    /**
     * {@inheritDoc}
     */
    public Collection<Email> processPendingDecisionRequest(SpecimenRequest request,
            RequestProcessingConfiguration config) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public Collection<Email> processPendingRequest(SpecimenRequest request, RequestProcessingConfiguration config) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public void setApplicationSettingService(ApplicationSettingServiceLocal applicationSettingService) {
        //no-op;
    }

    /**
     * {@inheritDoc}
     */
    public void setInstitutionService(InstitutionServiceLocal institutionService) {
        //no-op;
    }

    /**
     * {@inheritDoc}
     */
    @Inject
    public void setUserService(TissueLocatorUserServiceLocal userService) {
        this.userService = userService;

    }

}
