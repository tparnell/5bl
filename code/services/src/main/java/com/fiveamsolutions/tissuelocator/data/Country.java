/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.data;

/**
 * Countries in the world.
 * @author ddasgupta
 */
public enum Country {

    /** United States. */
    UNITED_STATES,

    /** Afghanistan. */
    AFGHANISTAN,

    /** Aland Islands. */
    ALAND_ISLANDS,

    /** Albania. */
    ALBANIA,

    /** Algeria. */
    ALGERIA,

    /** American Samoa. */
    AMERICAN_SAMOA,

    /** Andorra. */
    ANDORRA,

    /** Angola. */
    ANGOLA,

    /** Anguilla. */
    ANGUILLA,

    /** Antarctica. */
    ANTARCTICA,

    /** Antigua And Barbuda. */
    ANTIGUA_AND_BARBUDA,

    /** Argentina. */
    ARGENTINA,

    /** Armenia. */
    ARMENIA,

    /** Aruba. */
    ARUBA,

    /** Australia. */
    AUSTRALIA,

    /** Austria. */
    AUSTRIA,

    /** Azerbaijan. */
    AZERBAIJAN,

    /** Bahamas. */
    BAHAMAS,

    /** Bahrain. */
    BAHRAIN,

    /** Bangladesh. */
    BANGLADESH,

    /** Barbados. */
    BARBADOS,

    /** Belarus. */
    BELARUS,

    /** Belgium. */
    BELGIUM,

    /** Belize. */
    BELIZE,

    /** Benin. */
    BENIN,

    /** Bermuda. */
    BERMUDA,

    /** Bhutan. */
    BHUTAN,

    /** Bolivia. */
    BOLIVIA,

    /** Bosnia And Herzegovina. */
    BOSNIA_AND_HERZEGOVINA,

    /** Botswana. */
    BOTSWANA,

    /** Bouvet Island. */
    BOUVET_ISLAND,

    /** Brazil. */
    BRAZIL,

    /** British Indian Ocean Territory. */
    BRITISH_INDIAN_OCEAN_TERRITORY,

    /** Brunei Darussalam. */
    BRUNEI_DARUSSALAM,

    /** Bulgaria. */
    BULGARIA,

    /** Burkina Faso. */
    BURKINA_FASO,

    /** Burundi. */
    BURUNDI,

    /** Cambodia. */
    CAMBODIA,

    /** Cameroon. */
    CAMEROON,

    /** Canada. */
    CANADA,

    /** Cape Verde. */
    CAPE_VERDE,

    /** Cayman Islands. */
    CAYMAN_ISLANDS,

    /** Central African Republic. */
    CENTRAL_AFRICAN_REPUBLIC,

    /** Chad. */
    CHAD,

    /** Chile. */
    CHILE,

    /** China. */
    CHINA,

    /** Christmas Island. */
    CHRISTMAS_ISLAND,

    /** Cocos (Keeling) Islands. */
    COCOS_KEELING_ISLANDS,

    /** Colombia. */
    COLOMBIA,

    /** Comoros. */
    COMOROS,

    /** Congo. */
    CONGO,

    /** Congo, The Democratic Republic Of The. */
    CONGO_THE_DEMOCRATIC_REPUBLIC_OF_THE,

    /** Cook Islands. */
    COOK_ISLANDS,

    /** Costa Rica. */
    COSTA_RICA,

    /** Cote D'Ivoire. */
    COTE_DIVOIRE,

    /** Croatia. */
    CROATIA,

    /** Cuba. */
    CUBA,

    /** Cyprus. */
    CYPRUS,

    /** Czech Republic. */
    CZECH_REPUBLIC,

    /** Denmark. */
    DENMARK,

    /** Djibouti. */
    DJIBOUTI,

    /** Dominica. */
    DOMINICA,

    /** Dominican Republic. */
    DOMINICAN_REPUBLIC,

    /** Ecuador. */
    ECUADOR,

    /** Egypt. */
    EGYPT,

    /** El Salvador. */
    EL_SALVADOR,

    /** Equatorial Guinea. */
    EQUATORIAL_GUINEA,

    /** Eritrea. */
    ERITREA,

    /** Estonia. */
    ESTONIA,

    /** Ethiopia. */
    ETHIOPIA,

    /** Falkland Islands (Malvinas). */
    FALKLAND_ISLANDS_MALVINAS,

    /** Faroe Islands. */
    FAROE_ISLANDS,

    /** Fiji. */
    FIJI,

    /** Finland. */
    FINLAND,

    /** France. */
    FRANCE,

    /** French Guiana. */
    FRENCH_GUIANA,

    /** French Polynesia. */
    FRENCH_POLYNESIA,

    /** French Southern Territories. */
    FRENCH_SOUTHERN_TERRITORIES,

    /** Gabon. */
    GABON,

    /** Gambia. */
    GAMBIA,

    /** Georgia. */
    GEORGIA,

    /** Germany. */
    GERMANY,

    /** Ghana. */
    GHANA,

    /** Gibraltar. */
    GIBRALTAR,

    /** Greece. */
    GREECE,

    /** Greenland. */
    GREENLAND,

    /** Grenada. */
    GRENADA,

    /** Guadeloupe. */
    GUADELOUPE,

    /** Guam. */
    GUAM,

    /** Guatemala. */
    GUATEMALA,

    /** Guernsey. */
    GUERNSEY,

    /** Guinea. */
    GUINEA,

    /** Guinea-Bissau. */
    GUINEA_BISSAU,

    /** Guyana. */
    GUYANA,

    /** Haiti. */
    HAITI,

    /** Heard Island And Mcdonald Islands. */
    HEARD_ISLAND_AND_MCDONALD_ISLANDS,

    /** Holy See (Vatican City State). */
    HOLY_SEE_VATICAN_CITY_STATE,

    /** Honduras. */
    HONDURAS,

    /** Hong Kong. */
    HONG_KONG,

    /** Hungary. */
    HUNGARY,

    /** Iceland. */
    ICELAND,

    /** India. */
    INDIA,

    /** Indonesia. */
    INDONESIA,

    /** Iran, Islamic Republic Of. */
    IRAN_ISLAMIC_REPUBLIC_OF,

    /** Iraq. */
    IRAQ,

    /** Ireland. */
    IRELAND,

    /** Isle Of Man. */
    ISLE_OF_MAN,

    /** Israel. */
    ISRAEL,

    /** Italy. */
    ITALY,

    /** Jamaica. */
    JAMAICA,

    /** Japan. */
    JAPAN,

    /** Jersey. */
    JERSEY,

    /** Jordan. */
    JORDAN,

    /** Kazakhstan. */
    KAZAKHSTAN,

    /** Kenya. */
    KENYA,

    /** Kiribati. */
    KIRIBATI,

    /** Korea, Democratic People's Republic Of. */
    KOREA_DEMOCRATIC_PEOPLES_REPUBLIC_OF,

    /** Korea, Republic Of. */
    KOREA_REPUBLIC_OF,

    /** Kuwait. */
    KUWAIT,

    /** Kyrgyzstan. */
    KYRGYZSTAN,

    /** Lao People's Democratic Republic. */
    LAO_PEOPLES_DEMOCRATIC_REPUBLIC,

    /** Latvia. */
    LATVIA,

    /** Lebanon. */
    LEBANON,

    /** Lesotho. */
    LESOTHO,

    /** Liberia. */
    LIBERIA,

    /** Libyan Arab Jamahiriya. */
    LIBYAN_ARAB_JAMAHIRIYA,

    /** Liechtenstein. */
    LIECHTENSTEIN,

    /** Lithuania. */
    LITHUANIA,

    /** Luxembourg. */
    LUXEMBOURG,

    /** Macao. */
    MACAO,

    /** Macedonia, The Former Yugoslav Republic Of. */
    MACEDONIA_THE_FORMER_YUGOSLAV_REPUBLIC_OF,

    /** Madagascar. */
    MADAGASCAR,

    /** Malawi. */
    MALAWI,

    /** Malaysia. */
    MALAYSIA,

    /** Maldives. */
    MALDIVES,

    /** Mali. */
    MALI,

    /** Malta. */
    MALTA,

    /** Marshall Islands. */
    MARSHALL_ISLANDS,

    /** Martinique. */
    MARTINIQUE,

    /** Mauritania. */
    MAURITANIA,

    /** Mauritius. */
    MAURITIUS,

    /** Mayotte. */
    MAYOTTE,

    /** Mexico. */
    MEXICO,

    /** Micronesia, Federated States Of. */
    MICRONESIA_FEDERATED_STATES_OF,

    /** Moldova, Republic Of. */
    MOLDOVA_REPUBLIC_OF,

    /** Monaco. */
    MONACO,

    /** Mongolia. */
    MONGOLIA,

    /** Montenegro. */
    MONTENEGRO,

    /** Montserrat. */
    MONTSERRAT,

    /** Morocco. */
    MOROCCO,

    /** Mozambique. */
    MOZAMBIQUE,

    /** Myanmar. */
    MYANMAR,

    /** Namibia. */
    NAMIBIA,

    /** Nauru. */
    NAURU,

    /** Nepal. */
    NEPAL,

    /** Netherlands. */
    NETHERLANDS,

    /** Netherlands Antilles. */
    NETHERLANDS_ANTILLES,

    /** New Caledonia. */
    NEW_CALEDONIA,

    /** New Zealand. */
    NEW_ZEALAND,

    /** Nicaragua. */
    NICARAGUA,

    /** Niger. */
    NIGER,

    /** Nigeria. */
    NIGERIA,

    /** Niue. */
    NIUE,

    /** Norfolk Island. */
    NORFOLK_ISLAND,

    /** Northern Mariana Islands. */
    NORTHERN_MARIANA_ISLANDS,

    /** Norway. */
    NORWAY,

    /** Oman. */
    OMAN,

    /** Pakistan. */
    PAKISTAN,

    /** Palau. */
    PALAU,

    /** Palestinian Territory, Occupied. */
    PALESTINIAN_TERRITORY_OCCUPIED,

    /** Panama. */
    PANAMA,

    /** Papua New Guinea. */
    PAPUA_NEW_GUINEA,

    /** Paraguay. */
    PARAGUAY,

    /** Peru. */
    PERU,

    /** Philippines. */
    PHILIPPINES,

    /** Pitcairn. */
    PITCAIRN,

    /** Poland. */
    POLAND,

    /** Portugal. */
    PORTUGAL,

    /** Puerto Rico. */
    PUERTO_RICO,

    /** Qatar. */
    QATAR,

    /** Reunion. */
    REUNION,

    /** Romania. */
    ROMANIA,

    /** Russian Federation. */
    RUSSIAN_FEDERATION,

    /** Rwanda. */
    RWANDA,

    /** Saint Barthelemy. */
    SAINT_BARTHELEMY,

    /** Saint Helena. */
    SAINT_HELENA,

    /** Saint Kitts And Nevis. */
    SAINT_KITTS_AND_NEVIS,

    /** Saint Lucia. */
    SAINT_LUCIA,

    /** Saint Martin. */
    SAINT_MARTIN,

    /** Saint Pierre And Miquelon. */
    SAINT_PIERRE_AND_MIQUELON,

    /** Saint Vincent And The Grenadines. */
    SAINT_VINCENT_AND_THE_GRENADINES,

    /** Samoa. */
    SAMOA,

    /** San Marino. */
    SAN_MARINO,

    /** Sao Tome And Principe. */
    SAO_TOME_AND_PRINCIPE,

    /** Saudi Arabia. */
    SAUDI_ARABIA,

    /** Senegal. */
    SENEGAL,

    /** Serbia. */
    SERBIA,

    /** Seychelles. */
    SEYCHELLES,

    /** Sierra Leone. */
    SIERRA_LEONE,

    /** Singapore. */
    SINGAPORE,

    /** Slovakia. */
    SLOVAKIA,

    /** Slovenia. */
    SLOVENIA,

    /** Solomon Islands. */
    SOLOMON_ISLANDS,

    /** Somalia. */
    SOMALIA,

    /** South Africa. */
    SOUTH_AFRICA,

    /** South Georgia And The South Sandwich Islands. */
    SOUTH_GEORGIA_AND_THE_SOUTH_SANDWICH_ISLANDS,

    /** Spain. */
    SPAIN,

    /** Sri Lanka. */
    SRI_LANKA,

    /** Sudan. */
    SUDAN,

    /** Suriname. */
    SURINAME,

    /** Svalbard And Jan Mayen. */
    SVALBARD_AND_JAN_MAYEN,

    /** Swaziland. */
    SWAZILAND,

    /** Sweden. */
    SWEDEN,

    /** Switzerland. */
    SWITZERLAND,

    /** Syrian Arab Republic. */
    SYRIAN_ARAB_REPUBLIC,

    /** Taiwan, Province Of China. */
    TAIWAN_PROVINCE_OF_CHINA,

    /** Tajikistan. */
    TAJIKISTAN,

    /** Tanzania, United Republic Of. */
    TANZANIA_UNITED_REPUBLIC_OF,

    /** Thailand. */
    THAILAND,

    /** Timor-Leste. */
    TIMOR_LESTE,

    /** Togo. */
    TOGO,

    /** Tokelau. */
    TOKELAU,

    /** Tonga. */
    TONGA,

    /** Trinidad And Tobago. */
    TRINIDAD_AND_TOBAGO,

    /** Tunisia. */
    TUNISIA,

    /** Turkey. */
    TURKEY,

    /** Turkmenistan. */
    TURKMENISTAN,

    /** Turks And Caicos Islands. */
    TURKS_AND_CAICOS_ISLANDS,

    /** Tuvalu. */
    TUVALU,

    /** Uganda. */
    UGANDA,

    /** Ukraine. */
    UKRAINE,

    /** United Arab Emirates. */
    UNITED_ARAB_EMIRATES,

    /** United Kingdom. */
    UNITED_KINGDOM,

    /** United States Minor Outlying Islands. */
    UNITED_STATES_MINOR_OUTLYING_ISLANDS,

    /** Uruguay. */
    URUGUAY,

    /** Uzbekistan. */
    UZBEKISTAN,

    /** Vanuatu. */
    VANUATU,

    /** Venezuela. */
    VENEZUELA,

    /** Viet Nam. */
    VIET_NAM,

    /** Virgin Islands, British. */
    VIRGIN_ISLANDS_BRITISH,

    /** Virgin Islands, U.S.. */
    VIRGIN_ISLANDS_US,

    /** Wallis And Futuna. */
    WALLIS_AND_FUTUNA,

    /** Western Sahara. */
    WESTERN_SAHARA,

    /** Yemen. */
    YEMEN,

    /** Zambia. */
    ZAMBIA,

    /** Zimbabwe. */
    ZIMBABWE;
}
