/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.data.config.category;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.validator.Length;
import org.hibernate.validator.NotEmpty;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;

/**
 * User interface category.
 *
 * @author jstephens
 */
@MappedSuperclass
public abstract class AbstractUiCategory implements PersistentObject {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String categoryName;
    private String entityClassName;
    private Boolean viewable;

    /**
     * Maximum string field length.
     */
    protected static final int MAX_STRING_LENGTH = 255;

    /**
     * Gets the id of the category.
     *
     * @return the category id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Override
    public Long getId() {
        return id;
    }

    /**
     * Sets the id of the category.
     *
     * @param id the category id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the name of the category.
     *
     * @return the category name
     */
    @NotEmpty
    @Length(max = MAX_STRING_LENGTH)
    @Column(name = "category_name")
    public String getCategoryName() {
        return categoryName;
    }

    /**
     * Sets the name of the category.
     *
     * @param categoryName the category name
     */
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    /**
     * Returns the class name of the entity to which this category belongs.
     *
     * @return the entity class name to which this category belongs
     */
    @NotEmpty
    @Length(max = MAX_STRING_LENGTH)
    @Column(name = "entity_class_name")
    public String getEntityClassName() {
        return entityClassName;
    }

    /**
     * Sets the class name of the entity to which this category belongs.
     *
     * @param entityClassName the class name of the specified entity
     */
    public void setEntityClassName(String entityClassName) {
        this.entityClassName = entityClassName;
    }

    /**
     * Indicates whether the category name is viewable.
     *
     * @return true if the category name is viewable; false otherwise
     */
    @Column(name = "viewable")
    public Boolean isViewable() {
        return viewable;
    }

    /**
     * Sets the category name as viewable or not.
     *
     * @param viewable true if the category name is viewable;
     * false otherwise
     */
    public void setViewable(Boolean viewable) {
        this.viewable = viewable;
    }

    

}