/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.data;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.Index;
import org.hibernate.validator.Digits;
import org.hibernate.validator.Length;
import org.hibernate.validator.Min;
import org.hibernate.validator.NotEmpty;
import org.hibernate.validator.NotNull;
import org.hibernate.validator.Valid;

import com.fiveamsolutions.nci.commons.audit.Auditable;
import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.tissuelocator.data.code.SpecimenReceiptQuality;
import com.fiveamsolutions.tissuelocator.data.validation.LineItemDispositionChange;

/**
 * @author ddasgupta
 *
 */
@Entity
@LineItemDispositionChange
@Table(name = "aggregate_specimen_request_line_item")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AggregateSpecimenRequestLineItem implements PersistentObject, Auditable, SpecimenDispositionAssignable {

    private static final long serialVersionUID = -8203289925909288173L;
    private static final int MAX_LENGTH = 3999;
    private static final int INT_DIGITS = 17;

    private Long id;
    private String criteria;
    private Institution institution;
    private int quantity;
    private String note;
    private BigDecimal finalPrice;
    private Boolean received;
    private SpecimenReceiptQuality receiptQuality;
    private String receiptNote;
    private SpecimenDisposition disposition;
    private SpecimenDisposition previousDisposition;
    private SpecimenRequestReviewVote vote;

    /**
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the criteria
     */
    @NotEmpty
    @Length(max = MAX_LENGTH)
    public String getCriteria() {
        return criteria;
    }

    /**
     * @param criteria the criteria to set
     */
    public void setCriteria(String criteria) {
        this.criteria = criteria;
    }

    /**
     * @return the institution
     */
    @NotNull
    @ManyToOne
    @JoinColumn(name = "institution_id")
    @ForeignKey(name = "aggregate_line_item_institution_fk")
    @Index(name = "aggregate_line_item_institution_idx")
    public Institution getInstitution() {
        return institution;
    }

    /**
     * @param institution the institution to set
     */
    public void setInstitution(Institution institution) {
        this.institution = institution;
    }

    /**
     * @return the quantity
     */
    @NotNull
    @Min(value = 0)
    public int getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the note
     */
    @Length(max = MAX_LENGTH)
    public String getNote() {
        return note;
    }

    /**
     * @param note the note to set
     */
    public void setNote(String note) {
        this.note = note;
    }

    /**
     * @return the finalPrice
     */
    @Min(value = 0)
    @Digits(integerDigits = INT_DIGITS, fractionalDigits = 2)
    @Column(name = "final_price")
    public BigDecimal getFinalPrice() {
        return finalPrice;
    }

    /**
     * @param finalPrice the finalPrice to set
     */
    public void setFinalPrice(BigDecimal finalPrice) {
        this.finalPrice = finalPrice;
    }

    /**
     * {@inheritDoc}
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "disposition")
    public SpecimenDisposition getDisposition() {
        return disposition;
    }

    /**
     * {@inheritDoc}
     */
    public void setDisposition(SpecimenDisposition disposition) {
        this.disposition = disposition;
    }

    /**
     * {@inheritDoc}
     */
    @Enumerated(value = EnumType.STRING)
    @Column(name = "disposition", updatable = false, insertable = false)
    public SpecimenDisposition getPreviousDisposition() {
        return previousDisposition;
    }

    /**
     * {@inheritDoc}
     */
    public void setPreviousDisposition(SpecimenDisposition previousDisposition) {
        this.previousDisposition = previousDisposition;
    }

    /**
     * @return the receiptQuality
     */
    @ManyToOne
    @JoinColumn(name = "receipt_quality_id")
    @ForeignKey(name = "aggregate_line_item_receipt_quality_fk")
    @Index(name = "aggregate_line_item_receipt_quality_idx")
    public SpecimenReceiptQuality getReceiptQuality() {
        return receiptQuality;
    }

    /**
     * @param receiptQuality the receiptQuality to set
     */
    public void setReceiptQuality(SpecimenReceiptQuality receiptQuality) {
        this.receiptQuality = receiptQuality;
    }

    /**
     * @return the receiptNote
     */
    @Length(max = MAX_LENGTH)
    @Column(name = "receipt_note")
    public String getReceiptNote() {
        return receiptNote;
    }

    /**
     * @param receiptNote the receiptNote to set
     */
    public void setReceiptNote(String receiptNote) {
        this.receiptNote = receiptNote;
    }

    /**
     * @return the received
     */
    @Column(name = "received")
    public Boolean getReceived() {
        return received;
    }

    /**
     * @param received the received to set
     */
    public void setReceived(Boolean received) {
        this.received = received;
    }

    /**
     * @return the vote
     */
    @Valid
    @ManyToOne
    @JoinColumn(name = "vote_id")
    @ForeignKey(name = "aggregate_line_item_vote_fk")
    @Index(name = "aggregate_line_item_vote_idx")
    @Cascade(value = CascadeType.ALL)
    public SpecimenRequestReviewVote getVote() {
        return vote;
    }

    /**
     * @param vote the vote to set
     */
    public void setVote(SpecimenRequestReviewVote vote) {
        this.vote = vote;
    }
}
