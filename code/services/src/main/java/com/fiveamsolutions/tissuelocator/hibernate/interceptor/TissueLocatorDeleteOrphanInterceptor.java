/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.hibernate.interceptor;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.hibernate.type.Type;

import com.fiveamsolutions.dynamicextensions.AbstractDynamicFieldDefinition;
import com.fiveamsolutions.dynamicextensions.EntityDynamicFieldDefinition;
import com.fiveamsolutions.dynamicextensions.audit.DynamicExtensionsAuditLogInterceptor;
import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.nci.commons.util.HibernateHelper;
import com.fiveamsolutions.tissuelocator.service.GenericServiceLocal;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;

/**
 * Performs application-specific hibernate processing.
 * @author gvaughn
 *
 */
public class TissueLocatorDeleteOrphanInterceptor extends DynamicExtensionsAuditLogInterceptor {

    /**
     * Build the interceptor.
     * @param hh the helper.
     */
    public TissueLocatorDeleteOrphanInterceptor(HibernateHelper hh) {
        super(hh);
    }
    
    /**
     * Default constructor.
     */
    public TissueLocatorDeleteOrphanInterceptor() {
        super();
    }

    private static final long serialVersionUID = 689841721629554445L;
    private static final Logger LOG = Logger.getLogger(TissueLocatorDeleteOrphanInterceptor.class);


    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("PMD.ExcessiveParameterList")
    public boolean onFlushDirty(Object obj, Serializable id,
            Object[] newValues, Object[] oldValues, String[] properties,
            Type[] types) {
        if (oldValues != null) {
            // Determine if the dirty object contains potential orphans
            Map<String, String> orphanProperties = new HashMap<String, String>();
            Map<String, Class<? extends PersistentObject>> orphanClasses = 
                new HashMap<String, Class<? extends PersistentObject>>();
            DeleteOrphans annotation = obj.getClass().getAnnotation(DeleteOrphans.class);
            if (annotation != null) {
                for (int i = 0; i < annotation.targetProperties().length; i++) {
                    orphanProperties.put(annotation.targetProperties()[i], annotation.idProperties()[i]);
                    orphanClasses.put(annotation.targetProperties()[i], annotation.orphanClasses()[i]);
                }
            }
            addOrphanDeletableEntities(obj, orphanProperties, orphanClasses);
            
            if (!orphanProperties.isEmpty()) {
                deleteOrphans(orphanProperties, orphanClasses, newValues, oldValues, properties);
            }
        }        
        return super.onFlushDirty(obj, id, newValues, oldValues, properties, types);
    }
    
    private void deleteOrphans(Map<String, String> orphanProperties, 
            Map<String, Class<? extends PersistentObject>> orphanClasses, 
            Object[] newValues, Object[] oldValues, String[] properties) {
        GenericServiceLocal<PersistentObject> service = 
            TissueLocatorRegistry.getServiceLocator().getGenericService();
        for (int i = 0; i < properties.length; i++) {
            if (orphanProperties.keySet().contains(properties[i])) {
               Long orphanId = getOrphanId(orphanProperties.get(properties[i]), oldValues[i], newValues[i]);
               if (orphanId != null) {
                   service.deletePersistentObject((PersistentObject) 
                       TissueLocatorHibernateUtil.getHibernateHelper().getCurrentSession().load(
                       orphanClasses.get(properties[i]), orphanId));
               }
            }
        }
    }
    
    @SuppressWarnings("unchecked")
    private void addOrphanDeletableEntities(Object obj, Map<String, String> orphanProperties, 
            Map<String, Class<? extends PersistentObject>> orphanClasses) {
        // examine de definitions to determine potential orphans
        List<AbstractDynamicFieldDefinition> definitions = TissueLocatorHibernateUtil
                .getDynamicExtensionConfigurator()
                .getDynamicFieldDefinitionsForClass(obj.getClass().getName());
        for (AbstractDynamicFieldDefinition def : definitions) {
            OrphanDeletableEntity annotation = def.getClass().getAnnotation(
                    OrphanDeletableEntity.class);
            if (annotation != null) {
                Class<? extends PersistentObject> orphanClass = null;
                try {
                    orphanClass = (Class<? extends PersistentObject>) Class
                            .forName(((EntityDynamicFieldDefinition) def)
                                    .getReferencedEntityClassName());
                } catch (ClassNotFoundException e) {
                    LOG.error(e.getMessage(), e);
                }
                orphanProperties.put(def.getFieldName(), annotation
                        .idProperty());
                orphanClasses.put(def.getFieldName(), orphanClass);
            }
        }
    }
    
    private Long getOrphanId(String idProperty, Object oldValue, Object newValue) {
        // determine if an object has been orphaned and if so return the id
        try {
            if (oldValue == null) {
                return null;
            } else if (newValue == null) {
                return (Long) PropertyUtils.getProperty(oldValue, idProperty);
            }
            Long oldId = (Long) PropertyUtils.getProperty(oldValue, idProperty);
            Long newId = (Long) PropertyUtils.getProperty(newValue, idProperty);
            if (oldId != null && !oldId.equals(newId)) {
               return oldId;
            }
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        } 
        return null;    
    }
    

}
