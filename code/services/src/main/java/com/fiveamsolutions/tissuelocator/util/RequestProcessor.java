/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.util;

import java.util.Collection;

import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.service.InstitutionServiceLocal;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceLocal;
import com.fiveamsolutions.tissuelocator.service.user.TissueLocatorUserServiceLocal;

/**
 * @author ddasgupta
 *
 */
public interface RequestProcessor {

    /**
     * Initialize a request on save.  Implementations will do things like create consortium
     * and institutional review votes, and send email to users required to take action on
     * the request.
     * @param request the request to initialize.
     * @param emailBase the base of the email to be sent to all institutional administrators
     * @param leadReviewContent the content to be added to the email to an institutional administrator
     * if their institution is one of the lead reviewers.
     * @param institutionalApprovalContent the content to be added to the email to an institutional
     * administrator if the request includes specimens from their institution.
     * @param revisionEmail the vote reminder email to be sent to the committee on an request revision
     * @param reviewerNotAssignedBase the email base sent when a reviewer has not been assigned to a request
     * @param numReviewers the number of reviewers to assign.
     * @param isNew whether the request is new or is a resubmission
     * @return a collection of emails to send
     */
    @SuppressWarnings("PMD.ExcessiveParameterList")
    //CHECKSTYLE:OFF too many parameters
    Collection<Email> initializeRequest(SpecimenRequest request, Email emailBase, Email leadReviewContent,
            Email institutionalApprovalContent, Email revisionEmail, Email reviewerNotAssignedBase,
            int numReviewers, boolean isNew);
    //CHECKSTYLE:ON

    /**
     * Process a pending request.
     * @param request the request to process
     * @param config the request processing configuration
     * @return a collection of emails to send
     */
    Collection<Email> processPendingRequest(SpecimenRequest request, RequestProcessingConfiguration config);

    /**
     * Process a pending final decision request.
     * @param request the request to process
     * @param config the request processing configuration
     * @return a collection of emails to send
     */
    Collection<Email> processPendingDecisionRequest(SpecimenRequest request, RequestProcessingConfiguration config);

    /**
     * set the user service.
     * @param userService the user service
     */
    void setUserService(TissueLocatorUserServiceLocal userService);

    /**
     * set the institution service.
     * @param institutionService the institution service
     */
    void setInstitutionService(InstitutionServiceLocal institutionService);

    /**
     * set the application setting service.
     * @param applicationSettingService the application setting service
     */
    void setApplicationSettingService(ApplicationSettingServiceLocal applicationSettingService);
}
