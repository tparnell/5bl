/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.data;

import org.apache.commons.lang.StringUtils;

/**
 * @author ddasgupta
 *
 */
public enum ShippingMethod {

    /**
     * DHL.
     */
    DHL("http://www.dhl.com/content/g0/en/express/tracking.shtml?brand=DHL&AWB=%s", "shippingMethod.dhl"),

    /**
     * FedEx.
     */
    FEDEX("http://www.fedex.com/Tracking?tracknumber_list=%s", "shippingMethod.fedEx"),

    /**
     * Other.
     */
    OTHER(null, "shippingMethod.other"),

    /**
     * UPS.
     */
    UPS("http://wwwapps.ups.com/WebTracking/processInputRequest?tracknum=%s", "shippingMethod.ups"),

    /**
     * United States Postal Service.
     */
    USPS("http://trkcnfrm1.smi.usps.com/PTSInternetWeb/InterLabelInquiry.do?CAMEFROM=OK&strOrigTrackNum=%s",
            "shippingMethod.usps");


    private String trackingUrl;
    private String resourceKey;

    /**
     * constructor.
     * @param trackingUrl the tracking Url
     * @param resourceKey the key of the human readable text
     */
    private ShippingMethod(String trackingUrl, String resourceKey) {
        this.trackingUrl = trackingUrl;
        this.resourceKey = resourceKey;
    }

    /**
     * @return the resourceKey
     */
    public String getResourceKey() {
        return resourceKey;
    }

    /**
     * Get the formatted tracking url for a given tracking number.
     * @param trackingNumber the tracking number
     * @return the url for the site including tracking information for the shipment
     */
    public String getFormattedTrackingUrl(String trackingNumber) {
        if (StringUtils.isBlank(trackingUrl) || StringUtils.isBlank(trackingNumber)) {
            return StringUtils.EMPTY;
        }
        return String.format(trackingUrl, trackingNumber);
    }
}
