/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.data.support;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.apache.commons.lang.xwork.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.Index;
import org.hibernate.validator.Length;
import org.hibernate.validator.NotEmpty;
import org.hibernate.validator.NotNull;
import org.hibernate.validator.Valid;

import com.fiveamsolutions.nci.commons.audit.Auditable;
import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.nci.commons.search.Searchable;
import com.fiveamsolutions.tissuelocator.data.AccessRestricted;
import com.fiveamsolutions.tissuelocator.data.Person;
import com.fiveamsolutions.tissuelocator.data.Role;
import com.fiveamsolutions.tissuelocator.data.StatusTransitionable;
import com.fiveamsolutions.tissuelocator.data.Timestampable;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorFile;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.annotation.OnSave;
import com.fiveamsolutions.tissuelocator.data.validation.NotFuture;
import com.fiveamsolutions.tissuelocator.util.QuestionStatusTransitionHistoryHandler;

/**
 * @author ddasgupta
 *
 */
@Entity
@Table(name = "question")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Question implements PersistentObject, Auditable, AccessRestricted,
    StatusTransitionable<QuestionStatusTransition, QuestionStatus>, Timestampable {

    private static final long serialVersionUID = 6929260804370364485L;
    private static final String NAME = "name";
    private static final String CONTENT_TYPE = "contentType";
    private static final String LOB = "lob";
    private static final int QUESTION_LENGTH = 3999;

    private Long id;
    private TissueLocatorUser requestor;
    private Person investigator;
    private TissueLocatorFile resume;
    private TissueLocatorFile protocolDocument;
    private String questionText;
    private List<QuestionResponse> responses = new ArrayList<QuestionResponse>();

    private QuestionStatus status;
    private QuestionStatus previousStatus;
    private Date createdDate = new Date();
    private Date lastUpdatedDate = new Date();

    private List<QuestionStatusTransition> statusTransitionHistory
        = new ArrayList<QuestionStatusTransition>();
    private Date statusTransitionDate;

    /**
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the requestor
     */
    @NotNull
    @ManyToOne
    @JoinColumn(name = "requestor_id")
    @ForeignKey(name = "question_requestor_fk")
    @Index(name = "question_requestor_idx")
    @Searchable
    public TissueLocatorUser getRequestor() {
        return requestor;
    }

    /**
     * @param requestor the requestor to set
     */
    public void setRequestor(TissueLocatorUser requestor) {
        this.requestor = requestor;
    }

    /**
     * @return the investigator
     */
    @Valid
    @NotNull
    @ManyToOne
    @JoinColumn(name = "investigator_id")
    @ForeignKey(name = "question_investigator_fk")
    @Index(name = "question_investigator_idx")
    @Cascade(value = {CascadeType.ALL, CascadeType.DELETE_ORPHAN })
    public Person getInvestigator() {
        return investigator;
    }

    /**
     * @param investigator the investigator to set
     */
    public void setInvestigator(Person investigator) {
        this.investigator = investigator;
    }

    /**
     * @return the protocolDocument
     */
    @Valid
    @Embedded
    @AttributeOverrides({
        @AttributeOverride(name = CONTENT_TYPE, column = @Column(name = "protocol_document_content_type")),
        @AttributeOverride(name = NAME, column = @Column(name = "protocol_document_name"))
    })
    @AssociationOverrides({
        @AssociationOverride(name = LOB, joinColumns = @JoinColumn(name = "protocol_document_lob_id"))
    })
    public TissueLocatorFile getProtocolDocument() {
        return protocolDocument;
    }

    /**
     * @param protocolDocument the protocolDocument to set
     */
    public void setProtocolDocument(TissueLocatorFile protocolDocument) {
        this.protocolDocument = protocolDocument;
    }

    /**
     * @return the resume
     */
    @NotNull
    @Valid
    @Embedded
    @AttributeOverrides({
        @AttributeOverride(name = CONTENT_TYPE, column = @Column(name = "resume_content_type")),
        @AttributeOverride(name = NAME, column = @Column(name = "resume_name"))
    })
    @AssociationOverrides({
        @AssociationOverride(name = LOB, joinColumns = @JoinColumn(name = "resume_lob_id"))
    })
    public TissueLocatorFile getResume() {
        return resume;
    }

    /**
     * @param resume the resume to set
     */
    public void setResume(TissueLocatorFile resume) {
        this.resume = resume;
    }

    /**
     * @return the questionText
     */
    @NotEmpty
    @Column(name = "question_text")
    @Length(max = QUESTION_LENGTH)
    public String getQuestionText() {
        return questionText;
    }

    /**
     * @param questionText the questionText to set
     */
    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    /**
     * @return the responses
     */
    @Valid
    @OneToMany(mappedBy = "question")
    @OrderBy(value = "id")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @Cascade(value = {CascadeType.ALL, CascadeType.DELETE_ORPHAN })
    public List<QuestionResponse> getResponses() {
        return responses;
    }

    /**
     * @param responses the responses to set
     */
    public void setResponses(List<QuestionResponse> responses) {
        this.responses = responses;
    }

    /**
     * @return the status
     */
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    @Searchable
    @Index(name = "question_status_idx")
    public QuestionStatus getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(QuestionStatus status) {
        this.status = status;
    }

    /**
     * @return the specimen's status
     */
    @Enumerated(value = EnumType.STRING)
    @Column(name = "status", updatable = false, insertable = false, nullable = false)
    public QuestionStatus getPreviousStatus() {
        return previousStatus;
    }

    /**
     * @param previousStatus the status to set
     */
    public void setPreviousStatus(QuestionStatus previousStatus) {
        this.previousStatus = previousStatus;
    }

    /**
     * @return the status transitions.
     */
    @Fetch(FetchMode.SELECT)
    @ManyToMany(fetch = FetchType.EAGER)
    @Valid
    @JoinTable(name = "question_status_transition_history",
                joinColumns = @JoinColumn(name = "question_id"),
                inverseJoinColumns = @JoinColumn(name = "status_transition_id"))
    @ForeignKey(name = "QUESTION_TRANSITION_FK",
                inverseName = "TRANSITION_QUESTION_FK")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @Cascade(value = {CascadeType.ALL, CascadeType.DELETE_ORPHAN })
    @OrderBy(value = "systemTransitionDate")
    public List<QuestionStatusTransition> getStatusTransitionHistory() {
        return statusTransitionHistory;
    }

    /**
     * @param statusTransitionHistory the status transitions.
     */
    public void setStatusTransitionHistory(List<QuestionStatusTransition> statusTransitionHistory) {
        this.statusTransitionHistory = statusTransitionHistory;
    }

    /**
     * {@inheritDoc}
     */
    @Transient
    public Date getStatusTransitionDate() {
        return statusTransitionDate;
    }

    /**
     * {@inheritDoc}
     */
    public void setStatusTransitionDate(Date statusTransitionDate) {
        this.statusTransitionDate = statusTransitionDate;
    }

    /**
     * Update the status transition history based on the current and previous status.
     */
    @OnSave
    public void updateStatusTransitionHistory() {
        new QuestionStatusTransitionHistoryHandler().updateStatusTransitionHistory(this);
    }

    /**
     * {@inheritDoc}
     */
    @NotNull
    @NotFuture
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_date")
    @Override
    public Date getCreatedDate() {
        return createdDate;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * {@inheritDoc}
     */
    @NotNull
    @NotFuture
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_updated_date")
    @Override
    public Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setLastUpdatedDate(Date lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    /**
     * {@inheritDoc}
     */
    public boolean isAccessible(TissueLocatorUser user) {
        if (getRequestor().getUsername().equals(user.getUsername())) {
            return true;
        }

        if (user.inRole(Role.QUESTION_RESPONDER)) {
            return true;
        }

        if (user.inRole(Role.QUESTION_ADMINISTRATOR)) {
            return true;
        }
        return false;
    }

    /**
     * @return a String listing the names of the institutions from which a response has been requested.
     */
    @Transient
    public String getRespondingInstitutionNames() {
        List<String> institutionNames = new ArrayList<String>();
        for (QuestionResponse response : getResponses()) {
            institutionNames.add(response.getInstitution().getName());
        }
        return StringUtils.trim(StringUtils.join(institutionNames, ", "));

    }

    /**
     * @return whether this question already has a response from an administrator.
     */
    @Transient
    public boolean hasAdminResponse() {
        for (QuestionResponse response : getResponses()) {
            if (response.getResponder() != null && response.getResponder().inRole(Role.QUESTION_ADMINISTRATOR)) {
                return true;
            }
        }
        return false;
    }
}

