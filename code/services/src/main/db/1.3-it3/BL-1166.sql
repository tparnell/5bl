alter table collection_protocol drop column contact_fax;
alter table institution drop column contact_fax;

alter table institution add column mta_contact_id int8;
alter table institution add constraint inst_mta_contact_fk foreign key (mta_contact_id) references person;
create index inst_mta_contact_idx on institution (mta_contact_id);

insert into address (id, line1, line2, city, state, zip, country, phone, fax) select id, 'line 1', null, 'city', 'state', 'zip', 'UNITED_STATES', mta_contact_phone, mta_contact_fax from institution where mta_contact_first_name is not null;
insert into person (id, first_name, last_name, email, address_id, organization_id) select id, mta_contact_first_name, mta_contact_last_name, mta_contact_email, id, id from institution where mta_contact_first_name is not null;
update institution set mta_contact_id = id where mta_contact_first_name is not null;

alter table institution drop column mta_contact_first_name;
alter table institution drop column mta_contact_last_name;
alter table institution drop column mta_contact_email;
alter table institution drop column mta_contact_phone;
alter table institution drop column mta_contact_fax;

insert into applicationrole (id, name) values (26, 'institutionMtaAdmin');
