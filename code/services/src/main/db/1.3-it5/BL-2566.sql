alter table status_transition rename to specimen_status_transition;
create table request_status_transition (id int8 not null, previous_status varchar(255) not null, new_status varchar(255) not null, transition_date timestamp not null, system_transition_date timestamp not null, primary key (id));
create table request_status_transition_history (request_id int8 not null, status_transition_id int8 not null, primary key (request_id, status_transition_id));
alter table request_status_transition_history add constraint REQUEST_TRANSITION_FK foreign key (request_id) references specimen_request;
alter table request_status_transition_history add constraint TRANSITION_REQUEST_FK foreign key (status_transition_id) references request_status_transition;
