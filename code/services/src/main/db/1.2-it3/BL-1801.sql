alter table aggregate_specimen_request_line_item add column disposition varchar(255);
alter table aggregate_specimen_request_line_item add column receipt_note varchar(254);
alter table aggregate_specimen_request_line_item add column receipt_quality varchar(255);
alter table aggregate_specimen_request_line_item add column received bool;