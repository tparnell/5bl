alter table shipment add column mta_amendment_lob_id int8 unique;
alter table shipment add column mta_amendment_content_type varchar(255);
alter table shipment add column mta_amendment_name varchar(255);
alter table shipment add column signed_sender_mta_id int8;
alter table shipment add column signed_recipient_mta_id int8;

alter table shipment add constraint MTA_AMENDMENT_FK foreign key (mta_amendment_lob_id) references lob_holder;
alter table shipment add constraint SENDER_MTA_FK foreign key (signed_sender_mta_id) references signed_material_transfer_agreement;
alter table shipment add constraint RECIPIENT_MTA_FK foreign key (signed_recipient_mta_id) references signed_material_transfer_agreement;

insert into applicationrole (id, name) values (25, 'signedMtaViewer');