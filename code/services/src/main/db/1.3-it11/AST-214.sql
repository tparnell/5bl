create table search_result_field_display_setting (
    id int8 not null,
    field_display_label varchar(80),
    field_name varchar(80) not null,
    field_description varchar(80),
    object_property_name varchar(80) not null,
    display_flag varchar(80) not null,
    category_id int8,
    primary key (id)
);
alter table search_result_field_display_setting 
add constraint FK_FIELD_CATEGORY_ID 
foreign key (category_id) references ui_search_field_category(id);
