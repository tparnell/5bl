/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.test;

import java.util.ArrayList;
import java.util.Date;

import com.fiveamsolutions.tissuelocator.data.CollectionProtocol;
import com.fiveamsolutions.tissuelocator.data.Ethnicity;
import com.fiveamsolutions.tissuelocator.data.Gender;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.Participant;
import com.fiveamsolutions.tissuelocator.data.Race;
import com.fiveamsolutions.tissuelocator.data.SpecimenClass;
import com.fiveamsolutions.tissuelocator.data.code.Code;
import com.fiveamsolutions.tissuelocator.data.code.SpecimenType;
import com.fiveamsolutions.tissuelocator.data.code.UseCountedCode;
import com.fiveamsolutions.tissuelocator.service.CodeServiceLocal;
import com.fiveamsolutions.tissuelocator.service.GenericServiceLocal;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;

/**
 * @author bpickeral
 *
 */
public class SpecimenPersistenceHelper {

    /** a common date. */
    public static final Date DATE = new Date();

    /**
     * create and store a participant.
     * @param i the institution for the participant
     * @param codeSuffix the suffix for the participant codes
     * @return a newly persisted participant
     */
    public static Participant createAndStoreParticipant(Institution i, int codeSuffix) {
        Participant participant = createParticipant(i, codeSuffix);
        GenericServiceLocal<Participant> partService = EjbTestHelper.getGenericServiceBean(Participant.class);
        Long partId = partService.savePersistentObject(participant);
        participant = partService.getPersistentObject(Participant.class, partId);
        return participant;
    }

    /**
     * create a participant.
     * @param i the institution for the participant
     * @param codeSuffix the suffix for the participant codes
     * @return a new  participant
     */
    public static Participant createParticipant(Institution i, int codeSuffix) {
        Participant participant = new Participant();
        participant.setEthnicity(Ethnicity.HISPANIC_OR_LATINO);
        participant.setRaces(new ArrayList<Race>());
        participant.getRaces().add(Race.WHITE);
        participant.getRaces().add(Race.BLACK_OR_AFRICAN_AMERICAN);
        participant.setExternalId("external id 123" + codeSuffix);
        participant.setExternalIdAssigner(i);
        participant.setGender(Gender.FEMALE);
        return participant;
    }

    /**
     * create and store a collection protocol.
     * @param i the institution for the collection protocol
     * @param codeSuffix the suffix for the collection protocol codes
     * @return a newly persisted collection protocol
     */
    public static CollectionProtocol createAndStoreProtocol(Institution i, int codeSuffix) {
        CollectionProtocol protocol = createProtocol(i, codeSuffix);
        GenericServiceLocal<CollectionProtocol> cpService =
            EjbTestHelper.getGenericServiceBean(CollectionProtocol.class);
        Long cpId = cpService.savePersistentObject(protocol);
        protocol = cpService.getPersistentObject(CollectionProtocol.class, cpId);
        return protocol;
    }

    /**
     * create and store a collection protocol.
     * @param i the institution for the collection protocol
     * @param codeSuffix the suffix for the collection protocol codes
     * @return a new collection protocol
     */
    public static CollectionProtocol createProtocol(Institution i, int codeSuffix) {
        CollectionProtocol protocol = new CollectionProtocol();
        protocol.setEndDate(DATE);
        protocol.setName("test name" + codeSuffix);
        protocol.setStartDate(DATE);
        protocol.setInstitution(i);
        protocol.setSpecimens(null);
        return protocol;
    }

    /**
     * create a code.
     * @param <T> the type of code to create
     * @param codeType the type of code to create
     * @param codeSuffix the suffix to append to the code name
     * @return a new code of a given type
     */
    public static <T extends Code> T createCode(Class<T> codeType, int codeSuffix) {
        CodeServiceLocal codeService = TissueLocatorRegistry.getServiceLocator().getCodeService();
        try {
            T code = codeType.newInstance();
            code.setName(generateCodeName(codeType, codeSuffix));
            code.setValue(code.getName());
            code.setActive(true);
            if (codeType.isAssignableFrom(SpecimenType.class)) {
                ((SpecimenType) code).setSpecimenClass(SpecimenClass.TISSUE);
            }
            if (UseCountedCode.class.isAssignableFrom(codeType)) {
                ((UseCountedCode) code).setUseCount(0L);
            }
            Long codeId = codeService.savePersistentObject(code);
            return codeService.getPersistentObject(codeType, codeId);
        } catch (IllegalAccessException e)  {
            return null;
        } catch (InstantiationException e) {
            return null;
        }
    }

    /**
     * generate the name for a code.
     * @param <T> the type of code to create
     * @param codeType the type of code to create
     * @param codeSuffix the suffix to append to the code name
     * @return a code name
     */
    public static <T extends Code> String generateCodeName(Class<T> codeType, int codeSuffix) {
        return codeType.getSimpleName() + "-active" + codeSuffix;
    }
}
