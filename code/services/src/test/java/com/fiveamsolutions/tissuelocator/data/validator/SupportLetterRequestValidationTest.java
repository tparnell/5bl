/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.data.validator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Iterator;

import org.junit.Test;

import com.fiveamsolutions.nci.commons.validator.ValidationError;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorFile;
import com.fiveamsolutions.tissuelocator.data.code.FundingSource;
import com.fiveamsolutions.tissuelocator.data.support.SupportLetterRequest;
import com.fiveamsolutions.tissuelocator.data.support.SupportLetterType;
import com.fiveamsolutions.tissuelocator.data.support.SupportRequestStatus;
import com.fiveamsolutions.tissuelocator.data.validation.support.SupportLetterRequestStatusValidator;
import com.fiveamsolutions.tissuelocator.data.validation.support.SupportLetterRequestTypeValidator;
import com.fiveamsolutions.tissuelocator.test.AbstractHibernateTestCase;

/**
 * @author ddasgupta
 *
 */
public class SupportLetterRequestValidationTest extends AbstractHibernateTestCase {

    /**
     * Tests support letter status validator.
     */
    @Test
    public void testStatusValidator() {
        SupportLetterRequestStatusValidator validator = new SupportLetterRequestStatusValidator();
        String responseFieldName = "response";
        String responseMessageKey = "{validator.supportLetterRequest.response.required}";
        String letterFieldName = "letter";
        String letterMessageKey = "{validator.supportLetterRequest.letter.required}";
        String offilineNotesFieldName = "offlineResponseNotes";
        String offlineNotesMessageKey = "{validator.supportLetterRequest.offlineNotes.required}";
        SupportLetterRequest request = new SupportLetterRequest();
        assertTrue(validator.isValid(request));

        request.setStatus(SupportRequestStatus.PENDING);
        assertNotNull(request.getStatus().getAdministratorResourceKey());
        assertNotNull(request.getStatus().getResearcherResourceKey());
        assertTrue(validator.isValid(request));

        request.setStatus(SupportRequestStatus.RESPONDED);
        assertFalse(validator.isValid(request));
        assertEquals(2, validator.getValidationErrors().size());
        Iterator<ValidationError> it = validator.getValidationErrors().iterator();
        ValidationError ve = it.next();
        assertEquals(responseFieldName, ve.getFieldName());
        assertEquals(responseMessageKey, ve.getErrorMessageKey());
        ve = it.next();
        assertEquals(letterFieldName, ve.getFieldName());
        assertEquals(letterMessageKey, ve.getErrorMessageKey());

        request.setResponse("response");
        assertFalse(validator.isValid(request));
        assertEquals(1, validator.getValidationErrors().size());
        ve = validator.getValidationErrors().iterator().next();
        assertEquals(letterFieldName, ve.getFieldName());
        assertEquals(letterMessageKey, ve.getErrorMessageKey());

        request.setResponse(null);
        request.setLetter(new TissueLocatorFile(new byte[]{}, "filename", "contenttype"));
        assertFalse(validator.isValid(request));
        ve = validator.getValidationErrors().iterator().next();
        assertEquals(responseFieldName, ve.getFieldName());
        assertEquals(responseMessageKey, ve.getErrorMessageKey());

        request.setResponse("response");
        assertTrue(validator.isValid(request));

        request.setResponse(null);
        request.setLetter(null);
        request.setStatus(SupportRequestStatus.RESPONDED_OFFLINE);
        assertFalse(validator.isValid(request));
        ve = validator.getValidationErrors().iterator().next();
        assertEquals(offilineNotesFieldName, ve.getFieldName());
        assertEquals(offlineNotesMessageKey, ve.getErrorMessageKey());

        request.setOfflineResponseNotes("offlineResponseNotes");
        assertTrue(validator.isValid(request));
        assertTrue(validator.isValid(new SpecimenRequest()));
    }

    /**
     * Tests support letter type validator.
     */
    @Test
    public void testTypeValidator() {
        SupportLetterRequestTypeValidator validator = new SupportLetterRequestTypeValidator();
        String grantTypeFieldName = "grantType";
        String grantTypeMessageKey = "{validator.supportLetterRequest.grantType.required}";
        String fundingSourcesFieldName = "fundingSources";
        String fundingSourcesMessageKey = "{validator.supportLetterRequest.fundingSources.required}";
        String otherTypeFieldName = "otherType";
        String otherTypeMessageKey = "{validator.supportLetterRequest.otherType.required}";
        SupportLetterRequest request = new SupportLetterRequest();
        assertTrue(validator.isValid(request));

        request.setType(SupportLetterType.IRB);
        assertNotNull(request.getType().getResourceKey());
        assertTrue(validator.isValid(request));

        request.setType(SupportLetterType.GRANT);
        assertFalse(validator.isValid(request));
        assertEquals(2, validator.getValidationErrors().size());
        Iterator<ValidationError> it = validator.getValidationErrors().iterator();
        ValidationError ve = it.next();
        assertEquals(grantTypeFieldName, ve.getFieldName());
        assertEquals(grantTypeMessageKey, ve.getErrorMessageKey());
        ve = it.next();
        assertEquals(fundingSourcesFieldName, ve.getFieldName());
        assertEquals(fundingSourcesMessageKey, ve.getErrorMessageKey());

        request.setGrantType("grantType");
        assertFalse(validator.isValid(request));
        assertEquals(1, validator.getValidationErrors().size());
        ve = validator.getValidationErrors().iterator().next();
        assertEquals(fundingSourcesFieldName, ve.getFieldName());
        assertEquals(fundingSourcesMessageKey, ve.getErrorMessageKey());

        request.setFundingSources(new ArrayList<FundingSource>());
        assertFalse(validator.isValid(request));
        assertEquals(1, validator.getValidationErrors().size());
        ve = validator.getValidationErrors().iterator().next();
        assertEquals(fundingSourcesFieldName, ve.getFieldName());
        assertEquals(fundingSourcesMessageKey, ve.getErrorMessageKey());

        request.setGrantType(null);
        request.getFundingSources().add(new FundingSource());
        request.setLetter(new TissueLocatorFile(new byte[]{}, "filename", "contenttype"));
        assertFalse(validator.isValid(request));
        ve = validator.getValidationErrors().iterator().next();
        assertEquals(grantTypeFieldName, ve.getFieldName());
        assertEquals(grantTypeMessageKey, ve.getErrorMessageKey());

        request.setGrantType("grantType");
        assertTrue(validator.isValid(request));

        request.setGrantType(null);
        request.setFundingSources(null);
        request.setType(SupportLetterType.OTHER);
        assertFalse(validator.isValid(request));
        ve = validator.getValidationErrors().iterator().next();
        assertEquals(otherTypeFieldName, ve.getFieldName());
        assertEquals(otherTypeMessageKey, ve.getErrorMessageKey());

        request.setOtherType("otherType");
        assertTrue(validator.isValid(request));
        assertTrue(validator.isValid(new SpecimenRequest()));
    }
}
