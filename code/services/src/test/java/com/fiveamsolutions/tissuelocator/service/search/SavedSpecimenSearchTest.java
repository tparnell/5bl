/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.service.search;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.fiveamsolutions.tissuelocator.data.Race;
import com.fiveamsolutions.tissuelocator.data.SavedSpecimenSearch;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.test.AbstractHibernateTestCase;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;

/**
 * Test case for saved specimen searches.
 *
 * @author smiller
 */
public class SavedSpecimenSearchTest extends AbstractHibernateTestCase {

    /**
     * Test saving and retrieving a search.
     *
     * @throws Exception on error
     */
    @Test
    public void testSaveAndUpdateSearch() throws Exception {
        Specimen example = new Specimen();
        example.setCollectionYear(1);
        Map<String, Collection<Object>> multiSelectCollectionParamValues = new HashMap<String, Collection<Object>>();
        List<Object> collectionVals = new ArrayList<Object>();
        collectionVals.add(Race.ASIAN);
        collectionVals.add(Race.WHITE);
        multiSelectCollectionParamValues.put("testCollectionField", collectionVals);
        Map<String, Collection<Object>> multiSelectParamValues = new HashMap<String, Collection<Object>>();
        List<Object> vals = new ArrayList<Object>();
        vals.add("testVal1");
        vals.add("testVal2");
        multiSelectParamValues.put("testField1", vals);
        List<SearchCondition> searchConditions = new ArrayList<SearchCondition>();
        RangeSearchCondition condition = new RangeSearchCondition();
        condition.setPropertyName("testField2");
        condition.getConditionParameters().put("param1", "paramValue1");
        searchConditions.add(new RangeSearchCondition());
        SpecimenSearchCriteria criteria = new SpecimenSearchCriteria(example, 
                new BigDecimal(1), multiSelectCollectionParamValues,
                multiSelectParamValues, searchConditions);
        SavedSpecimenSearch search = new SavedSpecimenSearch();
        search.setName("Test name");
        search.setOwner(getCurrentUser());

        SavedSpecimenSearchServiceBean bean = new SavedSpecimenSearchServiceBean();
        Long id = bean.persistSavedSpecimenSearch(search, criteria);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        SavedSpecimenSearch retrievedSearch = bean.getSavedSpecimenSearchAndData(id);
        assertEquals(id, retrievedSearch.getId());
        assertEquals(search.getName(), retrievedSearch.getName());
        SpecimenSearchCriteria retrievedCriteria = retrievedSearch.getSearchCriteria();
        assertEquals(example.getCollectionYear(), retrievedCriteria.getCriteria().getCollectionYear());
        assertEquals(multiSelectCollectionParamValues.size(), 
                retrievedCriteria.getMultiSelectCollectionParamValues().size());
        assertEquals(multiSelectParamValues.size(), retrievedCriteria.getMultiSelectParamValues().size());
        assertEquals(searchConditions.size(), retrievedCriteria.getSearchConditions().size());

        List<SavedSpecimenSearch> searches = bean.getSavedSpecimenSearchesByOwner(getCurrentUser());
        assertEquals(1, searches.size());
    }

}