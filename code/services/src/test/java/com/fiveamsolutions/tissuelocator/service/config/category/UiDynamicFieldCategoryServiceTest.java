/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.service.config.category;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.junit.Before;
import org.junit.Test;

import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.config.GenericFieldConfig;
import com.fiveamsolutions.tissuelocator.data.config.category.UiDynamicCategoryField;
import com.fiveamsolutions.tissuelocator.data.config.category.UiDynamicFieldCategory;
import com.fiveamsolutions.tissuelocator.test.AbstractHibernateTestCase;

/**
 * Dynamic field user interface category service integration tests.
 *
 * @author jstephens
 */
public class UiDynamicFieldCategoryServiceTest extends AbstractHibernateTestCase {

    private static final int CATEGORY_COUNT = 6;

    /**
     * Initialize categories.
     */
    @Before
    public void setUpTest() {
        saveUiDynamicFieldCategories();
    }

    /**
     * Test category persistence.
     */
    @Test
    public void testUiDynamicFieldCategoryPersistence() {
        UiDynamicFieldCategoryServiceLocal service = new UiDynamicFieldCategoryServiceBean();
        UiDynamicFieldCategory expectedCategory = new UiDynamicFieldCategory();
        expectedCategory.setCategoryName("Dynamic Field Category");
        expectedCategory.setViewable(Boolean.TRUE);
        expectedCategory.setEntityClassName(Object.class.getName());
        expectedCategory.setUiDynamicCategoryFields(createUiCategoryFields(1));
        expectedCategory.getUiDynamicCategoryFields().get(0).setUiDynamicFieldCategory(expectedCategory);
        Long id = service.savePersistentObject(expectedCategory);
        UiDynamicFieldCategory returnedCategory = service.getPersistentObject(UiDynamicFieldCategory.class, id);
        assertTrue(EqualsBuilder.reflectionEquals(expectedCategory, returnedCategory));
    }

    /**
     * Test category ordering.
     */
    @Test
    public void testGetUiDynamicFieldCategoriesReturnsCategoriesInOrder() {
        List<UiDynamicFieldCategory> expectedCategories = createUiDynamicFieldCategories(CATEGORY_COUNT);
        Map<String, UiDynamicFieldCategory> returnedCategories = getService().getUiDynamicFieldCategories(Class.class);
        Iterator<UiDynamicFieldCategory> it = returnedCategories.values().iterator();
        assertEquals(expectedCategories.get(0).getCategoryName(), it.next().getCategoryName());
        assertEquals(expectedCategories.get(1).getCategoryName(), it.next().getCategoryName());
        assertEquals(expectedCategories.get(2).getCategoryName(), it.next().getCategoryName());
    }

    /**
     * Test field ordering.
     */
    @Test
    public void testGetUiDynamicFieldCategoriesReturnsCategoryFieldsInOrder() {
        int count = CATEGORY_COUNT;
        List<UiDynamicFieldCategory> expectedCategories = createUiDynamicFieldCategories(count);
        Map<String, UiDynamicFieldCategory> returnedCategories = getService().getUiDynamicFieldCategories(Class.class);
        Iterator<UiDynamicFieldCategory> it = returnedCategories.values().iterator();
        for (int i = 0; i < count - 1; i++) {
            it.next();
        }
        List<UiDynamicCategoryField> expectedFields = expectedCategories.get(count - 1).getUiDynamicCategoryFields();
        List<UiDynamicCategoryField> returnedFields = it.next().getUiDynamicCategoryFields();
        for (int i = 0; i < count; i++) {
            assertEquals(expectedFields.get(i).getFieldConfig().getFieldName(), 
                    returnedFields.get(i).getFieldConfig().getFieldName());
            assertEquals(expectedFields.get(i).getFieldConfig().getFieldDisplayName(), 
                    returnedFields.get(i).getFieldConfig().getFieldDisplayName());
            assertEquals(expectedFields.get(i).getFieldType(), returnedFields.get(i).getFieldType());
            assertEquals(expectedFields.get(i).getDisplayableDeciderClass(),
                    returnedFields.get(i).getDisplayableDeciderClass());
        }
    }

    /**
     * Test the isFieldDisplayed method.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testIsFieldDisplayed() {
        UiDynamicCategoryField field = createUiCategoryFields(1).get(0);
        field.setDisplayableDeciderClass(null);
        Specimen s = new Specimen();
        assertTrue(field.isFieldDisplayed(s, null));
        field.setDisplayableDeciderClass(
                "com.fiveamsolutions.tissuelocator.data.config.category.SpecimenExternalIdDecider");
        assertFalse(field.isFieldDisplayed(s, null));
        field.setDisplayableDeciderClass("invalid");
        field.isFieldDisplayed(s, null);
    }

    private void saveUiDynamicFieldCategories() {
        for (UiDynamicFieldCategory uiDynamicFieldCategory : createUiDynamicFieldCategories(CATEGORY_COUNT)) {
            getService().savePersistentObject(uiDynamicFieldCategory);
        }
    }

    private UiDynamicFieldCategoryServiceLocal getService() {
        return new UiDynamicFieldCategoryServiceBean();
    }

    private List<UiDynamicCategoryField> createUiCategoryFields(int numberOfFields) {
        List<UiDynamicCategoryField> categoryFields = new ArrayList<UiDynamicCategoryField>();
        for (int count = 1; count <= numberOfFields; count++) {
            UiDynamicCategoryField categoryField = new UiDynamicCategoryField();
            categoryField.setFieldConfig(new GenericFieldConfig());
            categoryField.getFieldConfig().setFieldName("fieldName" + count);
            categoryField.getFieldConfig().setFieldDisplayName("displayName" + count);
            categoryField.setFieldType(UiDynamicCategoryField.FieldType
                    .values()[(count - 1) % UiDynamicCategoryField.FieldType.values().length]);
            categoryField.setDisplayableDeciderClass("decider" + count);
            categoryFields.add(categoryField);
        }

        return categoryFields;
    }

    private List<UiDynamicFieldCategory> createUiDynamicFieldCategories(int numberOfCategories) {
        List<UiDynamicFieldCategory> categories = new ArrayList<UiDynamicFieldCategory>();
        for (int count = 1; count <= numberOfCategories; count++) {
            UiDynamicFieldCategory category = new UiDynamicFieldCategory();
            category.setCategoryName("Dynamic Field Category " + count);
            category.setViewable(Boolean.FALSE);
            category.setEntityClassName(Class.class.getName());
            category.setUiDynamicCategoryFields(createUiCategoryFields(count));
            categories.add(category);
        }

        return categories;
    }
}
