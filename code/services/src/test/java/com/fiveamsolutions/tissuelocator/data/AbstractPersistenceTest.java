/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.data;

import static org.junit.Assert.assertEquals;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import org.junit.Test;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.tissuelocator.service.GenericServiceLocal;
import com.fiveamsolutions.tissuelocator.test.AbstractHibernateTestCase;
import com.fiveamsolutions.tissuelocator.test.EjbTestHelper;
import com.fiveamsolutions.tissuelocator.util.Email;

/**
 * Base class for simple persistence tests.
 * @author smiller
 *
 * @param <T> the class the test tries to save.
 */
public abstract class AbstractPersistenceTest<T extends PersistentObject> extends AbstractHibernateTestCase {

    /**
     * default page size.
     */
    public static final int PAGE_SIZE = 10;
    private final Class<T> typeArgument;

    /**
     * Default constructor.
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public AbstractPersistenceTest() {
        Type t = getClass().getGenericSuperclass();
        if (!(t instanceof ParameterizedType)) {
            t = getClass().getSuperclass().getGenericSuperclass();
        }
        ParameterizedType parameterizedType = (ParameterizedType) t;
        typeArgument = (Class) parameterizedType.getActualTypeArguments()[0];
    }

    /**
     * Method to get a valid object.
     * @return the valid object.
     */
    public abstract T[] getValidObjects();

    /**
     * Verify that the retrieval is correct.
     * @param expected the object retrieved by getValidObject() that was saved.
     * @param actual the object retrieved from the db.
     */
    public abstract void verifyObjectRetrieval(T expected, T actual);

    /**
     * Test saving and retrieving the object.
     */
    @Test
    public void testSaveRetrieve() {
        GenericServiceLocal<T> service = getService();
        T[] objects = getValidObjects();
        for (T object : objects) {
            Long id = service.savePersistentObject(object);
            T retrieved = service.getPersistentObject(typeArgument, id);
            verifyObjectRetrieval(object, retrieved);
        }
        List<T> allObjects = service.getAll(typeArgument);
        assertEquals((typeArgument == Institution.class) ? objects.length + 1 : objects.length, allObjects.size());
    }

    /**
     * @return the service
     */
    public GenericServiceLocal<T> getService() {
        return EjbTestHelper.getGenericServiceBean(typeArgument);
    }

    /**
     * get an email to send.
     * @param content the content of the email
     * @return an email with the given content and replacement place holders.
     */
    protected Email getEmail(String content) {
        Email email = new Email();
        email.setSubject(content + " {1} {2}");
        email.setHtml(content + " {1} {2}");
        email.setText(content + " {1} {2}");
        return email;
    }

    /**
     * get an email to send with no replacement place holders.
     * @param content the content of the email
     * @return an email with the given content
     */
    protected Email getEmailNoReplacements(String content) {
        Email email = new Email();
        email.setSubject(content);
        email.setHtml(content);
        email.setText(content);
        return email;
    }
}
