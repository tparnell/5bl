/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.xwork.time.DateUtils;
import org.junit.Test;
import org.jvnet.mock_javamail.Mailbox;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.nci.commons.util.MailUtils;
import com.fiveamsolutions.tissuelocator.data.mta.MaterialTransferAgreement;
import com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreement;
import com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreementStatus;
import com.fiveamsolutions.tissuelocator.service.GenericServiceLocal;
import com.fiveamsolutions.tissuelocator.service.MaterialTransferAgreementServiceLocal;
import com.fiveamsolutions.tissuelocator.test.InstitutionPersistenceHelper;
import com.fiveamsolutions.tissuelocator.util.EmailHelper;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;

/**
 * @author ddasgupta
 *
 */
public class MtaPersistenceTest extends AbstractPersistenceTest<MaterialTransferAgreement> {

    private static final int MTA_SUBMISSION_DEADLINE = 30;
    private Date version1;
    private Date version2;

    /**
     * {@inheritDoc}
     */
    @Override
    public MaterialTransferAgreement[] getValidObjects() {
        MaterialTransferAgreement[] mtas = new MaterialTransferAgreement[2];
        Calendar cal = Calendar.getInstance();

        MaterialTransferAgreement mta = new MaterialTransferAgreement();
        mta.setDocument(new TissueLocatorFile(new byte[] {1}, "mta1.txt", "text/plain"));
        mta.setUploadTime(cal.getTime());
        mta.setVersion(cal.getTime());
        mtas[0] = mta;
        version1 = cal.getTime();

        cal.add(Calendar.YEAR, 1);
        mta = new MaterialTransferAgreement();
        mta.setDocument(new TissueLocatorFile(new byte[] {2}, "mta2.txt", "text/plain"));
        mta.setUploadTime(cal.getTime());
        mta.setVersion(cal.getTime());
        mtas[1] = mta;
        version2 = cal.getTime();

        return mtas;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void verifyObjectRetrieval(MaterialTransferAgreement expected, MaterialTransferAgreement actual) {
        assertTrue(EqualsBuilder.reflectionEquals(expected, actual));
    }

    /**
     * test the save mta method on the service bean.
     * @throws Exception on error
     */
    @Test
    public void testSaveMta() throws Exception {
        MailUtils.setMailEnabled(true);
        GenericServiceLocal<PersistentObject> service = TissueLocatorRegistry.getServiceLocator().getGenericService();
        MaterialTransferAgreementServiceLocal mtaService =
            TissueLocatorRegistry.getServiceLocator().getMaterialTransferAgreementService();
        for (SignedMaterialTransferAgreement signedMta : new SignedMtaPersistenceTest().getValidObjects()) {
            service.savePersistentObject(signedMta);
        }

        TissueLocatorUser currentUser = getCurrentUser();
        InstitutionPersistenceHelper iph = InstitutionPersistenceHelper.getInstance();
        Institution i = iph.createInstitution(currentUser.getInstitution().getType(), "institution3", true);
        Long id = service.savePersistentObject(i);
        i = service.getPersistentObject(Institution.class, id);
        MaterialTransferAgreement mta1 = getValidObjects()[0];
        mta1.setVersion(new Date());
        Long mta1Id = mtaService.savePersistentObject(mta1);
        mta1 = service.getPersistentObject(MaterialTransferAgreement.class, mta1Id);
        SignedMaterialTransferAgreement smta = new SignedMaterialTransferAgreement();
        smta.setDocument(new TissueLocatorFile(new byte[] {1}, "mta1.txt", "text/plain"));
        smta.setSendingInstitution(i);
        smta.setOriginalMta(mta1);
        smta.setStatus(SignedMaterialTransferAgreementStatus.OUT_OF_DATE);
        smta.setUploadTime(new Date());
        smta.setUploader(currentUser);
        service.savePersistentObject(smta);

        Institution noContact = iph.createInstitution(i.getType(), "institution4", false);
        id = service.savePersistentObject(noContact);
        noContact = service.getPersistentObject(Institution.class, id);
        Institution withContact = iph.createInstitution(i.getType(), "institution5", false);
        id = service.savePersistentObject(withContact);
        withContact = service.getPersistentObject(Institution.class, id);
        withContact.setMtaContact(new Person());
        withContact.getMtaContact().setFirstName("first name");
        withContact.getMtaContact().setLastName("last name");
        withContact.getMtaContact().setEmail("mta_contact@example.com");
        withContact.getMtaContact().setOrganization(withContact);
        withContact.getMtaContact().setAddress(getAddress());
        id = service.savePersistentObject(withContact);
        withContact = service.getPersistentObject(Institution.class, id);
        TissueLocatorUser admin = createUserInRole(Role.SIGNED_MTA_VIEWER, i);

        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        MaterialTransferAgreement mta = new MaterialTransferAgreement();
        mta.setDocument(new TissueLocatorFile(new byte[] {1}, "mta1.txt", "text/plain"));
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, 1);
        mta.setUploadTime(cal.getTime());
        mta.setVersion(cal.getTime());
        String versionString = new EmailHelper().getDefaultDateFormat().format(cal.getTime());
        
        Long mtaId = mtaService.saveMaterialTransferAgreement(mta);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        mta = mtaService.getPersistentObject(MaterialTransferAgreement.class, mtaId);
        assertTrue(DateUtils.isSameDay(mta.getVersion(), cal.getTime()));
        int expectedYear = cal.get(Calendar.YEAR);
        cal.setTime(mta.getUploadTime());
        assertEquals(expectedYear, cal.get(Calendar.YEAR));

        for (SignedMaterialTransferAgreement signedMta : service.getAll(SignedMaterialTransferAgreement.class)) {
            assertEquals(SignedMaterialTransferAgreementStatus.OUT_OF_DATE, signedMta.getStatus());
        }

        Date deadline = DateUtils.addDays(new Date(), MTA_SUBMISSION_DEADLINE);
        String dateString = new SimpleDateFormat("MM/dd/yyyy").format(deadline);
        assertTrue(Mailbox.get(getCurrentUser().getEmail()).isEmpty());
        assertFalse(Mailbox.get(admin.getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(admin.getEmail()).size());
        testEmail(admin.getEmail(), "Update Your Institution's MTA Signature Page", 
                "version " + versionString, dateString,
                "Please take the following actions", "your institution should not ship out any biospecimens",
                "researchers from your institution will not receive biospecimens");
        assertFalse(Mailbox.get(withContact.getMtaContact().getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(withContact.getMtaContact().getEmail()).size());
        testEmail(withContact.getMtaContact().getEmail(), "Update Your Institution's MTA Signature Page",
                "version " + versionString, dateString, "Please take the following actions",
                "researchers from your institution will not receive biospecimens");
        Mailbox.clearAll();
        MailUtils.setMailEnabled(false);
    }

    /**
     * test the get all method on the service bean.
     */
    @Test
    public void testGetAll() {
        testSaveRetrieve();
        MaterialTransferAgreementServiceLocal mtaService =
            TissueLocatorRegistry.getServiceLocator().getMaterialTransferAgreementService();
        List<MaterialTransferAgreement> mtas = mtaService.getAll();
        assertEquals(2, mtas.size());
        assertEquals(version2, mtas.get(0).getVersion());
        assertEquals(version1, mtas.get(1).getVersion());
        assertTrue(mtas.get(0).getUploadTime().compareTo(mtas.get(1).getUploadTime()) > 0);
    }

    /**
     * test the get current method on the service bean.
     */
    @Test
    public void testGetCurrent() {
        MaterialTransferAgreementServiceLocal mtaService =
            TissueLocatorRegistry.getServiceLocator().getMaterialTransferAgreementService();
        assertNull(mtaService.getCurrentMta());
        testSaveRetrieve();
        MaterialTransferAgreement mta = mtaService.getCurrentMta();
        assertNotNull(mta);
        assertEquals(version2, mta.getVersion());
    }
}
