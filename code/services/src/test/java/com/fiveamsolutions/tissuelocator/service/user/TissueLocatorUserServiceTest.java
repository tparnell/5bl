/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.service.user;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.mail.Message;
import javax.mail.MessagingException;

import org.apache.commons.lang.time.DateUtils;
import org.hibernate.Session;
import org.hibernate.validator.InvalidStateException;
import org.junit.Before;
import org.junit.Test;
import org.jvnet.mock_javamail.Mailbox;

import com.fiveamsolutions.nci.commons.data.search.PageSortParams;
import com.fiveamsolutions.nci.commons.data.security.AbstractUser;
import com.fiveamsolutions.nci.commons.data.security.AccountStatus;
import com.fiveamsolutions.nci.commons.data.security.ApplicationRole;
import com.fiveamsolutions.nci.commons.data.security.Password;
import com.fiveamsolutions.nci.commons.data.security.PasswordReset;
import com.fiveamsolutions.nci.commons.data.security.PasswordType;
import com.fiveamsolutions.nci.commons.data.security.UserGroup;
import com.fiveamsolutions.nci.commons.search.SearchCriteria;
import com.fiveamsolutions.nci.commons.util.MailUtils;
import com.fiveamsolutions.nci.commons.util.SecurityUtils;
import com.fiveamsolutions.nci.commons.util.UsernameHolder;
import com.fiveamsolutions.tissuelocator.data.AccountStatusTransition;
import com.fiveamsolutions.tissuelocator.data.ApplicationSetting;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.InstitutionType;
import com.fiveamsolutions.tissuelocator.data.Person;
import com.fiveamsolutions.tissuelocator.data.Role;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.validation.ConsistentNamesValidator;
import com.fiveamsolutions.tissuelocator.service.ForgotPasswordStatus;
import com.fiveamsolutions.tissuelocator.service.GenericServiceLocal;
import com.fiveamsolutions.tissuelocator.service.search.TissueLocatorAnnotatedBeanSearchCriteria;
import com.fiveamsolutions.tissuelocator.service.search.TissueLocatorUserSortCriterion;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceBean;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceLocal;
import com.fiveamsolutions.tissuelocator.test.AbstractHibernateTestCase;
import com.fiveamsolutions.tissuelocator.test.EjbTestHelper;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;

/**
 * @author ddasgupta
 *
 */
public class TissueLocatorUserServiceTest extends AbstractHibernateTestCase {

    private static final String TEST_ROLE = "-testRole-";
    private static final String TEST_GROUP = "-testGroup-";

    private static final int RESET_COUNT = 10;
    private static final int RESET_EXPIRATION_DAYS = 5;
    private static final int THREE = 3;
    private static final String NEW_PASS = "abC123";

    private TissueLocatorUserServiceLocal userServiceToTest;

    /**
     * Create the user service for test.
     */
    @Before
    public void createServiceForTest() {
        userServiceToTest = getGuiceInjector().getInstance(TissueLocatorUserServiceBean.class);
    }

    /**
     * test get by username.
     */
    @Test
    public void testGetByUsername() {
        InstitutionType type = new InstitutionType();
        type.setName("test type");
        TissueLocatorHibernateUtil.getCurrentSession().save(type);
        Institution i = new Institution();
        i.setName("institute");
        i.setType(type);
        TissueLocatorHibernateUtil.getCurrentSession().save(i);

        TissueLocatorUser u = new TissueLocatorUser();
        u.setFirstName("first");
        u.setLastName("last");
        u.setEmail("email@test.com");
        u.setUsername("email@test.com");
        u.setAddress(getAddress());
        u.setTitle("title");
        u.setDegree("degree");
        u.setDepartment("department");
        u.setResearchArea("research area");
        Password p = new Password();
        p.setValue("Password1");
        p.setType(PasswordType.PLAINTEXT);
        u.setPassword(p);
        u.setInstitution(i);
        TissueLocatorHibernateUtil.getCurrentSession().save(u);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        TissueLocatorUserServiceBean service = EjbTestHelper.getTissueLocatorUserServiceBean();
        assertNull(service.getByUsername("does not exist"));
        verify("email@test.com", u);
        verify("EMAIL@TEST.COM", u);
        verify("eMaIl@TeSt.CoM", u);
    }

    private void verify(String username, TissueLocatorUser expected) {
        TissueLocatorUserServiceBean service = EjbTestHelper.getTissueLocatorUserServiceBean();
        TissueLocatorUser result = service.getByUsername(username);
        assertNotNull(result);
        assertEquals(expected.getFirstName(), result.getFirstName());
        assertEquals(expected.getLastName(), result.getLastName());
        assertEquals(expected.getEmail(), result.getEmail());
        assertEquals(expected.getUsername(), result.getUsername());
        assertEquals(expected.getName(), result.getName());
        assertEquals(0, result.compareTo(expected));
        assertFalse(service.performAlternateChecks(result, result));
    }

    /**
     * tests retrieving a tissue locator user object.
     * also tests new user registration can bypass standard security,
     * specifically that an inactive user can be saved without a logged in user
     */
    @Test
    public void testUserRetrieval() {
        GenericServiceLocal<InstitutionType> typeService = EjbTestHelper.getGenericServiceBean(InstitutionType.class);
        InstitutionType type = new InstitutionType();
        type.setName("test institute type");
        Long typeId = typeService.savePersistentObject(type);
        type = typeService.getPersistentObject(InstitutionType.class, typeId);

        Institution institution = new Institution();
        institution.setName("test institute");
        institution.setType(type);
        Long instId = (Long) TissueLocatorHibernateUtil.getCurrentSession().save(institution);
        Institution retrievedInstitution =
            EjbTestHelper.getGenericServiceBean(Institution.class).getPersistentObject(Institution.class, instId);

        TissueLocatorUser user = new TissueLocatorUser();
        user.setEmail("test@example.com");
        user.setUsername("test@example.com");
        user.setFirstName("first");
        user.setLastName("last");
        user.setStatus(AccountStatus.INACTIVE);
        user.setAddress(getAddress());
        user.setTitle("title");
        user.setDegree("degree");
        user.setDepartment("department");
        user.setResearchArea("research area");
        Password password = new Password();
        password.setType(PasswordType.PLAINTEXT);
        password.setValue("Password1");
        user.setPassword(password);
        user.setInstitution(retrievedInstitution);
        TissueLocatorUserServiceBean service = EjbTestHelper.getTissueLocatorUserServiceBean();
        assertTrue(service.performAlternateChecks(user, user));
        Long userId = service.savePersistentObject(user);
        TissueLocatorUser retrievedUser = service.getPersistentObject(TissueLocatorUser.class, userId);
        assertEquals(userId, retrievedUser.getId());
        assertEquals(user.getEmail(), retrievedUser.getEmail());
        assertEquals(user.getUsername(), retrievedUser.getUsername());
        assertEquals(user.getPassword().getValue(), retrievedUser.getPassword().getValue());
        assertFalse(service.performAlternateChecks(retrievedUser, user));
        assertNotNull(retrievedUser.getInstitution());
        Institution userInst = retrievedUser.getInstitution();
        assertEquals(instId, userInst.getId());
        assertEquals(institution.getName(), userInst.getName());
    }

    /**
     * Tests retrieval of users by role and institution.
     */
    @Test
    public void testGetByRoleAndInstitution() {
        TissueLocatorUserServiceBean service = EjbTestHelper.getTissueLocatorUserServiceBean();

        InstitutionType type = new InstitutionType();
        type.setName("role test");
        TissueLocatorHibernateUtil.getCurrentSession().save(type);
        Institution i = createInstitution("role test", type);

        TissueLocatorUser u = createUser("roletest", i);

        ApplicationRole role = new ApplicationRole();
        role.setName("roleTest");
        TissueLocatorHibernateUtil.getCurrentSession().save(role);
        UserGroup group = new UserGroup();
        group.setName("groupTest");
        TissueLocatorHibernateUtil.getCurrentSession().save(group);

        TissueLocatorHibernateUtil.getCurrentSession().save(u);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        u = (TissueLocatorUser)
        TissueLocatorHibernateUtil.getCurrentSession().load(TissueLocatorUser.class, u.getId());

        u.getGroups().add(group);
        TissueLocatorHibernateUtil.getCurrentSession().save(u);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        role = (ApplicationRole)
            TissueLocatorHibernateUtil.getCurrentSession().load(ApplicationRole.class, role.getId());
        group = (UserGroup)
            TissueLocatorHibernateUtil.getCurrentSession().load(UserGroup.class, group.getId());
        group.getRoles().add(role);
        TissueLocatorHibernateUtil.getCurrentSession().save(group);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        u = (TissueLocatorUser)
            TissueLocatorHibernateUtil.getCurrentSession().load(TissueLocatorUser.class, u.getId());

        Set<TissueLocatorUser> users = service.getByRoleAndInstitution("roleTest", i);
        assertEquals(users.size(), 1);
        TissueLocatorUser result = users.iterator().next();
        assertEquals(u.getFirstName(), result.getFirstName());
        assertEquals(u.getLastName(), result.getLastName());
        assertEquals(u.getEmail(), result.getEmail());
        assertEquals(u.getUsername(), result.getUsername());
        assertEquals(u.getName(), result.getName());
    }

    /**
     *
     */
    @Test
    public void testGetByRoleAndInstitutions() {
        TissueLocatorUserServiceBean service = EjbTestHelper.getTissueLocatorUserServiceBean();

        InstitutionType type = new InstitutionType();
        type.setName("role test");
        TissueLocatorHibernateUtil.getCurrentSession().save(type);

        List<Institution> institutions = new ArrayList<Institution>();
        for (int i = 0; i < THREE; i++) {
            institutions.add(createInstitution("roletest" + i, type));
        }

        TissueLocatorUser u1 = createUser("roletest1", institutions.get(0));
        TissueLocatorUser u2 = createUser("roletest2", institutions.get(0));
        TissueLocatorUser u3 = createUser("roletest3", institutions.get(1));

        ApplicationRole role1 = new ApplicationRole();
        role1.setName("roleTest1");
        TissueLocatorHibernateUtil.getCurrentSession().save(role1);
        UserGroup group1 = new UserGroup();
        group1.setName("groupTest1");
        TissueLocatorHibernateUtil.getCurrentSession().save(group1);
        ApplicationRole role2 = new ApplicationRole();
        role2.setName("roleTest2");
        TissueLocatorHibernateUtil.getCurrentSession().save(role2);
        UserGroup group2 = new UserGroup();
        group2.setName("groupTest2");
        TissueLocatorHibernateUtil.getCurrentSession().save(group2);

        role1 = (ApplicationRole)
        TissueLocatorHibernateUtil.getCurrentSession().load(ApplicationRole.class, role1.getId());
        group1 = (UserGroup)
        TissueLocatorHibernateUtil.getCurrentSession().load(UserGroup.class, group1.getId());
        group1.getRoles().add(role1);
        TissueLocatorHibernateUtil.getCurrentSession().save(group1);
        role2 = (ApplicationRole)
        TissueLocatorHibernateUtil.getCurrentSession().load(ApplicationRole.class, role2.getId());
        group2 = (UserGroup)
        TissueLocatorHibernateUtil.getCurrentSession().load(UserGroup.class, group2.getId());
        group2.getRoles().add(role2);
        TissueLocatorHibernateUtil.getCurrentSession().save(group2);

        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        Session s = TissueLocatorHibernateUtil.getCurrentSession();
        u1.getGroups().add(group1);
        u1.setInstitution((Institution) s.load(Institution.class, u1.getInstitution().getId()));
        u2.getGroups().add(group2);
        u2.setInstitution((Institution) s.load(Institution.class, u2.getInstitution().getId()));
        u3.getGroups().add(group2);
        u3.setInstitution((Institution) s.load(Institution.class, u3.getInstitution().getId()));
        s.save(u1);
        s.save(u2);
        s.save(u3);

        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        Set<Institution> searchInst = new HashSet<Institution>();
        searchInst.add(institutions.get(0));
        Set<TissueLocatorUser> users = service.getByRoleAndInstitutions("roleTest1", searchInst);
        assertEquals(1, users.size());
        Iterator<TissueLocatorUser> iter = users.iterator();
        TissueLocatorUser result = iter.next();
        assertEquals(u1.getId(), result.getId());

        users = service.getByRoleAndInstitutions("roleTest2", searchInst);
        assertEquals(1, users.size());
        iter = users.iterator();
        result = iter.next();
        assertEquals(u2.getId(), result.getId());

        searchInst.add(institutions.get(1));
        users = service.getByRoleAndInstitutions("roleTest1", searchInst);
        assertEquals(1, users.size());
        iter = users.iterator();
        result = iter.next();
        assertEquals(u1.getId(), result.getId());

        users = service.getByRoleAndInstitutions("roleTest2", searchInst);
        assertEquals(2, users.size());
        iter = users.iterator();
        result = iter.next();
        assertEquals(u2.getId(), result.getId());
        result = iter.next();
        assertEquals(u3.getId(), result.getId());

        searchInst.add(institutions.get(2));
        users = service.getByRoleAndInstitutions("roleTest2", searchInst);
        assertEquals(2, users.size());
        iter = users.iterator();
        result = iter.next();
        assertEquals(u2.getId(), result.getId());
        result = iter.next();
        assertEquals(u3.getId(), result.getId());

        searchInst.clear();
        searchInst.add(institutions.get(2));
        users = service.getByRoleAndInstitutions("roleTest2", searchInst);
        assertEquals(0, users.size());
    }

    private Institution createInstitution(String name, InstitutionType type) {
        Institution i = new Institution();
        i.setName(name);
        i.setType(type);
        TissueLocatorHibernateUtil.getCurrentSession().save(i);
        return i;
    }

    private TissueLocatorUser createUser(String name, Institution i) {
        TissueLocatorUser u = new TissueLocatorUser();
        u.setFirstName(name);
        u.setLastName("test");
        u.setEmail(name + "@test.com");
        u.setUsername(name + "@test.com");
        u.setAddress(getAddress());
        u.setTitle("title");
        u.setDegree("degree");
        u.setDepartment("department");
        u.setResearchArea("research area");
        Password p = new Password();
        p.setValue("Password1");
        p.setType(PasswordType.PLAINTEXT);
        u.setPassword(p);
        u.setInstitution(i);
        return u;
    }

    /**
     * Tests the unique user validator.
     */
    @Test
    public void testUniqueTissueLocatorUserValidator() {
        createUserSkipRole();
        try {
            createUserSkipRole();
        } catch (InvalidStateException e) {
            assertTrue(e.getInvalidValues()[0].getMessage().contains("already exists"));
        }
    }

    /**
     * Tests the consistent names validator.
     */
    @Test
    public void testConsistentNamesValidator() {
        ConsistentNamesValidator validator = new ConsistentNamesValidator();
        validator.initialize(null);

        TissueLocatorUser u = createUserSkipRole();
        assertTrue(validator.isValid(u));

        u.setUsername("something else");
        assertFalse(validator.isValid(u));
    }

    /**
     * Tests the searchable framework as applied to the user object.
     */
    @Test
    public void testSearch() {
        TissueLocatorUser saved = getCurrentUser();
        TissueLocatorUser example = new TissueLocatorUser();
        SearchCriteria<TissueLocatorUser> criteria =
            new TissueLocatorAnnotatedBeanSearchCriteria<TissueLocatorUser>(example);
        TissueLocatorUserServiceBean service = EjbTestHelper.getTissueLocatorUserServiceBean();
        assertEquals(1, service.count(criteria));
        assertEquals(1, service.search(criteria).size());
        assertEquals(saved.getId(), service.search(criteria).get(0).getId());

        for (TissueLocatorUserSortCriterion sort : TissueLocatorUserSortCriterion.values()) {
            PageSortParams<TissueLocatorUser> pageSortParams =
                new PageSortParams<TissueLocatorUser>(1, 0, sort, false);
            assertEquals(1, service.search(criteria, pageSortParams).size());
            assertEquals(saved.getId(), service.search(criteria, pageSortParams).get(0).getId());
        }

        example.setStatus(AccountStatus.INACTIVE);
        assertEquals(0, service.count(criteria));
        assertEquals(0, service.search(criteria).size());

        SearchCriteria<Institution> crit = new TissueLocatorAnnotatedBeanSearchCriteria<Institution>(new Institution());
        assertTrue(crit.hasOneCriterionSpecified());
        SearchCriteria<Person> cpCrit = new TissueLocatorAnnotatedBeanSearchCriteria<Person>(new Person());
        assertFalse(cpCrit.hasOneCriterionSpecified());
    }

    private void createAccountApprovalSetting(boolean active) {
        ApplicationSettingServiceLocal asService = new ApplicationSettingServiceBean();
        ApplicationSetting as = new ApplicationSetting();
        as.setName("account_approval_active");
        as.setValue(String.valueOf(active));
        asService.savePersistentObject(as);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
    }

    /**
     * Tests the save user method, which also sends email, for configurations without account approval.
     * @throws MessagingException on error sending mail
     * @throws IOException on error
     */
    @Test
    public void testSaveUserNoApproval() throws MessagingException, IOException {
        createAccountApprovalSetting(false);
        TissueLocatorUser saved = getCurrentUser();
        TissueLocatorUser approver = createUserInRole(Role.USER_ACCOUNT_APPROVER, saved.getInstitution());
        TissueLocatorUserServiceBean service = EjbTestHelper.getTissueLocatorUserServiceBeanWithMockLoad();
        service.setApplicationSettingService(new ApplicationSettingServiceBean());
        MailUtils.setMailEnabled(true);

        Long id = service.saveUser(saved, false, "Password1");
        assertTrue(Mailbox.get(approver.getEmail()).isEmpty());
        Mailbox inbox = Mailbox.get(saved.getEmail());
        assertEquals(0, inbox.size());
        Mailbox.clearAll();
        TissueLocatorUserServiceLocal regularService = EjbTestHelper.getTissueLocatorUserServiceBean();
        TissueLocatorUser retrieved = regularService.getPersistentObject(TissueLocatorUser.class, id);
        assertEquals(id, retrieved.getId());
        assertEquals(saved.getUsername(), retrieved.getUsername());
        assertNotNull(retrieved.getPasswordResets());
        assertTrue(retrieved.getPasswordResets().isEmpty());
        assertEquals(retrieved.getStatus(), AccountStatus.ACTIVE);
        assertTrue(retrieved.getGroups().isEmpty());

        Institution i = getInstitution();
        TissueLocatorUser u1 = getUser("u1@example.com", i);
        TissueLocatorUser u2 = getUser("u2@example.com", i);

        id = service.saveUser(u1, true, "Password1");
        assertTrue(Mailbox.get(approver.getEmail()).isEmpty());
        inbox = Mailbox.get(u1.getEmail());
        assertEquals(1, inbox.size());
        testEmail(u1.getEmail(), "Account has been Created", "has been created for you");
        retrieved = regularService.getPersistentObject(TissueLocatorUser.class, id);
        assertEquals(id, retrieved.getId());
        assertEquals(u1.getUsername(), retrieved.getUsername());
        assertNotNull(retrieved.getPasswordResets());
        assertEquals(1, retrieved.getPasswordResets().size());
        assertEquals(retrieved.getStatus(), AccountStatus.ACTIVE);
        assertTrue(retrieved.getGroups().isEmpty());

        UsernameHolder.setUser(null);
        id = service.saveUser(u2, false, "Password1");
        assertTrue(Mailbox.get(approver.getEmail()).isEmpty());
        inbox = Mailbox.get(u2.getEmail());
        assertEquals(1, inbox.size());
        testEmail(u2.getEmail(), "Your Registration Request has been received",
                "has been successfully created");
        retrieved = regularService.getPersistentObject(TissueLocatorUser.class, id);
        assertEquals(id, retrieved.getId());
        assertEquals(u2.getUsername(), retrieved.getUsername());
        assertNotNull(retrieved.getPasswordResets());
        assertTrue(retrieved.getPasswordResets().isEmpty());
        assertEquals(retrieved.getStatus(), AccountStatus.ACTIVE);
        assertFalse(retrieved.getGroups().isEmpty());
        assertEquals(1, retrieved.getGroups().size());

        Mailbox.clearAll();
        MailUtils.setMailEnabled(false);
    }

    /**
     * Tests the save user method, which also sends email, for configurations without account approval.
     * @throws MessagingException on error sending mail
     * @throws IOException on error
     */
    @Test
    public void testSaveUserWithApproval() throws MessagingException, IOException {
        createAccountApprovalSetting(true);
        TissueLocatorUser saved = getCurrentUser();
        TissueLocatorUser approver = createUserInRole(Role.USER_ACCOUNT_APPROVER, saved.getInstitution());
        TissueLocatorUserServiceBean service = EjbTestHelper.getTissueLocatorUserServiceBeanWithMockLoad();
        service.setApplicationSettingService(new ApplicationSettingServiceBean());
        MailUtils.setMailEnabled(true);

        Institution i = getInstitution();
        TissueLocatorUser u = getUser("u2@example.com", i);

        UsernameHolder.setUser(null);
        Long id = service.saveUser(u, false, "Password1");
        assertFalse(Mailbox.get(approver.getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(approver.getEmail()).size());
        testEmail(approver.getEmail(), "Registration Request Received",
                "A new registration request has been received");
        Mailbox inbox = Mailbox.get(u.getEmail());
        assertEquals(1, inbox.size());
        testEmail(u.getEmail(), "Registration Request has been received",
                "review the details of your request and make a decision on account approval");
        TissueLocatorUser retrieved = userServiceToTest.getPersistentObject(TissueLocatorUser.class, id);
        assertEquals(id, retrieved.getId());
        assertEquals(u.getUsername(), retrieved.getUsername());
        assertNotNull(retrieved.getPasswordResets());
        assertTrue(retrieved.getPasswordResets().isEmpty());
        assertEquals(retrieved.getStatus(), AccountStatus.PENDING);
        assertFalse(retrieved.getGroups().isEmpty());
        assertEquals(1, retrieved.getGroups().size());

        Mailbox.clearAll();
        MailUtils.setMailEnabled(false);
    }

    private Institution getInstitution() {
        InstitutionType type = new InstitutionType();
        type.setName("test type");
        TissueLocatorHibernateUtil.getCurrentSession().save(type);
        Institution i = new Institution();
        i.setName("institute");
        i.setType(type);
        TissueLocatorHibernateUtil.getCurrentSession().save(i);
        return i;
    }
    private TissueLocatorUser getUser(String username, Institution institution) {
        TissueLocatorUser u = new TissueLocatorUser();
        u.setFirstName("first");
        u.setLastName("last");
        u.setEmail(username);
        u.setUsername(username);
        u.setAddress(getAddress());
        u.setTitle("title");
        u.setDegree("degree");
        u.setDepartment("department");
        u.setResearchArea("research area");
        Password p = new Password();
        p.setValue("Password1");
        p.setType(PasswordType.PLAINTEXT);
        u.setPassword(p);
        u.setInstitution(institution);
        return u;
    }


    /**
     * test group assignment checks.
     */
    @Test(expected = IllegalStateException.class)
    public void testIllegalGroupAssignment() {
        GenericServiceLocal<InstitutionType> typeService = EjbTestHelper.getGenericServiceBean(InstitutionType.class);
        InstitutionType type = new InstitutionType();
        type.setName("test institute type");
        Long typeId = typeService.savePersistentObject(type);
        type = typeService.getPersistentObject(InstitutionType.class, typeId);

        Institution institution = new Institution();
        institution.setName("test institute");
        institution.setType(type);
        Long instId = (Long) TissueLocatorHibernateUtil.getCurrentSession().save(institution);
        Institution retrievedInstitution =
            EjbTestHelper.getGenericServiceBean(Institution.class).getPersistentObject(Institution.class, instId);

        TissueLocatorUser user = new TissueLocatorUser();
        user.setEmail("test@example.com");
        user.setUsername("test@example.com");
        user.setFirstName("first");
        user.setLastName("last");
        user.setStatus(AccountStatus.INACTIVE);
        user.setAddress(getAddress());
        user.setTitle("title");
        user.setDegree("degree");
        user.setDepartment("department");
        user.setResearchArea("research area");
        Password password = new Password();
        password.setType(PasswordType.PLAINTEXT);
        password.setValue("Password1");
        user.setPassword(password);
        user.setInstitution(retrievedInstitution);

        UserGroup group = new UserGroup();
        group.setName("testGroup");

        ApplicationRole role = new ApplicationRole();
        role.setName("testRole");

        user.getGroups().add(group);

        TissueLocatorUserServiceBean service = EjbTestHelper.getTissueLocatorUserServiceBean();
        service.savePersistentObject(user);
    }

    /**
     * test group assignment checks.
     */
    @Test
    public void testGetAdministratableGroups() {
        TissueLocatorUserServiceLocal service = EjbTestHelper.getTissueLocatorUserServiceBeanWithMockLoad();

        TissueLocatorUser user = new TissueLocatorUser();
        assertEquals(0, service.getAdministratableGroups(user).size());

        long id = 1L;
        UserGroup g = new UserGroup();
        g.setId(id);
        g.setName(g.getId() + "group");
        user.getGroups().add(g);
        assertEquals(0, service.getAdministratableGroups(user).size());

        id++;
        g = new UserGroup();
        g.setId(id);
        g.setName(g.getId() + "group");
        user.getGroups().add(g);
        assertEquals(0, service.getAdministratableGroups(user).size());

        id++;
        g = new UserGroup();
        g.setId(id);
        g.setName(g.getId() + "group");
        user.getGroups().add(g);
        assertEquals(0, service.getAdministratableGroups(user).size());

        id++;
        g = new UserGroup();
        g.setId(id);
        g.setName(g.getId() + "group");
        user.getGroups().add(g);
        assertEquals(id, service.getAdministratableGroups(user).size());

        id++;
        g = new UserGroup();
        g.setId(id);
        g.setName(g.getId() + "group");
        user.getGroups().add(g);
        assertEquals(id, service.getAdministratableGroups(user).size());

        id++;
        g = new UserGroup();
        g.setId(id);
        g.setName(g.getId() + "group");
        user.getGroups().add(g);
        assertEquals(id, service.getAdministratableGroups(user).size());

        user.getGroups().clear();
        user.getGroups().add(g);
        assertEquals(id, service.getAdministratableGroups(user).size());
    }

    /**
     * Test the get user in role function.
     */
    @Test
    public void testGetUserInRole() {
        // init data
        TissueLocatorUserServiceBean service = EjbTestHelper.getTissueLocatorUserServiceBean();
        TissueLocatorUser user1 = createUserSkipRole();
        TissueLocatorUser user2 = createUserSkipRole();
        ApplicationRole role = new ApplicationRole();
        role.setName(TEST_ROLE);
        TissueLocatorHibernateUtil.getCurrentSession().save(role);
        UserGroup group = new UserGroup();
        group.setName(TEST_GROUP);
        TissueLocatorHibernateUtil.getCurrentSession().save(group);

        // reload data
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        user1 = (TissueLocatorUser)
            TissueLocatorHibernateUtil.getCurrentSession().load(TissueLocatorUser.class, user1.getId());
        user2 = (TissueLocatorUser)
            TissueLocatorHibernateUtil.getCurrentSession().load(TissueLocatorUser.class, user2.getId());
        role = (ApplicationRole)
            TissueLocatorHibernateUtil.getCurrentSession().load(ApplicationRole.class, role.getId());
        group = (UserGroup)
            TissueLocatorHibernateUtil.getCurrentSession().load(UserGroup.class, group.getId());
        assertEquals(0, service.getUsersInRole(TEST_ROLE).size());

        // add user to group
        user1.getGroups().add(group);
        TissueLocatorHibernateUtil.getCurrentSession().save(user1);
        assertEquals(0, service.getUsersInRole(TEST_ROLE).size());

        // add role to group
        group.getRoles().add(role);
        TissueLocatorHibernateUtil.getCurrentSession().save(group);
        assertEquals(1, service.getUsersInRole(TEST_ROLE).size());

        // add role to user
        user2.getRoles().add(role);
        TissueLocatorHibernateUtil.getCurrentSession().save(user2);
        assertEquals(2, service.getUsersInRole(TEST_ROLE).size());

        // add user to group
        user2.getGroups().add(group);
        TissueLocatorHibernateUtil.getCurrentSession().save(user2);
        assertEquals(2, service.getUsersInRole(TEST_ROLE).size());

        // add role to user
        user1.getRoles().add(role);
        TissueLocatorHibernateUtil.getCurrentSession().save(user1);
        assertEquals(2, service.getUsersInRole(TEST_ROLE).size());

        // remove role from group
        group.getRoles().clear();
        TissueLocatorHibernateUtil.getCurrentSession().save(group);
        assertEquals(2, service.getUsersInRole(TEST_ROLE).size());

        // remove role from user
        user2.getRoles().clear();
        TissueLocatorHibernateUtil.getCurrentSession().save(user2);
        assertEquals(1, service.getUsersInRole(TEST_ROLE).size());

        // remove role from user
        user1.getRoles().clear();
        TissueLocatorHibernateUtil.getCurrentSession().save(user1);
        assertEquals(0, service.getUsersInRole(TEST_ROLE).size());

        // add role to user
        user1.getRoles().add(role);
        TissueLocatorHibernateUtil.getCurrentSession().save(user1);
        assertEquals(1, service.getUsersInRole(TEST_ROLE).size());

        // inactivate user.
        user1.setStatus(AccountStatus.INACTIVE);
        user1.setStatusTransitionComment("test");
        TissueLocatorHibernateUtil.getCurrentSession().save(user1);
        assertEquals(0, service.getUsersInRole(TEST_ROLE).size());
    }

    /**
     * tests the forgot password method.
     * @throws Exception on error sending and reading mail
     */
    @Test
    public void testForgotPassword() throws Exception {
        MailUtils.setMailEnabled(true);
        TissueLocatorUserServiceBean service = EjbTestHelper.getTissueLocatorUserServiceBean();

        assertEquals(ForgotPasswordStatus.UNKNOWN_USERNAME, service.forgotPassword(null));
        assertEquals(ForgotPasswordStatus.UNKNOWN_USERNAME, service.forgotPassword("idontexist"));

        TissueLocatorUser u = createUserSkipRole();
        assertTrue(u.getPasswordResets().isEmpty());

        for (int i = 0; i < RESET_COUNT; ++i) {
            assertEquals(ForgotPasswordStatus.SUCCESS, service.forgotPassword(u.getUsername()));
            int size = u.getPasswordResets().size();
            assertEquals(i + 1, size);

            Set<String> currentNonces = new HashSet<String>();
            for (PasswordReset pr : u.getPasswordResets()) {
                assertTrue(currentNonces.add(pr.getNonce())); // make sure each element is unique
            }
        }
        assertEquals(ForgotPasswordStatus.TOO_MANY_REQUESTS, service.forgotPassword(u.getUsername()));

        // validate mail sending
        List<Message> inbox = Mailbox.get(u.getEmail());
        assertEquals(RESET_COUNT, inbox.size());
        for (int i = 0; i < RESET_COUNT; ++i) {
            testEmail(u.getEmail(), "Password Reset Request",
                    "reset your password");
        }

        // hack a bit to create an older change request. verify it gets deleted.
        PasswordReset pr = PasswordReset.newInstance(u);
        Method m = pr.getClass().getDeclaredMethod("setCreateDate", Date.class);
        m.setAccessible(true);
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, -RESET_EXPIRATION_DAYS);
        m.invoke(pr, c.getTime());
        TissueLocatorHibernateUtil.getCurrentSession().save(pr);
        assertEquals(ForgotPasswordStatus.TOO_MANY_REQUESTS, service.forgotPassword(u.getUsername()));
        assertEquals(RESET_COUNT, u.getPasswordResets().size());

        MailUtils.setMailEnabled(false);
        u = service.getByUsername(u.getUsername());
        u.getPasswordResets().clear();
        service.savePersistentObject(u);
        Mailbox.clearAll();
        assertEquals(ForgotPasswordStatus.SUCCESS, service.forgotPassword(u.getUsername()));
        inbox = Mailbox.get(u.getEmail());
        assertEquals(0, inbox.size());

        u = service.getByUsername(u.getUsername());
        u.setStatus(AccountStatus.INACTIVE);
        u.setStatusTransitionComment("test");
        service.savePersistentObject(u);
        assertEquals(ForgotPasswordStatus.INACTIVE_USER, service.forgotPassword(u.getUsername()));
        inbox = Mailbox.get(u.getEmail());
        assertEquals(0, inbox.size());
    }

    /**
     * Tests the update password method.
     * @throws Exception on error sending and viewing mail
     */
    @Test
    public void testUpdatePassword() throws Exception {
        TissueLocatorUserServiceBean service = EjbTestHelper.getTissueLocatorUserServiceBean();
        TissueLocatorUser u = createUserSkipRole();
        PasswordReset pr = PasswordReset.newInstance(u);
        service.savePersistentObject(u);

        try {
            service.updatePassword(null, NEW_PASS, pr.getNonce());
            fail("Invalid user update successful?");
        } catch (Exception e) {
            // expected
        }

        try {
            service.updatePassword(u, null, pr.getNonce());
            fail("Null password update successful?");
        } catch (Exception e) {
            // expected
        }

        try {
            service.updatePassword(u, "bogus", pr.getNonce());
            fail("Invalid new password update successful?");
        } catch (Exception e) {
            // expected
        }

        try {
            service.updatePassword(u, NEW_PASS, pr.getNonce() + "invalid");
            fail("Invalid nonce update successful?");
        } catch (Exception e) {
            // expected
        }

        try {
            service.updatePassword(u, NEW_PASS, null);
            fail("no nonce update successful?");
        } catch (Exception e) {
            // expected
        }

        assertFalse(u.getPasswordResets().isEmpty());
        MailUtils.setMailEnabled(true);
        service.updatePassword(u, NEW_PASS, pr.getNonce());
        assertTrue(SecurityUtils.matches(u.getPassword(), NEW_PASS));
        assertTrue(u.getPasswordResets().isEmpty());

        List<Message> inbox = Mailbox.get(u.getEmail());
        assertEquals(1, inbox.size());
        testEmail(u.getEmail(), "password has been changed", "You have successfully changed your password");

        Mailbox.clearAll();
        MailUtils.setMailEnabled(false);
    }

    /**
     * test for custom properties.
     */
    @Test
    public void testCustomProperties() {
        TissueLocatorUser user = new TissueLocatorUser();
        assertNotNull(user.getCustomProperties());
        String field = "field";
        assertNull(user.getCustomProperty(field));
        user.setCustomProperty(field, "value");
        assertNotNull(user.getCustomProperty(field));
        user.setCustomProperties(null);
        assertNull(user.getCustomProperties());
     }

    /**
     * When previous status is pending and status is active the user registration has
     * been approved. An email should be sent notifying the user.
     *
     * @throws InvocationTargetException if the underlying method throws an exception.
     * @throws IllegalAccessException if the underlying method is inaccessible.
     * @throws NoSuchMethodException if a matching method is not found.
     * @throws MessagingException if an error occurs while sending email.
     * @throws IOException if an input/output operation fails or is interrupted.
     */
    @Test
    public void approvingRegistrationSendsApprovalEmail() throws NoSuchMethodException,
            IllegalAccessException, InvocationTargetException, MessagingException,
            IOException {
        TissueLocatorUserServiceBean service = EjbTestHelper.getTissueLocatorUserServiceBeanWithMockLoad();
        createAccountApprovalSetting(true);
        service.setApplicationSettingService(new ApplicationSettingServiceBean());
        MailUtils.setMailEnabled(true);

        // create and save current user
        TissueLocatorUser currentUser = getCurrentUser();
        createUserInRole(Role.USER_ACCOUNT_APPROVER, currentUser.getInstitution());
        setPreviousStatus(currentUser, AccountStatus.PENDING);
        currentUser.setStatus(AccountStatus.ACTIVE);
        service.saveUser(currentUser, false, "password123");

        Mailbox inbox = Mailbox.get(currentUser.getEmail());
        assertEquals(1, inbox.size());
        testEmail(currentUser.getEmail(), "Created", "account with the");
    }

    /**
     * When previous status is pending and status is inactive the user registration has
     * been denied. The user and denial reason should be persisted.
     *
     * @throws InvocationTargetException if the underlying method throws an exception.
     * @throws IllegalAccessException if the underlying method is inaccessible.
     * @throws NoSuchMethodException if a matching method is not found.
     * @throws MessagingException if an errors while sending email.
     */
    @Test
    public void denyingRegistrationPersistsDenialReason() throws NoSuchMethodException,
            IllegalAccessException, InvocationTargetException, MessagingException {
        TissueLocatorUserServiceBean service = EjbTestHelper.getTissueLocatorUserServiceBeanWithMockLoad();
        createAccountApprovalSetting(true);
        service.setApplicationSettingService(new ApplicationSettingServiceBean());
        MailUtils.setMailEnabled(false);

        // create and save current user
        TissueLocatorUser currentUser = getCurrentUser();
        createUserInRole(Role.USER_ACCOUNT_APPROVER, currentUser.getInstitution());
        Long id = service.saveUser(currentUser, false, "password123");

        // retrieve user
        TissueLocatorUserServiceLocal regularService = EjbTestHelper.getTissueLocatorUserServiceBean();
        TissueLocatorUser retrievedUser = regularService.getPersistentObject(TissueLocatorUser.class, id);

        // deny user registration
        setPreviousStatus(retrievedUser, AccountStatus.PENDING);
        retrievedUser.setStatus(AccountStatus.INACTIVE);
        retrievedUser.setStatusTransitionComment("Reason for denial.");
        service.denyUser(retrievedUser);

        TissueLocatorUser deniedUser = regularService.getPersistentObject(TissueLocatorUser.class,
                retrievedUser.getId());
        assertNotNull(deniedUser);
        AccountStatusTransition transition = deniedUser.getStatusTransitionHistory()
            .get(deniedUser.getStatusTransitionHistory().size() - 1);
        assertEquals("Reason for denial.", transition.getComment());
    }

    /**
     * When previous status is pending and status is inactive the user registration has
     * been denied. An email should be sent to the user with the reason for denial.
     *
     * @throws InvocationTargetException if the underlying method throws an exception.
     * @throws IllegalAccessException if the underlying method is inaccessible.
     * @throws NoSuchMethodException if a matching method is not found.
     * @throws MessagingException if an error occurs while sending email.
     * @throws IOException if an input/output operation fails or is interrupted.
     */
    @Test
    public void denyingRegistrationSendsDeniedEmail() throws NoSuchMethodException,
            IllegalAccessException, InvocationTargetException, MessagingException,
            IOException {
        TissueLocatorUserServiceBean service = EjbTestHelper.getTissueLocatorUserServiceBeanWithMockLoad();
        createAccountApprovalSetting(true);
        service.setApplicationSettingService(new ApplicationSettingServiceBean());
        MailUtils.setMailEnabled(true);

        // create and save current user
        TissueLocatorUser currentUser = getCurrentUser();
        createUserInRole(Role.USER_ACCOUNT_APPROVER, currentUser.getInstitution());
        Long id = service.saveUser(currentUser, false, "password123");

        // retrieve user
        TissueLocatorUserServiceLocal regularService = EjbTestHelper.getTissueLocatorUserServiceBean();
        TissueLocatorUser retrievedUser = regularService.getPersistentObject(TissueLocatorUser.class, id);

        // deny user registration
        setPreviousStatus(retrievedUser, AccountStatus.PENDING);
        retrievedUser.setStatus(AccountStatus.INACTIVE);
        retrievedUser.setStatusTransitionComment("Reason for denial.");
        service.denyUser(retrievedUser);

        Mailbox inbox = Mailbox.get(retrievedUser.getEmail());
        assertEquals(1, inbox.size());
        testEmail(retrievedUser.getEmail(), "Denied", "has been denied");
    }

    /**
     * When previous status is active and status is inactive the user registration has
     * been deactivated. An email should be sent to the user with the reason for deactivation.
     *
     * @throws InvocationTargetException if the underlying method throws an exception.
     * @throws IllegalAccessException if the underlying method is inaccessible.
     * @throws NoSuchMethodException if a matching method is not found.
     * @throws MessagingException if an error occurs while sending email.
     * @throws IOException if an input/output operation fails or is interrupted.
     */
    @Test
    public void deactivatingUserSendsDeniedEmail() throws NoSuchMethodException,
            IllegalAccessException, InvocationTargetException, MessagingException,
            IOException {
        TissueLocatorUserServiceBean service = EjbTestHelper.getTissueLocatorUserServiceBeanWithMockLoad();
        createAccountApprovalSetting(true);
        service.setApplicationSettingService(new ApplicationSettingServiceBean());
        MailUtils.setMailEnabled(true);

        // create and save current user
        TissueLocatorUser currentUser = getCurrentUser();
        createUserInRole(Role.USER_ACCOUNT_APPROVER, currentUser.getInstitution());
        Long id = service.saveUser(currentUser, false, "password123");

        // retrieve user
        TissueLocatorUserServiceLocal regularService = EjbTestHelper.getTissueLocatorUserServiceBean();
        TissueLocatorUser retrievedUser = regularService.getPersistentObject(TissueLocatorUser.class, id);

        // deny user registration
        setPreviousStatus(retrievedUser, AccountStatus.ACTIVE);
        retrievedUser.setStatus(AccountStatus.INACTIVE);
        retrievedUser.setStatusTransitionComment("Reason for deactivation.");
        service.deactivateUser(retrievedUser);

        Mailbox inbox = Mailbox.get(retrievedUser.getEmail());
        assertEquals(1, inbox.size());
        testEmail(retrievedUser.getEmail(),
                "Your Account has been Inactivated", "Reason for deactivation.");
    }

    /**
     * Test status transition comment validation.
     */
    @Test
    public void testStatusTransitionCommentValidation() {
        TissueLocatorUserServiceBean service = EjbTestHelper.getTissueLocatorUserServiceBeanWithMockLoad();
        createAccountApprovalSetting(true);
        InstitutionType type = new InstitutionType();
        type.setName("denial test");
        TissueLocatorHibernateUtil.getCurrentSession().save(type);
        Institution i = createInstitution("denial test", type);
        TissueLocatorUser user = createUser("denied", i);
        user.setPreviousStatus(AccountStatus.PENDING);
        user.setStatus(AccountStatus.INACTIVE);
        try {
            service.savePersistentObject(user);
            fail("Validation exception expected.");
        } catch (InvalidStateException e) {
            // expected
        }
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        user = createUser("denied", i);
        user.setPreviousStatus(AccountStatus.ACTIVE);
        user.setStatus(AccountStatus.INACTIVE);
        try {
            service.savePersistentObject(user);
            fail("Validation exception expected.");
        } catch (InvalidStateException e) {
            // expected
        }
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        user = createUser("denied", i);
        user.setPreviousStatus(AccountStatus.INACTIVE);
        user.setStatus(AccountStatus.INACTIVE);
        service.savePersistentObject(user);

        user = createUser("denied2", i);
        user.setPreviousStatus(AccountStatus.INACTIVE);
        user.setStatus(AccountStatus.ACTIVE);
        user.setStatusTransitionComment(null);
        service.savePersistentObject(user);

        user = createUser("denied3", i);
        user.setPreviousStatus(AccountStatus.PENDING);
        user.setStatus(AccountStatus.ACTIVE);
        user.setStatusTransitionComment("denial reason");
        service.savePersistentObject(user);

        user = createUser("denied4", i);
        user.setPreviousStatus(AccountStatus.PENDING);
        user.setStatus(AccountStatus.ACTIVE);
        user.setStatusTransitionComment(null);
        service.savePersistentObject(user);

        user = createUser("denied5", i);
        user.setPreviousStatus(null);
        user.setStatus(AccountStatus.INACTIVE);
        service.savePersistentObject(user);
    }

    private void setPreviousStatus(TissueLocatorUser user, AccountStatus status) throws NoSuchMethodException,
            IllegalAccessException, InvocationTargetException {
        Method method = AbstractUser.class
            .getDeclaredMethod("setPreviousStatus", AccountStatus.class);
        method.setAccessible(true);
        method.invoke(user, status);
    }

    /**
     * test the get new user count method.
     */
    @Test
    public void testGetNewUserCount() {
        Date creationDate = getCurrentUser().getCreationDate();
        Date before = DateUtils.addDays(creationDate, -1);
        Date after = DateUtils.addDays(creationDate, 1);
        TissueLocatorUserServiceLocal service = EjbTestHelper.getTissueLocatorUserServiceBean();
        assertEquals(1, service.getNewUserCount(before, after));
        assertEquals(1, service.getNewUserCount(before, creationDate));
        assertEquals(1, service.getNewUserCount(creationDate, after));
        assertEquals(1, service.getNewUserCount(creationDate, creationDate));
        assertEquals(0, service.getNewUserCount(before, before));
        assertEquals(0, service.getNewUserCount(after, after));
    }

    /**
     * Test the handling of account status transitions.
     */
    @Test
    public void testUpdateStatusTransitionHistory() {
        TissueLocatorUserServiceLocal service = EjbTestHelper.getTissueLocatorUserServiceBean();
        TissueLocatorUser user = createUserSkipRole();
        user.setStatus(AccountStatus.PENDING);
        user.setPreviousStatus(null);
        Long id = service.savePersistentObject(user);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        user = service.getPersistentObject(TissueLocatorUser.class, id);
        int expectedSize = 0;
        assertEquals(expectedSize, user.getStatusTransitionHistory().size());

        user.setPreviousStatus(AccountStatus.PENDING);
        service.savePersistentObject(user);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        user = service.getPersistentObject(TissueLocatorUser.class, id);
        assertEquals(expectedSize, user.getStatusTransitionHistory().size());

        user.setStatus(AccountStatus.INACTIVE);
        user.setStatusTransitionDate(null);
        user.setStatusTransitionComment("status comment");
        service.savePersistentObject(user);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        user = service.getPersistentObject(TissueLocatorUser.class, id);
        expectedSize++;
        assertEquals(expectedSize, user.getStatusTransitionHistory().size());
        AccountStatusTransition transition = user.getStatusTransitionHistory().get(expectedSize - 1);
        assertEquals(AccountStatus.PENDING, transition.getPreviousStatus());
        assertEquals(AccountStatus.INACTIVE, transition.getNewStatus());
        assertNotNull(transition.getTransitionDate());
        assertEquals("status comment", transition.getComment());
        assertNotNull(transition.getSystemTransitionDate());
        assertEquals(AccountStatus.INACTIVE, user.getPreviousStatus());

        user.setStatus(AccountStatus.ACTIVE);
        Date tDate = new Date();
        user.setStatusTransitionDate(tDate);
        service.savePersistentObject(user);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        user = service.getPersistentObject(TissueLocatorUser.class, id);
        expectedSize++;
        assertEquals(expectedSize, user.getStatusTransitionHistory().size());
        transition = user.getStatusTransitionHistory().get(expectedSize - 1);
        assertEquals(AccountStatus.INACTIVE, transition.getPreviousStatus());
        assertEquals(AccountStatus.ACTIVE, transition.getNewStatus());
        assertEquals(tDate, transition.getTransitionDate());
        assertNull(transition.getComment());
        assertNotNull(transition.getSystemTransitionDate());
        assertEquals(AccountStatus.ACTIVE, user.getPreviousStatus());

        user.setStatus(AccountStatus.INACTIVE);
        user.setStatusTransitionComment("status comment");
        service.savePersistentObject(user);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        user = service.getPersistentObject(TissueLocatorUser.class, id);
        expectedSize++;
        assertEquals(expectedSize, user.getStatusTransitionHistory().size());
        transition = user.getStatusTransitionHistory().get(expectedSize - 1);
        assertEquals(AccountStatus.ACTIVE, transition.getPreviousStatus());
        assertEquals(AccountStatus.INACTIVE, transition.getNewStatus());
        assertEquals("status comment", transition.getComment());
        assertNotNull(transition.getTransitionDate());
        assertNotNull(transition.getSystemTransitionDate());
        assertEquals(AccountStatus.INACTIVE, user.getPreviousStatus());
    }
}
