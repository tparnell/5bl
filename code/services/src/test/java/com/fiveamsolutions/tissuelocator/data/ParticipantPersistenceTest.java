/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.data;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.junit.Test;

import com.fiveamsolutions.nci.commons.data.search.PageSortParams;
import com.fiveamsolutions.nci.commons.search.SearchCriteria;
import com.fiveamsolutions.tissuelocator.service.ParticipantServiceLocal;
import com.fiveamsolutions.tissuelocator.service.search.ParticipantSortCriterion;
import com.fiveamsolutions.tissuelocator.service.search.TissueLocatorAnnotatedBeanSearchCriteria;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;

/**
 * @author smiller
 *
 */
public class ParticipantPersistenceTest extends AbstractPersistenceTest<Participant> {


    /**
     * {@inheritDoc}
     */
    @Override
    public ParticipantServiceLocal getService() {
        return TissueLocatorRegistry.getServiceLocator().getParticipantService();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Participant[] getValidObjects() {
        Institution institution = getCurrentUser().getInstitution();
        Participant[] participants = new Participant[2];
        Participant participant = new Participant();
        participant.setEthnicity(Ethnicity.HISPANIC_OR_LATINO);
        assertNotNull(Ethnicity.HISPANIC_OR_LATINO.getResourceKey());
        participant.setRaces(new ArrayList<Race>());
        participant.getRaces().add(Race.WHITE);
        assertNotNull(Race.WHITE.getResourceKey());
        participant.getRaces().add(Race.BLACK_OR_AFRICAN_AMERICAN);
        participant.setExternalId("external id 123");
        participant.setExternalIdAssigner(institution);
        participant.setGender(Gender.FEMALE);
        assertNotNull(Gender.FEMALE.getResourceKey());
        participants[0] = participant;

        participant = new Participant();
        participant.setEthnicity(Ethnicity.NON_HISPANIC_OR_LATINO);
        participant.setRaces(new ArrayList<Race>());
        participant.getRaces().add(Race.ISLANDER);
        participant.setExternalId("external id 456");
        participant.setExternalIdAssigner(institution);
        participant.setGender(Gender.MALE);
        participants[1] = participant;
        return participants;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void verifyObjectRetrieval(Participant expected, Participant actual) {
        assertTrue(EqualsBuilder.reflectionEquals(expected, actual, new String[] {"races", "externalIdAssigner"}));
        if (expected.getRaces() == null) {
            assertNull(actual.getRaces());
        } else {
            assertTrue(CollectionUtils.isEqualCollection(expected.getRaces(), actual.getRaces()));
        }

        if (expected.getExternalIdAssigner() == null) {
            assertNull(actual.getExternalIdAssigner());
        } else {
            assertEquals(expected.getExternalIdAssigner().getId(), actual.getExternalIdAssigner().getId());
        }
    }

    /**
     * test search.
     */
    @Test
    public void testSearch() {
        testSaveRetrieve();
        Participant example = new Participant();
        example.setExternalId("id");
        SearchCriteria<Participant> sc =
            new TissueLocatorAnnotatedBeanSearchCriteria<Participant>(example);
        PageSortParams<Participant> psp =
              new PageSortParams<Participant>(PAGE_SIZE, 0, ParticipantSortCriterion.EXTERNAL_ID, true);
        List<Participant> results = getService().search(sc, psp);
        assertEquals(2, results.size());
        assertTrue(results.get(0).getExternalId().compareTo(results.get(1).getExternalId()) > 0);
    }
}
