/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.junit.Test;

import com.fiveamsolutions.dynamicextensions.ControlledVocabularyDynamicFieldDefinition;
import com.fiveamsolutions.nci.commons.data.security.ApplicationRole;
import com.fiveamsolutions.tissuelocator.data.Gender;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.Participant;
import com.fiveamsolutions.tissuelocator.data.QuantityUnits;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.code.AdditionalPathologicFinding;
import com.fiveamsolutions.tissuelocator.data.code.SpecimenType;
import com.fiveamsolutions.tissuelocator.data.config.search.AbstractSearchFieldConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.AutocompleteConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.CheckboxAutocompleteCompositeConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.CheckboxConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.DataType;
import com.fiveamsolutions.tissuelocator.data.config.search.MultiSelectValueType;
import com.fiveamsolutions.tissuelocator.data.config.search.RangeConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.RangeSelectCompositeConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.SearchSetType;
import com.fiveamsolutions.tissuelocator.data.config.search.SelectConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.SelectDynamicExtensionConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.SimpleSearchFieldConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.TextConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.TextSelectCompositeConfig;
import com.fiveamsolutions.tissuelocator.service.config.search.SearchConfig;
import com.fiveamsolutions.tissuelocator.service.config.search.SearchFieldConfigServiceBean;
import com.fiveamsolutions.tissuelocator.service.config.search.SearchFieldConfigServiceLocal;
import com.fiveamsolutions.tissuelocator.test.AbstractHibernateTestCase;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;

/**
 * @author gvaughn
 *
 */
public class SearchFieldConfigTest extends AbstractHibernateTestCase {

    /**
     * Test search config retrieval.
     */
    @Test
    public void testSaveRetrieve() {
        SearchFieldConfigServiceLocal service = new SearchFieldConfigServiceBean();
        Map<Class<? extends AbstractSearchFieldConfig>, Map<Long, AbstractSearchFieldConfig>> configMap
            = new HashMap<Class<? extends AbstractSearchFieldConfig>, Map<Long, AbstractSearchFieldConfig>>();
        for (AbstractSearchFieldConfig searchFieldConfig : createConfigs()) {
            service.savePersistentObject(searchFieldConfig);
            if (!configMap.containsKey(searchFieldConfig.getClass())) {
                configMap.put(searchFieldConfig.getClass(), new HashMap<Long, AbstractSearchFieldConfig>());
            }
            configMap.get(searchFieldConfig.getClass()).put(searchFieldConfig.getId(), searchFieldConfig);
        }

        Collection<AbstractSearchFieldConfig> configs = service.getSearchFieldConfigs(SearchSetType.ADVANCED);
        for (AbstractSearchFieldConfig searchFieldConfig : configs) {
            assertTrue(EqualsBuilder.reflectionEquals(configMap.get(searchFieldConfig.getClass())
                    .get(searchFieldConfig.getId()), searchFieldConfig, new String[]{"id"}));
        }
        configs = service.getSearchFieldConfigs(SearchSetType.SIMPLE);
        assertEquals(1, configs.size());
    }

    /**
     * Test the SearchConfig object.
     */
    @Test
    public void testSearchConfig() {
        testSaveRetrieve();
        SearchFieldConfigServiceLocal service = new SearchFieldConfigServiceBean();
        Collection<AbstractSearchFieldConfig> svcConfigs = service.getSearchFieldConfigs(SearchSetType.ADVANCED);
        SearchConfig config = new SearchConfig(SearchSetType.ADVANCED, new SearchFieldConfigServiceBean());
        assertEquals(svcConfigs.size(), config.getSearchFieldConfigs().size());
        for (AbstractSearchFieldConfig searchFieldConfig : svcConfigs) {
            assertTrue(EqualsBuilder.reflectionEquals(searchFieldConfig,
                    config.getConfig(searchFieldConfig.getClass(), searchFieldConfig.getId())));
        }
        // confirm lazy loading
        config = new SearchConfig(SearchSetType.ADVANCED, new SearchFieldConfigServiceBean());
        assertEquals(svcConfigs.size(), config.getSearchFieldConfigs().size());
        for (AbstractSearchFieldConfig searchFieldConfig : svcConfigs) {
            assertTrue(EqualsBuilder.reflectionEquals(searchFieldConfig,
                    config.getConfig(searchFieldConfig.getClass(), searchFieldConfig.getId())));
        }

        config = new SearchConfig(SearchSetType.SIMPLE, new SearchFieldConfigServiceBean());
        svcConfigs = service.getSearchFieldConfigs(SearchSetType.SIMPLE);
        assertEquals(svcConfigs.size(), config.getSearchFieldConfigs().size());
        for (AbstractSearchFieldConfig searchFieldConfig : svcConfigs) {
            assertTrue(EqualsBuilder.reflectionEquals(searchFieldConfig,
                    config.getConfig(searchFieldConfig.getClass(), searchFieldConfig.getId())));
        }

        config = new SearchConfig(SearchSetType.WIDGET, new SearchFieldConfigServiceBean());
        svcConfigs = service.getSearchFieldConfigs(SearchSetType.WIDGET);
        assertEquals(svcConfigs.size(), config.getSearchFieldConfigs().size());
        for (AbstractSearchFieldConfig searchFieldConfig : svcConfigs) {
            assertTrue(EqualsBuilder.reflectionEquals(searchFieldConfig,
                    config.getConfig(searchFieldConfig.getClass(), searchFieldConfig.getId())));
        }
    }

    /**
     * Tests controlled vocabulary data retrival.
     */
    @Test
    public void testGetData() {
        assertNotNull(DataType.CODE.getData(SpecimenType.class));
        assertNotNull(DataType.ENUM.getData(Gender.class));
        assertNotNull(DataType.INSTITUTION.getData(Institution.class));
    }

    /**
     * Tests value initialization and retrieval.
     */
    @Test
    public void testGetValue() {
        TextConfig text = new TextConfig();
        populateSimpleConfig(text);
        text.getFieldConfig().setSearchFieldName("externalId");
        text.setObjectProperty(false);
        Specimen specimen = new Specimen();
        specimen.setExternalId("specId");
        assertEquals("specId", text.getValues(specimen, null).get(text));

        SelectConfig select = new SelectConfig();
        populateSimpleConfig(select);
        select.getFieldConfig().setSearchFieldName("participant.gender");
        select.setObjectProperty(false);
        select.setListType(DataType.ENUM);
        select.setListClass(Gender.class.getName());
        select.setMinWidth("200");
        specimen.setParticipant(new Participant());
        specimen.getParticipant().setGender(Gender.FEMALE);
        assertEquals(Gender.FEMALE, select.getValues(specimen, null).get(select));

        select = new SelectConfig();
        populateSimpleConfig(select);
        select.getFieldConfig().setSearchFieldName("pathologicalCharacteristic");
        select.setObjectProperty(false);
        select.setListType(DataType.CODE);
        select.setListClass(AdditionalPathologicFinding.class.getName());
        select.setMinWidth("200");
        specimen.setPathologicalCharacteristic(new AdditionalPathologicFinding());
        specimen.getPathologicalCharacteristic().setId(1L);
        assertEquals(specimen.getPathologicalCharacteristic(), select.getValues(specimen, null).get(select));

        select = new SelectConfig();
        populateSimpleConfig(select);
        select.getFieldConfig().setSearchFieldName("storageCondition");
        select.setObjectProperty(false);
        select.setMultiSelect(true);
        select.setMinWidth("200");
        specimen.setCustomProperty("storageCondition", "cold");
        assertEquals("cold", select.getValues(specimen, "customProperties").get(select));

        CheckboxConfig cComp = new CheckboxConfig();
        populateSimpleConfig(cComp);
        AutocompleteConfig aComp = new AutocompleteConfig();
        populateSimpleConfig(aComp);
        aComp.getFieldConfig().setSearchFieldName("externalId");
        aComp.setObjectProperty(false);
        CheckboxAutocompleteCompositeConfig cac = new CheckboxAutocompleteCompositeConfig();
        cac.setAutocompleteConfig(aComp);
        cac.setCheckboxConfig(cComp);
        assertEquals("specId", cac.getValues(specimen, null).get(cac.getAutocompleteConfig()));

        RangeConfig rangeComp = new RangeConfig();
        populateSimpleConfig(rangeComp);
        rangeComp.getFieldConfig().setSearchFieldName("availableQuantity");
        rangeComp.setObjectProperty(false);
        SelectConfig rangeSelect = new SelectConfig();
        populateSimpleConfig(rangeSelect);
        rangeSelect.setListType(DataType.ENUM);
        rangeSelect.setListClass(QuantityUnits.class.getName());
        rangeSelect.getFieldConfig().setSearchFieldName("availableQuantityUnits");
        rangeSelect.setObjectProperty(false);
        rangeSelect.setMinWidth("200");
        RangeSelectCompositeConfig rsc = new RangeSelectCompositeConfig();
        rsc.setRangeConfig(rangeComp);
        rsc.setSelectConfig(rangeSelect);
        specimen.setAvailableQuantityUnits(QuantityUnits.MG);
        BigDecimal q = new BigDecimal(1);
        specimen.setAvailableQuantity(q);
        Map<AbstractSearchFieldConfig, Object> configMap = rsc.getValues(specimen, null);
        assertEquals(q, configMap.get(rsc.getRangeConfig()));
        assertEquals(QuantityUnits.MG, configMap.get(rsc.getSelectConfig()));

        TextConfig textComp = new TextConfig();
        populateSimpleConfig(textComp);
        textComp.getFieldConfig().setSearchFieldName("availableQuantity");
        textComp.setObjectProperty(false);
        SelectConfig textSelect = new SelectConfig();
        populateSimpleConfig(textSelect);
        textSelect.setListType(DataType.ENUM);
        textSelect.setListClass(QuantityUnits.class.getName());
        textSelect.getFieldConfig().setSearchFieldName("availableQuantityUnits");
        textSelect.setObjectProperty(false);
        textSelect.setMinWidth("200");
        TextSelectCompositeConfig tsc = new TextSelectCompositeConfig();
        tsc.setTextConfig(textComp);
        tsc.setSelectConfig(textSelect);
        configMap = tsc.getValues(specimen, null);
        assertEquals(q, configMap.get(tsc.getTextConfig()));
        assertEquals(QuantityUnits.MG, configMap.get(tsc.getSelectConfig()));
    }

    private List<AbstractSearchFieldConfig> createConfigs() {
        SearchFieldConfigServiceLocal service = new SearchFieldConfigServiceBean();
        List<AbstractSearchFieldConfig> configList = new ArrayList<AbstractSearchFieldConfig>();

        TextConfig text = new TextConfig();
        populateSimpleConfig(text);
        ApplicationRole role = new ApplicationRole();
        role.setName("dummy");
        TissueLocatorHibernateUtil.getCurrentSession().saveOrUpdate(role);
        text.getFieldConfig().setRequiredRole(role);
        text.setSearchSetType(SearchSetType.SIMPLE);
        text.setFilterCriteriaString(true);
        configList.add(text);

        text = new TextConfig();
        populateSimpleConfig(text);
        configList.add(text);

        SelectConfig select = new SelectConfig();
        populateSimpleConfig(select);
        select.setListType(DataType.CODE);
        select.setListClass(AdditionalPathologicFinding.class.getName());
        select.setMultiSelect(true);
        select.setMultiSelectValueType(MultiSelectValueType.STRING);
        select.setMinWidth("200");
        configList.add(select);

        RangeConfig range = new RangeConfig();
        range.setSearchConditionClass("RangeSearchCondition");
        populateSimpleConfig(range);
        configList.add(range);

        CheckboxConfig check = new CheckboxConfig();
        populateSimpleConfig(check);
        configList.add(check);

        AutocompleteConfig auto = new AutocompleteConfig();
        populateSimpleConfig(auto);
        auto.setListUrl("listUrl");
        auto.setValueProperty("valueProp");
        configList.add(auto);

        CheckboxConfig cComp = new CheckboxConfig();
        populateSimpleConfig(cComp);
        AutocompleteConfig aComp = new AutocompleteConfig();
        populateSimpleConfig(aComp);
        aComp.setListUrl("listUrl");
        aComp.setValueProperty("valueProp");
        service.savePersistentObject(cComp);
        service.savePersistentObject(aComp);
        CheckboxAutocompleteCompositeConfig cac = new CheckboxAutocompleteCompositeConfig();
        cac.setAutocompleteConfig(aComp);
        cac.setCheckboxConfig(cComp);
        cac.setCheckboxTextValue("textVal");
        cac.setSearchSetType(SearchSetType.ADVANCED);
        cac.setFilterCriteriaString(true);
        configList.add(cac);

        TextConfig textComp = new TextConfig();
        populateSimpleConfig(textComp);
        SelectConfig textSelect = new SelectConfig();
        populateSimpleConfig(textSelect);
        textSelect.setListType(DataType.ENUM);
        textSelect.setListClass(Gender.class.getName());
        textSelect.setMinWidth("200");
        service.savePersistentObject(textComp);
        service.savePersistentObject(textSelect);
        TextSelectCompositeConfig tsc = new TextSelectCompositeConfig();
        tsc.setSearchSetType(SearchSetType.ADVANCED);
        tsc.setFilterCriteriaString(true);
        tsc.setTextConfig(textComp);
        tsc.setSelectConfig(textSelect);
        configList.add(tsc);

        RangeConfig rangeComp = new RangeConfig();
        rangeComp.setSearchConditionClass("RangeSearchCondition");
        populateSimpleConfig(rangeComp);
        SelectConfig rangeSelect = new SelectConfig();
        populateSimpleConfig(rangeSelect);
        rangeSelect.setListType(DataType.INSTITUTION);
        rangeSelect.setListClass("test");
        rangeSelect.setMinWidth("200");
        service.savePersistentObject(rangeComp);
        service.savePersistentObject(rangeSelect);
        RangeSelectCompositeConfig rsc = new RangeSelectCompositeConfig();
        rsc.setSearchSetType(SearchSetType.ADVANCED);
        rsc.setFilterCriteriaString(true);
        rsc.setRangeConfig(rangeComp);
        rsc.setSelectConfig(rangeSelect);
        configList.add(rsc);

        SelectDynamicExtensionConfig sdeConfig = new SelectDynamicExtensionConfig();
        sdeConfig.setMinWidth("200");
        sdeConfig.setShowEmptyOption(true);
        populateSimpleConfig(sdeConfig);
        ControlledVocabularyDynamicFieldDefinition fd = new ControlledVocabularyDynamicFieldDefinition();
        fd.setEntityClassName(SpecimenRequest.class.getName());
        fd.setFieldDisplayName("displayName");
        fd.setFieldName("fieldName");
        fd.setNullable(true);
        List<String> optionsList = new ArrayList<String>();
        optionsList.add("option");
        fd.setOptions(optionsList);
        fd.setSearchable(false);
        TissueLocatorHibernateUtil.getCurrentSession().saveOrUpdate(fd);
        sdeConfig.setControlledVocabConfig(fd);
        configList.add(sdeConfig);

        SelectDynamicExtensionConfig sdeConfigWidget = new SelectDynamicExtensionConfig();
        sdeConfigWidget.setMinWidth("200");
        sdeConfigWidget.setShowEmptyOption(true);
        populateSimpleConfig(sdeConfigWidget);
        ControlledVocabularyDynamicFieldDefinition fdWidget = new ControlledVocabularyDynamicFieldDefinition();
        fdWidget.setEntityClassName(SpecimenRequest.class.getName());
        fdWidget.setFieldDisplayName("widgetDisplayName");
        fdWidget.setFieldName("widgetFieldName");
        fdWidget.setNullable(true);
        List<String> optionsListWidget = new ArrayList<String>();
        optionsListWidget.add("option");
        fdWidget.setOptions(optionsListWidget);
        fdWidget.setSearchable(false);
        TissueLocatorHibernateUtil.getCurrentSession().saveOrUpdate(fdWidget);
        sdeConfigWidget.setControlledVocabConfig(fdWidget);
        sdeConfigWidget.setSearchSetType(SearchSetType.WIDGET);
        sdeConfigWidget.setFilterCriteriaString(true);
        sdeConfigWidget.getFieldConfig().setSearchFieldDisplayName("Anatomic Source");
        sdeConfigWidget.getFieldConfig().setSearchFieldName("object.customProperties['anatomicSource']");
        configList.add(sdeConfigWidget);

        return configList;
    }

    private void populateSimpleConfig(SimpleSearchFieldConfig config) {
        config.setFieldConfig(createAndPersistGenericFieldConfig("property"));
        config.setNote("note");
        config.setObjectProperty(true);
        config.setErrorProperty("error property");
        config.setSearchSetType(SearchSetType.ADVANCED);
        config.setCssClasses("class");
    }
}
