/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.junit.Test;

import com.fiveamsolutions.nci.commons.data.search.PageSortParams;
import com.fiveamsolutions.tissuelocator.data.SpecimenClass;
import com.fiveamsolutions.tissuelocator.data.code.AdditionalPathologicFinding;
import com.fiveamsolutions.tissuelocator.data.code.Code;
import com.fiveamsolutions.tissuelocator.data.code.SpecimenType;
import com.fiveamsolutions.tissuelocator.data.code.UseCountedCode;
import com.fiveamsolutions.tissuelocator.service.search.CodeSortCriterion;
import com.fiveamsolutions.tissuelocator.service.search.TissueLocatorAnnotatedBeanSearchCriteria;
import com.fiveamsolutions.tissuelocator.service.search.UseCountedCodeSearchCriteria;
import com.fiveamsolutions.tissuelocator.test.AbstractHibernateTestCase;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;

/**
 * @author ddasgupta
 *
 */
public class CodeServiceTest extends AbstractHibernateTestCase {

    private static final int MAX_RESULTS = 100;

    /**
     * tests whether codes can be saved and retrieved.
     */
    @Test
    public void testSaveRetrieve() {
        saveRetrieve(AdditionalPathologicFinding.class);
        saveRetrieve(SpecimenType.class);
    }

    private <T extends Code> void saveRetrieve(Class<T> codeType) {
        CodeServiceLocal service = TissueLocatorRegistry.getServiceLocator().getCodeService();
        List<T> codes = createCodes(codeType);
        for (T code : codes) {
            Long id = service.savePersistentObject(code);
            T retrieved = service.getPersistentObject(codeType, id);
            assertEquals(codeType, retrieved.getClass());
            assertTrue(EqualsBuilder.reflectionEquals(code, retrieved));
        }
        List<T> allObjects = service.getAll(codeType);
        assertEquals(codes.size(), allObjects.size());
    }

    private <T extends Code> List<T> createCodes(Class<T> codeType) {
        List<T> codes = new ArrayList<T>();
        try {
            T active = codeType.newInstance();
            active.setName(codeType.getSimpleName() + "-active");
            active.setValue(active.getName());
            active.setActive(true);
            if (codeType.isAssignableFrom(SpecimenType.class)) {
                ((SpecimenType) active).setSpecimenClass(SpecimenClass.TISSUE);
            }
            if (UseCountedCode.class.isAssignableFrom(codeType)) {
                ((UseCountedCode) active).setUseCount(0L);
            }
            codes.add(active);
            T inactive = codeType.newInstance();
            inactive.setName(codeType.getSimpleName() + "-inactive");
            inactive.setActive(false);
            if (codeType.isAssignableFrom(SpecimenType.class)) {
                ((SpecimenType) inactive).setSpecimenClass(SpecimenClass.FLUID);
            }
            if (UseCountedCode.class.isAssignableFrom(codeType)) {
                ((UseCountedCode) inactive).setUseCount(0L);
            }
            codes.add(inactive);
        } catch (IllegalAccessException e)  {
            return codes;
        } catch (InstantiationException e) {
            return codes;
        }
        return codes;
    }

    /**
     * Test the getActiveCodes method.
     */
    @Test
    public void testGetActiveCodes() {
        testSaveRetrieve();
        activeCodeTest(AdditionalPathologicFinding.class);
        activeCodeTest(SpecimenType.class);
    }

    private <T extends Code> void activeCodeTest(Class<T> codeType) {
        CodeServiceLocal service = TissueLocatorRegistry.getServiceLocator().getCodeService();
        List<T> activeCodes = service.getActiveCodes(codeType);
        assertNotNull(activeCodes);
        assertEquals(1, activeCodes.size());
        T code = activeCodes.get(0);
        assertEquals(codeType, code.getClass());
        assertNotNull(code.getId());
        assertEquals(codeType.getSimpleName() + "-active", code.getName());
        assertEquals(codeType.getSimpleName() + "-active", code.getValue());
        assertTrue(code.getActive());
        if (codeType.isAssignableFrom(SpecimenType.class)) {
            assertNotNull(((SpecimenType) code).getSpecimenClass());
            assertNotNull(((SpecimenType) code).getSpecimenClass().getResourceKey());
            assertEquals(SpecimenClass.TISSUE, ((SpecimenType) code).getSpecimenClass());
        }

    }

    /**
     * Test code search.
     */
    @Test
    public void testSearch() {
        testSaveRetrieve();
        searchTest(AdditionalPathologicFinding.class);
        searchTest(SpecimenType.class);
    }

    private <T extends Code> void searchTest(Class<T> codeType) {
        searchTestHelper(codeType, codeType.getSimpleName(), true, 1);
        searchTestHelper(codeType, codeType.getSimpleName(), false, 1);
        searchTestHelper(codeType, "active", true, 1);
        searchTestHelper(codeType, "active", false, 1);
        searchTestHelper(codeType, "-active", true, 1);
        searchTestHelper(codeType, "-active", false, 0);
        searchTestHelper(codeType, "-inactive", true, 0);
        searchTestHelper(codeType, "-inactive", false, 1);
        searchTestHelper(codeType, "invalid", true, 0);
        searchTestHelper(codeType, "invalid", false, 0);
    }

    private <T extends Code> void searchTestHelper(Class<T> codeType, String searchString, boolean active,
            int resultCount) {
        try {
            CodeServiceLocal service = TissueLocatorRegistry.getServiceLocator().getCodeService();
            PageSortParams<Code> psp = new PageSortParams<Code>(2, 0, CodeSortCriterion.NAME, false);
            T example = codeType.newInstance();
            TissueLocatorAnnotatedBeanSearchCriteria<Code> sc =
                new TissueLocatorAnnotatedBeanSearchCriteria<Code>(example);
            example.setName(searchString);
            example.setActive(active);
            List<Code> codes = service.search(sc, psp);
            assertNotNull(codes);
            assertEquals(resultCount, codes.size());
            for (Code code : codes) {
                assertEquals(codeType, code.getClass());
            }
        } catch (IllegalAccessException e)  {
            fail();
        } catch (InstantiationException e) {
            fail();
        }
    }
    
    /**
     * Test searches for in-use codes.
     */
    @Test
    public void testMinimumUseCountSearch() {
        searchMinimumUseCount(0, 0);
        searchMinimumUseCount(1, 0);
        
        int total = 0;
        int totalInUse = 0;
        
        CodeServiceLocal service = TissueLocatorRegistry.getServiceLocator().getCodeService();
        UseCountedCode c = new AdditionalPathologicFinding();
        c.setName("code1");
        c.setActive(true);
        c.setUseCount(0L);
        total++;
        Long id = service.savePersistentObject(c);
        searchMinimumUseCount(1, totalInUse);
        searchMinimumUseCount(0, total);
        
        c = new AdditionalPathologicFinding();
        c.setName("code2");
        c.setActive(true);
        c.setUseCount(1L);
        total++;
        totalInUse++;
        service.savePersistentObject(c);
        searchMinimumUseCount(1, totalInUse);
        searchMinimumUseCount(0, total);
        
        c = service.getPersistentObject(AdditionalPathologicFinding.class, id);
        c.setUseCount(1L);
        totalInUse++;
        service.savePersistentObject(c);
        searchMinimumUseCount(1, totalInUse);
        searchMinimumUseCount(0, total);
        
        c = new AdditionalPathologicFinding();
        c.setName("code3");
        c.setActive(true);
        //CHECKSTYLE:OFF
        c.setUseCount(100L);
        //CHECKSTYLE:ON
        total++;
        totalInUse++;
        service.savePersistentObject(c);
        searchMinimumUseCount(1, totalInUse);
        searchMinimumUseCount(0, total);
        //CHECKSTYLE:OFF
        searchMinimumUseCount(100, 1);
        searchMinimumUseCount(101, 0);
        //CHECKSTYLE:ON
    }
    
    private void searchMinimumUseCount(int minCount, int expectedResults) {
        CodeServiceLocal service = TissueLocatorRegistry.getServiceLocator().getCodeService();
        PageSortParams<Code> psp = new PageSortParams<Code>(MAX_RESULTS, 0, CodeSortCriterion.NAME, false);
        AdditionalPathologicFinding example = new AdditionalPathologicFinding();
        UseCountedCodeSearchCriteria sc =
            new UseCountedCodeSearchCriteria(example, minCount);
        List<Code> codes = service.search(sc, psp);
        assertEquals(expectedResults, codes.size());
        for (Code code : codes) {
            UseCountedCode c = (UseCountedCode) code;
            assertTrue(c.getUseCount() >= minCount);
        }
        List<Code> sortedList = new ArrayList<Code>(codes);
        Collections.sort(codes, new Comparator<Code>() {

            /**
             * {@inheritDoc}
             */
            @Override
            public int compare(Code o1, Code o2) {
                return o1.getName().compareTo(o2.getName());
            }
            
        });
        assertTrue(CollectionUtils.isEqualCollection(sortedList, codes));
    }

    /**
     * Test the get by name method.
     */
    @Test
    public void testGetByName() {
        testSaveRetrieve();
        getByNameTest(AdditionalPathologicFinding.class);
        getByNameTest(SpecimenType.class);
    }

    private <T extends Code> void getByNameTest(Class<T> codeType) {
        CodeServiceLocal service = TissueLocatorRegistry.getServiceLocator().getCodeService();
        T code = service.getCode(codeType.getSimpleName(), codeType);
        assertNull(code);
        code = service.getCode(codeType.getSimpleName() + "-inactive", codeType);
        assertNull(code);
        code = service.getCode(codeType.getSimpleName() + "-active", codeType);
        testCode(code, codeType);
    }

    /**
     * Test the get by name method of the remote bean.
     */
    @Test
    public void testGetByNameRemote() {
        testSaveRetrieve();
        getByNameRemoteTest(AdditionalPathologicFinding.class);
        getByNameRemoteTest(SpecimenType.class);
    }

    private <T extends Code> void getByNameRemoteTest(Class<T> codeType) {
        CodeServiceRemoteBean service = new CodeServiceRemoteBean();
        service.setCodeService(TissueLocatorRegistry.getServiceLocator().getCodeService());
        T code = service.getCode(codeType.getSimpleName(), codeType);
        assertNull(code);
        code = service.getCode(codeType.getSimpleName() + "-inactive", codeType);
        assertNull(code);
        code = service.getCode(codeType.getSimpleName() + "-active", codeType);
        testCode(code, codeType);
    }

    private <T extends Code> void testCode(T code, Class<T> codeType) {
        assertNotNull(code);
        assertNotNull(code.getId());
        assertEquals(codeType, code.getClass());
        assertEquals(codeType.getSimpleName() + "-active", code.getName());
        assertEquals(codeType.getSimpleName() + "-active", code.getValue());
        assertTrue(code.getActive());
        if (codeType.isAssignableFrom(SpecimenType.class)) {
            assertNotNull(((SpecimenType) code).getSpecimenClass());
            assertNotNull(((SpecimenType) code).getSpecimenClass().getResourceKey());
            assertEquals(SpecimenClass.TISSUE, ((SpecimenType) code).getSpecimenClass());
        }
    }
}
