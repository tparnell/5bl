/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.service.search;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;

import com.fiveamsolutions.tissuelocator.data.Specimen;

/**
 * Range search condition unit tests.
 *
 * @author jstephens
 */
public class RangeSearchConditionTest {

    private Specimen specimen;
    private RangeSearchCondition condition;
    private Map<String, Object> conditionParameters;
    private Map<String, Object> bindParameters;
    private StringBuffer whereClause;

    /**
     * set up method to initialize local variables.
     */
    @Before
    public void setUp() {
        specimen = new Specimen();
        condition = new RangeSearchCondition();
        condition.setPropertyName("patientAgeAtCollection");
        conditionParameters = new HashMap<String, Object>();
        bindParameters = new HashMap<String, Object>();
        whereClause = new StringBuffer();
    }

    /**
     * verify the condition is appended to the where clause for the min value.
     */
    @Test
    public void shouldAppendToWhereClauseForMinValue() {
        conditionParameters.put("min_patientAgeAtCollection", 1);
        condition.setConditionParameters(conditionParameters);
        condition.appendCondition(specimen, whereClause, bindParameters);
        assertEquals(whereClauseForMinValue(), actualWhereClause());
    }

    /**
     * verify the condition is appended to the where clause for the max value.
     */
    @Test
    public void shouldAppendToWhereClauseForMaxValue() {
        conditionParameters.put("max_patientAgeAtCollection", 2);
        condition.setConditionParameters(conditionParameters);
        condition.appendCondition(specimen, whereClause, bindParameters);
        assertEquals(whereClauseForMaxValue(), actualWhereClause());
    }

    /**
     * verify the condition is appended to the where clause for the min and max values.
     */
    @Test
    public void shouldAppendToWhereClauseForMinAndMaxValue() {
        conditionParameters.put("min_patientAgeAtCollection", 1);
        conditionParameters.put("max_patientAgeAtCollection", 2);
        condition.setConditionParameters(conditionParameters);
        condition.appendCondition(specimen, whereClause, bindParameters);
        assertEquals(whereClauseForMinAndMaxValue(), actualWhereClause());
    }

    /**
     * verify that no condition is appended for empty parameters.
     */
    @Test
    public void shouldNotAppendToWhereClauseForEmptyParameters() {
        specimen.setPatientAgeAtCollectionUnits(null);
        conditionParameters.put("min_patientAgeAtCollection", StringUtils.EMPTY);
        conditionParameters.put("max_patientAgeAtCollection", StringUtils.EMPTY);
        condition.setConditionParameters(conditionParameters);
        condition.appendCondition(specimen, whereClause, bindParameters);
        assertEquals(StringUtils.EMPTY, whereClause.toString());
    }

    private String whereClauseForMinValue() {
        StringBuilder sb = new StringBuilder();
        sb.append("WHERE obj.patientAgeAtCollection >= 1");
        return sb.toString();
    }

    private String whereClauseForMaxValue() {
        StringBuilder sb = new StringBuilder();
        sb.append("WHERE obj.patientAgeAtCollection <= 2");
        return sb.toString();
    }

    private String whereClauseForMinAndMaxValue() {
        StringBuilder sb = new StringBuilder();
        sb.append("WHERE ");
        sb.append("obj.patientAgeAtCollection >= 1");
        sb.append(" AND ");
        sb.append("obj.patientAgeAtCollection <= 2");
        return sb.toString();
    }

    private String actualWhereClause() {
        String actualWhereClause = whereClause.toString().trim();
        for (Map.Entry<String, Object> entry : bindParameters.entrySet()) {
            String parameterName  = ":" + entry.getKey();
            String parameterValue = entry.getValue().toString();
            actualWhereClause = actualWhereClause.replaceAll(parameterName, parameterValue);
        }
        return actualWhereClause;
    }

}