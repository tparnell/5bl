/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.test.selenium.main;

import org.apache.commons.lang.StringUtils;

import com.fiveamsolutions.tissuelocator.test.ClientProperties;
import com.fiveamsolutions.tissuelocator.test.selenium.AbstractTissueLocatorSeleniumTest;


/**
 * @author aevansel
 */
public class RecordBiospecimenQualityTest extends AbstractTissueLocatorSeleniumTest {

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getScriptName() {
        return "ShipmentTest.sql";
    }

    /**
     * Tests the recording of biospecimen quality.
     * @throws Exception on sql error.
     */
    public void testRecordBiospecimenQuality() throws Exception {
        removeUnusedLineItems();
        goToRequestPage();
        verifyPendingShipments();
        if (ClientProperties.isOrderAdministrationActive()) {
            updateShipmentStatus();
            verifyHomePageMessage();
            recordBiospecimenQuality();
            verifyBiospecimenQuality();
            viewBiospecimenQuality();
        }

    }

    private void removeUnusedLineItems() throws Exception {
        if (ClientProperties.isAggregateSearchResultsActive()) {
            runSqlScript("ShipmentTest-removeLineItems.sql");
        } else {
            runSqlScript("ShipmentTest-removeAggregateLineItems.sql");
        }
    }

    private void goToRequestPage() {
        loginAsUser1();
        clickAndWait("link=My Requests");
        assertFalse(selenium.isElementPresent("link=Record"));
        clickAndWait("xpath=//table[@id='specimenRequest']/tbody/tr[1]/td[1]/a");
        assertTrue(selenium.isTextPresent("Request Details"));
    }

    private void verifyPendingShipments() {
        assertFalse(selenium.isTextPresent("Update Quality"));
        assertFalse(selenium.isElementPresent("link=Record Biospecimen Quality"));
        clickAndWait("xpath=//table[@id='shipment']/tbody/tr[1]/td[1]/a");
        assertTrue(selenium.isTextPresent("Order"));
        assertTrue(selenium.isTextPresent("Pending Shipment"));
        assertFalse(selenium.isElementPresent("link=Record Biospecimen Quality"));
        assertFalse(selenium.isElementPresent("link=Print Invoice"));
        clickAndWait("link=Sign Out");
    }

    private void verifyHomePageMessage() {
        loginAsUser1();
        clickAndWait("link=Home");
        assertTrue(selenium.isTextPresent("1 " + ClientProperties.getBiospecimenTerm() + " Request Requiring Action"));
        assertTrue(selenium.isElementPresent("link=1 " + ClientProperties.getBiospecimenTerm()
                + " Request Requiring Action"));
        clickAndWait("link=1 " + ClientProperties.getBiospecimenTerm() + " Request Requiring Action");
        assertTrue(selenium.isTextPresent("Record"));
        assertTrue(selenium.isElementPresent("link=Record"));
        assertEquals("Partially Processed", selenium.getTable("specimenRequest.1.5"));
        clickAndWait("link=Record");
        assertTrue(selenium.isTextPresent("Request Details"));
    }

    private void recordBiospecimenQuality() {
        String lineItemName = ClientProperties.isAggregateSearchResultsActive() ? "specificLineItems[0]"
                : "lineItems[0]";
        assertTrue(selenium.isTextPresent("You must take the following action(s) on this request"));
        assertTrue(selenium.isTextPresent("Update Received " + ClientProperties.getBiospecimenTerm() + " Quality"));
        assertTrue(selenium.isElementPresent("link=Record " + ClientProperties.getBiospecimenTerm() + " Quality"));
        clickAndWait("xpath=//table[@id='shipment']/tbody/tr[2]/td[1]/a");
        if (ClientProperties.isAggregateSearchResultsActive()) {
            assertTrue(selenium.isTextPresent("Criteria"));
        } else {
            assertTrue(selenium.isTextPresent(ClientProperties.getBiospecimenTerm() + "s in Request"));
        }
        assertFalse(selenium.isTextPresent("Receipt Quality"));
        assertTrue(selenium.isTextPresent("Order"));
        assertTrue(selenium.isTextPresent("Shipped"));
        assertTrue(selenium.isElementPresent("link=Record " + ClientProperties.getBiospecimenTerm() + " Quality"));
        assertTrue(selenium.isElementPresent("link=Print Invoice"));
        clickAndWait("link=Back");
        clickAndWait("link=Record " + ClientProperties.getBiospecimenTerm() + " Quality");

        String qualityUrlHackLocation = selenium.getLocation();

        assertTrue(selenium.isTextPresent("Shipment * (Shipped) : Record " + ClientProperties.getBiospecimenTerm()
                + " Quality"));
        assertTrue(selenium.isTextPresent("Please submit details regarding "
                + ClientProperties.getBiospecimenTermLowerCase() + " receipt quality"));
        assertTrue(selenium.isTextPresent("The condition of the " + ClientProperties.getBiospecimenTermLowerCase()
                + "(s) at the time of receipt."));
        assertTrue(selenium.isTextPresent("The current status of the " + ClientProperties.getBiospecimenTermLowerCase()
                + "(s)."));
        if (ClientProperties.isAggregateSearchResultsActive()) {
            assertEquals(ClientProperties.isInstitutionProfileUrlActive(),
                    selenium.isElementPresent("xpath=//tr[@id='specificLineItems_data0']/td[1]/a"));
        }
        selenium.check("name=" + lineItemName + ".received value=true");
        selenium.select("name=" + lineItemName + ".receiptQuality",
                "label=" + ClientProperties.getReceiptQualityOption());
        selenium.select("name=" + lineItemName + ".disposition", "label=In use");
        selenium.type("name=" + lineItemName + ".receiptNote", "Looks good");
        clickAndWait("id=btn_update");
        assertTrue(selenium.isTextPresent(ClientProperties.getBiospecimenTerm() + " quality successfully recorded."));
        clickAndWait("link=Back To Request");
        assertTrue(selenium.isTextPresent("You must take the following action(s) on this request"));
        assertTrue(selenium.isTextPresent("Update Received " + ClientProperties.getBiospecimenTerm() + " Quality"));
        clickAndWait("link=Record " + ClientProperties.getBiospecimenTerm() + " Quality");

        lineItemName = ClientProperties.isAggregateSearchResultsActive() ? "generalLineItems[0]"
                : "lineItems[1]";
        if (ClientProperties.isAggregateSearchResultsActive()) {
            assertEquals(ClientProperties.isInstitutionProfileUrlActive(),
                    selenium.isElementPresent("xpath=//tr[@id='generalLineItems_data0']/td[1]/a"));
        }
        selenium.check("name=" + lineItemName + ".received value=false");
        selenium.select("name=" + lineItemName + ".receiptQuality",
                "label=" + ClientProperties.getAlternateReceiptQualityOption());
        selenium.select("name=" + lineItemName + ".disposition", "label=Fully consumed");
        selenium.type("name=" + lineItemName + ".receiptNote", "Specimen not in shipment");
        clickAndWait("id=btn_update");
        assertTrue(selenium.isTextPresent("" + ClientProperties.getBiospecimenTerm()
                + " quality successfully recorded."));
        clickAndWait("link=Back To Request");
        assertFalse(selenium.isTextPresent("You must take the following action(s) on this request"));
        assertFalse(selenium.isTextPresent("Update Received " + ClientProperties.getBiospecimenTerm() + " Quality"));
        String urlToReturnTo = selenium.getLocation();

        urlHackBiospecimenQuality(qualityUrlHackLocation, urlToReturnTo);
    }


    private void urlHackBiospecimenQuality(String url, String urlToReturnTo) {
        clickAndWait("link=Sign Out");
        login("testunauthorized-user@example.com", "tissueLocator1", true);

        selenium.open(url);
        assertTrue(selenium.isTextPresent("java.lang.IllegalArgumentException"));
        assertTrue(selenium.isTextPresent("User does not have access to this object."));
        clickAndWait("link=Sign Out");
        loginAsUser1();
        selenium.open(urlToReturnTo);
    }


    private void verifyBiospecimenQuality() {
        assertFalse(selenium.isTextPresent("You must take the following action(s) on this request"));
        assertFalse(selenium.isTextPresent("Update Received " + ClientProperties.getBiospecimenTerm() + " Quality"));
        assertTrue(selenium.isElementPresent("link=Record " + ClientProperties.getBiospecimenTerm() + " Quality"));
        clickAndWait("link=Record " + ClientProperties.getBiospecimenTerm() + " Quality");
        assertTrue(selenium.isTextPresent("Shipment * (Received) : Record "
                + ClientProperties.getBiospecimenTerm() + " Quality"));
        String lineItemName = ClientProperties.isAggregateSearchResultsActive() ? "specificLineItems[0]"
                : "lineItems[0]";

        assertEquals("on", selenium.getValue("name=" + lineItemName + ".received"));
        assertEquals(ClientProperties.getReceiptQualityOption(),
                selenium.getSelectedLabel("name=" + lineItemName + ".receiptQuality"));
        assertEquals("In use", selenium.getSelectedLabel("name=" + lineItemName + ".disposition"));
        assertEquals("Looks good", selenium.getValue("name=" + lineItemName + ".receiptNote"));

        lineItemName = ClientProperties.isAggregateSearchResultsActive() ? "generalLineItems[0]"
                : "lineItems[1]";
        assertEquals("off", selenium.getValue("name=" + lineItemName + ".received"));
        assertEquals(ClientProperties.getAlternateReceiptQualityOption(),
                selenium.getSelectedLabel("name=" + lineItemName + ".receiptQuality"));
        assertEquals("Fully consumed", selenium.getSelectedLabel("name=" + lineItemName + ".disposition"));
        assertEquals("Specimen not in shipment", selenium.getValue("name=" + lineItemName + ".receiptNote"));
        clickAndWait("link=Home");
        assertFalse(selenium.isTextPresent("1 " + ClientProperties.getBiospecimenTerm() + " Request Requiring Action"));
        assertFalse(selenium.isElementPresent("link=1 " + ClientProperties.getBiospecimenTerm()
                + " Request Requiring Action"));
        clickAndWait("link=Sign Out");
    }

    private void updateShipmentStatus() {
        goToOrderAdminPage();
        clickAndWait("xpath=//table[@id='shipment']/tbody/tr[1]/td[1]/a");
        assertTrue(selenium.isTextPresent("Order Fulfillment"));
        assertTrue(selenium.isElementPresent("link=Mark As Shipped"));
        clickAndWait("link=Mark As Shipped");
        selenium.selectFrame("popupFrame");
        assertTrue(selenium.isTextPresent("Set Shipped Date"));
        clickAndWait("btn_setShipmentDate");
        pause(LONG_DELAY);
        selenium.selectWindow(null);
        waitForText("Shipment successfully marked as shipped.");
        assertTrue(selenium.isTextPresent("Receipt Quality"));
        int receiptQualityColumnIndex = ClientProperties.getReceiptQualityColumnIndex() + 1;
        String requestRowXPath = "xpath=//table[@id='request']/tbody/tr[1]/td[" + receiptQualityColumnIndex + "]";
        if (ClientProperties.isAggregateSearchResultsActive()) {
            requestRowXPath = "xpath=//tr[@id='aggregategeneral_data0']/td[" + receiptQualityColumnIndex + "]";
        }
        assertTrue(StringUtils.isBlank(selenium.getText(requestRowXPath)));
        requestRowXPath = "xpath=//table[@id='request']/tbody/tr[2]/td[" + receiptQualityColumnIndex + "]";
        if (ClientProperties.isAggregateSearchResultsActive()) {
            requestRowXPath = "xpath=//tr[@id='aggregatespecific_data0']/td[" + receiptQualityColumnIndex + "]";
        }
        assertTrue(StringUtils.isBlank(selenium.getText(requestRowXPath)));
        clickAndWait("link=Sign Out");
    }

    private void goToOrderAdminPage() {
        login(ClientProperties.getTissueTechEmail(), "tissueLocator1");
        goToOrderList();
    }

    private void viewBiospecimenQuality() {
        goToOrderAdminPage();
        selenium.select("status", "label=Received");
        waitForPageToLoad();
        clickAndWait("xpath=//table[@id='shipment']/tbody/tr[1]/td[1]/a");
        assertTrue(selenium.isTextPresent("Receipt Quality"));
        assertTrue(selenium.isTextPresent("Not Received, " + ClientProperties.getAlternateReceiptQualityOption()
                + ", Fully consumed, Specimen not in shipment"));
        assertTrue(selenium.isTextPresent("Received, " + ClientProperties.getReceiptQualityOption()
                + ", In use, Looks good"));
        assertTrue(selenium.isElementPresent("link=Print Invoice"));
    }

}
