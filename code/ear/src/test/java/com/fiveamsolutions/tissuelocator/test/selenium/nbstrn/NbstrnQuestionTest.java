/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.test.selenium.nbstrn;

import java.io.File;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import com.fiveamsolutions.tissuelocator.test.ClientProperties;
import com.fiveamsolutions.tissuelocator.test.selenium.AbstractTissueLocatorSeleniumTest;

/**
 * @author ddasgupta
 *
 */
public class NbstrnQuestionTest extends AbstractTissueLocatorSeleniumTest {

    private static final String REVIEW_BUTTON_XPATH = "xpath=//table[@id='questionResponse']/tbody/tr[1]/td[8]/a";
    private static final String ADMIN_REVIEW_BUTTON_XPATH = "xpath=//table[@id='question']/tbody/tr[%d]/td[8]/a";
    private static final String REVIEW_PAGE_TITLE = "Respond to Question";
    private static final String REVIEW_SUCCESS_MESSAGE = "Response to Question from Investigator successfully saved.";
    private static final String VIEW_LINK_XPATH = "xpath=//table[@id='%s']/tbody/tr[%d]/td[1]/a";
    private static final String BACK_BUTTON_XPATH = "xpath=//div[@class='topbtns']/a";
    private static final int RESPONSE_COUNT = 3;

    /**
     * Tests submission of questions to states.
     * @throws Exception on error
     */
    @Test
    public void testQuestion() throws Exception {
        goToQuestionPage();
        requiredValidation();
        copyRequestor();
        createSingleStateQuestion();
        createMultipleStateQuestion();
        institutionResponses();
        adminResponses();
        alternateInstitutionResponse();
        investigatorView();
        createFromDefault();
        verifyResourcesPage();
    }

    private void goToQuestionPage() {
        loginAsUser1();
        mouseOverAndPause("link=Research Support");
        clickAndWait("link=Submit Question to NBS Programs");
        assertTrue(selenium.isTextPresent("Submit Question to NBS Programs"));
        assertTrue(selenium.isTextPresent("Why submit a question?"));
        assertTrue(selenium.isTextPresent("Who will respond to my question?"));
        clickAndWait("xpath=//div[@id='secnav']/div/ul/li[2]/a");
        assertTrue(selenium.isTextPresent("Submit Question to NBS Programs"));
        assertTrue(selenium.isTextPresent("Why submit a question?"));
        assertTrue(selenium.isTextPresent("Who will respond to my question?"));
    }

    private void requiredValidation() {
        clearInvestigatorFields();
        clickAndWait("btn_submit");
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("First Name must be set"));
        assertTrue(selenium.isTextPresent("Last Name must be set"));
        assertTrue(selenium.isTextPresent("You must select a valid organization."));
        assertTrue(selenium.isTextPresent("Address must be set"));
        assertTrue(selenium.isTextPresent("City must be set"));
        assertTrue(selenium.isTextPresent("State must be set"));
        assertTrue(selenium.isTextPresent("Zip/Postal Code must be set"));
        assertTrue(selenium.isTextPresent("Email must be set"));
        assertTrue(selenium.isTextPresent("Phone must be set"));
        assertTrue(selenium.isTextPresent("At least one state must be selected"));
        assertTrue(selenium.isTextPresent("Your Question must be set"));
    }

    private void copyRequestor() {
        clearInvestigatorFields();
        selenium.click("changeresume");
        assertTrue(selenium.isVisible("newresume"));
        assertFalse(selenium.isVisible("existingresume"));
        selenium.type("resume", "invalid file name");
        selenium.click("copyinvestigator");
        pause(DELAY);
        selenium.click("copyinvestigator");
        pause(DELAY);
        String[] nameParts = StringUtils.split(ClientProperties.getResearcherName());
        assertEquals(nameParts[0], selenium.getValue("investigatorFirstName"));
        assertEquals(nameParts[1], selenium.getValue("investigatorLastName"));
        assertEquals("123 Main Street", selenium.getValue("investigatorLine1"));
        assertEquals("", selenium.getValue("investigatorLine2"));
        assertEquals("Anytown", selenium.getValue("investigatorCity"));
        assertEquals("Maryland", selenium.getSelectedLabel("investigatorState"));
        assertEquals("Maryland", selenium.getText("//div[@id='wwgrp_investigatorState']/button"));
        assertEquals("12345", selenium.getValue("investigatorZip"));
        assertEquals("United States", selenium.getSelectedLabel("investigatorCountry"));
        assertEquals("United States", selenium.getText("//div[@id='wwgrp_investigatorCountry']/button"));
        assertEquals(ClientProperties.getResearcherEmail(), selenium.getValue("investigatorEmail"));
        assertEquals("1234567890", selenium.getValue("investigatorPhone"));
        assertEquals(ClientProperties.getResearcherInstitution(), selenium.getValue("investigatorOrganization"));
        assertFalse(selenium.isVisible("newresume"));
        assertTrue(selenium.isVisible("existingresume"));
        assertTrue(StringUtils.isBlank(selenium.getValue("resume")));
    }

    private void createSingleStateQuestion() throws Exception {
        clickAndWait("xpath=//div[@id='secnav']/div/ul/li[2]/a");
        enterData("California");
        clickAndWait("btn_submit");
        assertTrue(selenium.isTextPresent("Question to States successfully submitted."));
        assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
        verifyViewPage("question", 1, 1, "Pending", "California");
        assertTrue(selenium.isTextPresent("1-1 of 1 Result"));    }

    private void createMultipleStateQuestion() throws Exception {
        clickAndWait("link=Submit Question to States");
        enterData("California", "Iowa");
        clickAndWait("btn_submit");
        assertTrue(selenium.isTextPresent("Question to States successfully submitted."));
        assertTrue(selenium.isTextPresent("1-2 of 2 Results"));
        verifyViewPage("question", 2, 2, "Pending", "California", "Iowa");
        assertTrue(selenium.isTextPresent("1-2 of 2 Results"));
        clickAndWait("link=Sign Out");
    }

    private void enterData(String... states) throws Exception {
        clearInvestigatorFields();
        selenium.type("investigatorFirstName", "test-first-name-investigator");
        selenium.type("investigatorLastName", "test-last-name-investigator");
        selenium.type("investigatorLine1", "test-line1-investigator");
        selenium.type("investigatorLine2", "test-line2-investigator");
        selenium.type("investigatorCity", "test-city-investigator");
        selenium.select("investigatorState", "label=Virginia");
        selenium.type("investigatorZip", "13579");
        selenium.select("investigatorCountry", "label=Canada");
        selenium.type("investigatorEmail", "test-email-investigator@email.com");
        selenium.type("investigatorPhone", "1234567890");
        selenium.type("investigatorPhoneExtension", "24680");
        String name = ClientProperties.getDefaultConsortiumMemberName();
        selectFromAutocomplete("investigatorOrganization", StringUtils.substringBefore(name, "'").toLowerCase(), name);

        String filePath = new File(ClassLoader.getSystemResource("test.properties").toURI()).toString();
        selenium.type("resume", filePath);
        filePath = new File(ClassLoader.getSystemResource("testui.properties.example").toURI()).toString();
        selenium.type("protocolDocument", filePath);

        for (String state : states) {
            selenium.addSelection("selectedInstitutions", "label=" + state);
        }
        selenium.type("questionText", "test question text");
    }

    private void verifyViewPage(String tableId, int linkIndex, int rowCount, String... additionalText) {
        clickAndWait(String.format(VIEW_LINK_XPATH, tableId, linkIndex));

        assertTrue(selenium.isTextPresent("Question to NBS Programs"));
        assertTrue(selenium.isTextPresent("Question"));
        assertTrue(selenium.isTextPresent("From Investigator"));
        assertTrue(selenium.isTextPresent("test-first-name-investigator test-last-name-investigator"));
        assertTrue(selenium.isTextPresent("California"));
        assertTrue(selenium.isTextPresent("test-email-investigator@email.com"));
        assertTrue(selenium.isTextPresent("1234567890"));
        assertTrue(selenium.isTextPresent("ext."));
        assertTrue(selenium.isTextPresent("24680"));
        assertTrue(selenium.isTextPresent("test-line1-investigator"));
        assertTrue(selenium.isTextPresent("test-line2-investigator"));
        assertTrue(selenium.isTextPresent("test-city-investigator"));
        assertTrue(selenium.isTextPresent("Virginia,"));
        assertTrue(selenium.isTextPresent("13579"));
        assertTrue(selenium.isTextPresent("Canada"));

        assertTrue(selenium.isTextPresent("Resume/CV/Biosketch"));
        assertTrue(selenium.isTextPresent("test.properties"));
        assertTrue(selenium.isElementPresent("link=test.properties"));
        assertTrue(selenium.isTextPresent("Protocol Document"));
        assertTrue(selenium.isTextPresent("testui.properties.example"));
        assertTrue(selenium.isElementPresent("link=testui.properties.example"));

        assertTrue(selenium.isTextPresent("Question"));
        assertTrue(selenium.isTextPresent("test question text"));

        for (String text : additionalText) {
            assertTrue(selenium.isTextPresent(text));
        }

        assertTrue(selenium.isTextPresent("Response"));
        assertTrue(selenium.isTextPresent("Status"));
        assertTrue(selenium.isTextPresent("State"));
        assertTrue(selenium.isTextPresent("Reviewer Name"));
        assertTrue(selenium.isTextPresent("Updated"));
        assertTrue(selenium.isTextPresent("Status"));
        assertTrue(selenium.isTextPresent("Message"));

        String rowXPath = "xpath=//table[@class='data']/tbody/tr[%d]";
        assertTrue(selenium.isElementPresent(String.format(rowXPath, rowCount)));
        assertFalse(selenium.isElementPresent(String.format(rowXPath, rowCount + 1)));
        clickAndWait(BACK_BUTTON_XPATH);
    }

    private void institutionResponses() {
        login(ClientProperties.getInstitutionalAdminEmail(), PASSWORD);
        assertTrue(selenium.isElementPresent("link=2 Question Responses"));
        clickAndWait("link=2 Question Responses");
        assertTrue(selenium.isTextPresent("Questions to States"));
        assertTrue(selenium.isTextPresent("1-2 of 2 Results"));
        verifyViewPage("questionResponse", 1, 1, "Pending", "California");

        clickAndWait(REVIEW_BUTTON_XPATH);
        assertTrue(selenium.isTextPresent(REVIEW_PAGE_TITLE));
        clickAndWait("btn_save_offline");
        assertTrue(selenium.isTextPresent("Requests"));
        assertTrue(selenium.isTextPresent("Questions to States"));
        assertTrue(selenium.isTextPresent(REVIEW_SUCCESS_MESSAGE));
        assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
        selenium.select("status", "Responded Off-line");
        waitForPageToLoad();
        verifyViewPage("questionResponse", 1, 1, "Fully Responded", "Responded Off-line", "California",
                ClientProperties.getInstitutionalAdminName());
        selenium.select("status", "Pending");
        waitForPageToLoad();

        verifyViewPage("questionResponse", 1, 2, "Pending", "California", "Iowa");
        clickAndWait(REVIEW_BUTTON_XPATH);
        assertTrue(selenium.isTextPresent(REVIEW_PAGE_TITLE));
        clickAndWait("btn_save_online");
        assertTrue(selenium.isTextPresent(REVIEW_PAGE_TITLE));
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("Message must be set."));
        selenium.type("response", "test online response comment");
        clickAndWait("btn_save_online");
        assertTrue(selenium.isTextPresent("Requests"));
        assertTrue(selenium.isTextPresent("Questions to States"));
        assertTrue(selenium.isTextPresent(REVIEW_SUCCESS_MESSAGE));
        assertTrue(selenium.isTextPresent("Nothing found to display."));
        assertTrue(selenium.isTextPresent("0 Results found."));
        selenium.select("status", "Responded");
        waitForPageToLoad();
        verifyViewPage("questionResponse", 1, 2, "Partially Responded", "Responded", "Pending", "California", "Iowa",
                "test online response comment", ClientProperties.getInstitutionalAdminName());
        clickAndWait("link=Sign Out");
    }

    private void adminResponses() {
        loginAsAdmin();
        mouseOverAndPause("link=Administration");
        clickAndWait("link=Request Administration");
        waitForPageToLoad();
        assertTrue(selenium.isTextPresent("Requests"));
        assertTrue(selenium.isTextPresent("DBS Specimens"));
        clickAndWait("link=Questions to States");
        assertTrue(selenium.isTextPresent("Questions to States"));
        assertTrue(selenium.isTextPresent("1-2 of 2 Results"));

        verifyViewPage("question", 1, 1, "Fully Responded", "Responded Off-line", "California",
                ClientProperties.getInstitutionalAdminName());
        clickAndWait(String.format(ADMIN_REVIEW_BUTTON_XPATH, 1));
        assertTrue(selenium.isTextPresent(REVIEW_PAGE_TITLE));
        clickAndWait("btn_save_online");
        assertTrue(selenium.isTextPresent(REVIEW_PAGE_TITLE));
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("Message must be set."));
        selenium.type("response", "test online admin response comment");
        clickAndWait("btn_save_online");
        assertTrue(selenium.isTextPresent("Requests"));
        assertTrue(selenium.isTextPresent("Questions to States"));
        assertTrue(selenium.isTextPresent(REVIEW_SUCCESS_MESSAGE));
        assertTrue(selenium.isTextPresent("1-2 of 2 Results"));
        verifyViewPage("question", 1, 2, "Fully Responded", "Responded", "Responded Off-line", "California",
                "test online admin response comment", ClientProperties.getInstitutionalAdminName());

        clickAndWait(String.format(ADMIN_REVIEW_BUTTON_XPATH, 1));
        assertFalse(selenium.isElementPresent("response"));
        assertFalse(selenium.isElementPresent("btn_save_online"));
        assertFalse(selenium.isElementPresent("btn_save_offline"));
        clickAndWait(BACK_BUTTON_XPATH);
        assertTrue(selenium.isTextPresent("Requests"));
        assertTrue(selenium.isTextPresent("Questions to States"));
        assertEquals("All", selenium.getSelectedLabel("status"));
        assertTrue(selenium.isTextPresent("1-2 of 2 Results"));

        verifyViewPage("question", 2, 2, "Partially Responded", "Responded", "Pending", "California", "Iowa",
                "test online response comment", ClientProperties.getInstitutionalAdminName());
        clickAndWait(String.format(ADMIN_REVIEW_BUTTON_XPATH, 2));
        assertTrue(selenium.isTextPresent(REVIEW_PAGE_TITLE));
        selenium.type("response", "test offline admin response comment");
        clickAndWait("btn_save_offline");
        assertTrue(selenium.isTextPresent("Requests"));
        assertTrue(selenium.isTextPresent("Questions to States"));
        assertTrue(selenium.isTextPresent(REVIEW_SUCCESS_MESSAGE));
        assertTrue(selenium.isTextPresent("1-2 of 2 Results"));
        verifyViewPage("question", 2, RESPONSE_COUNT, "Partially Responded", "Responded", "Pending",
                "Responded Off-line", "California", "Iowa", "test online response comment",
                "test offline admin response comment", ClientProperties.getInstitutionalAdminName());
        clickAndWait("link=Sign Out");
    }

    private void alternateInstitutionResponse() {
        login(ClientProperties.getAlternateInstitutionalAdminEmail(), PASSWORD);
        assertTrue(selenium.isElementPresent("link=1 Question Response"));
        clickAndWait("link=1 Question Response");
        assertTrue(selenium.isTextPresent("Questions to States"));
        assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
        verifyViewPage("questionResponse", 1, RESPONSE_COUNT, "Partially Responded", "Responded", "Pending",
                "Responded Off-line", "California", "Iowa", "test online response comment",
                "test offline admin response comment", ClientProperties.getInstitutionalAdminName());

        clickAndWait(REVIEW_BUTTON_XPATH);
        assertTrue(selenium.isTextPresent(REVIEW_PAGE_TITLE));
        clickAndWait(BACK_BUTTON_XPATH);
        assertTrue(selenium.isTextPresent("Requests"));
        assertTrue(selenium.isTextPresent("Questions to States"));
        assertEquals("Pending", selenium.getSelectedLabel("status"));
        assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
        clickAndWait(REVIEW_BUTTON_XPATH);
        assertTrue(selenium.isTextPresent(REVIEW_PAGE_TITLE));
        assertTrue(selenium.isTextPresent("test online response comment"));
        assertTrue(selenium.isTextPresent("Responded"));
        assertTrue(selenium.isTextPresent(ClientProperties.getInstitutionalAdminName()));
        assertTrue(selenium.isTextPresent("test offline admin response comment"));
        assertTrue(selenium.isTextPresent("Responded Off-line"));
        selenium.type("response", "test offline response comment");
        clickAndWait("btn_save_offline");
        assertTrue(selenium.isTextPresent("Requests"));
        assertTrue(selenium.isTextPresent("Questions to States"));
        assertTrue(selenium.isTextPresent(REVIEW_SUCCESS_MESSAGE));
        assertTrue(selenium.isTextPresent("Nothing found to display."));
        assertTrue(selenium.isTextPresent("0 Results found."));
        selenium.select("status", "Responded Off-line");
        waitForPageToLoad();
        verifyViewPage("questionResponse", 1, RESPONSE_COUNT, "Fully Responded", "Responded", "Responded Off-line",
                "California", "Iowa", "test online response comment", "test offline admin response comment",
                ClientProperties.getInstitutionalAdminName());
        clickAndWait("link=Sign Out");
    }

    private void investigatorView() {
        loginAsUser1();
        clickAndWait("link=My Requests");
        assertTrue(selenium.isTextPresent("My Requests"));
        assertTrue(selenium.isTextPresent("DBS Specimens"));
        clickAndWait("link=Questions to States");
        assertTrue(selenium.isTextPresent("Questions to States"));
        assertTrue(selenium.isTextPresent("1-2 of 2 Results"));
        verifyViewPage("question", 1, 2, "Fully Received", "Received", "Received Off-line", "California",
                "test online admin response comment", ClientProperties.getInstitutionalAdminName());
        verifyViewPage("question", 2, RESPONSE_COUNT, "Fully Received", "Received", "Received Off-line",
                "California", "Iowa", "test online response comment", "test offline admin response comment",
                ClientProperties.getInstitutionalAdminName());
     }

    private void clearInvestigatorFields() {
        selenium.type("investigatorFirstName", "");
        selenium.type("investigatorLastName", "");
        selenium.type("investigatorLine1", "");
        selenium.type("investigatorLine2", "");
        selenium.type("investigatorCity", "");
        selenium.select("investigatorState", "label=- Select -");
        selenium.type("investigatorZip", "");
        selenium.select("investigatorCountry", "label=United States");
        selenium.type("investigatorEmail", "");
        selenium.type("investigatorPhone", "");
        selenium.type("investigatorPhoneExtension", "");
        selenium.type("investigatorOrganization", "");
        selenium.type("resume", "");
    }

    private void createFromDefault() throws Exception {
        mouseOverAndPause("link=Research Support");
        clickAndWait("link=Submit Question to NBS Programs");
        assertTrue(selenium.isTextPresent("Submit Question to NBS Programs"));
        selenium.type("investigatorLastName", "test-last-name-investigator");
        String filePath = new File(ClassLoader.getSystemResource("testui.properties.example").toURI()).toString();
        selenium.type("protocolDocument", filePath);
        selenium.addSelection("selectedInstitutions", "label=California");
        selenium.type("questionText", "test question text");
        clickAndWait("btn_submit");
        assertTrue(selenium.isTextPresent("Question to States successfully submitted."));
        assertTrue(selenium.isTextPresent("1-3 of 3 Results"));
    }

    private void verifyResourcesPage() {
        mouseOverAndPause("link=Research Support");
        clickAndWait("link=Resources");
        assertTrue(selenium.isTextPresent("Resources"));
        assertTrue(selenium.isTextPresent("Grant"));
        assertTrue(selenium.isTextPresent("State IRB"));
        assertTrue(selenium.isTextPresent("Investigator Collaboration"));
        clickAndWait("xpath=//div[@id='secnav']/div/ul/li[1]/a");
        assertTrue(selenium.isTextPresent("Resources"));
        assertTrue(selenium.isTextPresent("Grant"));
        assertTrue(selenium.isTextPresent("State IRB"));
        assertTrue(selenium.isTextPresent("Investigator Collaboration"));
    }
}
