/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.test.selenium;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.xwork.time.DateUtils;

/**
 * @author ddasgupta
 *
 */
public class AbstractReportTest extends AbstractTissueLocatorSeleniumTest {

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getScriptName() {
        return "UsageReportTest.sql";
    }

    /**
     * Go to the report page.
     * @param reportName the name of the report.
     */
    protected void goToReportPage(String reportName) {
        loginAsAdmin();
        mouseOverAndPause("link=Reports");
        clickAndWait("link=" + reportName);
        assertTrue(selenium.isTextPresent(reportName));

        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        String startDate = sdf.format(DateUtils.addMonths(new Date(), -1));
        String endDate = sdf.format(new Date());
        assertEquals(startDate, selenium.getValue("startDate"));
        assertEquals(endDate, selenium.getValue("endDate"));
        assertTrue(selenium.isTextPresent("Report for " + startDate + " - " + endDate));
    }

    /**
     * Change date range to be from 1/1 to 2/1 of a given year.
     * @param year the year
     */
    protected void changeDates(String year) {
        selenium.type("startDate", "01/01/" + year);
        selenium.type("endDate", "02/01/" + year);
        clickAndWait("reportDateRangeForm_btn_save");
        assertTrue(selenium.isTextPresent("Reports"));
        assertEquals("01/01/" + year, selenium.getValue("startDate"));
        assertEquals("02/01/" + year, selenium.getValue("endDate"));
        assertTrue(selenium.isTextPresent("Report for 01/01/" + year + " - 02/01/" + year));
    }
}
