/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.test.selenium.main;

import java.text.ParseException;

import org.apache.commons.lang.StringUtils;

import com.fiveamsolutions.tissuelocator.test.ClientProperties;
import com.fiveamsolutions.tissuelocator.test.selenium.AbstractListTest;

/**
 * @author smiller
 *
 */
public class InstitutionListTest extends AbstractListTest {

    private static final int COL_COUNT = 3;
    private static final int RESULT_COUNT = 54;
    private static final int BUTTON_COL = 4;

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getScriptName() {
        return "InstitutionListTest.sql";
    }

    /**
     * test the user list page.
     * @throws ParseException on error
     */
    public void testList() throws ParseException {
        loginAsAdmin();
        goToListPage();
        nameFilter();
        consortiumFilter();
        verifyExportPresent();
        sorting(COL_COUNT, "institution", false);
        paging("institution");
        pageSize(RESULT_COUNT + ClientProperties.getDefaultInstitutionCount());
        institutionRestriction("institution", "Institution", null, 0, BUTTON_COL, true, false);
    }

    private void goToListPage() {
        mouseOverAndPause("link=Administration");
        clickAndWait("link=Institution Administration");
        assertTrue(selenium.isTextPresent("Institution Administration"));
    }

    private void nameFilter() {
        String name  = selenium.getTable("institution.1.0");
        selenium.type("name", StringUtils.substring(name, 0, name.length() - 1));
        clickAndWait("btn_search");
        assertTrue(selenium.isTextPresent("1 of 1 Page"));
        selenium.type("name", StringUtils.substring(name, 2, name.length()));
        clickAndWait("btn_search");
        assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
        assertTrue(selenium.isTextPresent("1 of 1 Page"));
        selenium.type("name", "");
        clickAndWait("btn_search");
        assertTrue(selenium.isTextPresent("1 of " + getPages() + " Pages"));
    }

    private void consortiumFilter() {
        goToListPage();
        clickAndWait("consortiumMembers");
        assertTrue(selenium.isTextPresent("1 of 1 Page"));
        clickAndWait("consortiumMembers");
        assertTrue(selenium.isTextPresent("1 of " + getPages() + " Pages"));
    }

    private int getPages() {
        int totalInstitutions = RESULT_COUNT + ClientProperties.getDefaultInstitutionCount();
        return (int) Math.ceil((double) totalInstitutions / (double) DEFAULT_PAGE_SIZE);
    }
}
