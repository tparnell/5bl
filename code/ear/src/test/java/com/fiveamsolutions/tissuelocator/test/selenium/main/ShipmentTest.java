/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.test.selenium.main;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import com.fiveamsolutions.tissuelocator.test.ClientProperties;
import com.fiveamsolutions.tissuelocator.test.selenium.AbstractTissueLocatorSeleniumTest;

/**
 * @author smiller
 *
 */
public class ShipmentTest extends AbstractTissueLocatorSeleniumTest {

    /**
     * Delay.
     */
    private static final int DELAY = 4000;

    private static final String REQUEST_TABLE_XPATH = "xpath=//table[@id='%srequest']/tbody/tr[%d]/td[2]";
    private static final String AGGREGATE_TABLE_XPATH = "xpath=//tr[@id='aggregate%s_data%d']/td[2]";

    /**
     * Count.
     */
    private int count = 0;

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getScriptName() {
        return "ShipmentTest.sql";
    }

    /**
     * test the edit page.
     * @throws Exception on error
     */
    @Test
    public void testEdit() throws Exception {
        if (ClientProperties.isOrderAdministrationActive()) {
            String[] lineItemPrefixes = ClientProperties.isAggregateSearchResultsActive()
                ?  new String[] {"specificLineItems", "generalLineItems"}
                : new String[] {"lineItems"};
            setCount(2);
            goToEditPage();
            reviewDetails();
            formatValidation(lineItemPrefixes);
            assertLineItemCount(true);
            lineItemPrefixes = ClientProperties.isAggregateSearchResultsActive()
            ?  new String[] {"specificLineItems[0]", "generalLineItems[0]"}
            : new String[] {"lineItems[0]", "lineItems[1]"};
            setPrices(lineItemPrefixes);
            if (ClientProperties.isSimpleSearchEnabled()) {
                verifySimpleSearchWindow();
            }
            addAllItems();
            assertLineItemCount(true);
            removeItem();
            emptyOrder();
            addItem();
            if (!ClientProperties.isAggregateSearchResultsActive()) {
                addItem();
            }
            verifySingleSearchResultIgnored();
            editPage();
            verifyEdit(true);
            if (ClientProperties.isDisplayShipmentBillingRecipient()) {
                clickAndWait("xpath=//table[@id='shipment']/tbody/tr[1]/td[1]/a");
                removeBillingRecipient();
                verifyEdit(false);
            }
            markAsShipped();
            cancel();
        }
    }

    /**
     * Tests line item item.
     * @param hasSpecific Whether or not there are specific line items in the cart.
     */
    private void assertLineItemCount(boolean hasSpecific) {
        if (ClientProperties.isAggregateSearchResultsActive()) {
            assertAggregateLineItemCount(hasSpecific);
        } else {
            assertIndividualLineItemCount();
        }
    }

    private void assertIndividualLineItemCount() {

        if (getCount() > 0) {
            assertTrue(selenium.isElementPresent(String.format(getIndividualRequestTableXpath(getCount()))));
        }
        assertFalse(
                selenium.isElementPresent(getIndividualRequestTableXpath(getCount() + 1)));
    }

    private void assertAggregateLineItemCount(boolean hasSpecific) {

        if (getCount() > 0) {
            if (hasSpecific) {
                assertTrue(selenium.isElementPresent(getSpecificRequestTableXpath(1)));
            }
            assertFalse(selenium.isElementPresent(getSpecificRequestTableXpath(hasSpecific ? 2 : 1)));
            for (int i = 1; i < (hasSpecific ? getCount() - 1 : getCount()); i++) {
                assertTrue(selenium.isElementPresent(getGeneralRequestTableXpath(i)));
            }
            assertFalse(selenium.isElementPresent(getGeneralRequestTableXpath(getCount() + 1)));
        } else {
            assertFalse(selenium.isElementPresent(getGeneralRequestTableXpath(getCount() + 1)));
            assertFalse(selenium.isElementPresent(getSpecificRequestTableXpath(getCount() + 1)));
        }

    }

    private String getIndividualRequestTableXpath(int rowNum) {
        return String.format(REQUEST_TABLE_XPATH, "", rowNum);
    }

    private String getSpecificRequestTableXpath(int rowNum) {
        return String.format(AGGREGATE_TABLE_XPATH, "specific", rowNum - 1);
    }

    private String getGeneralRequestTableXpath(int rowNum) {
        return String.format(AGGREGATE_TABLE_XPATH, "general", rowNum - 1);
    }
    /**
     * Tests format validation.
     * @param lineItemPrefixes prefix for line item input elements.
     */
    private void formatValidation(String[] lineItemPrefixes) {
        if (ClientProperties.isDisplayPrices()) {
            for (String lineItemPrefix : lineItemPrefixes) {
                selenium.type(lineItemPrefix + "[0].finalPrice", "-100.00");
                clickAndWait("btn_save");
                assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
                assertTrue(selenium.isTextPresent("Final Fee must be greater than or equal to 0"));

                selenium.type(lineItemPrefix + "[0].finalPrice", "100.0123");
                clickAndWait("btn_save");
                assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
                assertTrue(selenium.isTextPresent("Final Fee should contain at most 2 decimal digits"));

                selenium.type("price", "-100");
                selenium.type("fees", "-100");
                clickAndWait("btn_save");
                assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
                assertTrue(selenium.isTextPresent("Shipping Price must be greater than or equal to 0"));
                assertTrue(selenium.isTextPresent("Fees must be greater than or equal to 0"));

                selenium.type("price", "100.123");
                selenium.type("fees", "100.123");
                clickAndWait("btn_save");
                assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
                assertTrue(selenium.isTextPresent("Shipping Price should contain at most 2 decimal digits"));
                assertTrue(selenium.isTextPresent("Fees should contain at most 2 decimal digits"));

                clickAndWait("link=Back To List");
                clickAndWait("xpath=//table[@id='shipment']/tbody/tr[1]/td[1]/a");
                assertTrue(selenium.isTextPresent("Order Fulfillment"));
            }
        }
    }

    /**
     * Go to edit page.
     */
    private void goToEditPage() {
        login(ClientProperties.getTissueTechEmail(), "tissueLocator1");
        goToOrderList();
        clickAndWait("xpath=//table[@id='shipment']/tbody/tr[1]/td[1]/a");
        assertTrue(selenium.isTextPresent("Order Fulfillment"));
    }

    /**
     * Review details page.
     */
    private void reviewDetails() {
        assertTrue(selenium.isElementPresent("link=View Review Details"));
        clickAndWait("link=View Review Details");
        assertTrue(selenium.isTextPresent("Order Fulfillment"));
        assertTrue(selenium.isTextPresent("(Approved)"));
        assertTrue(selenium.isTextPresent("(Pending Shipment)"));
        String dateString = new SimpleDateFormat("MMMMM dd, yyyy").format(new Date());
        String comment = "This is a good study. Full institutional approval granted.";
        if (ClientProperties.isAggregateSearchResultsActive()) {
            String cellXpath = "xpath=//tr[@id='%s_data0']/td[%d]";
            int index = 1;
            assertTrue(selenium.isTextPresent("Specific DBS Specimens"));
            assertEquals("Sex: Male", selenium.getText(String.format(cellXpath, "specific", index++)));
            assertEquals("600", selenium.getText(String.format(cellXpath, "specific", index++)));
            assertEquals("California, Admin", selenium.getText(String.format(cellXpath, "specific", index++)));
            assertEquals("Approved", selenium.getText(String.format(cellXpath, "specific", index++)));
            assertTrue(selenium.getText(String.format(cellXpath, "specific", index++)).contains(dateString));
            assertEquals(comment, selenium.getText(String.format(cellXpath, "specific", index++)));

            index = 1;
            assertTrue(selenium.isTextPresent("General Population Requests"));
            assertEquals("Birth Year: 2005", selenium.getText(String.format(cellXpath, "general", index++)));
            assertEquals("500", selenium.getText(String.format(cellXpath, "general", index++)));
            assertEquals("California, Admin", selenium.getText(String.format(cellXpath, "general", index++)));
            assertEquals("Approved", selenium.getText(String.format(cellXpath, "general", index++)));
            assertTrue(selenium.getText(String.format(cellXpath, "general", index++)).contains(dateString));
            assertEquals(comment, selenium.getText(String.format(cellXpath, "general", index++)));
        } else {
            assertTrue(selenium.isTextPresent("Review Details"));
            assertTrue(selenium.isTextPresent("Reviewer"));
            assertTrue(selenium.isTextPresent(ClientProperties.getInstitutionalAdminName()));
            assertTrue(selenium.isTextPresent("Vote"));
            assertTrue(selenium.isTextPresent("Approved"));
            assertTrue(selenium.isTextPresent("Date"));
            assertTrue(selenium.isTextPresent("Comments"));
            assertTrue(selenium.isTextPresent(dateString));
            assertTrue(selenium.isTextPresent(comment));
        }
        assertTrue(selenium.isElementPresent("link=Go Back To Order Administration"));
        clickAndWait("link=Go Back To Order Administration");
    }

    /**
     * Set prices.
     * @param lineItemPrefixes prefix for line item input elements.
     */
    private void setPrices(String[] lineItemPrefixes) {
        if (ClientProperties.isDisplayPrices()) {
            assertFalse(selenium.isTextPresent("150.50"));
            selenium.type(lineItemPrefixes[0] + ".finalPrice", "100.25");
            selenium.type(lineItemPrefixes[1] + ".finalPrice", "50.25");
            selenium.type("price", "11.11");
            selenium.type("fees", "222.22");
            clickAndWait("btn_save");
            assertTrue(selenium.isTextPresent("Order pricing successfully saved."));
            assertTrue(selenium.isTextPresent("383"));
            assertEquals("100.25", selenium.getValue(lineItemPrefixes[0] + ".finalPrice"));
            assertEquals("50.25", selenium.getValue(lineItemPrefixes[1] + ".finalPrice"));
            assertEquals("11.11", selenium.getValue("price"));
            assertEquals("222.22", selenium.getValue("fees"));
        }
    }

    private void verifySimpleSearchWindow() {
        clickAndWait("addToOrder_btn");
        selenium.selectFrame("popupFrame");
        clickAndWait("simpleSearch");
        assertFalse(selenium.isElementPresent("Link=Home"));
        assertFalse(selenium.isElementPresent("Link=Account"));
        assertTrue(selenium.isElementPresent("filters"));
        clickAndWait("simpleSearch");
        assertFalse(selenium.isElementPresent("Link=Home"));
        assertFalse(selenium.isElementPresent("Link=Account"));
        assertTrue(selenium.isElementPresent("filters"));
        selenium.selectWindow(null);
        clickAndWait("xpath=//div[@id='popupControls']/a");
    }

    /**
     * Add all line items.
     */
    private void addAllItems() {
        int numAdded = ClientProperties.isAggregateSearchResultsActive() ? 1 : 2;
        clickAndWait("addToOrder_btn");
        if (ClientProperties.getFluidSpecimenCount() > 0) {
            showOnlyTestResults();
        }
        clickAndWait("btn_search");
        pause(DELAY);
        if (!ClientProperties.isAggregateSearchResultsActive()) {
            selenium.click("selectall");
        } else {
            String textFieldXPath = "xpath=//table[@id='specimen']/tbody/tr[1]/td[3]/input[3]";
            selenium.type(textFieldXPath, "2");
            selenium.keyUp(textFieldXPath, "2");
        }
        pause(DELAY);
        clickAndWait("btn_addtoorder");
        pause(DELAY);
        assertTrue(selenium.isTextPresent("Added "
                + numAdded + " " + ClientProperties.getSpecimenTermLowerCase()
                + "(s) to this shipment."));
        count += numAdded;
    }

    /**
     * Add line item.
     */
    private void addItem() {
        String addLocator = ClientProperties.isAggregateSearchResultsActive() ? "btn_addtoorder"
                : "Link=Add To Order";
        clickAndWait("addToOrder_btn");
        clickAndWait("btn_search");
        if (ClientProperties.isAggregateSearchResultsActive()) {
            selenium.type("xpath=//table[@id='specimen']/tbody/tr[1]/td[3]/input[3]", "1");
            selenium.keyUp("xpath=//table[@id='specimen']/tbody/tr[1]/td[3]/input[3]", "1");
        } else {
            clickAndWait("xpath=//table[@id='specimen']/tbody/tr[1]/td[2]/a");
        }
        pause(LONG_DELAY);
        assertTrue("Add button not found, expected: " + addLocator
                + ", body text: " + selenium.getBodyText(), selenium.isElementPresent(addLocator));
        clickAndWait(addLocator);
        count += 1;
        pause(DELAY);
        selenium.selectWindow(null);
        assertLineItemCount(false);
        assertTrue(selenium.isTextPresent("Added 1 "
                + ClientProperties.getSpecimenTermLowerCase()
                + "(s) to this shipment."));
    }

    private void verifySingleSearchResultIgnored() {
        clickAndWait("addToOrder_btn");
        if (ClientProperties.isAggregateSearchResultsActive()) {
            selenium.select("multiSelectParamMap['externalIdAssigner.id']", "label=California");
        } else {
            clickAndWait("btn_search");
            String externalId = selenium.getTable("specimen.1.1");
            selenium.type("externalId", externalId);
        }
        clickAndWait("btn_search");
        selenium.selectFrame("popupFrame");
        assertTrue(selenium.isElementPresent("xpath=//table[@id='specimen']/tbody/tr[1]/td[2]"));
        assertFalse(selenium.isElementPresent("xpath=//table[@id='specimen']/tbody/tr[2]"));
        clickAndWait("btn_addtoorder");
        pause(LONG_DELAY);
        selenium.selectWindow(null);
        assertTrue(selenium.isTextPresent("Added 0 "
                + ClientProperties.getSpecimenTermLowerCase()
                + "(s) to this shipment."));
    }

    /**
     * Remove line item.
     */
    private void removeItem() {
        String removeTerm = ClientProperties.getSpecimenTermLowerCase();
        String removeLocator = ClientProperties.isAggregateSearchResultsActive() ? "aggregategeneral_remove0"
                : "link=Remove";
        clickAndWait(removeLocator);
        count -= 1;
        assertLineItemCount(true);
        assertTrue(selenium.isTextPresent("Removed 1 " + removeTerm
                + " from this shipment."));
    }

    /**
     * Empty order.
     */
    private void emptyOrder() {
        clickAndWait("link=Remove All");
        assertTrue(selenium.isTextPresent("Removed " + getCount() + " " + ClientProperties.getSpecimenTermLowerCase()
                + "(s) from this shipment."));
        if (ClientProperties.isAggregateSearchResultsActive()) {
            assertTrue(selenium.isTextPresent("Specific " + ClientProperties.getBiospecimenTerm() + "s"));
            assertTrue(selenium.isTextPresent("Not Specified"));
            assertTrue(selenium.isTextPresent(ClientProperties.getProspectiveCollectionTerm() + "s"));
            assertTrue(selenium.isTextPresent("Not Applicable"));
        } else {
            assertTrue(selenium.isTextPresent("No " + ClientProperties.getBiospecimenTerm() + "s Selected."));
        }

        count = 0;
        assertLineItemCount(false);
    }

    /**
     * Edit shipment.
     */
    private void editPage() throws Exception {
        boolean isMtaActive = ClientProperties.isMtaActive();
        selenium.select("sv", "FedEx");
        selenium.type("tn", "FEDEX");
        selenium.type("si", "test instructions edit");
        selenium.type("recipientFirstName", "test first edit");
        selenium.type("recipientLastName", "test last edit");
        selenium.type("recipientLine1", "123 edit street");
        selenium.type("recipientLine2", "test");

        //Clear out the old value first
        selenium.type("recipientOrganization", "");
        String instName =
            StringUtils.substringBefore(ClientProperties.getDefaultConsortiumMemberName(), "'").toLowerCase();
        selectFromAutocomplete("recipientOrganization", instName, ClientProperties.getDefaultConsortiumMemberName());

        boolean billingRecipientDisplayed = ClientProperties.isDisplayShipmentBillingRecipient();
        assertEquals(billingRecipientDisplayed, selenium.isTextPresent("Billing Address"));
        if (billingRecipientDisplayed) {
            assertFalse(selenium.isChecked("includeBillingRecipient"));
            assertTrue(selenium.isChecked("noBillingRecipient"));
            assertFalse(selenium.isVisible("billingRecipient"));
            selenium.click("includeBillingRecipient");
            enterPersonData("billingRecipient", "Georgia", "France",
                    ClientProperties.getShipmentRecipientInstitution(), 2);
            assertTrue(selenium.isVisible("billingRecipient"));
        }

        selenium.type("notes", "test notes");
        String requestorName = ClientProperties.getResearcherInstitution();
        String senderName = ClientProperties.getDefaultConsortiumMemberName();
        assertEquals(isMtaActive, selenium.isTextPresent("There is no valid MTA for the requestor: " + requestorName));
        assertEquals(isMtaActive, selenium.isElementPresent("link=Sender: " + senderName + " signed ABC MTA"));
        if (isMtaActive) {
            String amendmentPath =
                new File(ClassLoader.getSystemResource("testui.properties.example").toURI()).toString();
            selenium.type("mtaAmendment", amendmentPath);
        }


        selenium.check("readyForResearcherReview");
        clickAndWait("btn_save_shipment");
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("Shipment successfully saved."));
        clickAndWait("link=Back To List");
    }

    /**
     * Verify the edit.
     * @param billingRecipientIncluded If a billing recipient is expected.
     */
    private void verifyEdit(boolean billingRecipientIncluded) {
        boolean isMtaActive = ClientProperties.isMtaActive();
        clickAndWait("xpath=//table[@id='shipment']/tbody/tr[1]/td[1]/a");
        assertTrue(selenium.isTextPresent("Shipping Vendor"));
        assertEquals("FEDEX", selenium.getValue("sv"));
        assertTrue(selenium.isTextPresent("Tracking Number"));
        assertEquals("FEDEX", selenium.getValue("tn"));
        assertTrue(selenium.isTextPresent("Shipping Instructions"));
        assertEquals("test instructions edit", selenium.getValue("si"));

        assertTrue(selenium.isTextPresent("First Name"));
        assertEquals("test first edit", selenium.getValue("recipientFirstName"));
        assertTrue(selenium.isTextPresent("Last Name"));
        assertEquals("test last edit", selenium.getValue("recipientLastName"));
        assertTrue(selenium.isTextPresent("Address"));
        assertEquals("123 edit street", selenium.getValue("recipientLine1"));
        assertEquals("test", selenium.getValue("recipientLine2"));
        assertEquals(ClientProperties.getDefaultConsortiumMemberName(), selenium.getValue("recipientOrganization"));

        if (billingRecipientIncluded && ClientProperties.isDisplayShipmentBillingRecipient()) {
            assertEquals("test-first-name-billingRecipient", selenium.getValue("billingRecipientFirstName"));
            assertEquals("test-last-name-billingRecipient", selenium.getValue("billingRecipientLastName"));
            assertEquals("test-line1-billingRecipient", selenium.getValue("billingRecipientLine1"));
            assertEquals("test-line2-billingRecipient", selenium.getValue("billingRecipientLine2"));
            assertEquals(ClientProperties.getShipmentRecipientInstitution(),
                    selenium.getValue("billingRecipientOrganization"));
        }

        assertEquals("test notes", selenium.getValue("notes"));

        String requestorName = ClientProperties.getResearcherInstitution();
        String senderName = ClientProperties.getDefaultConsortiumMemberName();
        assertEquals(isMtaActive, selenium.isElementPresent("link=Requestor: " + requestorName + " signed ABC MTA"));
        assertEquals(isMtaActive, selenium.isElementPresent("link=Sender: " + senderName + " signed ABC MTA"));

        if (isMtaActive) {
            String fileTarget = selenium.getAttribute("link=Requestor: " + requestorName + " signed ABC MTA@href");
            assertTrue(Pattern.matches(".*/protected/downloadFile[^/]*.*text[^/]*plain[^/]*.*", fileTarget));
            fileTarget = selenium.getAttribute("link=Sender: " + senderName + " signed ABC MTA@href");
            assertTrue(Pattern.matches(".*/protected/downloadFile[^/]*.*text[^/]*plain[^/]*.*", fileTarget));
            fileTarget = selenium.getAttribute("link=MTA Amendment@href");
            assertTrue(Pattern.matches(".*/protected/downloadFile[^/]*.*testui.properties.example[^/]*.*", fileTarget));
        }

        assertTrue(selenium.isChecked("readyForResearcherReview"));
        clickAndWait("link=Back To List");
    }

    private void removeBillingRecipient() {
        assertTrue(selenium.isChecked("includeBillingRecipient"));
        selenium.click("noBillingRecipient");
        pause(SHORT_DELAY);
        assertFalse(selenium.isVisible("billingRecipient"));
        clickAndWait("btn_save_shipment");
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("Shipment successfully saved."));
        clickAndWait("link=Back To List");
    }

    /**
     * Mark as shipped.
     */
    private void markAsShipped() {
        boolean isMtaActive = ClientProperties.isMtaActive();
        clickAndWait("xpath=//table[@id='shipment']/tbody/tr[1]/td[1]/a");
        assertTrue(selenium.isElementPresent("link=Mark As Shipped"));
        clickAndWait("link=Mark As Shipped");
        selenium.selectFrame("popupFrame");
        assertTrue(selenium.isTextPresent("Set Shipped Date"));
        selenium.type("shipmentDate", "01/01/2020");
        clickAndWait("btn_setShipmentDate");
        waitForElementById("study_details");
        assertTrue(selenium.isTextPresent("Shipment successfully marked as shipped."));
        if (!ClientProperties.isAggregateSearchResultsActive()) {
            assertTrue(selenium.isTextPresent("Receipt Quality"));
            if (getCount() > 0) {
                int receiptQualityColumnIndex = ClientProperties.getReceiptQualityColumnIndex();
                assertTrue(StringUtils.isBlank(selenium.getTable("request.1." + receiptQualityColumnIndex)));
                assertTrue(StringUtils.isBlank(selenium.getTable("request.2." + receiptQualityColumnIndex)));
            }
        }
        String requestorName = ClientProperties.getResearcherInstitution();
        String senderName = ClientProperties.getDefaultConsortiumMemberName();
        assertEquals(isMtaActive, selenium.isElementPresent("link=Requestor: " + requestorName + " signed ABC MTA"));
        assertEquals(isMtaActive, selenium.isElementPresent("link=Sender: " + senderName + " signed ABC MTA"));
        if (isMtaActive) {
            String fileTarget = selenium.getAttribute("link=Requestor: " + requestorName + " signed ABC MTA@href");
            assertTrue(Pattern.matches(".*/protected/downloadFile[^/]*.*text[^/]*plain[^/]*.*", fileTarget));
            fileTarget = selenium.getAttribute("link=Sender: " + senderName + " signed ABC MTA@href");
            assertTrue(Pattern.matches(".*/protected/downloadFile[^/]*.*text[^/]*plain[^/]*.*", fileTarget));
            fileTarget = selenium.getAttribute("link=MTA Amendment@href");
            assertTrue(Pattern.matches(".*/protected/downloadFile[^/]*.*testui.properties.example[^/]*.*", fileTarget));
        }
        assertTrue(selenium.isElementPresent("link=Print Invoice"));
        if (!ClientProperties.isAggregateSearchResultsActive()) {
            clickAndWait("xpath=//table[@id='request']/tbody/tr[1]/td[1]/a");
            assertTrue(selenium.isTextPresent("Shipped"));
            assertTrue(selenium.isTextPresent("01/01/2020"));
        }
        clickAndWait("link=Sign Out");
        verifyVotingResults(1, "Partially", "Line item automatically approved");
    }

    /**
     * cancel.
     */
    private void cancel() {
        String confirm = "Once you cancel an order, you cannot reopen it. Are you sure you want to cancel the order?"
            + " Click OK to cancel the order and Cancel to remain on the current page.";
        goToEditPage();
        assertTrue(selenium.isTextPresent("Pending Shipment"));
        assertTrue(selenium.isElementPresent("btn_cancel_shipment"));
        selenium.chooseCancelOnNextConfirmation();
        selenium.click("btn_cancel_shipment");
        assertEquals(confirm, selenium.getConfirmation());
        assertTrue(selenium.isElementPresent("btn_cancel_shipment"));
        selenium.chooseOkOnNextConfirmation();
        clickAndWait("btn_cancel_shipment");
        assertEquals(confirm, selenium.getConfirmation());
        assertTrue(selenium.isTextPresent("Shipment successfully canceled."));
        assertFalse(selenium.isTextPresent("Receipt Quality"));
        assertFalse(selenium.isElementPresent("link=Print Invoice"));
        clickAndWait("link=Sign Out");
        verifyVotingResults(2, "Fully", "This is a good study. Full institutional approval granted.");
    }

    private void verifyVotingResults(int requestIndex, String prefix, String comment) {
        loginAsUser1();
        clickAndWait("link=My Requests");
        clickAndWait(String.format("xpath=//table[@id='specimenRequest']/tbody/tr[%d]/td[1]/a", requestIndex));
        assertTrue(selenium.isTextPresent("(" + prefix + " Processed)"));
        if (ClientProperties.isDisplayRequestReviewVotes()) {
            assertTrue(selenium.isTextPresent("Request Approved"));
            assertTrue("Expected comment not found, expected: " + comment + ", body text: "
                    + selenium.getBodyText(), selenium.isTextPresent(comment));
        } else {
            assertTrue(selenium.isTextPresent(ClientProperties.getFinalVoteLabel() + ": Approved"));
        }
        clickAndWait("link=Sign Out");
    }

    /**
     * @return the count.
     */
    public int getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(int count) {
        this.count = count;
    }
}
