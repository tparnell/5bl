/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.test.selenium.main;

import java.text.NumberFormat;
import java.text.ParseException;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import com.fiveamsolutions.tissuelocator.test.ClientProperties;
import com.fiveamsolutions.tissuelocator.test.selenium.AbstractListTest;

/**
 * @author ddasgupta
 *
 */
public class SpecimenListTest extends AbstractListTest {

    /**
     * Institution column.
     */
    private static final int INSTITUTION_COLUMN = 6;

    private static final int BUTTON_COL_WITH_INST_INDEX = 10;
    private static final int BUTTON_COL_NO_INST_INDEX = 9;

    /**
     * Results count.
     */
    private static final int RESULT_COUNT = 72;

    private static final int AVAILABLE_COUNT = 10;
    private static final int UNAVAILABLE_COUNT = 10;
    private static final int SHIPPED_COUNT = 10;
    private static final int RETURNED_TO_SOURCE_COUNT = 10;
    private static final int DESTROYED_COUNT = 10;
    private static final int UNDER_REVIEW_COUNT = 10;
    private static final int PENDING_SHIPMENT_COUNT = 10;
    private static final int RETURNED_TO_PARTICIPANT_COUNT = 2;

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getScriptName() {
        return "SpecimenListTest.sql";
    }

    /**
     * test the specimen0 list page.
     *
     * @throws ParseException on error
     */
    @Test
    public void testList() throws ParseException {
        loginAsAdmin();
        goToListPage();
        verifyColumns();
        showOnlyTestResults();
        statusFilter();
        extIdFilter();
        verifyExportPresent();
        showOnlyTestResults();
        showAllResults();
        paging("specimen");
        final int count = RESULT_COUNT + ClientProperties.getFluidSpecimenCount()
                + ClientProperties.getTissueSpecimenCount() + ClientProperties.getCellsSpecimenCount()
                + ClientProperties.getMolecularSpecimenCount();
        pageSize(count);
        final String objName = StringUtils
                .substringBeforeLast(ClientProperties.getBiospecimenAdministrationLink(), " ");
        institutionRestriction("specimen", objName, "xpath=//select[@id='institution']", INSTITUTION_COLUMN,
                getButtonColIndex(), false, ClientProperties.getFluidSpecimenCount() > 0);
        editButtonOnViewPage();
        institutionFilter();
    }

    /**
     * Go to list page.
     */
    private void goToListPage() {
        mouseOverAndPause("link=Administration");
        clickAndWait("link=" + ClientProperties.getBiospecimenAdministrationLink());
        assertTrue(selenium.isTextPresent(ClientProperties.getBiospecimenTerm() + " Administration"));
    }

    /**
     * Test status filter.
     */
    private void statusFilter() {
        assertTrue(selenium.isTextPresent("1-20 of " + RESULT_COUNT + " Results"));
        assertEquals("All", selenium.getSelectedLabel("status"));
        verifyStatus("Unavailable", UNAVAILABLE_COUNT);
        verifyStatus("Under Review", UNDER_REVIEW_COUNT);
        verifyStatus("Pending Shipment", PENDING_SHIPMENT_COUNT);
        verifyStatus("Shipped", SHIPPED_COUNT);
        verifyStatus("Returned to Source Institution", RETURNED_TO_SOURCE_COUNT);
        verifyStatus("Destroyed", DESTROYED_COUNT);
        verifyStatus("Available", AVAILABLE_COUNT);
        verifyStatus("Returned to " + ClientProperties.getParticipantTerm(), RETURNED_TO_PARTICIPANT_COUNT);
        selenium.select("status", "label=All");
        waitForPageToLoad();
        assertEquals("All", selenium.getSelectedLabel("status"));
        assertTrue(selenium.isTextPresent("1-20 of " + RESULT_COUNT + " Results"));
    }

    /**
     * Tests id filter.
     */
    private void extIdFilter() {
        if (ClientProperties.getParticipantCount() > 0) {
            showOnlyTestResults();
        }
        assertTrue(selenium.isTextPresent("1-20 of 72 Results"));
        assertTrue(selenium.isTextPresent("1 of 4 Pages"));
        final String extId = selenium.getTable("specimen.2.0");
        selenium.type("externalId", extId);
        clickAndWait("btn_search");
        assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
        assertTrue(selenium.isTextPresent("1 of 1 Page"));
        selenium.type("externalId", "");
        clickAndWait("btn_search");
        final int count = RESULT_COUNT + ClientProperties.getFluidSpecimenCount()
                + ClientProperties.getTissueSpecimenCount() + ClientProperties.getCellsSpecimenCount()
                + ClientProperties.getMolecularSpecimenCount();
        final String formattedResultCount = NumberFormat.getInstance().format(count);
        assertTrue(selenium.isTextPresent("1-20 of " + formattedResultCount + " Results"));
    }

    /**
     * Tests edit button.
     */
    private void editButtonOnViewPage() {
        final String xpath = "xpath=//table[@id='specimen']/tbody/tr[%d]/td[1]/a";
        final String editButton = "link=Edit";
        loginAsAdmin();
        goToListPage();
        showOnlyTestResults();
        final String instName = ClientProperties.getDefaultConsortiumMemberName();
        int instRow = 1;
        while (!selenium.getTable("specimen." + instRow + "." + INSTITUTION_COLUMN).contains(instName)
                && instRow < DEFAULT_PAGE_SIZE) {
            instRow++;
        }
        int otherRow = 1;
        while (selenium.getTable("specimen." + otherRow + "." + INSTITUTION_COLUMN).contains(instName)
                && otherRow < DEFAULT_PAGE_SIZE) {
            otherRow++;
        }

        if (instRow < DEFAULT_PAGE_SIZE) {
            clickAndWait(String.format(xpath, instRow));
            assertTrue(selenium.isElementPresent(editButton));
            goToListPage();
        }
        if (otherRow < DEFAULT_PAGE_SIZE) {
            clickAndWait(String.format(xpath, otherRow));
            assertTrue(selenium.isElementPresent(editButton));
        }
        clickAndWait("link=Sign Out");

        login(ClientProperties.getInstitutionalAdminEmail(), "tissueLocator1");

        if (instRow < DEFAULT_PAGE_SIZE) {
            goToListPage();
            showOnlyTestResults();
            clickAndWait(String.format(xpath, instRow));
            assertTrue(selenium.isElementPresent(editButton));
        }
        if (otherRow < DEFAULT_PAGE_SIZE) {
            goToListPage();
            showOnlyTestResults();
            clickAndWait(String.format(xpath, otherRow));
            assertFalse(selenium.isElementPresent(editButton));
        }
        clickAndWait("link=Sign Out");
    }

    /**
     * Verify status.
     *
     * @param status the status to verify
     * @param resultCount the number of results with that status.
     */
    private void verifyStatus(final String status, final int resultCount) {
        selenium.select("status", "label=" + status);
        waitForPageToLoad();
        assertEquals(status, selenium.getSelectedLabel("status"));
        if (ClientProperties.isDisplayInstitutionSpecimenAdmin()) {
            assertEquals(status, selenium.getTable("specimen.1.8"));
        } else {
            assertEquals(status, selenium.getTable("specimen.1.7"));
        }
        assertTrue(selenium.isTextPresent("1-" + resultCount + " of " + resultCount + " Results"));
        assertTrue(selenium.isElementPresent("link=Edit"));
    }

    /**
     * verify the specimen list has the right columns.
     */
    private void verifyColumns() {
        int colNum = 0;
        assertEquals(ClientProperties.getBiospecimenTerm() + " ID", selenium.getTable("specimen.0." + colNum++));
        assertEquals("Type", selenium.getTable("specimen.0." + colNum++));
        assertEquals(ClientProperties.getDiseaseTerm() + "", selenium.getTable("specimen.0." + colNum++));
        assertEquals(ClientProperties.getQuantityTerm(), selenium.getTable("specimen.0." + colNum++));
        assertEquals("Collection Age", selenium.getTable("specimen.0." + colNum++));
        assertEquals(ClientProperties.getCollectionYearTerm(), selenium.getTable("specimen.0." + colNum++));
        assertEquals("External ID (Record ID)", selenium.getTable("specimen.0." + colNum++));

        if (ClientProperties.isDisplayInstitutionSpecimenAdmin()) {
            assertEquals(ClientProperties.getInstitutionTerm(), selenium.getTable("specimen.0." + colNum));
            assertNotNull(selenium.getTable("specimen.1." + colNum++));
        }

        assertEquals("Status", selenium.getTable("specimen.0." + colNum++));
        assertEquals("Action", selenium.getTable("specimen.0." + colNum++));
    }

    private int getButtonColIndex() {
        if (ClientProperties.isDisplayInstitutionSpecimenAdmin()) {
            return BUTTON_COL_WITH_INST_INDEX;
        }
        return BUTTON_COL_NO_INST_INDEX;
    }

    /**
     * Tests institution filter.
     */
    private void institutionFilter() {
        login(ClientProperties.getInstitutionalAdminEmail(), PASSWORD);
        goToListPage();
        if (ClientProperties.getParticipantCount() > 0) {
            showOnlyTestResults();
        }
        assertTrue(selenium.isTextPresent("1-20 of " + RESULT_COUNT + " Results"));
        assertTrue(selenium.isTextPresent("1 of 4 Pages"));
        clickAndWait("filterByInstitution");
        assertFalse(selenium.isTextPresent("1-20 of " + RESULT_COUNT + " Results"));
        assertFalse(selenium.isTextPresent("1 of 4 Pages"));
        assertTrue(selenium.isTextPresent("1 of 1 Page"));
        clickAndWait("filterByInstitution");
        assertTrue(selenium.isTextPresent("1-20 of " + RESULT_COUNT + " Results"));
        assertTrue(selenium.isTextPresent("1 of 4 Pages"));
        clickAndWait("link=Sign Out");
    }
}
