/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.test.selenium.nfcr;

import com.fiveamsolutions.tissuelocator.test.ClientProperties;
import com.fiveamsolutions.tissuelocator.test.selenium.AbstractSpecimenTest;

/**
 * @author ddasgupta
 *
 */
public class NfcrSpecimenTest extends AbstractSpecimenTest {

    private static final String CUSTOM_PROPERTIES_SPECIMEN_DENSITY = "object.customProperties['specimenDensity']";
    private static final String CUSTOM_PROPERTIES_TIME_LAPSE_TO_PROCESSING =
        "object.customProperties['timeLapseToProcessing']";
    private static final String CUSTOM_PROPERTIES_PRESERVATION_TYPE =
        "object.customProperties['preservationType']";
    private static final String CUSTOM_PROPERTIES_ANATOMIC_SOURCE =
        "object.customProperties['anatomicSource']";
    private static final String CUSTOM_PROPERTIES_COLLECTION_TYPE =
        "object.customProperties['collectionType']";
    private static final String CUSTOM_PROPERTIES_CLINICAL_DATA =
        "object.customProperties['additionalClinicalData']";
    private static final String CUSTOM_PROPERTIES_STORAGE_TEMPERATURE =
        "object.customProperties['storageTemperature']";
    private static final String CUSTOM_PROPERTIES_DATE_OF_DIAGNOSIS =
        "object.customProperties['dateOfDiagnosis']";
    private static final String CUSTOM_PROPERTIES_SURVIVAL_IN_MONTHS =
        "object.customProperties['survivalInMonths']";
    private static final String CUSTOM_PROPERTIES_DISEASE_FREE_SURVIVAL_IN_MONTHS =
        "object.customProperties['diseaseFreeSurvivalInMonths']";
    private static final String CUSTOM_PROPERTIES_TUMOR_SIZE_CM =
        "object.customProperties['tumorSizeCm']";
    private static final String CUSTOM_PROPERTIES_SMOKING_STATUS =
        "object.customProperties['smokingStatus']";
    private static final String CUSTOM_PROPERTIES_DATE_OF_SURGERY =
        "object.customProperties['dateOfSurgery']";
    private static final String CUSTOM_PROPERTIES_AGE_AT_DIAGNOSIS_IN_YEARS =
        "object.customProperties['ageAtDiagnosisInYears']";
    private static final String CUSTOM_PROPERTIES_WEIGHT_IN_KG =
        "object.customProperties['weightInKg']";
    private static final String CUSTOM_PROPERTIES_HEIGHT_IN_CM =
        "object.customProperties['heightInCm']";
    private static final String CUSTOM_PROPERTIES_PACK_YEARS_OF_SMOKING =
        "object.customProperties['packYearsOfSmoking']";
    private static final String CUSTOM_PROPERTIES_YEARS_SINCE_SMOKING_CESSATION =
        "object.customProperties['yearsSinceSmokingCessation']";
    private static final String CUSTOM_PROPERTIES_ALCOHOL_USE_STATUS =
        "object.customProperties['alcoholUseStatus']";
    private static final String CUSTOM_PROPERTIES_RELATIVE_ALCOHOL_USE =
        "object.customProperties['relativeAlcoholUse']";
    private static final String CUSTOM_PROPERTIES_YEARS_SINCE_ALCOHOL_CESSATION =
        "object.customProperties['yearsSinceAlcoholCessation']";
    private static final String CUSTOM_PROPERTIES_FAMILY_HISTORY_OF_CANCER =
        "object.customProperties['familyHistoryOfCancer']";
    private static final String CUSTOM_PROPERTIES_FAMILY_HISTORY_OF_GASTRIC_CANCER =
        "object.customProperties['familyHistoryOfGastricCancer']";
    private static final String CUSTOM_PROPERTIES_FAMILY_HISTORY_OF_LUNG_CANCER =
        "object.customProperties['familyHistoryOfLungCancer']";
    private static final String CUSTOM_PROPERTIES_FAMILY_HISTORY_OF_LIVER_CANCER =
        "object.customProperties['familyHistoryOfLiverCancer']";
    private static final String CUSTOM_PROPERTIES_FAMILY_HISTORY_OF_COLON_CANCER =
        "object.customProperties['familyHistoryOfColonCancer']";
    private static final String CUSTOM_PROPERTIES_FAMILY_HISTORY_OF_PANCREATIC_CANCER =
        "object.customProperties['familyHistoryOfPancreaticCancer']";
    private static final String CUSTOM_PROPERTIES_FAMILY_HISTORY_OF_GBM_CANCER =
        "object.customProperties['familyHistoryOfGbmCancer']";
    private static final String CUSTOM_PROPERTIES_OCCUPATION =
        "object.customProperties['occupation']";
    private static final String CUSTOM_PROPERTIES_CHILDS_GRADE =
        "object.customProperties['childsGrade']";
    private static final String CUSTOM_PROPERTIES_EDMONDSONS =
        "object.customProperties['edmondsonsGrade']";
    private static final String CUSTOM_PROPERTIES_TNM_STAGE_T =
        "object.customProperties['tnmStageT']";
    private static final String CUSTOM_PROPERTIES_TNM_STAGE_N =
        "object.customProperties['tnmStageN']";
    private static final String CUSTOM_PROPERTIES_TNM_STAGE_M =
        "object.customProperties['tnmStageM']";
    private static final String CUSTOM_PROPERTIES_ECOG_PERFORMANCE_STATUS =
        "object.customProperties['ecogPerformanceStatus']";
    private static final String CUSTOM_PROPERTIES_H_PYLORI_STATUS =
        "object.customProperties['hPyloriStatus']";
    private static final String CUSTOM_PROPERTIES_H_PYLORI_STRAIN =
        "object.customProperties['hPyloriStrain']";
    private static final String CUSTOM_PROPERTIES_BCLCC_STAGE =
        "object.customProperties['bclccStage']";
    private static final String CUSTOM_PROPERTIES_PERCENTAGE_OF_TUMOR_CELLS =
        "object.customProperties['percentageOfTumorCells']";
    private static final String CUSTOM_PROPERTIES_PERCENTAGE_OF_NECROSIS =
        "object.customProperties['percentageOfNecrosis']";
    private static final String CUSTOM_PROPERTIES_ETIOLOGY =
        "object.customProperties['etiology']";
    private static final String CUSTOM_PROPERTIES_COLON_CANCER_HISTOLOGY =
        "object.customProperties['colonCancerHistology']";
    private static final String CUSTOM_PROPERTIES_GASTRIC_CANCER_HISTOLOGY =
        "object.customProperties['gastricCancerHistology']";
    private static final String CUSTOM_PROPERTIES_NON_TUMEROUS_LIVER_HISTOLOGY =
        "object.customProperties['nonTumerousLiverHistology']";
    private static final String CUSTOM_PROPERTIES_VEINOUS_INFILTRATION =
        "object.customProperties['veinousInfiltration']";
    private static final String CUSTOM_PROPERTIES_PORTAL_VEIN_THROMBOSIS =
        "object.customProperties['portalVeinThrombosis']";
    private static final String CUSTOM_PROPERTIES_NUMBER_OF_NODULES =
        "object.customProperties['numberOfNodules']";
    private static final String CUSTOM_PROPERTIES_HCVAB_STATUS =
        "object.customProperties['hcvabStatus']";
    private static final String CUSTOM_PROPERTIES_HBSAG_STATUS =
        "object.customProperties['hbsAgStatus']";
    private static final String CUSTOM_PROPERTIES_ALANIN_TRANSAMINASE =
        "object.customProperties['alanineTransaminase']";
    private static final String CUSTOM_PROPERTIES_SERUM_ALBUMIN =
        "object.customProperties['serumAlbumin']";
    private static final String CUSTOM_PROPERTIES_ASPARTATE_AMINO_TRANSFERASE =
        "object.customProperties['aspartateAminotransferase']";
    private static final String CUSTOM_PROPERTIES_TOTAL_BILIRUBIN =
        "object.customProperties['totalBilirubin']";
    private static final String CUSTOM_PROPERTIES_SERUM_GLUTAMATE_OXALOACETIC_TRANSAMINASE =
        "object.customProperties['serumGlutamateOxaloaceticTransaminase']";
    private static final String CUSTOM_PROPERTIES_SERUM_GLUTAMATE_PYRUVATE_TRANSAMINASE =
        "object.customProperties['serumGlutamatePyruvateTransaminase']";
    private static final String CUSTOM_PROPERTIES_ALBUMIN =
        "object.customProperties['albumin']";
    private static final String CUSTOM_PROPERTIES_ANATOMICAL_LOCATION_OF_PANCREATIC_CANCER =
        "object.customProperties['anatomicalLocationPancreaticCancer']";
    private static final String CUSTOM_PROPERTIES_PROPORTION_OF_BAC =
        "object.customProperties['proportionOfBronchioloalveolarCarcinoma']";
    private static final String CUSTOM_PROPERTIES_ANATOMICAL_LOCATION_OF_GBM =
        "object.customProperties['anatomicalLocationGlioblastomaMultiformecancer']";
    private static final String CUSTOM_PROPERTIES_SURGERY_TYPE =
        "object.customProperties['surgeryType']";
    private static final String CUSTOM_PROPERTIES_COMPLETE_RESECTION_ACHIEVED =
        "object.customProperties['completeResectionAchieved']";
    private static final String CUSTOM_PROPERTIES_COMPLETE_RESECTION_NOT_POSSIBLE_BECAUSE =
        "object.customProperties['completeResectionNotPossibleBecause']";
    private static final String CUSTOM_PROPERTIES_PRE_OPERATIVE_CHEMO =
        "object.customProperties['preoperativeChemotherapy']";
    private static final String CUSTOM_PROPERTIES_PRE_OPERATIVE_CHEMO_DRUG_OR_REGIME =
        "object.customProperties['preOperativeChemotherapyDrugOrRegimen']";
    private static final String CUSTOM_PROPERTIES_PRE_OPERATIVE_CHEMO_DOSEAGE =
        "object.customProperties['preOperativeChemotherapyDosage']";
    private static final String CUSTOM_PROPERTIES_PRE_OPERATIVE_CHEMO_CYCLES =
        "object.customProperties['preOperativeChemotherapyCycles']";
    private static final String CUSTOM_PROPERTIES_PRE_OPERATIVE_CHEMO_START_DATE =
        "object.customProperties['preOperativeChemotherapyStartDate']";
    private static final String CUSTOM_PROPERTIES_PRE_OPERATIVE_CHEMO_END_DATE =
        "object.customProperties['preOperativeChemotherapyEndDate']";
    private static final String CUSTOM_PROPERTIES_TREATMENT_DRUG_OR_REGIME_SINCE_SURGERY =
        "object.customProperties['treatmentDrugOrRegimenSinceSurgery']";
    private static final String CUSTOM_PROPERTIES_TREATMENT_DOSEAGE_SINCE_SURGERY =
        "object.customProperties['treatmentDosageSinceSurgery']";
    private static final String CUSTOM_PROPERTIES_TREATMENT_CYCLES_SINCE_SURGERY =
        "object.customProperties['treatmentCyclesSinceSurgery']";
    private static final String CUSTOM_PROPERTIES_TREATMENT_SINCE_SURGERY_START_DATE =
        "object.customProperties['treatmentSinceSurgeryStartDate']";
    private static final String CUSTOM_PROPERTIES_TREATMENT_SINCE_SURGERY_END_DATE =
        "object.customProperties['treatmentSinceSurgeryEndDate']";
    private static final String CUSTOM_PROPERTIES_RADIATION =
        "object.customProperties['radiation']";
    private static final String CUSTOM_PROPERTIES_HAS_REGULAR_FOLLOW_UP =
        "object.customProperties['hasRegularFollowUp']";
    private static final String CUSTOM_PROPERTIES_MONTHS_BETWEEN_FOLLOW_UP =
        "object.customProperties['monthsBetweenFollowUp']";
    private static final String CUSTOM_PROPERTIES_FOLLOW_UP_START_DATE =
        "object.customProperties['followUpStartDate']";
    private static final String CUSTOM_PROPERTIES_FOLLOW_UP_END_DATE =
        "object.customProperties['followUpEndDate']";
    private static final String CUSTOM_PROPERTIES_RECURRANCE_STATUS =
        "object.customProperties['recurranceStatus']";
    private static final String CUSTOM_PROPERTIES_RECURRANCE_LOCATION =
        "object.customProperties['recurranceLocation']";
    private static final String CUSTOM_PROPERTIES_RECURRANCE_ORGAN_SITE =
        "object.customProperties['distantRecurranceOrganSite']";
    private static final String CUSTOM_PROPERTIES_RECURRANCE_FOUND_BY_INTERVENING_SYMPTOMS =
        "object.customProperties['recurranceFoundByInterveningSymptoms']";
    private static final String CUSTOM_PROPERTIES_NO_RECURRANCE_AND_DATE_LAST_KNOWN_DISEASE_FREE =
        "object.customProperties['noRecurranceAndDateLastKnownDiseaseFree']";
    private static final String CUSTOM_PROPERTIES_PROGRESSION_STATUS =
        "object.customProperties['progressionStatus']";
    private static final String CUSTOM_PROPERTIES_PROGRESSING_DATE_OF_FIRST_KNOWN_PROGRESSION =
        "object.customProperties['progressingDateOfFirstKnownProgression']";
    private static final String CUSTOM_PROPERTIES_PROGRESSION_SYMPTOMATIC =
        "object.customProperties['progressionSymptomatic']";
    private static final String CUSTOM_PROPERTIES_PROGRESSION_LOCATION =
        "object.customProperties['progressionLocation']";
    private static final String CUSTOM_PROPERTIES_DISTANT_PROGRESSION_ORGAN_SITE =
        "object.customProperties['distantProgressionOrganSite']";
    private static final String CUSTOM_PROPERTIES_NO_PROGRESSION_AND_DATE_LAST_KNOWN_WITHOUT_PROGRESSION =
        "object.customProperties['noProgressionLastDateKnownWithoutProgression']";
    private static final String CUSTOM_PROPERTIES_PATIENT_VITAL_STATUS =
        "object.customProperties['patientsVitalStatus']";
    private static final String CUSTOM_PROPERTIES_DATE_OF_DEATH =
        "object.customProperties['dateOfDeath']";
    private static final String CUSTOM_PROPERTIES_CAUSE_OF_DEATH =
        "object.customProperties['causeOfDeath']";
    private static final String CUSTOM_PROPERTIES_NOT_DEAD_LAST_KNOWN_DATE_ALIVE =
        "object.customProperties['notDeadLastKnownDateAlive']";
    private static final String CUSTOM_PROPERTIES_HER2_MUTATION_STATUS =
        "object.customProperties['her2MutationStatus']";
    private static final String CUSTOM_PROPERTIES_HER2_MUTATION_TYPE =
        "object.customProperties['her2MutationType']";
    private static final String CUSTOM_PROPERTIES_HER2_MUTATION_SITE =
        "object.customProperties['her2MutationSite']";
    private static final String CUSTOM_PROPERTIES_LUNG_CANCER_KRAS_MUTATION_STATUS =
        "object.customProperties['lungCancerKrasMutationStatus']";
    private static final String CUSTOM_PROPERTIES_LUNG_CANCER_KRAS_MUTATION_TYPE =
        "object.customProperties['lungCancerKrasMutationType']";
    private static final String CUSTOM_PROPERTIES_LUNG_CANCER_KRAS_MUTATION_SITE =
        "object.customProperties['lungCancerKrasMutationSite']";
    private static final String CUSTOM_PROPERTIES_LUNG_CANCER_EGFR_MUTATION_STATUS =
        "object.customProperties['lungCancerEgfrMutationStatus']";
    private static final String CUSTOM_PROPERTIES_LUNG_CANCER_EGFR_MUTATION_TYPE =
        "object.customProperties['lungCancerEgfrMutationType']";
    private static final String CUSTOM_PROPERTIES_LUNG_CANCER_EGFR_MUTATION_SITE =
        "object.customProperties['lungCancerEgfrMutationSite']";
    private static final String CUSTOM_PROPERTIES_ALPHA_FETO_PROTEIN =
        "object.customProperties['alphaFetoprotein']";
    private static final String CUSTOM_PROPERTIES_COLON_CANCER_KRAS_MUTATION_STATUS =
        "object.customProperties['colonCancerKrasMutationStatus']";
    private static final String CUSTOM_PROPERTIES_COLON_CANCER_KRAS_MUTATION_TYPE =
        "object.customProperties['colonCancerKrasMutationType']";
    private static final String CUSTOM_PROPERTIES_COLON_CANCER_KRAS_MUTATION_SITE =
        "object.customProperties['colonCancerKrasMutationSite']";
    private static final String CUSTOM_PROPERTIES_APC_STATUS =
        "object.customProperties['apcStatus']";
    private static final String CUSTOM_PROPERTIES_BRAF_MUTATION_STATUS =
        "object.customProperties['brafMutationStatus']";
    private static final String CUSTOM_PROPERTIES_BRAF_MUTATION_TYPE =
        "object.customProperties['brafMutationType']";
    private static final String CUSTOM_PROPERTIES_BRAF_MUTATION_SITE =
        "object.customProperties['brafMutationSite']";
    private static final String CUSTOM_PROPERTIES_PI3K_MUTATION_STATUS =
        "object.customProperties['pi3kMutationStatus']";
    private static final String CUSTOM_PROPERTIES_PI3K_MUTATION_TYPE =
        "object.customProperties['pi3kMutationType']";
    private static final String CUSTOM_PROPERTIES_PI3K_MUTATION_SITE =
        "object.customProperties['pi3kMutationSite']";
    private static final String CUSTOM_PROPERTIES_BETA_CATENIN_MUTATION_STATUS =
        "object.customProperties['betaCateninMutationStatus']";
    private static final String CUSTOM_PROPERTIES_BETA_CATENIN_EXPRESSION_LEVEL =
        "object.customProperties['betaCateninExpressionLevel']";
    private static final String CUSTOM_PROPERTIES_MSI_MUTATION_STATUS =
        "object.customProperties['msiMutationStatus']";
    private static final String CUSTOM_PROPERTIES_PANCREATIC_CANCER_KRAS_MUTATION_STATUS =
        "object.customProperties['pancreaticCancerKrasMutationStatus']";
    private static final String CUSTOM_PROPERTIES_PANCREATIC_CANCER_KRAS_MUTATION_TYPE =
        "object.customProperties['pancreaticCancerKrasMutationType']";
    private static final String CUSTOM_PROPERTIES_PANCREATIC_CANCER_KRAS_MUTATION_SITE =
        "object.customProperties['pancreaticCancerKrasMutationSite']";
    private static final String CUSTOM_PROPERTIES_PANCREATIC_CANCER_EGFR_MUTATION_STATUS =
        "object.customProperties['pancreaticCancerEgfrMutationStatus']";
    private static final String CUSTOM_PROPERTIES_PANCREATIC_CANCER_EGFR_MUTATION_TYPE =
        "object.customProperties['pancreaticCancerEgfrMutationType']";
    private static final String CUSTOM_PROPERTIES_PANCREATIC_CANCER_EGFR_MUTATION_SITE =
        "object.customProperties['pancreaticCancerEgfrMutationSite']";
    private static final String CUSTOM_PROPERTIES_PANCREATIC_CANCER_P53_MUTATION_STATUS =
        "object.customProperties['pancreaticCancerP53MutationStatus']";
    private static final String CUSTOM_PROPERTIES_PANCREATIC_CANCER_P53_MUTATION_TYPE =
        "object.customProperties['pancreaticCancerP53MutationType']";
    private static final String CUSTOM_PROPERTIES_PANCREATIC_CANCER_P53_MUTATION_SITE =
        "object.customProperties['pancreaticCancerP53MutationSite']";
    private static final String CUSTOM_PROPERTIES_GBM_CANCER_KRAS_MUTATION_STATUS =
        "object.customProperties['gbmCancerKrasMutationStatus']";
    private static final String CUSTOM_PROPERTIES_GBM_CANCER_KRAS_MUTATION_TYPE =
        "object.customProperties['gbmCancerKrasMutationType']";
    private static final String CUSTOM_PROPERTIES_GBM_CANCER_KRAS_MUTATION_SITE =
        "object.customProperties['gbmCancerKrasMutationSite']";
    private static final String CUSTOM_PROPERTIES_GBM_CANCER_EGFR_MUTATION_STATUS =
        "object.customProperties['gbmCancerEgfrMutationStatus']";
    private static final String CUSTOM_PROPERTIES_GBM_CANCER_EGFR_MUTATION_TYPE =
        "object.customProperties['gbmCancerEgfrMutationType']";
    private static final String CUSTOM_PROPERTIES_GBM_CANCER_EGFR_MUTATION_SITE =
        "object.customProperties['gbmCancerEgfrMutationSite']";
    private static final String CUSTOM_PROPERTIES_GBM_CANCER_EGFR_V3_MUTATION_STATUS =
        "object.customProperties['gbmCancerEgfrv3MutationStatus']";
    private static final String CUSTOM_PROPERTIES_GBM_CANCER_PTEN_EXPRESSION =
        "object.customProperties['gbmPtenExpression']";
    private static final String CUSTOM_PROPERTIES_LIVER_CANCER_TREATMENT =
        "object.customProperties['liverCancerTreatment']";
    private static final String CUSTOM_PROPERTIES_TREATMENT_SINCE_SURGERY =
        "object.customProperties['treatmentSinceSurgery']";

    private static final String SELENIUM_LABEL_PREPEND = "label=";
    private static final String FAKE_DATE = "April 1, 1991";
    private static final String FAKE_STRING = "Foobar";
    private static final String FAKE_FLOAT = "6.6";
    private static final String FAKE_INT = "6";
    private static final String FAKE_FAMILY_HISTORY = "Mother, Father, Brother, and Sister";
    private static final String FAKE_OCCUPATION = "Non-Farming";
    private static final String FAKE_CHILDS_GRADE = "B";
    private static final String FAKE_TNM_GRADE = "1";
    private static final String FAKE_PERFORMANCE_STATUS = "3";
    private static final String FAKE_BCLCC_STAGE = "C";
    private static final String FAKE_ETIOLOGY = "HBV+HCV";
    private static final String FAKE_COLON_CANCER_HISTOLOGY = "Medullary Carcinoma";
    private static final String FAKE_GASTRIC_CANCER_HISTOLOGY = "Lauren Intestinal";
    private static final String FAKE_NON_TUMEROUS_LIVER_HISTOLOGY = "Chronic Hepatitis";
    private static final String FAKE_VEINOUS_INFILTRATION = "Present";
    private static final String FAKE_YES = "Yes";
    private static final String FAKE_NUMBER_OF_NODULES = "Multiple";
    private static final String FAKE_POSITIVE = "Positive";
    private static final String FAKE_EDMONDSONS_GRADE = "Moderately Differentiated";
    private static final String FAKE_DRINKING_STATUS = "Ex-Drinker";
    private static final String FAKE_SMOKING_STATUS = "Ex-Smoker";
    private static final String FAKE_USE_LEVEL = "Moderate";
    private static final String FAKE_PANCREAS_ANATOMICAL_LOCATION = "Tail Of Pancreas";
    private static final String FAKE_PROPORTION = "0.88";
    private static final String FAKE_RESECTION_REASON = "Disease Left Behind";
    private static final String FAKE_LOCATION = "Distant Organ Site";
    private static final String FAKE_PROGRESSING = "Progressing";
    private static final String FAKE_SYMPTOMATIC = "Symptomatic";
    private static final String FAKE_DECEASED = "Deceased";
    private static final String FAKE_COD = "Other Organ Failure";
    private static final String FAKE_MUTANT = "Mutant";
    private static final String FAKE_MSI_MUTATION_STATUS = "MSI High";
    private static final String FAKE_NEGATIVE_INT = "-1";
    private static final String FAKE_NEGATIVE_FLOAT = "-1.1";
    private static final String FAKE_LIVER_CANCER_TREATMENT = "Transarterial Chemoembolization (TACE)";

    private static final int[] CUSTOM_PROPERTY_VIEW_INDEXES = new int[] {4, 7, 8, 11, 12, 13, 15};
    private static final String CUSTOM_PROPERTY_UNAVAILABLE_TEXT = "Available on Request";
    private static final String VIEW_PAGE_XPATH_FIELD_VALUE_TEMPLATE =
        "xpath=//div[@id='table-format']/table[%d]/tbody/tr[%d]/td[2]";

    /**
     * {@inheritDoc}
     */
    @Override
    protected void extensionFormatValidationEntry() {
        setDynamicTextSearchFields("invalid");
        enterInvalidEnhancedExtensionData();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void extensionFormatValidationMessage() {
        assertTrue(selenium.isTextPresent("Time Lapse To Processing (minutes) is invalid."));
        assertTrue(selenium.isTextPresent("Specimen Density is invalid."));
        assertTrue(selenium.isTextPresent("Storage Temperature is invalid."));
        enhancedExtensionFormatValidationMessage();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void extensionMinMaxValidationEntry() {
        setDynamicTextSearchFields("-20");
        enterInvalidRangedEnhancedExtensionData();
    }

    private void validateEnhancedExtensionValidationMessaqeIsPresent(String expectedMessage) {
        assertTrue(selenium.isTextPresent("Survival in Months " + expectedMessage));
        assertTrue(selenium.isTextPresent("Disease Free Survival in Months " + expectedMessage));
        assertTrue(selenium.isTextPresent("Age At Diagnosis In Years " + expectedMessage));
        assertTrue(selenium.isTextPresent("Weight (kg) " + expectedMessage));
        assertTrue(selenium.isTextPresent("Height (cm) " + expectedMessage));
        assertTrue(selenium.isTextPresent("Pack Years Of Smoking " + expectedMessage));
        assertTrue(selenium.isTextPresent("Years Since Smoking Cessation " + expectedMessage));
        assertTrue(selenium.isTextPresent("Years Since Alcohol Cessation " + expectedMessage));
        assertTrue(selenium.isTextPresent("Proportion Of Tumor That Is Bronchioloalveolar Carcinoma "
                + expectedMessage));
        assertTrue(selenium.isTextPresent("Percentage Of Tumor Cells " + expectedMessage));
        assertTrue(selenium.isTextPresent("Percentage Of Necrosis " + expectedMessage));
        assertTrue(selenium.isTextPresent("Alanine Transaminase (IU/L) " + expectedMessage));
        assertTrue(selenium.isTextPresent("Albumin (g/dL) " + expectedMessage));
        assertTrue(selenium.isTextPresent("Serum Albumin (g/dL) " + expectedMessage));
        assertTrue(selenium.isTextPresent("Aspartate Aminotransferase (IU/L) " + expectedMessage));
        assertTrue(selenium.isTextPresent("Total Bilirubin (mg/dL) " + expectedMessage));
        assertTrue(selenium.isTextPresent("Serum GOT (IU/L) " + expectedMessage));
        assertTrue(selenium.isTextPresent("Serum GPT (IU/L) " + expectedMessage));
        assertTrue(selenium.isTextPresent("Preoperative Chemotherapy Cycles " + expectedMessage));
        assertTrue(selenium.isTextPresent("Treatment Cycles Since Surgery " + expectedMessage));
        assertTrue(selenium.isTextPresent("Months Between Follow Up " + expectedMessage));
        assertTrue(selenium.isTextPresent("Alpha Fetoprotein (ng/mL) " + expectedMessage));
    }

    private void enhancedExtensionFormatValidationMessage() {
        validateEnhancedExtensionValidationMessaqeIsPresent("is invalid.");
    }

    private void enhancedExtensionMinMaxValidationMessage() {
        validateEnhancedExtensionValidationMessaqeIsPresent("must be greater than or equal to 0.00");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void extensionMinMaxValidationMessage() {
        assertTrue(selenium.isTextPresent("Time Lapse To Processing (minutes) must be greater than or equal to 0"));
        assertTrue(selenium.isTextPresent("Specimen Density must be greater than or equal to 0."));
        selenium.type(CUSTOM_PROPERTIES_SPECIMEN_DENSITY, "120");
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("Specimen Density must be less than or equal to 100"));
        enhancedExtensionMinMaxValidationMessage();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void enterExtensionData() {
        selenium.select(CUSTOM_PROPERTIES_COLLECTION_TYPE, "label=Autopsy");
        selenium.select(CUSTOM_PROPERTIES_ANATOMIC_SOURCE, "label="
                + ClientProperties.getAnatomicSource());
        selenium.select(CUSTOM_PROPERTIES_PRESERVATION_TYPE, "label=Fixed Formalin");
        selenium.type(CUSTOM_PROPERTIES_STORAGE_TEMPERATURE, "32");
        selenium.type(CUSTOM_PROPERTIES_TIME_LAPSE_TO_PROCESSING, "25");
        selenium.type(CUSTOM_PROPERTIES_SPECIMEN_DENSITY, "50");
        selenium.check(CUSTOM_PROPERTIES_CLINICAL_DATA);
        enterValidEnhancedExtensionData();
    }

    private void enterInvalidRangedEnhancedExtensionData() {
        selenium.type(CUSTOM_PROPERTIES_SURVIVAL_IN_MONTHS, FAKE_NEGATIVE_FLOAT);
        selenium.type(CUSTOM_PROPERTIES_DISEASE_FREE_SURVIVAL_IN_MONTHS, FAKE_NEGATIVE_FLOAT);
        selenium.type(CUSTOM_PROPERTIES_TUMOR_SIZE_CM, FAKE_NEGATIVE_FLOAT);
        selenium.type(CUSTOM_PROPERTIES_AGE_AT_DIAGNOSIS_IN_YEARS, FAKE_NEGATIVE_INT);
        selenium.type(CUSTOM_PROPERTIES_WEIGHT_IN_KG, FAKE_NEGATIVE_FLOAT);
        selenium.type(CUSTOM_PROPERTIES_HEIGHT_IN_CM, FAKE_NEGATIVE_FLOAT);
        selenium.type(CUSTOM_PROPERTIES_PACK_YEARS_OF_SMOKING, FAKE_NEGATIVE_FLOAT);
        selenium.type(CUSTOM_PROPERTIES_YEARS_SINCE_SMOKING_CESSATION, FAKE_NEGATIVE_FLOAT);
        selenium.type(CUSTOM_PROPERTIES_YEARS_SINCE_ALCOHOL_CESSATION, FAKE_NEGATIVE_FLOAT);
        selenium.type(CUSTOM_PROPERTIES_PERCENTAGE_OF_TUMOR_CELLS, FAKE_NEGATIVE_FLOAT);
        selenium.type(CUSTOM_PROPERTIES_PERCENTAGE_OF_NECROSIS, FAKE_NEGATIVE_FLOAT);
        selenium.type(CUSTOM_PROPERTIES_ALANIN_TRANSAMINASE, FAKE_NEGATIVE_FLOAT);
        selenium.type(CUSTOM_PROPERTIES_SERUM_ALBUMIN, FAKE_NEGATIVE_FLOAT);
        selenium.type(CUSTOM_PROPERTIES_ASPARTATE_AMINO_TRANSFERASE, FAKE_NEGATIVE_FLOAT);
        selenium.type(CUSTOM_PROPERTIES_TOTAL_BILIRUBIN, FAKE_NEGATIVE_FLOAT);
        selenium.type(CUSTOM_PROPERTIES_SERUM_GLUTAMATE_OXALOACETIC_TRANSAMINASE, FAKE_NEGATIVE_FLOAT);
        selenium.type(CUSTOM_PROPERTIES_SERUM_GLUTAMATE_PYRUVATE_TRANSAMINASE, FAKE_NEGATIVE_FLOAT);
        selenium.type(CUSTOM_PROPERTIES_ALBUMIN, FAKE_NEGATIVE_FLOAT);
        selenium.type(CUSTOM_PROPERTIES_PROPORTION_OF_BAC, FAKE_NEGATIVE_FLOAT);
        selenium.type(CUSTOM_PROPERTIES_PRE_OPERATIVE_CHEMO_CYCLES, FAKE_NEGATIVE_INT);
        selenium.type(CUSTOM_PROPERTIES_TREATMENT_CYCLES_SINCE_SURGERY, FAKE_NEGATIVE_INT);
        selenium.type(CUSTOM_PROPERTIES_MONTHS_BETWEEN_FOLLOW_UP, FAKE_NEGATIVE_INT);
        selenium.type(CUSTOM_PROPERTIES_ALPHA_FETO_PROTEIN, FAKE_NEGATIVE_FLOAT);
    }

    private void enterInvalidEnhancedExtensionData() {
        selenium.type(CUSTOM_PROPERTIES_SURVIVAL_IN_MONTHS, FAKE_STRING);
        selenium.type(CUSTOM_PROPERTIES_DISEASE_FREE_SURVIVAL_IN_MONTHS, FAKE_STRING);
        selenium.type(CUSTOM_PROPERTIES_TUMOR_SIZE_CM, FAKE_STRING);
        selenium.type(CUSTOM_PROPERTIES_AGE_AT_DIAGNOSIS_IN_YEARS, FAKE_STRING);
        selenium.type(CUSTOM_PROPERTIES_WEIGHT_IN_KG, FAKE_STRING);
        selenium.type(CUSTOM_PROPERTIES_HEIGHT_IN_CM, FAKE_STRING);
        selenium.type(CUSTOM_PROPERTIES_PACK_YEARS_OF_SMOKING, FAKE_STRING);
        selenium.type(CUSTOM_PROPERTIES_YEARS_SINCE_SMOKING_CESSATION, FAKE_STRING);
        selenium.type(CUSTOM_PROPERTIES_YEARS_SINCE_ALCOHOL_CESSATION, FAKE_STRING);
        selenium.type(CUSTOM_PROPERTIES_PERCENTAGE_OF_TUMOR_CELLS, FAKE_STRING);
        selenium.type(CUSTOM_PROPERTIES_PERCENTAGE_OF_NECROSIS, FAKE_STRING);
        selenium.type(CUSTOM_PROPERTIES_ALANIN_TRANSAMINASE, FAKE_STRING);
        selenium.type(CUSTOM_PROPERTIES_SERUM_ALBUMIN, FAKE_STRING);
        selenium.type(CUSTOM_PROPERTIES_ASPARTATE_AMINO_TRANSFERASE, FAKE_STRING);
        selenium.type(CUSTOM_PROPERTIES_TOTAL_BILIRUBIN, FAKE_STRING);
        selenium.type(CUSTOM_PROPERTIES_SERUM_GLUTAMATE_OXALOACETIC_TRANSAMINASE, FAKE_STRING);
        selenium.type(CUSTOM_PROPERTIES_SERUM_GLUTAMATE_PYRUVATE_TRANSAMINASE, FAKE_STRING);
        selenium.type(CUSTOM_PROPERTIES_ALBUMIN, FAKE_STRING);
        selenium.type(CUSTOM_PROPERTIES_PROPORTION_OF_BAC, FAKE_STRING);
        selenium.type(CUSTOM_PROPERTIES_PRE_OPERATIVE_CHEMO_CYCLES, FAKE_STRING);
        selenium.type(CUSTOM_PROPERTIES_TREATMENT_CYCLES_SINCE_SURGERY, FAKE_STRING);
        selenium.type(CUSTOM_PROPERTIES_MONTHS_BETWEEN_FOLLOW_UP, FAKE_STRING);
        selenium.type(CUSTOM_PROPERTIES_ALPHA_FETO_PROTEIN, FAKE_STRING);
    }

    //CHECKSTYLE:OFF
    private void enterValidEnhancedExtensionData() {
        selenium.type(CUSTOM_PROPERTIES_DATE_OF_DIAGNOSIS, FAKE_DATE);
        selenium.type(CUSTOM_PROPERTIES_SURVIVAL_IN_MONTHS, FAKE_FLOAT);
        selenium.type(CUSTOM_PROPERTIES_DISEASE_FREE_SURVIVAL_IN_MONTHS, FAKE_FLOAT);
        selenium.type(CUSTOM_PROPERTIES_TUMOR_SIZE_CM, FAKE_FLOAT);
        selenium.select(CUSTOM_PROPERTIES_SMOKING_STATUS, SELENIUM_LABEL_PREPEND + FAKE_SMOKING_STATUS);
        selenium.type(CUSTOM_PROPERTIES_DATE_OF_SURGERY, FAKE_DATE);
        selenium.type(CUSTOM_PROPERTIES_AGE_AT_DIAGNOSIS_IN_YEARS, FAKE_INT);
        selenium.type(CUSTOM_PROPERTIES_WEIGHT_IN_KG, FAKE_FLOAT);
        selenium.type(CUSTOM_PROPERTIES_HEIGHT_IN_CM, FAKE_FLOAT);
        selenium.type(CUSTOM_PROPERTIES_PACK_YEARS_OF_SMOKING, FAKE_FLOAT);
        selenium.type(CUSTOM_PROPERTIES_YEARS_SINCE_SMOKING_CESSATION, FAKE_FLOAT);
        selenium.select(CUSTOM_PROPERTIES_ALCOHOL_USE_STATUS, SELENIUM_LABEL_PREPEND + FAKE_DRINKING_STATUS);
        selenium.select(CUSTOM_PROPERTIES_RELATIVE_ALCOHOL_USE, SELENIUM_LABEL_PREPEND + FAKE_USE_LEVEL);
        selenium.type(CUSTOM_PROPERTIES_YEARS_SINCE_ALCOHOL_CESSATION, FAKE_FLOAT);
        selenium.select(CUSTOM_PROPERTIES_FAMILY_HISTORY_OF_CANCER, SELENIUM_LABEL_PREPEND + FAKE_FAMILY_HISTORY);
        selenium.select(CUSTOM_PROPERTIES_FAMILY_HISTORY_OF_GASTRIC_CANCER, SELENIUM_LABEL_PREPEND + FAKE_FAMILY_HISTORY);
        selenium.select(CUSTOM_PROPERTIES_FAMILY_HISTORY_OF_LUNG_CANCER, SELENIUM_LABEL_PREPEND + FAKE_FAMILY_HISTORY);
        selenium.select(CUSTOM_PROPERTIES_FAMILY_HISTORY_OF_LIVER_CANCER, SELENIUM_LABEL_PREPEND + FAKE_FAMILY_HISTORY);
        selenium.select(CUSTOM_PROPERTIES_FAMILY_HISTORY_OF_COLON_CANCER, SELENIUM_LABEL_PREPEND + FAKE_FAMILY_HISTORY);
        selenium.select(CUSTOM_PROPERTIES_FAMILY_HISTORY_OF_PANCREATIC_CANCER, SELENIUM_LABEL_PREPEND + FAKE_FAMILY_HISTORY);
        selenium.select(CUSTOM_PROPERTIES_FAMILY_HISTORY_OF_GBM_CANCER, SELENIUM_LABEL_PREPEND + FAKE_FAMILY_HISTORY);
        selenium.select(CUSTOM_PROPERTIES_OCCUPATION, SELENIUM_LABEL_PREPEND + FAKE_OCCUPATION);
        selenium.select(CUSTOM_PROPERTIES_CHILDS_GRADE, SELENIUM_LABEL_PREPEND + FAKE_CHILDS_GRADE);
        selenium.select(CUSTOM_PROPERTIES_EDMONDSONS, SELENIUM_LABEL_PREPEND + FAKE_EDMONDSONS_GRADE);
        selenium.select(CUSTOM_PROPERTIES_TNM_STAGE_T, SELENIUM_LABEL_PREPEND + FAKE_TNM_GRADE);
        selenium.select(CUSTOM_PROPERTIES_TNM_STAGE_N, SELENIUM_LABEL_PREPEND + FAKE_TNM_GRADE);
        selenium.select(CUSTOM_PROPERTIES_TNM_STAGE_M, SELENIUM_LABEL_PREPEND + FAKE_TNM_GRADE);
        selenium.select(CUSTOM_PROPERTIES_ECOG_PERFORMANCE_STATUS, SELENIUM_LABEL_PREPEND + FAKE_PERFORMANCE_STATUS);
        selenium.select(CUSTOM_PROPERTIES_H_PYLORI_STATUS, SELENIUM_LABEL_PREPEND + FAKE_POSITIVE);
        selenium.type(CUSTOM_PROPERTIES_H_PYLORI_STRAIN, FAKE_STRING);
        selenium.select(CUSTOM_PROPERTIES_BCLCC_STAGE, SELENIUM_LABEL_PREPEND + FAKE_BCLCC_STAGE);
        selenium.type(CUSTOM_PROPERTIES_PERCENTAGE_OF_TUMOR_CELLS, FAKE_FLOAT);
        selenium.type(CUSTOM_PROPERTIES_PERCENTAGE_OF_NECROSIS, FAKE_FLOAT);
        selenium.select(CUSTOM_PROPERTIES_ETIOLOGY, SELENIUM_LABEL_PREPEND + FAKE_ETIOLOGY);
        selenium.select(CUSTOM_PROPERTIES_COLON_CANCER_HISTOLOGY, SELENIUM_LABEL_PREPEND + FAKE_COLON_CANCER_HISTOLOGY);
        selenium.select(CUSTOM_PROPERTIES_GASTRIC_CANCER_HISTOLOGY, SELENIUM_LABEL_PREPEND + FAKE_GASTRIC_CANCER_HISTOLOGY);
        selenium.select(CUSTOM_PROPERTIES_NON_TUMEROUS_LIVER_HISTOLOGY, SELENIUM_LABEL_PREPEND + FAKE_NON_TUMEROUS_LIVER_HISTOLOGY);
        selenium.select(CUSTOM_PROPERTIES_VEINOUS_INFILTRATION, SELENIUM_LABEL_PREPEND + FAKE_VEINOUS_INFILTRATION);
        selenium.select(CUSTOM_PROPERTIES_PORTAL_VEIN_THROMBOSIS, SELENIUM_LABEL_PREPEND + FAKE_YES);
        selenium.select(CUSTOM_PROPERTIES_NUMBER_OF_NODULES, SELENIUM_LABEL_PREPEND + FAKE_NUMBER_OF_NODULES);
        selenium.select(CUSTOM_PROPERTIES_HCVAB_STATUS, SELENIUM_LABEL_PREPEND + FAKE_POSITIVE);
        selenium.select(CUSTOM_PROPERTIES_HBSAG_STATUS, SELENIUM_LABEL_PREPEND + FAKE_POSITIVE);
        selenium.type(CUSTOM_PROPERTIES_ALANIN_TRANSAMINASE, FAKE_FLOAT);
        selenium.type(CUSTOM_PROPERTIES_SERUM_ALBUMIN, FAKE_FLOAT);
        selenium.type(CUSTOM_PROPERTIES_ASPARTATE_AMINO_TRANSFERASE, FAKE_FLOAT);
        selenium.type(CUSTOM_PROPERTIES_TOTAL_BILIRUBIN, FAKE_FLOAT);
        selenium.type(CUSTOM_PROPERTIES_SERUM_GLUTAMATE_OXALOACETIC_TRANSAMINASE, FAKE_FLOAT);
        selenium.type(CUSTOM_PROPERTIES_SERUM_GLUTAMATE_PYRUVATE_TRANSAMINASE, FAKE_FLOAT);
        selenium.type(CUSTOM_PROPERTIES_ALBUMIN, FAKE_FLOAT);
        selenium.select(CUSTOM_PROPERTIES_ANATOMICAL_LOCATION_OF_PANCREATIC_CANCER, SELENIUM_LABEL_PREPEND + FAKE_PANCREAS_ANATOMICAL_LOCATION);
        selenium.type(CUSTOM_PROPERTIES_PROPORTION_OF_BAC, FAKE_PROPORTION);
        selenium.type(CUSTOM_PROPERTIES_ANATOMICAL_LOCATION_OF_GBM, FAKE_STRING);
        selenium.type(CUSTOM_PROPERTIES_SURGERY_TYPE, FAKE_STRING);
        selenium.select(CUSTOM_PROPERTIES_COMPLETE_RESECTION_ACHIEVED, FAKE_YES);
        selenium.type(CUSTOM_PROPERTIES_COMPLETE_RESECTION_NOT_POSSIBLE_BECAUSE, FAKE_RESECTION_REASON);
        selenium.select(CUSTOM_PROPERTIES_PRE_OPERATIVE_CHEMO, FAKE_YES);
        selenium.select(CUSTOM_PROPERTIES_TREATMENT_SINCE_SURGERY, FAKE_YES);
        selenium.type(CUSTOM_PROPERTIES_PRE_OPERATIVE_CHEMO_DRUG_OR_REGIME, FAKE_STRING);
        selenium.type(CUSTOM_PROPERTIES_PRE_OPERATIVE_CHEMO_DOSEAGE, FAKE_STRING);
        selenium.type(CUSTOM_PROPERTIES_PRE_OPERATIVE_CHEMO_CYCLES, FAKE_INT);
        selenium.type(CUSTOM_PROPERTIES_PRE_OPERATIVE_CHEMO_START_DATE, FAKE_DATE);
        selenium.type(CUSTOM_PROPERTIES_PRE_OPERATIVE_CHEMO_END_DATE, FAKE_DATE);
        selenium.type(CUSTOM_PROPERTIES_TREATMENT_DRUG_OR_REGIME_SINCE_SURGERY, FAKE_STRING);
        selenium.type(CUSTOM_PROPERTIES_TREATMENT_DOSEAGE_SINCE_SURGERY, FAKE_STRING);
        selenium.type(CUSTOM_PROPERTIES_TREATMENT_CYCLES_SINCE_SURGERY, FAKE_INT);
        selenium.type(CUSTOM_PROPERTIES_TREATMENT_SINCE_SURGERY_START_DATE, FAKE_DATE);
        selenium.type(CUSTOM_PROPERTIES_TREATMENT_SINCE_SURGERY_END_DATE, FAKE_DATE);
        selenium.select(CUSTOM_PROPERTIES_RADIATION, FAKE_YES);
        selenium.select(CUSTOM_PROPERTIES_HAS_REGULAR_FOLLOW_UP, FAKE_YES);
        selenium.type(CUSTOM_PROPERTIES_MONTHS_BETWEEN_FOLLOW_UP, FAKE_INT);
        selenium.type(CUSTOM_PROPERTIES_FOLLOW_UP_START_DATE, FAKE_DATE);
        selenium.type(CUSTOM_PROPERTIES_FOLLOW_UP_END_DATE, FAKE_DATE);
        selenium.select(CUSTOM_PROPERTIES_RECURRANCE_STATUS, SELENIUM_LABEL_PREPEND + FAKE_YES);
        selenium.select(CUSTOM_PROPERTIES_RECURRANCE_LOCATION, SELENIUM_LABEL_PREPEND + FAKE_LOCATION);
        selenium.type(CUSTOM_PROPERTIES_RECURRANCE_ORGAN_SITE, FAKE_STRING);
        selenium.select(CUSTOM_PROPERTIES_RECURRANCE_FOUND_BY_INTERVENING_SYMPTOMS, FAKE_YES);
        selenium.type(CUSTOM_PROPERTIES_NO_RECURRANCE_AND_DATE_LAST_KNOWN_DISEASE_FREE, FAKE_DATE);
        selenium.type(CUSTOM_PROPERTIES_PROGRESSING_DATE_OF_FIRST_KNOWN_PROGRESSION, FAKE_DATE);
        selenium.select(CUSTOM_PROPERTIES_PROGRESSION_STATUS, SELENIUM_LABEL_PREPEND + FAKE_PROGRESSING);
        selenium.select(CUSTOM_PROPERTIES_PROGRESSION_SYMPTOMATIC, SELENIUM_LABEL_PREPEND + FAKE_SYMPTOMATIC);
        selenium.select(CUSTOM_PROPERTIES_PROGRESSION_LOCATION, SELENIUM_LABEL_PREPEND + FAKE_LOCATION);
        selenium.type(CUSTOM_PROPERTIES_DISTANT_PROGRESSION_ORGAN_SITE, FAKE_STRING);
        selenium.type(CUSTOM_PROPERTIES_NO_PROGRESSION_AND_DATE_LAST_KNOWN_WITHOUT_PROGRESSION, FAKE_DATE);
        selenium.select(CUSTOM_PROPERTIES_PATIENT_VITAL_STATUS, SELENIUM_LABEL_PREPEND + FAKE_DECEASED);
        selenium.type(CUSTOM_PROPERTIES_DATE_OF_DEATH, FAKE_DATE);
        selenium.select(CUSTOM_PROPERTIES_CAUSE_OF_DEATH, SELENIUM_LABEL_PREPEND + FAKE_COD);
        selenium.type(CUSTOM_PROPERTIES_NOT_DEAD_LAST_KNOWN_DATE_ALIVE, FAKE_DATE);
        selenium.select(CUSTOM_PROPERTIES_HER2_MUTATION_STATUS, SELENIUM_LABEL_PREPEND + FAKE_MUTANT);
        selenium.type(CUSTOM_PROPERTIES_HER2_MUTATION_TYPE, FAKE_STRING);
        selenium.type(CUSTOM_PROPERTIES_HER2_MUTATION_SITE, FAKE_STRING);
        selenium.select(CUSTOM_PROPERTIES_LUNG_CANCER_KRAS_MUTATION_STATUS, SELENIUM_LABEL_PREPEND + FAKE_MUTANT);
        selenium.type(CUSTOM_PROPERTIES_LUNG_CANCER_KRAS_MUTATION_TYPE, FAKE_STRING);
        selenium.type(CUSTOM_PROPERTIES_LUNG_CANCER_KRAS_MUTATION_SITE, FAKE_STRING);
        selenium.select(CUSTOM_PROPERTIES_LUNG_CANCER_EGFR_MUTATION_STATUS, SELENIUM_LABEL_PREPEND + FAKE_MUTANT);
        selenium.type(CUSTOM_PROPERTIES_LUNG_CANCER_EGFR_MUTATION_TYPE, FAKE_STRING);
        selenium.type(CUSTOM_PROPERTIES_LUNG_CANCER_EGFR_MUTATION_SITE, FAKE_STRING);
        selenium.type(CUSTOM_PROPERTIES_ALPHA_FETO_PROTEIN, FAKE_FLOAT);
        selenium.select(CUSTOM_PROPERTIES_COLON_CANCER_KRAS_MUTATION_STATUS, SELENIUM_LABEL_PREPEND + FAKE_MUTANT);
        selenium.type(CUSTOM_PROPERTIES_COLON_CANCER_KRAS_MUTATION_TYPE, FAKE_STRING);
        selenium.type(CUSTOM_PROPERTIES_COLON_CANCER_KRAS_MUTATION_SITE, FAKE_STRING);
        selenium.type(CUSTOM_PROPERTIES_APC_STATUS, FAKE_STRING);
        selenium.select(CUSTOM_PROPERTIES_BRAF_MUTATION_STATUS, SELENIUM_LABEL_PREPEND + FAKE_MUTANT);
        selenium.type(CUSTOM_PROPERTIES_BRAF_MUTATION_TYPE, FAKE_STRING);
        selenium.type(CUSTOM_PROPERTIES_BRAF_MUTATION_SITE, FAKE_STRING);
        selenium.select(CUSTOM_PROPERTIES_PI3K_MUTATION_STATUS, SELENIUM_LABEL_PREPEND + FAKE_MUTANT);
        selenium.type(CUSTOM_PROPERTIES_PI3K_MUTATION_TYPE, FAKE_STRING);
        selenium.type(CUSTOM_PROPERTIES_PI3K_MUTATION_SITE, FAKE_STRING);
        selenium.select(CUSTOM_PROPERTIES_BETA_CATENIN_MUTATION_STATUS, SELENIUM_LABEL_PREPEND + FAKE_MUTANT);
        selenium.type(CUSTOM_PROPERTIES_BETA_CATENIN_EXPRESSION_LEVEL, FAKE_STRING);
        selenium.select(CUSTOM_PROPERTIES_MSI_MUTATION_STATUS, SELENIUM_LABEL_PREPEND + FAKE_MSI_MUTATION_STATUS);
        selenium.select(CUSTOM_PROPERTIES_PANCREATIC_CANCER_KRAS_MUTATION_STATUS, SELENIUM_LABEL_PREPEND + FAKE_MUTANT);
        selenium.type(CUSTOM_PROPERTIES_PANCREATIC_CANCER_KRAS_MUTATION_TYPE, FAKE_STRING);
        selenium.type(CUSTOM_PROPERTIES_PANCREATIC_CANCER_KRAS_MUTATION_SITE, FAKE_STRING);
        selenium.select(CUSTOM_PROPERTIES_PANCREATIC_CANCER_EGFR_MUTATION_STATUS, SELENIUM_LABEL_PREPEND + FAKE_MUTANT);
        selenium.type(CUSTOM_PROPERTIES_PANCREATIC_CANCER_EGFR_MUTATION_TYPE, FAKE_STRING);
        selenium.type(CUSTOM_PROPERTIES_PANCREATIC_CANCER_EGFR_MUTATION_SITE, FAKE_STRING);
        selenium.select(CUSTOM_PROPERTIES_PANCREATIC_CANCER_P53_MUTATION_STATUS, SELENIUM_LABEL_PREPEND + FAKE_MUTANT);
        selenium.type(CUSTOM_PROPERTIES_PANCREATIC_CANCER_P53_MUTATION_TYPE, FAKE_STRING);
        selenium.type(CUSTOM_PROPERTIES_PANCREATIC_CANCER_P53_MUTATION_SITE, FAKE_STRING);
        selenium.select(CUSTOM_PROPERTIES_GBM_CANCER_KRAS_MUTATION_STATUS, SELENIUM_LABEL_PREPEND + FAKE_MUTANT);
        selenium.type(CUSTOM_PROPERTIES_GBM_CANCER_KRAS_MUTATION_TYPE, FAKE_STRING);
        selenium.type(CUSTOM_PROPERTIES_GBM_CANCER_KRAS_MUTATION_SITE, FAKE_STRING);
        selenium.select(CUSTOM_PROPERTIES_GBM_CANCER_EGFR_MUTATION_STATUS, SELENIUM_LABEL_PREPEND + FAKE_MUTANT);
        selenium.type(CUSTOM_PROPERTIES_GBM_CANCER_EGFR_MUTATION_TYPE, FAKE_STRING);
        selenium.type(CUSTOM_PROPERTIES_GBM_CANCER_EGFR_MUTATION_SITE, FAKE_STRING);
        selenium.select(CUSTOM_PROPERTIES_GBM_CANCER_EGFR_V3_MUTATION_STATUS, FAKE_YES);
        selenium.select(CUSTOM_PROPERTIES_GBM_CANCER_PTEN_EXPRESSION, FAKE_YES);
        selenium.select(CUSTOM_PROPERTIES_LIVER_CANCER_TREATMENT, SELENIUM_LABEL_PREPEND + FAKE_LIVER_CANCER_TREATMENT);
    }
    //CHECKSTYLE:ON

    /**
     * {@inheritDoc}
     */
    @Override
    protected void viewExtensionData() {
        assertTrue(selenium.isTextPresent("Fixed Formalin"));
        assertTrue(selenium.isTextPresent(ClientProperties.getAnatomicSource()));
        assertTrue(selenium.isTextPresent("32"));
        assertTrue(selenium.isTextPresent("25"));
        assertTrue(selenium.isTextPresent("50"));
        assertTrue(selenium.isTextPresent("Yes"));
        ViewPageCustomFieldValueWrappers[] wrappers = getViewPageCustomFieldValueWrappers();
        for (int i = 0; i < wrappers.length; i++) {
            String xPath = String.format(VIEW_PAGE_XPATH_FIELD_VALUE_TEMPLATE, wrappers[i].getTableNumber(),
                    wrappers[i].getRowNumber());
            assertEquals(wrappers[i].getExpectedStringValue(), selenium.getText(xPath));
        }
    }

    private void setDynamicTextSearchFields(String value) {
        selenium.type(CUSTOM_PROPERTIES_STORAGE_TEMPERATURE, value);
        selenium.type(CUSTOM_PROPERTIES_TIME_LAPSE_TO_PROCESSING, value);
        selenium.type(CUSTOM_PROPERTIES_SPECIMEN_DENSITY, value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getExtensionUnavailableText() {
        return CUSTOM_PROPERTY_UNAVAILABLE_TEXT;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int[] getExtensionViewIndexes() {
        return CUSTOM_PROPERTY_VIEW_INDEXES;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void viewEmptyExtensions() {
        super.viewEmptyExtensions();
        ViewPageCustomFieldValueWrappers[] wrappers = getViewPageCustomFieldValueWrappers();
        for (int i = 0; i < wrappers.length; i++) {
            String xPath = String.format(VIEW_PAGE_XPATH_FIELD_VALUE_TEMPLATE, wrappers[i].getTableNumber(),
                    wrappers[i].getRowNumber());
            assertEquals(getExtensionUnavailableText(), selenium.getText(xPath));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void setUpExpectedDetailsPanelColumns() {
        super.setUpExpectedDetailsPanelColumns();
        setExpectedDetailsPanelColumn1Label("Anatomic Source");
        setExpectedDetailsPanelColumn1Value(ClientProperties.getAnatomicSource());
        setExpectedDetailsPanelColumn3Label("Preservation Type");
        setExpectedDetailsPanelColumn3Value("Fixed Formalin");
        setExpectedDetailsPanelColumn4Value("Available on Request");
    }

    //CHECKSTYLE:OFF
    private ViewPageCustomFieldValueWrappers[] getViewPageCustomFieldValueWrappers() {
        return new ViewPageCustomFieldValueWrappers[] {
            new ViewPageCustomFieldValueWrappers(4, 4, FAKE_DATE),
            new ViewPageCustomFieldValueWrappers(4, 5, FAKE_FLOAT),
            new ViewPageCustomFieldValueWrappers(4, 6, FAKE_FLOAT),
            new ViewPageCustomFieldValueWrappers(4, 7, FAKE_SMOKING_STATUS),
            new ViewPageCustomFieldValueWrappers(4, 8, FAKE_DATE),
            new ViewPageCustomFieldValueWrappers(4, 9, FAKE_INT),
            new ViewPageCustomFieldValueWrappers(4, 10, FAKE_FLOAT),
            new ViewPageCustomFieldValueWrappers(4, 11, FAKE_FLOAT),
            new ViewPageCustomFieldValueWrappers(4, 12, FAKE_FLOAT),
            new ViewPageCustomFieldValueWrappers(4, 13, FAKE_FLOAT),
            new ViewPageCustomFieldValueWrappers(4, 14, FAKE_DRINKING_STATUS),
            new ViewPageCustomFieldValueWrappers(4, 15, FAKE_USE_LEVEL),
            new ViewPageCustomFieldValueWrappers(4, 16, FAKE_FLOAT),
            new ViewPageCustomFieldValueWrappers(4, 17, FAKE_FAMILY_HISTORY),
            new ViewPageCustomFieldValueWrappers(6, 1, FAKE_FAMILY_HISTORY),
            new ViewPageCustomFieldValueWrappers(8, 1, FAKE_FAMILY_HISTORY),
            new ViewPageCustomFieldValueWrappers(10, 1, FAKE_FAMILY_HISTORY),
            new ViewPageCustomFieldValueWrappers(12, 1, FAKE_FAMILY_HISTORY),
            new ViewPageCustomFieldValueWrappers(14, 1, FAKE_FAMILY_HISTORY),
            new ViewPageCustomFieldValueWrappers(16, 1, FAKE_FAMILY_HISTORY),
            new ViewPageCustomFieldValueWrappers(16, 2, FAKE_OCCUPATION),
            new ViewPageCustomFieldValueWrappers(18, 1, FAKE_FLOAT),
            new ViewPageCustomFieldValueWrappers(18, 2, FAKE_CHILDS_GRADE),
            new ViewPageCustomFieldValueWrappers(18, 3, FAKE_EDMONDSONS_GRADE),
            new ViewPageCustomFieldValueWrappers(18, 4, FAKE_TNM_GRADE),
            new ViewPageCustomFieldValueWrappers(18, 5, FAKE_TNM_GRADE),
            new ViewPageCustomFieldValueWrappers(18, 6, FAKE_TNM_GRADE),
            new ViewPageCustomFieldValueWrappers(18, 7, FAKE_PERFORMANCE_STATUS),
            new ViewPageCustomFieldValueWrappers(20, 1, FAKE_POSITIVE),
            new ViewPageCustomFieldValueWrappers(20, 2, FAKE_STRING),
            new ViewPageCustomFieldValueWrappers(24, 1, FAKE_BCLCC_STAGE),
            new ViewPageCustomFieldValueWrappers(24, 2, FAKE_FLOAT),
            new ViewPageCustomFieldValueWrappers(24, 3, FAKE_FLOAT),
            new ViewPageCustomFieldValueWrappers(24, 4, FAKE_NUMBER_OF_NODULES),
            new ViewPageCustomFieldValueWrappers(24, 5, FAKE_FLOAT),
            new ViewPageCustomFieldValueWrappers(26, 1, FAKE_COLON_CANCER_HISTOLOGY),
            new ViewPageCustomFieldValueWrappers(20, 3, FAKE_GASTRIC_CANCER_HISTOLOGY),
            new ViewPageCustomFieldValueWrappers(50, 1, FAKE_ETIOLOGY),
            new ViewPageCustomFieldValueWrappers(50, 2, FAKE_NON_TUMEROUS_LIVER_HISTOLOGY),
            new ViewPageCustomFieldValueWrappers(50, 3, FAKE_VEINOUS_INFILTRATION),
            new ViewPageCustomFieldValueWrappers(50, 4, FAKE_YES),
            new ViewPageCustomFieldValueWrappers(50, 5, FAKE_POSITIVE),
            new ViewPageCustomFieldValueWrappers(50, 6, FAKE_POSITIVE),
            new ViewPageCustomFieldValueWrappers(50, 7, FAKE_FLOAT),
            new ViewPageCustomFieldValueWrappers(50, 8, FAKE_FLOAT),
            new ViewPageCustomFieldValueWrappers(50, 9, FAKE_FLOAT),
            new ViewPageCustomFieldValueWrappers(50, 10, FAKE_FLOAT),
            new ViewPageCustomFieldValueWrappers(50, 11, FAKE_FLOAT),
            new ViewPageCustomFieldValueWrappers(28, 1, FAKE_FLOAT),
            new ViewPageCustomFieldValueWrappers(28, 2, FAKE_PANCREAS_ANATOMICAL_LOCATION),
            new ViewPageCustomFieldValueWrappers(22, 1, FAKE_PROPORTION),
            new ViewPageCustomFieldValueWrappers(30, 1, FAKE_STRING),
            new ViewPageCustomFieldValueWrappers(32, 1, FAKE_STRING),
            new ViewPageCustomFieldValueWrappers(32, 2, FAKE_YES),
            new ViewPageCustomFieldValueWrappers(32, 3, FAKE_RESECTION_REASON),
            new ViewPageCustomFieldValueWrappers(32, 4, FAKE_YES),
            new ViewPageCustomFieldValueWrappers(32, 5, FAKE_STRING),
            new ViewPageCustomFieldValueWrappers(32, 6, FAKE_STRING),
            new ViewPageCustomFieldValueWrappers(32, 7, FAKE_INT),
            new ViewPageCustomFieldValueWrappers(32, 8, FAKE_DATE),
            new ViewPageCustomFieldValueWrappers(32, 9, FAKE_DATE),
            new ViewPageCustomFieldValueWrappers(32, 10, FAKE_YES),
            new ViewPageCustomFieldValueWrappers(32, 11, FAKE_STRING),
            new ViewPageCustomFieldValueWrappers(32, 12, FAKE_STRING),
            new ViewPageCustomFieldValueWrappers(32, 13, FAKE_INT),
            new ViewPageCustomFieldValueWrappers(32, 14, FAKE_DATE),
            new ViewPageCustomFieldValueWrappers(32, 15, FAKE_DATE),
            new ViewPageCustomFieldValueWrappers(32, 16, FAKE_YES),
            new ViewPageCustomFieldValueWrappers(34, 1, FAKE_LIVER_CANCER_TREATMENT),
            new ViewPageCustomFieldValueWrappers(36, 1, FAKE_YES),
            new ViewPageCustomFieldValueWrappers(36, 2, FAKE_INT),
            new ViewPageCustomFieldValueWrappers(36, 3, FAKE_DATE),
            new ViewPageCustomFieldValueWrappers(36, 4, FAKE_DATE),
            new ViewPageCustomFieldValueWrappers(36, 5, FAKE_YES),
            new ViewPageCustomFieldValueWrappers(36, 6, FAKE_LOCATION),
            new ViewPageCustomFieldValueWrappers(36, 7, FAKE_STRING),
            new ViewPageCustomFieldValueWrappers(36, 8, FAKE_YES),
            new ViewPageCustomFieldValueWrappers(36, 9, FAKE_DATE),
            new ViewPageCustomFieldValueWrappers(36, 10, FAKE_PROGRESSING),
            new ViewPageCustomFieldValueWrappers(36, 11, FAKE_DATE),
            new ViewPageCustomFieldValueWrappers(36, 12, FAKE_SYMPTOMATIC),
            new ViewPageCustomFieldValueWrappers(36, 13, FAKE_LOCATION),
            new ViewPageCustomFieldValueWrappers(36, 14, FAKE_STRING),
            new ViewPageCustomFieldValueWrappers(36, 15, FAKE_DATE),
            new ViewPageCustomFieldValueWrappers(36, 16, FAKE_DECEASED),
            new ViewPageCustomFieldValueWrappers(36, 17, FAKE_DATE),
            new ViewPageCustomFieldValueWrappers(36, 18, FAKE_COD),
            new ViewPageCustomFieldValueWrappers(36, 19, FAKE_DATE),
            new ViewPageCustomFieldValueWrappers(38, 1, FAKE_MUTANT),
            new ViewPageCustomFieldValueWrappers(38, 2, FAKE_STRING),
            new ViewPageCustomFieldValueWrappers(38, 3, FAKE_STRING),
            new ViewPageCustomFieldValueWrappers(40, 1, FAKE_MUTANT),
            new ViewPageCustomFieldValueWrappers(40, 2, FAKE_STRING),
            new ViewPageCustomFieldValueWrappers(40, 3, FAKE_STRING),
            new ViewPageCustomFieldValueWrappers(40, 4, FAKE_MUTANT),
            new ViewPageCustomFieldValueWrappers(40, 5, FAKE_STRING),
            new ViewPageCustomFieldValueWrappers(40, 6, FAKE_STRING),
            new ViewPageCustomFieldValueWrappers(42, 1, FAKE_FLOAT),
            new ViewPageCustomFieldValueWrappers(44, 1, FAKE_MUTANT),
            new ViewPageCustomFieldValueWrappers(44, 2, FAKE_STRING),
            new ViewPageCustomFieldValueWrappers(44, 3, FAKE_STRING),
            new ViewPageCustomFieldValueWrappers(44, 4, FAKE_STRING),
            new ViewPageCustomFieldValueWrappers(44, 5, FAKE_MUTANT),
            new ViewPageCustomFieldValueWrappers(44, 6, FAKE_STRING),
            new ViewPageCustomFieldValueWrappers(44, 7, FAKE_STRING),
            new ViewPageCustomFieldValueWrappers(44, 8, FAKE_MUTANT),
            new ViewPageCustomFieldValueWrappers(44, 9, FAKE_STRING),
            new ViewPageCustomFieldValueWrappers(44, 10, FAKE_STRING),
            new ViewPageCustomFieldValueWrappers(44, 11, FAKE_MUTANT),
            new ViewPageCustomFieldValueWrappers(44, 12, FAKE_STRING),
            new ViewPageCustomFieldValueWrappers(44, 13, FAKE_MSI_MUTATION_STATUS),
            new ViewPageCustomFieldValueWrappers(46, 1, FAKE_MUTANT),
            new ViewPageCustomFieldValueWrappers(46, 2, FAKE_STRING),
            new ViewPageCustomFieldValueWrappers(46, 3, FAKE_STRING),
            new ViewPageCustomFieldValueWrappers(46, 4, FAKE_MUTANT),
            new ViewPageCustomFieldValueWrappers(46, 5, FAKE_STRING),
            new ViewPageCustomFieldValueWrappers(46, 6, FAKE_STRING),
            new ViewPageCustomFieldValueWrappers(46, 7, FAKE_MUTANT),
            new ViewPageCustomFieldValueWrappers(46, 8, FAKE_STRING),
            new ViewPageCustomFieldValueWrappers(46, 9, FAKE_STRING),
            new ViewPageCustomFieldValueWrappers(48, 1, FAKE_MUTANT),
            new ViewPageCustomFieldValueWrappers(48, 2, FAKE_STRING),
            new ViewPageCustomFieldValueWrappers(48, 3, FAKE_STRING),
            new ViewPageCustomFieldValueWrappers(48, 4, FAKE_MUTANT),
            new ViewPageCustomFieldValueWrappers(48, 5, FAKE_STRING),
            new ViewPageCustomFieldValueWrappers(48, 6, FAKE_STRING),
            new ViewPageCustomFieldValueWrappers(48, 7, FAKE_YES),
            new ViewPageCustomFieldValueWrappers(48, 8, FAKE_YES)
        };
    }
    //CHECKSTYLE:ON

    /**
     * private inner class storing view page custom field values.
     * @author dharley
     */
    private static class ViewPageCustomFieldValueWrappers {
        private final int tableNumber;
        private final int rowNumber;
        private final String expectedValueString;
        ViewPageCustomFieldValueWrappers(int tableNumber, int rowNumber, String expectedValueString) {
            this.tableNumber = tableNumber;
            this.rowNumber = rowNumber;
            this.expectedValueString = expectedValueString;
        }
        int getTableNumber() {
            return tableNumber;
        }
        int getRowNumber() {
            return rowNumber;
        }
        String getExpectedStringValue() {
            return expectedValueString;
        }
    }

}
