/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.test.selenium.main;

import org.junit.Test;

import com.fiveamsolutions.tissuelocator.test.ClientProperties;
import com.fiveamsolutions.tissuelocator.test.selenium.AbstractTissueLocatorSeleniumTest;

/**
 * @author ddasgupta
 */
public class LoginTest extends AbstractTissueLocatorSeleniumTest {

    private static final String NEW_PASS = "abC123";
    private static final String CONTACT = "Please contact your administrator for assistance.";
    private static final String MISSING_USERNAME = "Email address must be provided.";
    private static final String UNKNOWN_USERNAME = "That email address is unknown.";
    private static final String MISSING_PASSWORD = "New Password Confirmation must be provided.";
    private static final String MISSING_PASSWORD_CONFIRMATION = "New Password Confirmation must be provided.";
    private static final String INACTIVE_USER = "That user has been deactivated.";
    private static final String TOO_MANY_REQUESTS = "Too many requests for that user have been submitted.";

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getScriptName() {
        return "LoginTest.sql";
    }

    /**
     * verify that the login page works.
     */
    @Test
    public void testLogin() {
        loginAsUser1();
        clickAndWait("link=Sign Out");
        login(ClientProperties.getResearcherEmail().toUpperCase(), PASSWORD, true);
        clickAndWait("link=Sign Out");
        loginAsAdmin();
        clickAndWait("link=Sign Out");
        login(ClientProperties.getConsortiumAdminEmail().toUpperCase(), PASSWORD, true);
        clickAndWait("link=Sign Out");
    }

    /**
     * verify that the login page works.
     */
    @Test
    public void testInvalidLogins() {
        login(ClientProperties.getResearcherEmail(), "invalidpassword", false);
        login(ClientProperties.getResearcherEmail(), "", false);
        login("idontexist", PASSWORD, false);
        login("", PASSWORD, false);
    }

    /**
     * test the forgot password functionality.
     */
    @Test
    public void testChangePassword() {
        // get back to login page
        selenium.open(CONTEXT_ROOT);
        if (selenium.isElementPresent("link=Logout")) {
            clickAndWait("link=Logout");
        }
        assertTrue(selenium.isElementPresent("link=exact:Forgot password?"));

        // get over to the forgot page & test a couple bogus emails
        clickAndWait("link=exact:Forgot password?");
        selenium.selectFrame("popupFrame");
        assertTrue(selenium.isTextPresent("Type in your email address"));
        assertTrue(selenium.isElementPresent("username"));
        clickAndWait("forgotPasswordSubmit");
        assertTrue(selenium.isTextPresent(MISSING_USERNAME));
        selenium.type("username", "idontexist");
        clickAndWait("forgotPasswordSubmit");
        assertTrue(selenium.isTextPresent(UNKNOWN_USERNAME));
        assertTrue(selenium.isTextPresent(CONTACT));
        selenium.type("username", "test-user-1@example.com");
        clickAndWait("forgotPasswordSubmit");
        assertTrue(selenium.isTextPresent(INACTIVE_USER));
        assertTrue(selenium.isTextPresent(CONTACT));
        selenium.type("username", "test-user-4@example.com");
        clickAndWait("forgotPasswordSubmit");
        assertTrue(selenium.isTextPresent(TOO_MANY_REQUESTS));
        assertTrue(selenium.isTextPresent(CONTACT));

        // press cancel, verify get back to login page
        clickAndWait("forgotPasswordCancel");
        selenium.selectWindow(null);
        pause(LONG_DELAY);
        assertTrue(selenium.isElementPresent("link=Forgot password?"));

        // get to forgot & type in a real username
        clickAndWait("link=exact:Forgot password?");
        selenium.selectFrame("popupFrame");
        selenium.type("username", "test-user-2@example.com");
        clickAndWait("forgotPasswordSubmit");
        assertTrue(selenium.isTextPresent("Your request has been submitted"));
        clickAndWait("link=Return to login.");
        selenium.selectWindow(null);
        pause(LONG_DELAY);
        assertTrue(selenium.isElementPresent("link=Forgot password?"));

        selenium.open(CONTEXT_ROOT + "/protected/request/viewCartSession.action");
        assertTrue(selenium.isElementPresent("link=exact:Forgot password?"));
        clickAndWait("link=exact:Forgot password?");
        selenium.selectFrame("popupFrame");
        assertTrue(selenium.isTextPresent("Type in your email address"));
        assertTrue(selenium.isElementPresent("username"));
        clickAndWait("forgotPasswordCancel");
        selenium.selectWindow(null);
        assertTrue(selenium.isElementPresent("link=Forgot password?"));

        // now go use the sql-script faked nonce to simulate receiving the email
        selenium.open(CONTEXT_ROOT + "/changePassword/input.action?key=1");
        // first try varous wrong username/mismatched password type tests
        clickAndWait("changePasswordSubmit");
        assertTrue(selenium.isTextPresent(MISSING_USERNAME));
        assertTrue(selenium.isTextPresent(MISSING_PASSWORD));
        assertTrue(selenium.isTextPresent(MISSING_PASSWORD_CONFIRMATION));

        selenium.type("username", "idontexist");
        selenium.type("requested", NEW_PASS);
        selenium.type("retyped", NEW_PASS);
        clickAndWait("changePasswordSubmit");
        assertTrue(selenium.isTextPresent(UNKNOWN_USERNAME));

        // this would mean that the user knows a valid nonce, but not the correct username
        selenium.type("username", "test-user-5@example.com");
        selenium.type("requested", "abC123");
        selenium.type("retyped", "abC123");
        clickAndWait("changePasswordSubmit");
        assertTrue(selenium.isTextPresent(UNKNOWN_USERNAME));

        selenium.type("username", "test-user-3@example.com");
        selenium.type("requested", NEW_PASS);
        selenium.type("retyped", "abC123455555");
        clickAndWait("changePasswordSubmit");
        assertTrue(selenium.isTextPresent("Passwords do not match."));

        selenium.type("username", "test-user-3@example.com");
        selenium.type("requested", "badpass");
        selenium.type("retyped", "badpass");
        clickAndWait("changePasswordSubmit");
        assertTrue(selenium.isTextPresent("New password must be at least 6 characters and contain one lowercase, "
                + "one uppercase, and one number or symbol."));

        // finally, test a combo that really works
        selenium.type("username", "test-user-3@example.com");
        selenium.type("requested", NEW_PASS);
        selenium.type("retyped", NEW_PASS);
        clickAndWait("changePasswordSubmit");
        assertTrue(selenium.isTextPresent("Password successfully changed"));

        // now retry the nonce to verify that the key has been destroyed
        selenium.open(CONTEXT_ROOT + "/changePassword/input.action?key=1");
        selenium.type("username", "test-user-3@example.com");
        selenium.type("requested", NEW_PASS);
        selenium.type("retyped", NEW_PASS);
        clickAndWait("changePasswordSubmit");
        assertTrue(selenium.isTextPresent(UNKNOWN_USERNAME));

        // and finally, log in with the new password
        login("test-user-3@example.com", NEW_PASS);
    }
}
