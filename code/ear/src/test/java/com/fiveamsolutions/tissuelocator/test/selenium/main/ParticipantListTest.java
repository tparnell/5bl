/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.test.selenium.main;

import java.text.NumberFormat;
import java.text.ParseException;

import com.fiveamsolutions.tissuelocator.test.ClientProperties;
import com.fiveamsolutions.tissuelocator.test.selenium.AbstractListTest;

/**
 * @author smiller
 *
 */
public class ParticipantListTest extends AbstractListTest {
    /**
     * Column count.
     */
    private static final int COL_COUNT = 4;

    /**
     * Button column.
     */
    private static final int BUTTON_COL = 6;
    private static final int RESULT_COUNT = 56;

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getScriptName() {
        return "ParticipantListTest.sql";
    }

    /**
     * test the participant list page.
     * @throws ParseException on error
     */
    public void testList() throws ParseException {
        loginAsAdmin();
        goToListPage();
        extIdFilter();
        verifyExportPresent();
        sorting(COL_COUNT, "participant", false, true);
        paging("participant");
        pageSize(RESULT_COUNT + ClientProperties.getParticipantCount());
        institutionRestriction("participant", ClientProperties.getParticipantTerm(), "institution",
                1, BUTTON_COL, true, ClientProperties.getParticipantCount() > 0);
    }

    /**
     * Go to list page.
     */
    private void goToListPage() {
        mouseOverAndPause("link=Administration");
        clickAndWait("link=" + ClientProperties.getParticipantTerm() + " Administration");
        assertTrue(selenium.isTextPresent(ClientProperties.getParticipantTerm() + " Administration"));
    }

    /**
     * test external id filter.
     */
    private void extIdFilter() {
        if (ClientProperties.getParticipantCount() > 0) {
            showOnlyTestResults();
        }
        assertTrue(selenium.isTextPresent("1-20 of 56 Results"));
        assertTrue(selenium.isTextPresent("1 of 3 Pages"));
        String extId  = selenium.getTable("participant.2.0");
        selenium.type("externalId", extId);
        clickAndWait("btn_search");
        assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
        assertTrue(selenium.isTextPresent("1 of 1 Page"));
        selenium.type("externalId", "");
        clickAndWait("btn_search");
        NumberFormat format = NumberFormat.getInstance();
        String formattedResultCount = format.format(RESULT_COUNT + ClientProperties.getParticipantCount());
        assertTrue(selenium.isTextPresent("1-20 of " + formattedResultCount + " Results"));
    }
}
