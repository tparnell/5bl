<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<%@page import="com.fiveamsolutions.tissuelocator.data.RequestStatus"%>
<%@page import="com.fiveamsolutions.tissuelocator.service.report.ScrollingDataSource"%>
<%@page import="com.fiveamsolutions.tissuelocator.service.report.ReportDataProvider"%>
<%@page import="java.util.HashMap"%>
<%@page import="org.hibernate.validator.InvalidValue"%>
<%@page import="java.util.List"%>
<%@page import="com.fiveamsolutions.nci.commons.data.persistent.PersistentObject"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="com.fiveamsolutions.nci.commons.validator.MultipleCriteriaMessageInterpolator"%>
<%@page import="com.fiveamsolutions.tissuelocator.data.SpecimenRequest"%>
<%@page import="org.hibernate.validator.MessageInterpolator"%>
<%@page import="org.hibernate.validator.ClassValidator"%>
<%@page import="com.fiveamsolutions.tissuelocator.web.listener.SpecimenRequestVotingTask"%>
<%@page import="com.fiveamsolutions.tissuelocator.web.TissueLocatorContextParameterHelper"%>
<%@page import="com.fiveamsolutions.tissuelocator.web.listener.ScheduledRemindersTask"%>
<%@page import="com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry"%>
<%@page import="com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil"%>

<html>
<head>
    <title>Data Validation</title>
</head>
<body>
    <h2>Select a class</h2>
    <br>
    <form action="validateData.jsp">
        <select name="clazz" onchange="submit();">
            <option value="">Select a class</option>
            <option value="com.fiveamsolutions.tissuelocator.data.SpecimenRequest">SpecimenRequest</option>
            <option value="com.fiveamsolutions.tissuelocator.data.Specimen">Specimen</option>
            <option value="com.fiveamsolutions.tissuelocator.data.Participant">Participant</option>
            <option value="com.fiveamsolutions.tissuelocator.data.CollectionProtocol">CollectionProtocol</option>
            <option value="com.fiveamsolutions.tissuelocator.data.Institution" >Institution</option>
            <option value="com.fiveamsolutions.tissuelocator.data.TissueLocatorUser">TissueLocatorUser</option>
            <option value="com.fiveamsolutions.tissuelocator.data.Shipment">Shipment</option>
            <option value="com.fiveamsolutions.tissuelocator.data.RequestStatusTransition">RequestStatusTransition</option>
            <option value="com.fiveamsolutions.tissuelocator.data.SpecimenStatusTransition">SpecimenStatusTransition</option>
            <option value="com.fiveamsolutions.tissuelocator.data.mta.MaterialTransferAgreement">MaterialTransferAgreement</option>
            <option value="com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreement">SignedMaterialTransferAgreement</option>
        </select>
    </form>

    <%
    if (request.getParameter("clazz") != null) {
        int objectCount = 0;
        int errorCount = 0;
        long startTime = System.nanoTime();
    %>

    <h2>Data Validation Results for <%= request.getParameter("clazz") %></h2>
    <br>
    <table class="data">
        <tr>
            <th><div>Id</div></th>
            <th><div>Property Path</div></th>
            <th><div>Message</div></th>
        </tr>


        <%
        try {
            TissueLocatorHibernateUtil.getHibernateHelper().openAndBindSession();
            Class<?> clazz = Class.forName(request.getParameter("clazz"));
            MessageInterpolator interpolator = new MultipleCriteriaMessageInterpolator();
            ClassValidator cv = new ClassValidator(clazz, interpolator);

            ReportDataProvider provider = new ReportDataProvider("from " + clazz.getName(),
                    new HashMap<String, Object>());
            ScrollingDataSource scrollingDataSource = new ScrollingDataSource(provider);
            PersistentObject obj = (PersistentObject) scrollingDataSource.getObject(objectCount++);
            while (obj != null) {
                if (!(obj instanceof SpecimenRequest) || !RequestStatus.DRAFT.equals(((SpecimenRequest) obj).getStatus())) {
                    for (InvalidValue iv : cv.getInvalidValues(obj)) {
                        out.println("<tr>");
                        out.println("<td>");
                        out.println(obj.getId().toString());
                        out.println("</td>");
                        out.println("<td>");
                        out.println(iv.getPropertyPath());
                        out.println("</td>");
                        out.println("<td>");
                        out.println(iv.getMessage());
                        out.println("</td>");
                        out.println("</tr>");
                        errorCount++;
                    }
                }
                obj = (PersistentObject) scrollingDataSource.getObject(objectCount++);
            }
        } catch (Exception e) {
            out.println(e.getMessage());
        } finally {
            TissueLocatorHibernateUtil.getHibernateHelper().unbindAndCleanupSession();
        }
        %>

    </table>

    <h3>Processed objects: <%= objectCount - 1 %></h3>
    <h3>Errors found: <%= errorCount %></h3>
    <h3>Processing Time: <%= (System.nanoTime() - startTime) / 1000000000.0 %> seconds</h3>

    <% } %>

</body>
</html>
