/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.struts2.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;

import org.junit.Test;

import com.fiveamsolutions.tissuelocator.data.AggregateSpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.Person;
import com.fiveamsolutions.tissuelocator.data.Shipment;
import com.fiveamsolutions.tissuelocator.data.ShipmentStatus;
import com.fiveamsolutions.tissuelocator.data.ShippingMethod;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.SpecimenDisposition;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestReviewVote;
import com.fiveamsolutions.tissuelocator.data.SpecimenStatus;
import com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreement;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.fiveamsolutions.tissuelocator.web.test.AbstractTissueLocatorWebTest;
import com.fiveamsolutions.tissuelocator.web.test.ShipmentServiceStub;
import com.opensymphony.xwork2.Action;

/**
 * @author ddasgupta
 *
 */
public class ShipmentActionTest extends AbstractTissueLocatorWebTest {

    /**
     * Tests the list method.
     */
    @Test
    public void testlist() {
        ShipmentServiceStub stub = (ShipmentServiceStub)
            TissueLocatorRegistry.getServiceLocator().getShipmentService();
        assertFalse(stub.isSearchCalled());
        ShipmentAction action = new ShipmentAction(getTestAppSettingService());
        action.prepare();
        assertEquals(Action.SUCCESS, action.list());
        assertEquals(0, action.getObjects().getList().size());
        assertTrue(stub.isSearchCalled());
    }

    /**
     * test the view action.
     */
    @Test
    public void testView() {
        ShipmentAction action = new ShipmentAction(getTestAppSettingService());
        action.prepare();
        assertEquals("edit", action.edit());
        assertTrue(action.getActionMessages().isEmpty());
        action.setCartSize(0);
        assertEquals("edit", action.edit());
        assertFalse(action.getActionMessages().isEmpty());
        assertEquals(1, action.getActionMessages().size());
    }

    /**
     * Test the remove line item functionality.
     */
    @Test
    public void testRemoveLineItem() {
        Shipment s = createShipment();
        SpecimenRequestLineItem li = new SpecimenRequestLineItem();
        s.getLineItems().add(li);

        ShipmentAction action = new ShipmentAction(getTestAppSettingService());
        action.setObject(s);
        action.prepare();
        assertFalse(action.getObject().getLineItems().isEmpty());
        assertEquals("edit", action.removeLineItem());
        assertFalse(action.getObject().getLineItems().isEmpty());
        assertFalse(action.getActionMessages().isEmpty());
        assertEquals(1, action.getActionMessages().size());
        action.clearErrorsAndMessages();
        action.setLineItem(li);
        assertEquals("edit", action.removeLineItem());
        assertTrue(action.getObject().getLineItems().isEmpty());
        assertFalse(action.getActionMessages().isEmpty());
        assertEquals(1, action.getActionMessages().size());
    }

    /**
     * Test aggregate line item removal.
     */
    @Test
    public void testRemoveAggregateLineItem() {
        Shipment s = createShipment();
        AggregateSpecimenRequestLineItem li = new AggregateSpecimenRequestLineItem();
        li.setCriteria("No Criteria Specified");
        s.getAggregateLineItems().add(li);

        ShipmentAction action = new ShipmentAction(getTestAppSettingService());
        action.setObject(s);
        action.prepare();
        assertFalse(action.getObject().getAggregateLineItems().isEmpty());
        assertEquals("edit", action.removeAggregateLineItem());
        assertFalse(action.getObject().getAggregateLineItems().isEmpty());
        assertFalse(action.getActionMessages().isEmpty());
        assertEquals(1, action.getActionMessages().size());
        action.clearErrorsAndMessages();
        action.setAggregateLineItem(li);
        assertEquals("edit", action.removeAggregateLineItem());
        assertTrue(action.getObject().getAggregateLineItems().isEmpty());
        assertFalse(action.getActionMessages().isEmpty());
        assertEquals(1, action.getActionMessages().size());
    }

    /**
     * Test the empty order..
     */
    @Test
    public void testEmpty() {
        Shipment s = createShipment();
        SpecimenRequestLineItem li = new SpecimenRequestLineItem();
        s.getLineItems().add(li);

        ShipmentAction action = new ShipmentAction(getTestAppSettingService());
        action.prepare();
        action.setObject(s);
        assertFalse(action.getObject().getLineItems().isEmpty());
        assertEquals("edit", action.emptyOrder());
        assertTrue(action.getObject().getLineItems().isEmpty());
        assertFalse(action.getActionMessages().isEmpty());
        assertEquals(1, action.getActionMessages().size());
    }

    /**
     * Test removing aggregate line items.
     */
    @Test
    public void testEmptyAggregateOrder() {
        Shipment s = createShipment();
        AggregateSpecimenRequestLineItem li = new AggregateSpecimenRequestLineItem();
        s.getAggregateLineItems().add(li);

        ShipmentAction action = new ShipmentAction(getTestAppSettingService());
        action.prepare();
        action.setObject(s);
        assertFalse(action.getObject().getAggregateLineItems().isEmpty());
        assertEquals("edit", action.emptyAggregateOrder());
        assertTrue(action.getObject().getAggregateLineItems().isEmpty());
        assertFalse(action.getActionMessages().isEmpty());
        assertEquals(1, action.getActionMessages().size());
    }

    /**
     * test going to the request details page.
     */
    @Test
    public void testViewRequestDetails() {
        ShipmentAction action = new ShipmentAction(getTestAppSettingService());
        assertEquals("viewRequestDetails", action.viewRequestDetails());
    }

    /**
     * test going to the request details page.
     * @throws MessagingException on error
     */
    @Test
    public void testSave() throws MessagingException {
        Shipment s = createShipment();
        ShipmentAction action = new ShipmentAction(getTestAppSettingService());
        action.setObject(s);
        s.setLineItems(null);
        s.setAggregateLineItems(null);
        assertNull(action.getLineItems());
        assertEquals(0, action.getSpecificLineItems().length);
        assertEquals(0, action.getGeneralLineItems().length);
        action.prepare();
        assertNull(action.getLineItems());
        assertEquals(0, action.getSpecificLineItems().length);
        assertEquals(0, action.getGeneralLineItems().length);

        s = createShipment();
        action.setObject(s);
        s.getLineItems().add(new SpecimenRequestLineItem());
        AggregateSpecimenRequestLineItem ali1 = createUniqueLineItem("test", "No Criteria Specified", 1);
        AggregateSpecimenRequestLineItem ali2 = createUniqueLineItem("test", "criteria", 1);
        s.getAggregateLineItems().add(ali1);
        s.getAggregateLineItems().add(ali2);
        assertNull(action.getLineItems());
        assertEquals(0, action.getSpecificLineItems().length);
        assertEquals(0, action.getGeneralLineItems().length);
        action.prepare();
        assertEquals(1, action.getLineItems().length);
        assertEquals(1, action.getSpecificLineItems().length);
        assertEquals(1, action.getGeneralLineItems().length);
        action.setIncludeBillingRecipient(false);
        assertEquals("edit", action.save());
        assertFalse(action.getActionMessages().isEmpty());
        assertEquals(1, action.getActionMessages().size());
        ShipmentServiceStub stub = (ShipmentServiceStub)
            TissueLocatorRegistry.getServiceLocator().getShipmentService();
        assertNotNull(stub.getObject());
        assertEquals(s, stub.getObject());
        assertNull(action.getObject().getBillingRecipient());

        s = createShipment();
        action.setObject(s);
        action.prepare();
        action.setIncludeBillingRecipient(true);
        assertEquals("edit", action.save());
        assertNotNull(action.getObject().getBillingRecipient());
    }

    /**
     * Tests marking a shipment as shipped.
     * @throws MessagingException on error
     */
    @Test
    public void testMarkAsShipped() throws MessagingException {
        for (Shipment shipment : createShipments()) {
            ShipmentAction action = new ShipmentAction(getTestAppSettingService());
            Specimen specimen = new Specimen();
            SpecimenRequestLineItem li = new SpecimenRequestLineItem();
            li.setSpecimen(specimen);
            shipment.getLineItems().add(li);

            action.setObject(shipment);
            action.prepare();
            action.setIncludeBillingRecipient(true);
            assertEquals("view", action.markAsShipped());
            assertEquals(action.getObject().getStatus(), ShipmentStatus.SHIPPED);
            assertFalse(action.getActionMessages().isEmpty());
            assertEquals(1, action.getActionMessages().size());
            assertEquals(SpecimenStatus.SHIPPED,
                    action.getObject().getLineItems().iterator().next().getSpecimen().getStatus());
            assertNotNull(action.getObject().getBillingRecipient());
        }

        ShipmentAction action = new ShipmentAction(getTestAppSettingService());
        action.setObject(createShipment());
        action.prepare();
        action.setIncludeBillingRecipient(false);
        assertEquals("view", action.markAsShipped());
        assertNull(action.getObject().getBillingRecipient());
    }

    /**
     * Tests canceling a shipment.
     * @throws MessagingException on error
     */
    @Test
    public void testCancel() throws MessagingException {
        for (Shipment shipment : createShipments()) {
            ShipmentAction action = new ShipmentAction(getTestAppSettingService());
            Specimen specimen = new Specimen();
            SpecimenRequestLineItem li = new SpecimenRequestLineItem();
            li.setSpecimen(specimen);
            shipment.getLineItems().add(li);

            action.setObject(shipment);
            action.prepare();
            assertEquals("view", action.cancel());
            assertEquals(action.getObject().getStatus(), ShipmentStatus.CANCELED);
            assertFalse(action.getActionMessages().isEmpty());
            assertEquals(1, action.getActionMessages().size());
            assertEquals(SpecimenStatus.AVAILABLE,
                    action.getObject().getLineItems().iterator().next().getSpecimen().getStatus());
        }
    }

    /**
     * Test viewing shipment biospecimen quality.
     */
    @Test
    public void testViewShipmentQuality() {
        ShipmentAction action = new ShipmentAction(getTestAppSettingService());
        Shipment s = createShipment();
        action.setObject(s);
        action.prepare();
        assertEquals("quality", action.viewShipmentQuality());
    }

    /**
     * Tests updating shipment biospecimen quality.
     */
    @Test
    public void testUpdateShipmentQuality() {
        ShipmentAction action = new ShipmentAction(getTestAppSettingService());
        Shipment s = createShipment();

        Specimen spec1 = new Specimen();
        spec1.setStatus(SpecimenStatus.SHIPPED);
        Specimen spec2 = new Specimen();
        spec2.setStatus(SpecimenStatus.SHIPPED);
        Specimen spec3 = new Specimen();
        spec3.setStatus(SpecimenStatus.SHIPPED);
        Specimen spec4 = new Specimen();
        spec4.setStatus(SpecimenStatus.SHIPPED);

        SpecimenRequestLineItem li1 = new SpecimenRequestLineItem();
        SpecimenRequestLineItem li2 = new SpecimenRequestLineItem();
        SpecimenRequestLineItem li3 = new SpecimenRequestLineItem();
        SpecimenRequestLineItem li4 = new SpecimenRequestLineItem();

        li1.setSpecimen(spec1);
        li1.setDisposition(SpecimenDisposition.DAMAGED);
        li2.setSpecimen(spec2);
        li2.setDisposition(SpecimenDisposition.PARTIALLY_CONSUMED_DESTROYED);
        li3.setSpecimen(spec3);
        li3.setDisposition(SpecimenDisposition.IN_USE);
        li4.setSpecimen(spec4);
        li4.setDisposition(SpecimenDisposition.IN_USE);

        s.getLineItems().add(li1);
        s.getLineItems().add(li2);
        s.getLineItems().add(li3);
        s.getLineItems().add(li4);

        action.setObject(s);
        action.prepare();
        assertEquals("success", action.updateShipmentQuality());
        assertEquals(action.getObject().getStatus(), ShipmentStatus.RECEIVED);
        assertFalse(action.getActionMessages().isEmpty());
        assertEquals(1, action.getActionMessages().size());

        for (SpecimenRequestLineItem li : action.getObject().getLineItems()) {
            SpecimenDisposition dispo = li.getDisposition();
            if (dispo.equals(SpecimenDisposition.IN_USE)) {
                assertEquals(li.getSpecimen().getStatus(), SpecimenStatus.SHIPPED);
            } else {
                assertEquals(li.getSpecimen().getStatus(), SpecimenStatus.DESTROYED);
            }
        }
    }

    /**
     * test the update pricing action.
     */
    @Test
    public void testUpdatePricing() {
        ShipmentAction action = new ShipmentAction(getTestAppSettingService());
        assertEquals("edit", action.updatePricing());
        assertFalse(action.getActionMessages().isEmpty());
        assertEquals(1, action.getActionMessages().size());
    }

    /**
     * test setting the shipment date.
     * @throws Exception on error.
     */
    @Test
    public void testSetShipmentDate() throws Exception {
        ShipmentAction action = new ShipmentAction(getTestAppSettingService());
        Shipment s = new Shipment();
        s.setShipmentDate(new Date());
        action.setObject(s);
        assertEquals("success", action.setShipmentDate());
        assertNotNull(action.getObject().getShipmentDate());
    }

    /**
     * Tests the prepare method.
     * @throws Exception on error
     */
    @Test
    public void testPrepare() throws Exception {
        ShipmentAction action = new ShipmentAction(getTestAppSettingService());
        Shipment s = new Shipment();

        action.setObject(s);
        action.prepare();
        assertNull(action.getRecipientOrganizationName());
        assertNull(action.getBillingRecipientOrganizationName());
        assertNull(action.getSignedRecipientMta());
        assertNull(action.getSignedSenderMta());
        assertNotNull(action.getReceiptQualities());
        assertEquals(1, action.getReceiptQualities().size());
        assertNotNull(action.getReceiptQualities().get(0));
        assertEquals(1, action.getReceiptQualities().get(0).getId().intValue());

        s = new Shipment();
        s.setRecipient(new Person());
        action.setObject(s);
        action.prepare();
        assertNull(action.getBillingRecipientOrganizationName());
        assertTrue(action.isDisplayPILegalField());
        assertTrue(action.isDisplayUsageRestrictions());
        assertTrue(action.isDisplayShipmentBillingRecipient());

        s = new Shipment();
        s.setRecipient(new Person());
        s.setBillingRecipient(new Person());
        Institution institution = new Institution();
        institution.setName("test");
        s.getRecipient().setOrganization(institution);
        s.getBillingRecipient().setOrganization(institution);
        action.setObject(s);
        action.prepare();
        assertEquals("test", action.getRecipientOrganizationName());
        assertEquals("test", action.getBillingRecipientOrganizationName());
        assertTrue(action.isDisplayPILegalField());
        assertTrue(action.isDisplayUsageRestrictions());

        s = new Shipment();
        s.setBillingRecipient(new Person());
        action.setObject(s);
        action.prepare();
        assertTrue(action.isIncludeBillingRecipient());

        s = new Shipment();
        s.setBillingRecipient(null);
        action.setObject(s);
        action.prepare();
        assertFalse(action.isIncludeBillingRecipient());

        action = new ShipmentAction(getTestAppSettingService());
        s = new Shipment();
        SignedMaterialTransferAgreement recipient = new SignedMaterialTransferAgreement();
        SignedMaterialTransferAgreement sender = new SignedMaterialTransferAgreement();
        s.setSignedRecipientMta(recipient);
        s.setSignedSenderMta(sender);
        action.setObject(s);
        action.setMtaAmendment(new File(ClassLoader.getSystemResource("log4j.properties").toURI()));
        action.setMtaAmendmentContentType("text/plain");
        action.setMtaAmendmentFileName("log4j.properties");
        action.prepare();
        assertEquals(recipient, action.getSignedRecipientMta());
        assertEquals(sender, action.getSignedSenderMta());
        assertNotNull(action.getObject().getMtaAmendment());

        action = new ShipmentAction(getTestAppSettingService());
        s = new Shipment();
        action.setObject(s);
        AggregateSpecimenRequestLineItem[] generalLineItems =
            createGeneralLineItems();
        AggregateSpecimenRequestLineItem[] specificLineItems =
            createSpecificLineItems();

        List<AggregateSpecimenRequestLineItem> allLineItems =
            new ArrayList<AggregateSpecimenRequestLineItem>();
        allLineItems.addAll(Arrays.asList(generalLineItems));
        allLineItems.addAll(Arrays.asList(specificLineItems));
        action.getObject().getAggregateLineItems().addAll(allLineItems);
        action.prepare();
        verifyLineItems(specificLineItems, Arrays.asList(action.getSpecificLineItems()), generalLineItems,
                Arrays.asList(action.getGeneralLineItems()));
        verifyLineItems(specificLineItems, action.getSpecificLineItemList(), generalLineItems,
                action.getGeneralLineItemList());
    }

    private Shipment createShipment() {
        Shipment s = new Shipment();
        Person p = new Person();
        Institution i = new Institution();
        i.setName("Test Org");
        p.setOrganization(i);
        p.setEmail("text@example.com");
        s.setRecipient(p);
        s.setBillingRecipient(p);

        SpecimenRequest request = new SpecimenRequest();
        request.setId(1L);
        request.getOrders().add(s);
        s.setRequest(request);
        s.setShippingMethod(ShippingMethod.OTHER);

        return s;
    }

    private List<Shipment> createShipments() {
        Shipment s = new Shipment();
        Person p = new Person();
        Institution i = new Institution();
        i.setName("Test Org");
        p.setOrganization(i);
        p.setEmail("text@example.com");
        s.setRecipient(p);
        s.setBillingRecipient(p);

        SpecimenRequest request = new SpecimenRequest();
        request.setId(1L);
        request.getOrders().add(s);
        s.setRequest(request);
        s.setShippingMethod(ShippingMethod.DHL);
        s.setTrackingNumber("123456789");

        Shipment s2 = new Shipment();
        s2.setRecipient(p);
        s2.setBillingRecipient(p);
        s2.setRequest(request);
        s2.setShippingMethod(ShippingMethod.FEDEX);

        return Arrays.asList(s, s2);
    }

    /**
     * Test the review details action and supporting methods.
     */
    @Test
    public void testReviewDetails() {
        ShipmentAction action = new ShipmentAction(getTestAppSettingService());
        Shipment s = new Shipment();
        Institution shipmentInst = new Institution();
        shipmentInst.setName("match");
        s.setSendingInstitution(shipmentInst);
        action.setObject(s);
        SpecimenRequest request = new SpecimenRequest();
        s.setRequest(request);
        Institution matchingInst = new Institution();
        matchingInst.setName("match");
        Institution unmatchingInst = new Institution();
        unmatchingInst.setName("no match");
        SpecimenRequestReviewVote matchingConsortium = new SpecimenRequestReviewVote();
        matchingConsortium.setInstitution(matchingInst);
        request.getConsortiumReviews().add(matchingConsortium);
        SpecimenRequestReviewVote unmatchingConsortium = new SpecimenRequestReviewVote();
        unmatchingConsortium.setInstitution(unmatchingInst);
        request.getConsortiumReviews().add(unmatchingConsortium);
        SpecimenRequestReviewVote matchingInstitutional = new SpecimenRequestReviewVote();
        matchingInstitutional.setInstitution(matchingInst);
        request.getInstitutionalReviews().add(matchingInstitutional);
        SpecimenRequestReviewVote unmatchingInstitutional = new SpecimenRequestReviewVote();
        unmatchingInstitutional.setInstitution(unmatchingInst);
        request.getInstitutionalReviews().add(unmatchingInstitutional);
        assertEquals(Action.SUCCESS, action.reviewDetails());
        assertEquals(matchingConsortium, action.getConsortiumReviewVote());
        assertEquals(matchingInstitutional, action.getInstitutionalReviewVote());
        s.getSendingInstitution().setName("another no match");
        assertNull(action.getConsortiumReviewVote());
        assertNull(action.getInstitutionalReviewVote());
    }
}
