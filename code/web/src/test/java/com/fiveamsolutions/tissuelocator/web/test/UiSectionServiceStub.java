/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.web.test;

import java.util.ArrayList;
import java.util.List;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.config.GenericFieldConfig;
import com.fiveamsolutions.tissuelocator.data.config.category.UiDynamicCategoryField;
import com.fiveamsolutions.tissuelocator.data.config.category.UiDynamicFieldCategory;
import com.fiveamsolutions.tissuelocator.data.config.category.UiSearchCategoryField;
import com.fiveamsolutions.tissuelocator.data.config.category.UiSearchFieldCategory;
import com.fiveamsolutions.tissuelocator.data.config.category.UiSection;
import com.fiveamsolutions.tissuelocator.data.config.search.SearchSetType;
import com.fiveamsolutions.tissuelocator.service.config.category.UiSectionServiceLocal;

/**
 * User interface section service stub.
 *
 * @author jstephens
 */
public class UiSectionServiceStub extends GenericServiceStub<UiSection> implements UiSectionServiceLocal {

    private static final int NUMBER_OF_UI_CATEGORY_FIELDS = 3;

    /**
     * Gets the user interface sections associated with an entity.
     *
     * @param entityClass entity to which the sections belong
     * @return user interface sections associated with an entity
     */
    public List<UiSection> getUiSections(Class<? extends PersistentObject> entityClass) {
        List<UiSection> uiSections = new ArrayList<UiSection>();
        UiSection uiSection = new UiSection();
        uiSection.setSectionName(UiSection.class.getSimpleName());
        uiSection.setEntityClassName(entityClass.getName());
        uiSection.setUiDynamicFieldCategories(createUiDynamicFieldCategories());
        uiSection.setUiSearchFieldCategories(createUiSearchFieldCategories());
        uiSections.add(uiSection);
        return uiSections;
    }

    private List<UiDynamicFieldCategory> createUiDynamicFieldCategories() {
        List<UiDynamicFieldCategory> categories = new ArrayList<UiDynamicFieldCategory>();
        UiDynamicFieldCategory category = new UiDynamicFieldCategory();
        category.setCategoryName("specimen.biospecimenCharacteristics");
        category.setEntityClassName(Class.class.getName());
        category.setUiDynamicCategoryFields(createUiDynamicCategoryFields(NUMBER_OF_UI_CATEGORY_FIELDS));
        categories.add(category);
        return categories;
    }

    private List<UiSearchFieldCategory> createUiSearchFieldCategories() {
        List<UiSearchFieldCategory> categories = new ArrayList<UiSearchFieldCategory>();
        UiSearchFieldCategory category = new UiSearchFieldCategory();
        category.setCategoryName("specimen.search.biospecimen.title");
        category.setViewable(Boolean.TRUE);
        category.setEntityClassName(Specimen.class.getName());
        category.setSearchSetType(SearchSetType.ADVANCED.name());
        category.setUiSearchCategoryFields(createUiSearchCategoryFields(NUMBER_OF_UI_CATEGORY_FIELDS));
        category.getUiSearchCategoryFields().get(0).getFieldConfig().setSearchFieldName("object.specimenType");
        category.getUiSearchCategoryFields().get(1).getFieldConfig().setSearchFieldName("collectionYear");
        category.getUiSearchCategoryFields().get(2).getFieldConfig().setSearchFieldName("priceNegotiable");
        categories.add(category);
        return categories;
    }

    private List<UiDynamicCategoryField> createUiDynamicCategoryFields(int numberOfFields) {
        List<UiDynamicCategoryField> categoryFields = new ArrayList<UiDynamicCategoryField>();
        for (int i = 1; i <= numberOfFields; i++) {
            UiDynamicCategoryField cf = new UiDynamicCategoryField();
            cf.setFieldConfig(new GenericFieldConfig());
            cf.getFieldConfig().setFieldName("fieldName" + i);
            categoryFields.add(cf);
        }

        return categoryFields;
    }
    
    private List<UiSearchCategoryField> createUiSearchCategoryFields(int numberOfFields) {
        List<UiSearchCategoryField> categoryFields = new ArrayList<UiSearchCategoryField>();
        for (int i = 1; i <= numberOfFields; i++) {
            UiSearchCategoryField cf = new UiSearchCategoryField();
            cf.setFieldConfig(new GenericFieldConfig());
            cf.getFieldConfig().setSearchFieldName("fieldName" + i);
            categoryFields.add(cf);
        }

        return categoryFields;
    }

}
