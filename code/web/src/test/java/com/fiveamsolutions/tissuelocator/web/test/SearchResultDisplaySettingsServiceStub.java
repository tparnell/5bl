/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.web.test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.config.GenericFieldConfig;
import com.fiveamsolutions.tissuelocator.data.config.category.DisplayOption;
import com.fiveamsolutions.tissuelocator.data.config.category.SearchResultFieldDisplaySetting;
import com.fiveamsolutions.tissuelocator.data.config.category.UiSearchCategoryField;
import com.fiveamsolutions.tissuelocator.data.config.category.UiSearchFieldCategory;
import com.fiveamsolutions.tissuelocator.data.config.search.SearchSetType;
import com.fiveamsolutions.tissuelocator.service.config.category.SearchResultDisplaySettingsServiceLocal;

/**
 * @author cgoina
 *
 */
public class SearchResultDisplaySettingsServiceStub extends GenericServiceStub<SearchResultFieldDisplaySetting>
        implements SearchResultDisplaySettingsServiceLocal {

    private final UiSearchFieldCategoryServiceStub searchCategoriesService = new UiSearchFieldCategoryServiceStub();
    private List<SearchResultFieldDisplaySetting> defaultDisplayPreferences;
    private final Map<String, Map<String, SearchResultFieldDisplaySetting>> userPreferencesMap;

    /**
     * Default constructor.
     */
    public SearchResultDisplaySettingsServiceStub() {
        defaultDisplayPreferences = new ArrayList<SearchResultFieldDisplaySetting>();
        userPreferencesMap = new HashMap<String, Map<String, SearchResultFieldDisplaySetting>>();
        prepareData();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<SearchResultFieldDisplaySetting> getDefaultSearchResultDisplaySettings() {
        return defaultDisplayPreferences;
    }
    
    /**
     * Set the default display settings.
     * @param settings the default display settings.
     */
    public void setDefaultSearchResultDisplaySettings(List<SearchResultFieldDisplaySetting> settings) {
        defaultDisplayPreferences = settings;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<SearchResultFieldDisplaySetting> getUserSearchResultDisplaySettings(String user) {
        Map<String, SearchResultFieldDisplaySetting> userPreferences =
            userPreferencesMap.get(user);
        if (userPreferences == null) {
            userPreferences = new LinkedHashMap<String, SearchResultFieldDisplaySetting>();
        }
        List<SearchResultFieldDisplaySetting> userPreferencesList = new ArrayList<SearchResultFieldDisplaySetting>();
        for (SearchResultFieldDisplaySetting fieldDisplaySetting : defaultDisplayPreferences) {
            if (userPreferences.get(fieldDisplaySetting.getFieldConfig().getSearchFieldName()) != null) {
                userPreferencesList.add(userPreferences.get(fieldDisplaySetting.getFieldConfig().getSearchFieldName()));
            } else {
                userPreferencesList.add(fieldDisplaySetting);
            }
        }
        return userPreferencesList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void storeUserSearchResultDisplaySettings(TissueLocatorUser user,
            Collection<SearchResultFieldDisplaySetting> fieldDisplaySettings) {
        Map<String, SearchResultFieldDisplaySetting> userPreferences =
            userPreferencesMap.get(user.getUsername());
        if (userPreferences == null) {
            userPreferences = new LinkedHashMap<String, SearchResultFieldDisplaySetting>();
            userPreferencesMap.put(user.getUsername(), userPreferences);
        }
        if (fieldDisplaySettings != null) {
            for (SearchResultFieldDisplaySetting fieldDisplaySetting : fieldDisplaySettings) {
                if (userPreferences.get(fieldDisplaySetting.getFieldConfig().getSearchFieldName()) != null) {
                    userPreferences.get(fieldDisplaySetting.getFieldConfig().getSearchFieldName())
                    .setDisplayFlag(fieldDisplaySetting.getDisplayFlag());
                } else {
                    userPreferences.put(fieldDisplaySetting.getFieldConfig().getSearchFieldName(), fieldDisplaySetting);
                }
            }
        }
    }

    /**
     * Reset the data.
     */
    public void resetData() {
        defaultDisplayPreferences.clear();
        prepareData();
    }

    private void prepareData() {
        // add a field that has no category
        SearchResultFieldDisplaySetting nonCategorizedField = new SearchResultFieldDisplaySetting();
        nonCategorizedField.setId(null);
        nonCategorizedField.setFieldConfig(new GenericFieldConfig());
        nonCategorizedField.getFieldConfig().setSearchFieldName("emptyfield");
        nonCategorizedField.setDisplayFlag(DisplayOption.ENABLED);
        nonCategorizedField.setFieldCategory(null);
        defaultDisplayPreferences.add(nonCategorizedField);
        // retrieve a set of test categories
        List<UiSearchFieldCategory> searchCategories = searchCategoriesService.getUiSearchFieldCategories(
                Specimen.class, SearchSetType.ADVANCED);
        // skip the first category so that I can have categories that don't have display settings configured
        for (int i = 0; i < searchCategories.size(); i++) {
            UiSearchFieldCategory searchCategory = searchCategories.get(i);
            for (UiSearchCategoryField categoryField : searchCategory.getUiSearchCategoryFields()) {
                SearchResultFieldDisplaySetting fieldDisplaySetting = new SearchResultFieldDisplaySetting();
                fieldDisplaySetting.setId(categoryField.getId());
                fieldDisplaySetting.setFieldConfig(new GenericFieldConfig());
                fieldDisplaySetting.getFieldConfig().setSearchFieldName(
                        categoryField.getFieldConfig().getSearchFieldName());
                fieldDisplaySetting.getFieldConfig().setFieldName(categoryField.getFieldConfig().getSearchFieldName());
                if (i + 1 < searchCategories.size()) {
                    fieldDisplaySetting.setDisplayFlag(DisplayOption.ENABLED);
                } else {
                    fieldDisplaySetting.setDisplayFlag(DisplayOption.NOT_APPLICABLE);
                }
                fieldDisplaySetting.setFieldCategory(searchCategory);
                defaultDisplayPreferences.add(fieldDisplaySetting);
            }
        }
    }

}
