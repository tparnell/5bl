/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.struts2.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;

import org.apache.struts2.ServletActionContext;
import org.junit.Test;
import org.springframework.mock.web.MockServletContext;

import com.fiveamsolutions.dynamicextensions.AbstractDynamicFieldDefinition;
import com.fiveamsolutions.nci.commons.util.UsernameHolder;
import com.fiveamsolutions.tissuelocator.data.RequestStatus;
import com.fiveamsolutions.tissuelocator.data.ReviewComment;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestReviewVote;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.Vote;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.fiveamsolutions.tissuelocator.web.test.AbstractTissueLocatorWebTest;
import com.fiveamsolutions.tissuelocator.web.test.FixedConsortiumReviewServiceStub;
import com.opensymphony.xwork2.Action;

/**
 * @author smiller
 *
 */
public class FixedConsortiumReviewActionTest extends AbstractTissueLocatorWebTest {

    private static final int VOTING_PERIOD = 15;
    private static final int REVIEWER_ASSIGNMENT_PERIOD = 1;

    /**
     * tests the commenting process.
     * @throws MessagingException on error.
     */
    @Test
    public void testComment() throws MessagingException {
        FixedConsortiumReviewAction action = getFixedConsortiumReviewAction();
        action.setComment(new ReviewComment());
        assertEquals(Action.INPUT, action.loadCommentForm());
        action.getComment().setComment("test");

        assertEquals(Action.SUCCESS, action.addComment());
        assertNotNull(action.getComment().getComment());
        assertEquals(1, action.getObject().getComments().size());
        assertNotNull(action.getComment().getDate());
        FixedConsortiumReviewServiceStub stub = (FixedConsortiumReviewServiceStub)
            TissueLocatorRegistry.getServiceLocator().getFixedConsortiumReviewService();
        assertEquals(stub.getObject(), action.getObject());
    }

    /**
     * tests the prepare method.
     */
    @Test
    public void testPrepare() {
        FixedConsortiumReviewAction action = getFixedConsortiumReviewAction();
        action.getObject().getConsortiumReviews().add(new SpecimenRequestReviewVote());
        action.prepare();
        assertNotNull(action.getReviewers());
        assertFalse(action.getReviewers().isEmpty());
        assertEquals(1, action.getReviewers().size());
        assertNotNull(action.getConsortiumReviews());
        assertEquals(1, action.getConsortiumReviews().length);
        assertNull(action.getFinalVote());
        assertNull(action.getConsortiumReview());
        assertTrue(action.isDisplayPILegalField());

        TissueLocatorUser user = getTestUserService().getByUsername(UsernameHolder.getUser());
        SpecimenRequestReviewVote vote = action.getObject().getConsortiumReviews().iterator().next();
        vote.setUser(user);
        action.getObject().setFinalVote(RequestStatus.APPROVED);
        action.getObject().getConsortiumReviews().add(new SpecimenRequestReviewVote());
        action.prepare();
        assertNotNull(action.getFinalVote());
        assertEquals(Vote.APPROVE, action.getFinalVote());
        assertNotNull(action.getConsortiumReview());
        assertEquals(vote, action.getConsortiumReview());
        assertTrue(action.isDisplayPILegalField());
    }

    /**
     * test the assign reviewer action.
     * @throws MessagingException on error
     */
    @Test
    public void testAssignReviewer() throws MessagingException {
        MockServletContext context = (MockServletContext) ServletActionContext.getServletContext();
        context.addInitParameter("votingPeriod", String.valueOf(VOTING_PERIOD));
        FixedConsortiumReviewAction action = getFixedConsortiumReviewAction();
        assertEquals(Action.INPUT, action.assignScientificReviewer());
        assertFalse(action.getActionMessages().isEmpty());
        assertEquals(1, action.getActionMessages().size());
        action.clearErrorsAndMessages();
        FixedConsortiumReviewServiceStub stub = (FixedConsortiumReviewServiceStub)
            TissueLocatorRegistry.getServiceLocator().getFixedConsortiumReviewService();
        assertEquals(stub.getObject(), action.getObject());
        assertEquals(VOTING_PERIOD, stub.getVotingPeriod());
    }

    /**
     * test the finalize vote action.
     * @throws MessagingException on error
     */
    @Test
    public void testFinalizeVote() throws MessagingException {
        FixedConsortiumReviewAction action = getFixedConsortiumReviewAction();
        action.setFinalVote(Vote.APPROVE);
        assertEquals(RequestStatus.APPROVED, Vote.APPROVE.getStatus());
        assertEquals(Action.INPUT, action.finalizeVote());
        assertFalse(action.getActionMessages().isEmpty());
        assertEquals(1, action.getActionMessages().size());
        assertEquals(RequestStatus.APPROVED, action.getObject().getFinalVote());
        action.clearErrorsAndMessages();
        FixedConsortiumReviewServiceStub stub = (FixedConsortiumReviewServiceStub)
            TissueLocatorRegistry.getServiceLocator().getFixedConsortiumReviewService();
        assertEquals(stub.getObject(), action.getObject());
     }

    /**
     * test the decline review action.
     * @throws MessagingException on error
     */
    @Test
    public void testDeclineReview() throws MessagingException {
        MockServletContext context = (MockServletContext) ServletActionContext.getServletContext();
        context.addInitParameter("votingPeriod", String.valueOf(VOTING_PERIOD));
        context.addInitParameter("reviewerAssignmentPeriod", String.valueOf(REVIEWER_ASSIGNMENT_PERIOD));
        FixedConsortiumReviewAction action = getFixedConsortiumReviewAction();
        TissueLocatorUser assignee = new TissueLocatorUser();
        assignee.setFirstName("first");
        assignee.setLastName("last");
        SpecimenRequestReviewVote vote = new SpecimenRequestReviewVote();
        vote.setUser(assignee);
        action.getObject().getConsortiumReviews().add(vote);
        action.setConsortiumReview(vote);

        assertEquals(Action.SUCCESS, action.declineReview());
        assertFalse(action.getActionMessages().isEmpty());
        assertEquals(1, action.getActionMessages().size());
        action.clearErrorsAndMessages();
        FixedConsortiumReviewServiceStub stub = (FixedConsortiumReviewServiceStub)
            TissueLocatorRegistry.getServiceLocator().getFixedConsortiumReviewService();
        assertEquals(stub.getObject(), action.getObject());
        assertNotNull(stub.getDeclinedReview());
        assertEquals(action.getConsortiumReview(), stub.getDeclinedReview());
        assertEquals(VOTING_PERIOD, stub.getVotingPeriod());
        assertEquals(REVIEWER_ASSIGNMENT_PERIOD, stub.getReviewerAssignmentPeriod());
    }

    /**
     * Tests notifying research of final decision action.
     * @throws MessagingException on error
     */
    @Test
    public void testNotifiyResearcherOfDecision() throws MessagingException {
        FixedConsortiumReviewAction action = getFixedConsortiumReviewAction();
        TissueLocatorUser requestor = getTestUserService().getByUsername(UsernameHolder.getUser());
        SpecimenRequest request = new SpecimenRequest();
        request.setExternalComment("test comment");
        request.setFinalVote(RequestStatus.APPROVED);
        request.setRequestor(requestor);
        action.setObject(request);

        assertEquals(Action.SUCCESS, action.notifyResearcherOfDecision());
        assertFalse(action.getActionMessages().isEmpty());
        assertEquals(1, action.getActionMessages().size());
        assertEquals(RequestStatus.APPROVED, action.getObject().getStatus());
        action.clearErrorsAndMessages();
        FixedConsortiumReviewServiceStub stub = (FixedConsortiumReviewServiceStub)
            TissueLocatorRegistry.getServiceLocator().getFixedConsortiumReviewService();
        assertEquals(stub.getObject(), action.getObject());
        assertFalse(stub.isCreateShipments());
    }

    private FixedConsortiumReviewAction getFixedConsortiumReviewAction() {
        return new FixedConsortiumReviewAction(getTestUserService(), getTestAppSettingService(), 
                getTestUiDynamicFieldCategoryService()) {
            private static final long serialVersionUID = 1L;

            /** {@inheritDoc} */
            @Override
            public List<AbstractDynamicFieldDefinition> getDynamicFieldDefinitions() {
                return new ArrayList<AbstractDynamicFieldDefinition>();
            }
        };
    }
}
