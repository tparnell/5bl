/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.web.struts2.action;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.ListResourceBundle;

import org.junit.Before;
import org.junit.Test;

import com.fiveamsolutions.dynamicextensions.AbstractDynamicFieldDefinition;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.SpecimenClass;
import com.fiveamsolutions.tissuelocator.data.code.SpecimenType;
import com.fiveamsolutions.tissuelocator.web.test.AbstractTissueLocatorWebTest;
import com.fiveamsolutions.tissuelocator.web.test.SpecimenSearchServiceStub;
import com.opensymphony.xwork2.TextProviderSupport;

/**
 * @author jstephens
 */
public class SpecimenDetailsPanelActionTest extends AbstractTissueLocatorWebTest {

    private SpecimenDetailsPanelAction action;
    private TextProviderSupport textProvider;

    /**
     * Set up the test.
     */
    @Before
    public void setUp() {
        action = getSpecimenDetailsPanelAction();
        textProvider = new TextProviderSupport();
        textProvider.setBundle(new TestResources());
        textProvider.setLocaleProvider(action);
    }

    /**
     * Tests column 1.
     */
    @Test
    public void testColumn1() {
        Specimen specimen = new Specimen();
        specimen.setCustomProperty("anatomicSource", "Heart");
        action.setObject(specimen);
        action.prepare();
        assertEquals("Anatomic Source", action.getColumn1Label());
        assertEquals("Heart", action.getColumn1Value());
    }

    /**
     * Tests column 2.
     */
    @Test
    public void testColumn2() {
        Specimen specimen = new Specimen();
        SpecimenType specimenType = new SpecimenType();
        specimenType.setName(textProvider.getText(SpecimenClass.TISSUE.getResourceKey()));
        specimen.setSpecimenType(specimenType);
        action.setObject(specimen);
        action.prepare();
        assertEquals("Type", action.getColumn2Label());
        assertEquals(specimenType, action.getColumn2Value());
    }

    /**
     * Tests column 3.
     */
    @Test
    public void testColumn3() {
        Specimen specimen = new Specimen();
        specimen.setCustomProperty("preservationType", "Frozen");
        action.setObject(specimen);
        action.prepare();
        assertEquals("Preservation Type", action.getColumn3Label());
        assertEquals("Frozen", action.getColumn3Value());
    }

    /**
     * Tests column 4.
     */
    @Test
    public void testColumn4() {
        Specimen specimen = new Specimen();
        specimen.setAvailableQuantity(BigDecimal.TEN);
        action.setObject(specimen);
        action.prepare();
        assertEquals("Available Quantity", action.getColumn4Label());
        assertEquals(BigDecimal.TEN, action.getColumn4Value());
    }

    /**
     * Tests null value.
     */
    @Test
    public void testNullValue() {
        action.setObject(new Specimen());
        action.prepare();
        assertEquals(null, action.getColumn1Value());
    }

    private SpecimenDetailsPanelAction getSpecimenDetailsPanelAction() {
        return new SpecimenDetailsPanelAction(new SpecimenSearchServiceStub(), getTestUserService(),
                getTestAppSettingService(), getTestUiDynamicFieldCategoryService(), getTestUiSectionService()) {
            private static final long serialVersionUID = 1L;

            /** {@inheritDoc} */
            @Override
            public List<AbstractDynamicFieldDefinition> getDynamicFieldDefinitions() {
                return new ArrayList<AbstractDynamicFieldDefinition>();
            }
        };
    }

    /**
     * the test resources.
     */
    private class TestResources extends ListResourceBundle {
        /**
         * {@inheritDoc}
         */
        @Override
        protected Object[][] getContents() {
            return new Object[][] {
                {"specimenClass.tissue", "Tissue"}
            };
        }
    }

}
