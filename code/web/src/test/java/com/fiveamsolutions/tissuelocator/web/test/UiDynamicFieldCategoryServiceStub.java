/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.web.test;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.fiveamsolutions.tissuelocator.data.config.GenericFieldConfig;
import com.fiveamsolutions.tissuelocator.data.config.category.UiDynamicCategoryField;
import com.fiveamsolutions.tissuelocator.data.config.category.UiDynamicFieldCategory;
import com.fiveamsolutions.tissuelocator.data.config.category.UiDynamicCategoryField.FieldType;
import com.fiveamsolutions.tissuelocator.service.config.category.UiDynamicFieldCategoryServiceLocal;

/**
 * Dynamic field user interface category service stub.
 *
 * @author jstephens
 */
public class UiDynamicFieldCategoryServiceStub extends GenericServiceStub<UiDynamicFieldCategory>
    implements UiDynamicFieldCategoryServiceLocal {

    // CHECKSTYLE:OFF

    /**
     * Returns the dynamic field user interface categories that belong to an entity.
     *
     * @param entityClass entity to which the dynamic field categories belong
     * @return list of dynamic field user interface categories
     */
    @Override
    @SuppressWarnings("rawtypes")
    public Map<String, UiDynamicFieldCategory> getUiDynamicFieldCategories(final Class entityClass) {
        final Map<String, UiDynamicFieldCategory> uiDynamicFieldCategories = createUiDynamicFieldCategories();
        return uiDynamicFieldCategories;
    }

    private Map<String, UiDynamicFieldCategory> createUiDynamicFieldCategories() {
        final Map<String, UiDynamicFieldCategory> categories = new LinkedHashMap<String, UiDynamicFieldCategory>();

        final UiDynamicFieldCategory c1 = new UiDynamicFieldCategory();
        c1.setCategoryName("specimen.biospecimenCharacteristics");
        c1.setEntityClassName(Class.class.getName());
        c1.setUiDynamicCategoryFields(createUiCategoryFields(4));
        categories.put(c1.getCategoryName(), c1);

        final UiDynamicFieldCategory c2 = new UiDynamicFieldCategory();
        c2.setCategoryName("specimen.participantHeader");
        c2.setEntityClassName(Class.class.getName());
        c2.setUiDynamicCategoryFields(createUiCategoryFields(3));
        categories.put(c2.getCategoryName(), c2);

        final UiDynamicFieldCategory c3 = new UiDynamicFieldCategory();
        c3.setCategoryName("specimen.collectionProtocol");
        c3.setEntityClassName(Class.class.getName());
        c3.setUiDynamicCategoryFields(createUiCategoryFields(3));
        categories.put(c3.getCategoryName(), c3);

        final UiDynamicFieldCategory c4 = new UiDynamicFieldCategory();
        c4.setCategoryName("Patient");
        c4.setEntityClassName(Class.class.getName());
        c4.setUiDynamicCategoryFields(createUiCategoryFields(3));
        categories.put(c4.getCategoryName(), c4);

        final UiDynamicFieldCategory c5 = new UiDynamicFieldCategory();
        c4.setCategoryName("Miscellaneous Category");
        c4.setEntityClassName(Class.class.getName());
        c4.setUiDynamicCategoryFields(createUiCategoryFields(3));
        categories.put(c5.getCategoryName(), c5);

        return categories;
    }

    private List<UiDynamicCategoryField> createUiCategoryFields(final int numberOfFields) {
        final List<UiDynamicCategoryField> categoryFields = new ArrayList<UiDynamicCategoryField>();

        for (int i = 1; i <= numberOfFields; i++) {
            final UiDynamicCategoryField cf = new UiDynamicCategoryField();
            cf.setFieldConfig(new GenericFieldConfig());
            cf.getFieldConfig().setFieldName("fieldName" + i);
            cf.setFieldType(FieldType.CUSTOM_PROPERTY);
            categoryFields.add(cf);
        }

        return categoryFields;
    }
    // CHECKSTYLE:ON
}
