/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.web.struts2.action;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.apache.struts2.ServletActionContext;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;

import com.fiveamsolutions.tissuelocator.data.Role;
import com.fiveamsolutions.tissuelocator.web.test.AbstractTissueLocatorWebTest;
import com.fiveamsolutions.tissuelocator.web.util.HomePageWidget;

/**
 * @author bhumphrey
 *
 */
public class BrowseDiseaseWidgetActionTest extends AbstractTissueLocatorWebTest {

    /**
     * test counts by pathological finding.
     */
    @Test
    public void testCountsByPathologicalFindings() {
        MockHttpServletRequest request = (MockHttpServletRequest) ServletActionContext.getRequest();
        request.setRemoteUser("testuser");
        request.addUserRole(Role.SCIENTIFIC_REVIEWER_ASSIGNER.getName());
        BrowseDiseaseWidgetAction action = new BrowseDiseaseWidgetAction();
        action.prepare();
        assertNull(action.getCountsByPathologicalFinding());
        assertNull(action.getPathologicalCharacteristics());
        assertFalse(action.isDisplayDiseaseCount());

        HomePageWidget widget = HomePageWidget.DISEASE_WITH_COUNT;
        ServletActionContext.getServletContext().setAttribute(widget.getAttributeName(), widget.getData());
        action = new BrowseDiseaseWidgetAction();
        action.prepare();
        assertNotNull(action.getCountsByPathologicalFinding());
        assertTrue(action.getCountsByPathologicalFinding().isEmpty());
        assertNull(action.getPathologicalCharacteristics());
        assertTrue(action.isDisplayDiseaseCount());

        ServletActionContext.getServletContext().removeAttribute(widget.getAttributeName());
        widget = HomePageWidget.DISEASE_NO_COUNT;
        ServletActionContext.getServletContext().setAttribute(widget.getAttributeName(), widget.getData());
        action = new BrowseDiseaseWidgetAction();
        action.prepare();
        assertNull(action.getCountsByPathologicalFinding());
        assertNotNull(action.getPathologicalCharacteristics());
        assertTrue(action.getPathologicalCharacteristics().isEmpty());
        assertFalse(action.isDisplayDiseaseCount());
    }
}
