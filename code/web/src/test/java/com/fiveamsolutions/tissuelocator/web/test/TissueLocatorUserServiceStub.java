/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.mail.MessagingException;

import org.apache.commons.lang.StringUtils;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.nci.commons.data.security.AbstractUser;
import com.fiveamsolutions.nci.commons.data.security.UserGroup;
import com.fiveamsolutions.nci.commons.util.SecurityUtils;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.service.ForgotPasswordStatus;
import com.fiveamsolutions.tissuelocator.service.user.TissueLocatorUserServiceLocal;

/**
 * @author ddasgupta
 *
 */
public class TissueLocatorUserServiceStub extends GenericServiceStub<TissueLocatorUser>
    implements TissueLocatorUserServiceLocal {

    private boolean isAdmin;
    private String password;
    private String username;
    private String nonce;

    /**
     * {@inheritDoc}
     */
    public TissueLocatorUser getByUsername(String usernameArg) {
        username = usernameArg;
        if (StringUtils.isBlank(usernameArg)) {
            return null;
        }
        TissueLocatorUser u = new TissueLocatorUser();
        u.setId(1L);
        u.setUsername(usernameArg);
        u.setInstitution(new Institution());
        return u;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public <TYPE extends PersistentObject> TYPE getPersistentObject(Class<TYPE> toClass, Long id) {
        if (TissueLocatorUser.class.equals(toClass)) {
            TissueLocatorUser u = new TissueLocatorUser();
            Institution i = new Institution();
            i.setId(1L);
            u.setInstitution(i);
            u.setId(id);
            return (TYPE) u;
        }
        return super.getPersistentObject(toClass, id);
    }

    /**
     * {@inheritDoc}
     */
     public void denyUser(TissueLocatorUser user) {
         savePersistentObject(user);
     }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deactivateUser(TissueLocatorUser user)
            throws MessagingException {
        user.setStatusTransitionComment("deactivated");
        savePersistentObject(user);
    }

    /**
     * {@inheritDoc}
     */
    public Long saveUser(TissueLocatorUser u, boolean isAdminArg, String passwordArg) throws MessagingException {
        isAdmin = isAdminArg;
        password = passwordArg;
        return savePersistentObject(u);
    }

    /**
     * {@inheritDoc}
     */
    public List<UserGroup> getAdministratableGroups(TissueLocatorUser user) {
        List<UserGroup> groups = new ArrayList<UserGroup>();
        UserGroup ug = new UserGroup();
        ug.setName(user.getUsername());
        groups.add(ug);
        return groups;
    }

    /**
     * {@inheritDoc}
     */
    public Set<AbstractUser> getUsersInRole(String roleName) {
        Set<AbstractUser> users = new HashSet<AbstractUser>();
        users.add(new TissueLocatorUser());
        return users;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<TissueLocatorUser> getByRoleAndInstitutions(String roleName,
            Set<Institution> institutions) {
        Set<TissueLocatorUser> users = new HashSet<TissueLocatorUser>();
        users.add(new TissueLocatorUser());
        return users;
    }

    /**
     * {@inheritDoc}
     */
    public ForgotPasswordStatus forgotPassword(String usernameArg) throws MessagingException {
        username = usernameArg;
        if (StringUtils.isBlank(usernameArg)) {
            return ForgotPasswordStatus.UNKNOWN_USERNAME;
        }
        return ForgotPasswordStatus.SUCCESS;
    }

    /**
     * {@inheritDoc}
     */
    public TissueLocatorUser updatePassword(TissueLocatorUser user, String newPassword, String nonceArg)
            throws MessagingException {
        password = newPassword;
        nonce = nonceArg;
        user.setPassword(SecurityUtils.create(newPassword));
        savePersistentObject(user);
        return user;
    }

    /**
     * @return the isAdmin
     */
    public boolean isAdmin() {
        return isAdmin;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @return the nonce
     */
    public String getNonce() {
        return nonce;
    }

    /**
     * {@inheritDoc}
     */
    public Set<TissueLocatorUser> getByRoleAndInstitution(String roleName, Institution i) {
        Set<TissueLocatorUser> users = new HashSet<TissueLocatorUser>();
        users.add(new TissueLocatorUser());
        return users;
    }

    /**
     * {@inheritDoc}
     */
    public int getNewUserCount(Date startDate, Date endDate) {
        return 1;
    }
}
