/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.test;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.mail.MessagingException;

import com.fiveamsolutions.tissuelocator.data.AggregateSpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.RequestStatus;
import com.fiveamsolutions.tissuelocator.data.Shipment;
import com.fiveamsolutions.tissuelocator.data.ShipmentStatus;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.SpecimenDisposition;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.SpecimenStatus;
import com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreement;
import com.fiveamsolutions.tissuelocator.service.ShipmentServiceLocal;

/**
 * @author ddasgupta
 *
 */
public class ShipmentServiceStub extends GenericServiceStub<Shipment> implements
    ShipmentServiceLocal {

    /**
     * {@inheritDoc}
     */
    public void removeAllLineItems(Shipment shipment) {
        shipment.getLineItems().clear();
    }

    /**
     * {@inheritDoc}
     */
    public void removeLineItem(Shipment shipment, SpecimenRequestLineItem lineItem) {
        shipment.getLineItems().remove(lineItem);
    }

    /**
     * {@inheritDoc}
     */
    public void removeAggregateLineItem(Shipment shipment,
            AggregateSpecimenRequestLineItem lineItem) {
        shipment.getAggregateLineItems().remove(lineItem);
    }

    /**
     * {@inheritDoc}
     */
    public void removeAllAggregateLineItems(Shipment shipment) {
        shipment.getAggregateLineItems().clear();
    }

    /**
     * {@inheritDoc}
     */
    public void addSpecimenToOrder(Set<Specimen> specimens, Shipment order) {
        for (Specimen s : specimens) {
            SpecimenRequestLineItem li = new SpecimenRequestLineItem();
            li.setSpecimen(s);
            order.getLineItems().add(li);
        }
    }

    /**
     * {@inheritDoc}
     */
    public void updateLineItemSpecimenStatus(Shipment shipment, SpecimenStatus status) {
        for (SpecimenRequestLineItem li : shipment.getLineItems()) {
            li.getSpecimen().setStatus(status);
        }
    }

    /**
     * {@inheritDoc}
     */
    public void updateLineItemDispostitions(Shipment shipment) {
        shipment.setStatus(ShipmentStatus.RECEIVED);
        for (SpecimenRequestLineItem li : shipment.getLineItems()) {
            SpecimenDisposition sd = li.getDisposition();
            if (sd.equals(SpecimenDisposition.DAMAGED) || sd.equals(SpecimenDisposition.PARTIALLY_CONSUMED_DESTROYED)) {
                li.getSpecimen().setStatus(SpecimenStatus.DESTROYED);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    public Shipment getShipmentWithSpecimen(Specimen specimen) {
        SpecimenRequestLineItem li = new SpecimenRequestLineItem();
        li.setSpecimen(specimen);

        SpecimenRequest request = new SpecimenRequest();
        request.setId(2L);

        Shipment shipment = new Shipment();
        shipment.setId(1L);
        shipment.getLineItems().add(li);
        shipment.setStatus(ShipmentStatus.PENDING);
        shipment.setRequest(request);
        return specimen.getId() == null ? null : shipment;
    }

    /**
     * {@inheritDoc}
     */
    public void markOrderAsShipped(Shipment order) throws MessagingException {
        order.setStatus(ShipmentStatus.SHIPPED);
        order.setShipmentDate(new Date());
        updateRequestStatus(order.getRequest());
        updateLineItemSpecimenStatus(order, SpecimenStatus.SHIPPED);
        order.setSignedRecipientMta(getSignedRecipientMta(order));
        order.setSignedSenderMta(getSignedSenderMta(order));
        savePersistentObject(order);
    }

    /**
     * {@inheritDoc}
     */
    public SignedMaterialTransferAgreement getSignedSenderMta(
            Shipment shipment) {
        if (shipment.getSignedSenderMta() != null) {
            return shipment.getSignedSenderMta();
        }
        if (shipment.getSendingInstitution() != null) {
            return shipment.getSendingInstitution().getCurrentSignedSenderMta();
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public SignedMaterialTransferAgreement getSignedRecipientMta(Shipment shipment) {
        if (shipment.getSignedRecipientMta() != null) {
            return shipment.getSignedRecipientMta();
        }
        if (shipment.getRecipient() != null && shipment.getRecipient().getOrganization() != null) {
            return shipment.getRecipient().getOrganization().getCurrentSignedSenderMta();
        }
        return null;
    }

    private void updateRequestStatus(SpecimenRequest request) {
        request.setStatus(RequestStatus.FULLY_PROCESSED);
        for (Shipment shipment : request.getOrders()) {
            if (shipment.getStatus().equals(ShipmentStatus.PENDING)) {
                request.setStatus(RequestStatus.PARTIALLY_PROCESSED);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    public void cancelOrder(Shipment order) throws MessagingException {
        order.setStatus(ShipmentStatus.CANCELED);
        updateRequestStatus(order.getRequest());
        updateLineItemSpecimenStatus(order, SpecimenStatus.AVAILABLE);
        savePersistentObject(order);
    }

    /**
     * {@inheritDoc}
     */
    public List<Shipment> getUnreceivedShippedOrders(int gracePeriod) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public void saveOrder(Shipment order) {
        savePersistentObject(order);
    }

    /**
     * {@inheritDoc}
     */
    public int getShippedOrderCount(Date startDate, Date endDate) {
        return 1;
    }
}
