/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.struts2.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.ServletActionContext;
import org.displaytag.properties.SortOrderEnum;
import org.displaytag.tags.TableTagParameters;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockServletContext;

import com.fiveamsolutions.dynamicextensions.AbstractDynamicFieldDefinition;
import com.fiveamsolutions.dynamicextensions.StringDynamicFieldDefinition;
import com.fiveamsolutions.nci.commons.data.security.AbstractUser;
import com.fiveamsolutions.nci.commons.data.security.AccountStatus;
import com.fiveamsolutions.nci.commons.data.security.ApplicationRole;
import com.fiveamsolutions.nci.commons.data.security.PasswordType;
import com.fiveamsolutions.nci.commons.data.security.UserGroup;
import com.fiveamsolutions.nci.commons.util.SecurityUtils;
import com.fiveamsolutions.nci.commons.util.UsernameHolder;
import com.fiveamsolutions.tissuelocator.data.Country;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.Role;
import com.fiveamsolutions.tissuelocator.data.State;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.config.category.UiDynamicFieldCategory;
import com.fiveamsolutions.tissuelocator.data.extension.FileDynamicFieldDefinition;
import com.fiveamsolutions.tissuelocator.service.search.TissueLocatorUserSortCriterion;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.fiveamsolutions.tissuelocator.web.TissueLocatorSessionHelper;
import com.fiveamsolutions.tissuelocator.web.struts2.action.AbstractDynamicExtensionAction.FileUpload;
import com.fiveamsolutions.tissuelocator.web.test.AbstractTissueLocatorWebTest;
import com.fiveamsolutions.tissuelocator.web.test.InstitutionServiceStub;
import com.fiveamsolutions.tissuelocator.web.util.SelectOption;
import com.opensymphony.xwork2.Action;

/**
 * @author ddasgupta
 */
public class UserActionTest extends AbstractTissueLocatorWebTest {

    private static final int PAGE_SIZE = 20;
    private static final int MAX_INSTITUTION_COUNT = 100;

    /**
     * test user list.
     */
    @Test
    public void testList() {
        //case 1 - nothing in request or session - use defaults
        UserAction ua = getUserAction();
        assertEquals(Action.SUCCESS, ua.list());
        assertEquals(PAGE_SIZE, ua.getPageSize());
        assertEquals(1, ua.getPage());
        assertTrue(StringUtils.isBlank(ua.getSort()));
        assertEquals(SortOrderEnum.ASCENDING.getName(), ua.getDir());
        assertTrue(getTestUserService().isSearchCalled());
        assertNotNull(ua.getObjects());

        //case 2 - nothing in request, params stored in session - use session values
        ua.getObject().setStatus(AccountStatus.INACTIVE);
        ua.setObjects(null);
        ua.setPageSize(2);
        ua.setPage(2);
        ua.setSort("dummysort");
        ua.setDir("dummydir");
        ((MockServletContext) ServletActionContext.getServletContext()).addInitParameter("defaultPageSize", "30");
        assertEquals(Action.SUCCESS, ua.list());
        assertEquals(PAGE_SIZE, ua.getPageSize());
        assertEquals(1, ua.getPage());
        assertEquals("LAST_NAME,FIRST_NAME", ua.getSort());
        assertEquals(SortOrderEnum.ASCENDING.getName(), ua.getDir());
        assertTrue(getTestUserService().isSearchCalled());
        assertNotNull(ua.getObjects());

        //case 3 - params in request, params stored in session - use request params
        ua.setObjects(null);
        MockHttpServletRequest request = (MockHttpServletRequest) ServletActionContext.getRequest();
        request.addParameter("pageSize", "1");
        ua.setPageSize(1);
        request.addParameter("page", "2");
        ua.setPage(2);
        request.addParameter("sort", TissueLocatorUserSortCriterion.EMAIL.name());
        ua.setSort(TissueLocatorUserSortCriterion.EMAIL.name());
        request.addParameter("dir", SortOrderEnum.DESCENDING.getName());
        ua.setDir(SortOrderEnum.DESCENDING.getName());
        assertEquals(Action.SUCCESS, ua.list());
        ua.setObject(ua.getObject());
        assertEquals(1, ua.getPageSize());
        assertEquals(1, ua.getPage());
        assertEquals(TissueLocatorUserSortCriterion.EMAIL.name(), ua.getSort());
        assertEquals(SortOrderEnum.DESCENDING.getName(), ua.getDir());
        assertTrue(getTestUserService().isSearchCalled());
        assertNotNull(ua.getObjects());

        //case 4 - params in request, nothing in session - use request params
        request.getSession().removeAttribute("_paging");
        assertEquals(Action.SUCCESS, ua.list());
        assertEquals(1, ua.getPageSize());
        assertEquals(1, ua.getPage());
        assertEquals(TissueLocatorUserSortCriterion.EMAIL.name(), ua.getSort());
        assertEquals(SortOrderEnum.DESCENDING.getName(), ua.getDir());
        assertTrue(getTestUserService().isSearchCalled());
        assertNotNull(ua.getObjects());

        //case 5 - testing export branches
        request.addParameter(TableTagParameters.PARAMETER_EXPORTING, "1");
        assertEquals(Action.SUCCESS, ua.list());
        assertEquals(TissueLocatorUserSortCriterion.EMAIL.name(), ua.getSort());
        assertEquals(SortOrderEnum.DESCENDING.getName(), ua.getDir());
        assertTrue(getTestUserService().isSearchCalled());
        assertNotNull(ua.getObjects());

        //case 2 - repeating with different data in the session now
        request.removeAllParameters();
        ua.getObject().setStatus(AccountStatus.INACTIVE);
        ua.setObjects(null);
        ua.setPageSize(2);
        ua.setPage(1);
        ua.setSort("dummysort");
        ua.setDir("dummydir");
        assertEquals(Action.SUCCESS, ua.list());
        assertEquals(1, ua.getPageSize());
        assertEquals(1, ua.getPage());
        assertEquals(TissueLocatorUserSortCriterion.EMAIL.name(), ua.getSort());
        assertEquals(SortOrderEnum.DESCENDING.getName(), ua.getDir());
        assertTrue(getTestUserService().isSearchCalled());
        assertNotNull(ua.getObjects());

        TissueLocatorUser u = TissueLocatorSessionHelper.
            getLoggedInUser(ServletActionContext.getRequest().getSession());
        ApplicationRole ar = new ApplicationRole();
        ar.setName(Role.CROSS_INSTITUTION.getName());
        u.getRoles().add(ar);
        assertEquals(Action.SUCCESS, ua.list());
        assertEquals(1, ua.getPageSize());
        assertEquals(1, ua.getPage());
        assertEquals(TissueLocatorUserSortCriterion.EMAIL.name(), ua.getSort());
        assertEquals(SortOrderEnum.DESCENDING.getName(), ua.getDir());
        assertTrue(getTestUserService().isSearchCalled());
        assertNotNull(ua.getObjects());
    }

    /**
     * test user create action.
     * @throws Exception on error
     */
    @Test
    public void testCreate() throws Exception  {
        TissueLocatorUser session = TissueLocatorSessionHelper.getLoggedInUser(getSession());
        UserAction ua = getUserAction();
        ua.setPassword("password");
        ua.getObject().setPassword(SecurityUtils.create("password"));
        ua.setConfirmPassword("password");
        ua.getObject().getInstitution().setId(1L);
        ua.getObject().setEmail("test@test.com");
        ua.getObject().setStatus(AccountStatus.INACTIVE);
        assertEquals(Action.INPUT, ua.input());
        ua.getObject().getGroups().add(new UserGroup());
        assertFalse(ua.getObject().getGroups().isEmpty());
        assertNull(ua.getObject().getInstitution().getConsortiumMember());
        assertEquals("list", ua.save());
        assertEquals(1, ua.getActionMessages().size());
        ua.clearErrorsAndMessages();
        assertEquals(session, TissueLocatorSessionHelper.getLoggedInUser(getSession()));
        assertFalse(ua.getObject().getInstitution().getConsortiumMember().booleanValue());

        assertEquals(AccountStatus.INACTIVE, ua.getObject().getStatus());
        assertNotNull(ua.getObject().getInstitution());
        assertEquals("password", getTestUserService().getPassword());
        assertFalse(getTestUserService().isAdmin());
        assertEquals(ua.getObject(), getTestUserService().getObject());
        assertNotNull(ua.getPassword());
        assertNotNull(ua.getConfirmPassword());
        assertNotNull(ua.getObject().getPassword());
        assertEquals(PasswordType.SHA_SALTED, ua.getObject().getPassword().getType());
        assertTrue(ua.getObject().getGroups().isEmpty());

        ua.setPassword(null);
        ua.setConfirmPassword(null);
        ua.setObject(new TissueLocatorUser());
        ua.getObject().setEmail("test@test.com");
        ua.getObject().setInstitution(new Institution());
        ua.getObject().getGroups().add(new UserGroup());
        assertFalse(ua.getObject().getGroups().isEmpty());
        getRequest().addParameter("object.groups", "1");
        assertEquals("list", ua.save());
        assertEquals(1, ua.getActionMessages().size());
        ua.clearErrorsAndMessages();
        assertEquals(session, TissueLocatorSessionHelper.getLoggedInUser(getSession()));
        assertNotNull(ua.getObject().getInstitution());
        assertNull(getTestUserService().getPassword());
        assertFalse(getTestUserService().isAdmin());
        assertEquals(ua.getObject(), getTestUserService().getObject());
        assertFalse(ua.getObject().getGroups().isEmpty());

        getRequest().addUserRole(Role.USER_ADMIN.getName());
        assertEquals("list", ua.save());
        assertNull(getTestUserService().getPassword());
        assertTrue(getTestUserService().isAdmin());
        assertEquals(ua.getObject(), getTestUserService().getObject());
        assertEquals(1, ua.getActionMessages().size());
        ua.clearErrorsAndMessages();
        assertEquals(session, TissueLocatorSessionHelper.getLoggedInUser(getSession()));
        assertFalse(ua.getObject().getInstitution().getConsortiumMember().booleanValue());

        ua.getObject().setId(1L);
        ua.getObject().setStatus(AccountStatus.INACTIVE);
        ua.getObject().getInstitution().setConsortiumMember(true);
        assertEquals("list", ua.save());
        assertNull(getTestUserService().getPassword());
        assertTrue(getTestUserService().isAdmin());
        assertEquals(ua.getObject(), getTestUserService().getObject());
        assertEquals(1, ua.getActionMessages().size());
        ua.clearErrorsAndMessages();
        assertEquals(session, TissueLocatorSessionHelper.getLoggedInUser(getSession()));
        assertTrue(ua.getObject().getInstitution().getConsortiumMember().booleanValue());

        session.setId(1L);
        session.setUsername("username");

        assertEquals("logout", ua.save());
        assertNull(getTestUserService().getPassword());
        assertTrue(getTestUserService().isAdmin());
        assertEquals(ua.getObject(), getTestUserService().getObject());
        TissueLocatorUser newSession = TissueLocatorSessionHelper.getLoggedInUser(getSession());
        assertNotSame(session, newSession);
        assertNotSame(session, ua.getObject());
        assertNotSame(newSession, ua.getObject());
        assertNull(ua.getObject().getUsername());
        assertNull(newSession.getUsername());
        assertEquals(1L, session.getId().longValue());
        assertEquals(1L, newSession.getId().longValue());
        assertEquals(1L, ua.getObject().getId().longValue());
        assertEquals(1, ua.getActionMessages().size());
        ua.clearErrorsAndMessages();

        ua.getObject().setUsername(UsernameHolder.getUser());
        assertEquals("list", ua.save());
        ua.clearErrorsAndMessages();

        TissueLocatorSessionHelper.setLoggedInUser(getSession(), null);
        assertEquals("list", ua.save());
        assertNull(TissueLocatorSessionHelper.getLoggedInUser(getSession()));
        assertEquals(1, ua.getActionMessages().size());
        ua.clearErrorsAndMessages();

        ua.getObject().getGroups().add(new UserGroup());
        ua.getObject().getGroups().add(new UserGroup());  // due to set mechanics we added the same element twice
        UserGroup ug = new UserGroup();
        ug.setName("testgroup");
        ua.getObject().getGroups().add(ug);
        assertEquals(2, ua.getObjectGroupIds().size());
    }

    /**
     * test prep of user.
     * @throws Exception on error
     */
    @Test
    public void testPrepare() throws Exception {
        TissueLocatorUser u = TissueLocatorSessionHelper.getLoggedInUser(getSession());
        u.getAddress().setLine1("line 1");
        u.getAddress().setLine2("line 2");
        u.getAddress().setCity("city");
        u.getAddress().setState(State.MARYLAND);
        u.getAddress().setZip("zip");
        u.getAddress().setCountry(Country.AFGHANISTAN);

        UserAction ua = getUserAction();
        ua.prepare();
        assertNotNull(ua.getTypes());
        assertEquals(1, ua.getTypes().size());
        assertNotNull(ua.getInstitutions());
        assertEquals(1, ua.getInstitutions().size());
        assertNull(ua.getObject().getId());
        assertNotNull(ua.getGroups());
        assertTrue(ua.getGroups().isEmpty());
        assertEquals(u.getAddress().getLine1(), ua.getObject().getAddress().getLine1());
        assertEquals(u.getAddress().getLine2(), ua.getObject().getAddress().getLine2());
        assertEquals(u.getAddress().getCity(), ua.getObject().getAddress().getCity());
        assertEquals(u.getAddress().getState(), ua.getObject().getAddress().getState());
        assertEquals(u.getAddress().getZip(), ua.getObject().getAddress().getZip());
        assertEquals(u.getAddress().getCountry(), ua.getObject().getAddress().getCountry());
        assertTrue(ua.isAccountApprovalActive());
        assertTrue(ua.isUserResumeRequired());
        assertNull(ua.getObject().getResume());

        assertTrue(StringUtils.isBlank(ua.getPassword()));
        assertTrue(StringUtils.isBlank(ua.getConfirmPassword()));
        MockHttpServletRequest request = (MockHttpServletRequest) ServletActionContext.getRequest();
        request.addUserRole(Role.USER_ADMIN.getName());
        ua.setResume(new File(ClassLoader.getSystemResource("log4j.properties").toURI()));
        ua.setResumeFileName("log4j.properties");
        ua.setResumeContentType("text/plain");
        ua.prepare();
        assertEquals("changeMe!", ua.getPassword());
        assertEquals("changeMe!", ua.getConfirmPassword());
        assertNotNull(ua.getObject().getPassword());
        assertEquals(PasswordType.PLAINTEXT, ua.getObject().getPassword().getType());
        assertEquals("changeMe!", ua.getObject().getPassword().getValue());
        assertNotNull(ua.getObject().getResume());
        assertTrue(ua.getObject().getResume().getLob().getData().length > 0);
        assertNotNull(ua.getObject().getResume().getName());
        assertNotNull(ua.getObject().getResume().getContentType());

        ua.getTypes().clear();
        ua.getInstitutions().clear();
        ua.getObject().setId(1L);
        ua.setPassword("Password1");
        UsernameHolder.setUser("testUser");
        ua.prepare();
        assertNotNull(ua.getTypes());
        assertEquals(1, ua.getTypes().size());
        assertNotNull(ua.getInstitutions());
        assertEquals(1, ua.getInstitutions().size());
        assertNotNull(ua.getObject().getId());
        assertNotNull(ua.getGroups());
        assertFalse(ua.getGroups().isEmpty());
        assertEquals(1, ua.getGroups().size());

        ApplicationRole crossInstitution = new ApplicationRole();
        crossInstitution.setName(Role.CROSS_INSTITUTION.getName());
        u.getRoles().add(crossInstitution);
        ua = getUserAction();
        ua.prepare();
        assertNull(ua.getObject().getAddress().getLine1());
        assertNull(ua.getObject().getAddress().getLine2());
        assertNull(ua.getObject().getAddress().getCity());
        assertNull(ua.getObject().getAddress().getState());
        assertNull(ua.getObject().getAddress().getZip());
        assertEquals(Country.UNITED_STATES, ua.getObject().getAddress().getCountry());

        TissueLocatorSessionHelper.setLoggedInUser(getSession(), null);
        ua = getUserAction();
        ua.prepare();
        assertNull(ua.getObject().getAddress().getLine1());
        assertNull(ua.getObject().getAddress().getLine2());
        assertNull(ua.getObject().getAddress().getCity());
        assertNull(ua.getObject().getAddress().getState());
        assertNull(ua.getObject().getAddress().getZip());
        assertEquals(Country.UNITED_STATES, ua.getObject().getAddress().getCountry());
    }

    /**
     * test file extensions.
     * @throws URISyntaxException on error
     */
    @Test
    public void testFileExtensions() throws URISyntaxException {
        UserAction ua = getUserAction();
        FileDynamicFieldDefinition fileDef = (FileDynamicFieldDefinition) ua.getDynamicFieldDefinitions().get(1);
        assertNotNull(ua.getFiles());
        assertEquals(1, ua.getFiles().size());
        assertNull(ua.getFiles().values().iterator().next().getFile());
        assertNull(ua.getFiles().values().iterator().next().getFileContentType());
        assertNull(ua.getFiles().values().iterator().next().getFileFileName());
        ua.prepare();
        assertEquals(1, ua.getFiles().size());
        assertNull(ua.getFiles().values().iterator().next().getFile());
        assertNull(ua.getFiles().values().iterator().next().getFileContentType());
        assertNull(ua.getFiles().values().iterator().next().getFileFileName());
        assertNull(ua.getObject().getCustomProperties().get(fileDef.getFieldName()));
        assertNull(ua.getObject().getCustomProperties().get(fileDef.getContentTypeFieldName()));
        assertNull(ua.getObject().getCustomProperties().get(fileDef.getFileNameFieldName()));

        @SuppressWarnings("rawtypes")
        FileUpload upload = ua.getFiles().values().iterator().next();
        upload.setFile(new File(ClassLoader.getSystemResource("log4j.properties").toURI()));
        upload.setFileContentType("text/xml");
        upload.setFileFileName("log4j.properties");
        ua.prepare();
        assertEquals(1, ua.getFiles().size());
        assertNotNull(ua.getFiles().values().iterator().next().getFile());
        assertNotNull(ua.getFiles().values().iterator().next().getFileContentType());
        assertNotNull(ua.getFiles().values().iterator().next().getFileFileName());
        assertNotNull(ua.getObject().getCustomProperties().get(fileDef.getFieldName()));
        assertNotNull(ua.getObject().getCustomProperties().get(fileDef.getContentTypeFieldName()));
        assertNotNull(ua.getObject().getCustomProperties().get(fileDef.getFileNameFieldName()));
        assertEquals(ua.getFiles().values().iterator().next().getFileContentType(),
                ua.getObject().getCustomProperties().get(fileDef.getContentTypeFieldName()));
        assertEquals(ua.getFiles().values().iterator().next().getFileFileName(),
                ua.getObject().getCustomProperties().get(fileDef.getFileNameFieldName()));
    }

    /**
     * Validate that the password field value is acceptable.
     */
    @Test
    public void checkPasswordIsValid() {
        UserAction ua = getUserAction();
        ua.getObject().setId(1L);
        ua.setPassword("tissueLocator1");
        ua.validate();
        assertNull(ua.getFieldErrors().get("password"));
        ua.setPassword("");
        ua.validate();
        assertNull(ua.getFieldErrors().get("password"));
        ua.setPassword("abcd");
        ua.validate();
        assertTrue(ua.getFieldErrors().get("password").contains("errors.changepassword.badRequested"));
    }

    /**
     * When status is null the user has not been saved so the user action should be valid.
     * @throws InvocationTargetException if the underlying method throws an exception.
     * @throws IllegalAccessException if the underlying method is inaccessible.
     * @throws NoSuchMethodException if a matching method is not found.
     */
    @Test
    public void statusIsNullIsValid() throws NoSuchMethodException,
            IllegalAccessException, InvocationTargetException {
        UserAction ua = getUserAction();
        setPreviousStatus(ua.getObject(), AccountStatus.ACTIVE);
        ua.getObject().setStatus(null);
        ua.validate();
        assertNull(ua.getFieldErrors().get("denialReason"));
    }

    /**
     * When previous status is pending and status is inactive the user registration has
     * been denied. The user should be deleted.
     *
     * @throws InvocationTargetException if the underlying method throws an exception.
     * @throws IllegalAccessException if the underlying method is inaccessible.
     * @throws NoSuchMethodException if a matching method is not found.
     * @throws MessagingException if an error occurs while sending email.
     * @throws IOException if an input/output operation fails or is interrupted.
     */
    @Test
    public void denyingRegistrationShouldNotDeleteUser() throws NoSuchMethodException,
            IllegalAccessException, InvocationTargetException, IOException,
            MessagingException {
        UserAction ua = getUserAction();
        ua.setAccountApprovalActive(true);
        ua.getObject().setId(1L);
        setPreviousStatus(ua.getObject(), AccountStatus.PENDING);
        ua.getObject().setStatus(AccountStatus.INACTIVE);
        ua.save();
        assertNotNull(getTestUserService().getObject());
    }

    /**
     * When previous status is active and status is inactive the user account has
     * been deactivated and the appropriate service method should be called.
     * @throws NoSuchMethodException on error.
     * @throws IllegalAccessException on error.
     * @throws InvocationTargetException on error.
     * @throws IOException on error.
     * @throws MessagingException on error.
     */
    @Test
    public void testDeactivateUser() throws NoSuchMethodException, IllegalAccessException,
        InvocationTargetException, IOException, MessagingException {
        UserAction ua = getUserAction();
        ua.setAccountApprovalActive(true);
        ua.getObject().setId(1L);
        setPreviousStatus(ua.getObject(), AccountStatus.PENDING);
        ua.getObject().setStatus(AccountStatus.INACTIVE);
        ua.save();
        assertNotNull(getTestUserService().getObject());
        assertNull(getTestUserService().getObject().getStatusTransitionComment());

        setPreviousStatus(ua.getObject(), AccountStatus.ACTIVE);
        ua.getObject().setStatus(AccountStatus.INACTIVE);
        ua.save();
        assertNotNull(getTestUserService().getObject());
        assertEquals("deactivated", getTestUserService().getObject().getStatusTransitionComment());
    }

    private UserAction getUserAction() {
        return new UserAction(getTestUserService(), getTestAppSettingService(),
                getTestUiDynamicFieldCategoryService()) {
            private static final long serialVersionUID = 1L;

            /** {@inheritDoc} */
            @Override
            public List<AbstractDynamicFieldDefinition> getDynamicFieldDefinitions() {
                List<AbstractDynamicFieldDefinition> extensions =  new ArrayList<AbstractDynamicFieldDefinition>();
                StringDynamicFieldDefinition stringDef = new StringDynamicFieldDefinition();
                stringDef.setFieldName("string field");
                extensions.add(stringDef);
                FileDynamicFieldDefinition fileDef = new FileDynamicFieldDefinition();
                fileDef.setFieldDisplayName("file field");
                fileDef.setFileNameFieldName("file name field");
                fileDef.setContentTypeFieldName("content name field");
                extensions.add(fileDef);
                return extensions;
            }
        };
    }

    private void setPreviousStatus(TissueLocatorUser user, AccountStatus status) throws NoSuchMethodException,
            IllegalAccessException, InvocationTargetException {
        Method method = AbstractUser.class
            .getDeclaredMethod("setPreviousStatus", AccountStatus.class);
        method.setAccessible(true);
        method.invoke(user, status);
    }

    /**
     * test dynamic extension categorization decider for the user action.
     * @throws Exception on error
     */
    @Test
    public void testDecider() throws Exception  {
        UserAction ua = getUserAction();
        assertTrue(ua.getUiDynamicFieldCategoryDecider().decide(new UiDynamicFieldCategory()));
    }

    /**
     * Test the autocomplete method.
     */
    @Test
    public void testAutocomplete() {
        autocompleteTestHelper(true);
        autocompleteTestHelper(false);
    }

    private void autocompleteTestHelper(boolean dispalConsortiumRegistration) {
        InstitutionServiceStub instStub = (InstitutionServiceStub)
            TissueLocatorRegistry.getServiceLocator().getInstitutionService();
        getTestAppSettingService().setDisplayConsortiumRegistration(dispalConsortiumRegistration);
        instStub.setSearchResultsSize(1);
        UserAction ua = getUserAction();
        assertEquals(Action.SUCCESS, ua.autocompleteInstitution());
        ua.setTerm(null);
        assertEquals(0, ua.getRegistrationInstitutionsMap().length);
        ua.setTerm("test");
        assertEquals(1, ua.getRegistrationInstitutionsMap().length);
        SelectOption result = ua.getRegistrationInstitutionsMap()[0];
        assertNotNull(result.getId());
        assertNotNull(result.getLabel());
        assertNotNull(result.getValue());

        ua = getUserAction();
        ua.setTerm("test");

        instStub.setSearchResultsSize(MAX_INSTITUTION_COUNT);
        SelectOption[] results = ua.getRegistrationInstitutionsMap();
        assertEquals(MAX_INSTITUTION_COUNT + 1, results.length);
        assertEquals("-1", results[0].getId());
    }
}
