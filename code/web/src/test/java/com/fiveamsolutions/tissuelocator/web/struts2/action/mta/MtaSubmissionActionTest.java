/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.web.struts2.action.mta;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.Test;

import com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreement;
import com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreementStatus;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.fiveamsolutions.tissuelocator.web.TissueLocatorFileHelper;
import com.fiveamsolutions.tissuelocator.web.test.AbstractTissueLocatorWebTest;
import com.fiveamsolutions.tissuelocator.web.test.MaterialTransferAgreementServiceStub;
import com.fiveamsolutions.tissuelocator.web.test.SignedMaterialTransferAgreementServiceStub;
import com.opensymphony.xwork2.Action;

/**
 * @author ddasgupta
 *
 */
public class MtaSubmissionActionTest extends AbstractTissueLocatorWebTest {

    /**
     * tests the prepare method.
     * @throws Exception on error
     */
    @Test
    public void testPrepare() throws Exception {
        MtaSubmissionAction action = new MtaSubmissionAction(getTestUserService());
        assertNull(action.getCurrentMta());
        assertNotNull(action.getSignedMta());
        assertNull(action.getSignedMta().getUploader());
        assertNull(action.getSignedMta().getUploadTime());
        assertNull(action.getSignedMta().getReceivingInstitution());
        assertNull(action.getSignedMta().getStatus());
        assertNull(action.getSignedMta().getOriginalMta());
        assertNull(action.getSignedMta().getDocument());
        assertNull(action.getSignedMta().getId());
        assertNull(action.getSignedMta().getSendingInstitution());
        action.prepare();
        assertNotNull(action.getCurrentMta());
        assertEquals(1L, action.getCurrentMta().getId().longValue());
        assertEquals(MaterialTransferAgreementServiceStub.MTA_VERSION, action.getCurrentMta().getVersion());
        assertNotNull(action.getSignedMta());
        assertNotNull(action.getSignedMta().getUploader());
        assertNotNull(action.getSignedMta().getUploadTime());
        assertNotNull(action.getSignedMta().getReceivingInstitution());
        assertEquals(action.getSignedMta().getUploader().getInstitution(),
                action.getSignedMta().getReceivingInstitution());
        assertNotNull(action.getSignedMta().getStatus());
        assertEquals(SignedMaterialTransferAgreementStatus.PENDING_REVIEW, action.getSignedMta().getStatus());
        assertNotNull(action.getSignedMta().getOriginalMta());
        assertEquals(action.getCurrentMta(), action.getSignedMta().getOriginalMta());
        assertNull(action.getSignedMta().getDocument());
        assertNull(action.getSignedMta().getId());
        assertNull(action.getSignedMta().getSendingInstitution());

        action.setDocument(new File(ClassLoader.getSystemResource("log4j.properties").toURI()));
        action.setDocumentFileName("log4j.properties");
        action.setDocumentContentType("text/plain");
        action.setSendingMta(true);
        action.getSignedMta().setReceivingInstitution(null);
        action.getSignedMta().setSendingInstitution(null);
        action.prepare();
        assertNotNull(action.getSignedMta().getDocument());
        assertTrue(action.getSignedMta().getDocument().getLob().getData().length > 0);
        assertNotNull(action.getSignedMta().getDocument().getName());
        assertNotNull(action.getSignedMta().getDocument().getContentType());
        assertNotNull(action.getSignedMta().getReceivingInstitution());
        assertEquals(action.getSignedMta().getUploader().getInstitution(),
                action.getSignedMta().getReceivingInstitution());
        assertNull(action.getSignedMta().getSendingInstitution());
    }


    /**
     * tests the save method.
     * @throws Exception on error
     */
    @Test
    public void testSave() throws Exception {
        MtaSubmissionAction action = new MtaSubmissionAction(getTestUserService());
        SignedMaterialTransferAgreement mta = new SignedMaterialTransferAgreement();
        action.setSignedMta(mta);
        action.prepare();
        String result = action.saveMta();
        assertEquals(Action.SUCCESS, result);
        SignedMaterialTransferAgreementServiceStub stub = (SignedMaterialTransferAgreementServiceStub)
            TissueLocatorRegistry.getServiceLocator().getSignedMaterialTransferAgreementService();
        assertEquals(mta, stub.getObject());
        assertNotSame(action.getSignedMta(), stub.getObject());
        assertFalse(action.getActionMessages().isEmpty());
        assertEquals(1, action.getActionMessages().size());
    }

    /**
     * Tests the validate method.
     */
    @Test
    public void testValidate() {
        MtaSubmissionAction action = new MtaSubmissionAction(getTestUserService());
        assertNull(action.getObjects().getList());
        action.validate();
        assertNull(action.getObjects().getList());
        action.addActionError("error message");
        action.validate();
        assertNotNull(action.getObjects().getList());
        assertEquals(0, action.getObjects().getList().size());
    }

    /**
     * Test file helper with no file.
     */
    @Test
    public void testFileHelper() {
        byte[] emptyContent = TissueLocatorFileHelper.getFileContents(new File("dummy"));
        assertTrue(emptyContent.length == 0);
    }

}
