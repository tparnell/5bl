/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.struts2.action;

import static com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreementStatus.APPROVED;
import static com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreementStatus.OUT_OF_DATE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.lang.reflect.Field;
import java.util.Calendar;
import java.util.Date;

import org.junit.Test;

import com.fiveamsolutions.tissuelocator.data.Address;
import com.fiveamsolutions.tissuelocator.data.Country;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.InstitutionType;
import com.fiveamsolutions.tissuelocator.data.Person;
import com.fiveamsolutions.tissuelocator.data.State;
import com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreement;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.fiveamsolutions.tissuelocator.web.test.AbstractTissueLocatorWebTest;
import com.fiveamsolutions.tissuelocator.web.test.InstitutionServiceStub;
import com.fiveamsolutions.tissuelocator.web.test.MaterialTransferAgreementServiceStub;
import com.fiveamsolutions.tissuelocator.web.test.SignedMaterialTransferAgreementServiceStub;
import com.opensymphony.xwork2.Action;

/**
 * @author smiller
 */
public class InstitutionActionTest extends AbstractTissueLocatorWebTest {

    /**
     * Tests the load method.
     */
    @Test
    public void testLoad() {
        assertEquals(Action.INPUT, new InstitutionAction(getTestUserService()).input());
    }

    /**
     * Tests the list method.
     */
    @Test
    public void testlist() {
        InstitutionAction action = new InstitutionAction(getTestUserService());
        assertEquals(Action.SUCCESS, action.list());
        assertEquals(0, action.getObjects().getList().size());
    }

    /**
     *
     */
    @Test
    public void testFilter() {
        InstitutionAction action = new InstitutionAction(getTestUserService());
        action.setConsortiumMembersOnly(true);
        assertEquals(Action.SUCCESS, action.filter());
        assertTrue(action.getObject().getConsortiumMember());

        action.setConsortiumMembersOnly(false);
        assertEquals(Action.SUCCESS, action.filter());
        assertNull(action.getObject().getConsortiumMember());
    }

    /**
     * Tests the prepare method.
     * @throws Exception on error
     */
    @Test
    public void testPrepare() throws Exception {
        InstitutionAction action = new InstitutionAction(getTestUserService());
        assertNull(action.getCurrentMta());
        assertNotNull(action.getSignedMta());
        assertNull(action.getSignedMta().getUploader());
        assertNull(action.getSignedMta().getUploadTime());
        assertNull(action.getSignedMta().getReceivingInstitution());
        assertNull(action.getSignedMta().getSendingInstitution());
        assertNull(action.getSignedMta().getStatus());
        assertNull(action.getSignedMta().getOriginalMta());
        assertNull(action.getSignedMta().getDocument());
        assertNull(action.getSignedMta().getId());
        action.prepare();
        assertNull(action.getObject().getId());
        assertNotNull(action.getCurrentMta());
        assertEquals(1L, action.getCurrentMta().getId().longValue());
        assertEquals(MaterialTransferAgreementServiceStub.MTA_VERSION, action.getCurrentMta().getVersion());
        assertNotNull(action.getSignedMta());
        assertNotNull(action.getSignedMta().getUploader());
        assertNotNull(action.getSignedMta().getUploadTime());
        assertNotNull(action.getSignedMta().getReceivingInstitution());
        assertEquals(action.getObject(), action.getSignedMta().getReceivingInstitution());
        assertNull(action.getSignedMta().getSendingInstitution());
        assertNotNull(action.getSignedMta().getStatus());
        assertEquals(APPROVED, action.getSignedMta().getStatus());
        assertNotNull(action.getSignedMta().getOriginalMta());
        assertEquals(action.getCurrentMta(), action.getSignedMta().getOriginalMta());
        assertNull(action.getSignedMta().getDocument());
        assertNull(action.getSignedMta().getId());

        action.setObject(new Institution());
        action.getObject().setId(1L);
        action.setDocument(new File(ClassLoader.getSystemResource("log4j.properties").toURI()));
        action.setDocumentFileName("log4j.properties");
        action.setDocumentContentType("text/plain");
        action.setSendingMta(true);
        action.getSignedMta().setReceivingInstitution(null);
        action.getSignedMta().setSendingInstitution(null);
        action.getObject().setConsortiumMember(true);
        action.prepare();
        assertNotNull(action.getObject());
        assertEquals(1, action.getTypes().size());
        assertNotNull(action.getSignedMta().getDocument());
        assertTrue(action.getSignedMta().getDocument().getLob().getData().length > 0);
        assertNotNull(action.getSignedMta().getDocument().getName());
        assertNotNull(action.getSignedMta().getDocument().getContentType());
        assertNull(action.getSignedMta().getReceivingInstitution());
        assertNotNull(action.getSignedMta().getSendingInstitution());
        assertEquals(action.getObject(), action.getSignedMta().getSendingInstitution());
    }

    /**
     * Tests the save method.
     * @throws Exception on error
     */
    @Test
    public void testSave() throws Exception {
        InstitutionAction action = new InstitutionAction(getTestUserService());
        Institution institution = new Institution();
        institution.setName("test");
        action.setObject(institution);
        institution.setMtaContact(new Person());
        action.save();
        InstitutionServiceStub stub = (InstitutionServiceStub)
            TissueLocatorRegistry.getServiceLocator().getInstitutionService();
        assertEquals(action.getObject(), stub.getObject());
        assertFalse(action.getActionMessages().isEmpty());
        assertEquals(1, action.getActionMessages().size());

        getSession().clearAttributes();
        institution = new Institution();
        institution.setName("test 2");
        institution.setConsortiumMember(true);
        institution.setMtaContact(new Person());
        institution.getMtaContact().setId(1L);
        action.setObject(institution);
        action.prepare();
        action.save();
        assertEquals(action.getObject(), stub.getObject());

        getSession().clearAttributes();
        institution = new Institution();
        institution.setName("test 3");
        institution.setConsortiumMember(false);
        action.setObject(institution);
        action.save();
        assertEquals(action.getObject(), stub.getObject());

        institution = new Institution();
        institution.setConsortiumMember(true);
        SignedMaterialTransferAgreement mta = new SignedMaterialTransferAgreement();
        mta.setReceivingInstitution(action.getObject());
        mta.getReceivingInstitution().setMtaContact(createPerson(1L));
        mta.setUploadTime(new Date());
        institution.getSignedRecipientMtas().add(mta);
        institution.setMtaContact(new Person());
        action.setObject(institution);
        action.save();

        action.setDocument(new File(ClassLoader.getSystemResource("log4j.properties").toURI()));
        action.setDocumentFileName("log4j.properties");
        action.setDocumentContentType("text/plain");
        institution = new Institution();
        institution.getSignedRecipientMtas().add(mta);
        institution.setMtaContact(new Person());
        institution.getMtaContact().setId(1L);
        action.setObject(institution);
        action.prepare();
        action.save();
        assertNotNull(action.getObject().getMtaContact());
        SignedMaterialTransferAgreementServiceStub mtaStub = (SignedMaterialTransferAgreementServiceStub)
                TissueLocatorRegistry.getServiceLocator().getSignedMaterialTransferAgreementService();
        assertEquals(action.getSignedMta(), mtaStub.getObject());

        action.setObject(institution);
        action.setSendingMta(true);
        action.getSignedMta().setSendingInstitution(institution);
        action.getSignedMta().setReceivingInstitution(null);
        action.setDocument(new File(ClassLoader.getSystemResource("log4j.properties").toURI()));
        action.setDocumentFileName("log4j.properties");
        action.setDocumentContentType("text/plain");
        action.prepare();
        action.save();
        assertNotNull(action.getSignedMta().getSendingInstitution());
        assertNull(action.getSignedMta().getReceivingInstitution());
        mtaStub = (SignedMaterialTransferAgreementServiceStub)
                TissueLocatorRegistry.getServiceLocator().getSignedMaterialTransferAgreementService();
        assertEquals(action.getSignedMta(), mtaStub.getObject());

        action.setSendingMta(false);
        action.getSignedMta().setReceivingInstitution(institution);
        action.prepare();
        action.save();
        assertNotNull(action.getSignedMta().getReceivingInstitution());
        mtaStub = (SignedMaterialTransferAgreementServiceStub)
                TissueLocatorRegistry.getServiceLocator().getSignedMaterialTransferAgreementService();
        assertEquals(action.getSignedMta(), mtaStub.getObject());

        action.setObject(institution);
        action.setSendingMta(true);
        action.getSignedMta().setSendingInstitution(institution);
        action.prepare();
        action.save();
        assertNotNull(action.getSignedMta().getSendingInstitution());
        mtaStub = (SignedMaterialTransferAgreementServiceStub)
                   TissueLocatorRegistry.getServiceLocator().getSignedMaterialTransferAgreementService();
        assertEquals(action.getSignedMta(), mtaStub.getObject());

        // Test that the mta contact is ignored if sending institution is being updated
        action.prepare();
        action.setObject(institution);
        action.setSendingMta(true);
        action.getSignedMta().setSendingInstitution(institution);
        Person person = createPerson(1L);
        person.setFirstName("badfirstname");
        action.getSignedMta().getSendingInstitution().setMtaContact(person);
        action.getObject().setMtaContact(person);
        action.getSignedMta().setReceivingInstitution(null);
        action.save();
        assertNotNull(action.getObject().getMtaContact());
        assertEquals("firstName", action.getObject().getMtaContact().getFirstName());
        mtaStub = (SignedMaterialTransferAgreementServiceStub)
                TissueLocatorRegistry.getServiceLocator().getSignedMaterialTransferAgreementService();
        assertEquals(action.getSignedMta(), mtaStub.getObject());

        // Test that the mta contact is not ignored if not performing a send mta
        action.setSendingMta(false);
        institution.setConsortiumMember(false);
        action.getSignedMta().setReceivingInstitution(institution);
        person = createPerson(1L);
        person.setFirstName("newfirstName");
        action.getObject().setMtaContact(person);
        action.save();
        assertNotNull(action.getObject().getMtaContact());
        assertEquals("newfirstName", action.getObject().getMtaContact().getFirstName());
        mtaStub = (SignedMaterialTransferAgreementServiceStub)
                TissueLocatorRegistry.getServiceLocator().getSignedMaterialTransferAgreementService();
        assertEquals(action.getSignedMta(), mtaStub.getObject());
        verifySaveWithMtaContact(action, mta);
    }

    private void verifySaveWithMtaContact(InstitutionAction action, SignedMaterialTransferAgreement mta)
        throws Exception {
        // Test that mta contact is set to null if performing a send mta when there
        // have been no previous contact set yet
        InstitutionAction action2 = new InstitutionAction(getTestUserService());
        Institution institution = new Institution();
        institution.setName("test");
        institution.setId(1L);
        action2.setObject(institution);
        action2.prepare();
        action2.setSendingMta(true);
        action2.getSignedMta().setSendingInstitution(action.getObject());
        Person person = createPerson(2L);
        action2.getSignedMta().getSendingInstitution().setMtaContact(person);
        action2.getSignedMta().setReceivingInstitution(null);
        action2.save();
        assertNull(action2.getObject().getMtaContact());

        // Test that send MTAs are set to out of date if consortium membership
        // is removed
        institution = createInstitution(1L, "test");
        institution.setConsortiumMember(true);
        institution.getSignedSenderMtas().add(mta);
        mta.setStatus(APPROVED);
        action.setObject(institution);
        action.setSendingMta(true);
        action.getSignedMta().setSendingInstitution(institution);
        action.getSignedMta().setReceivingInstitution(null);
        action.save();
        action.setObject(institution);
        institution.setConsortiumMember(true);
        // Ideally, we call action.prepare(), but test stubs are are just creating generic classes
        Field field = InstitutionAction.class.getDeclaredField("wasConsortiumMember");
        field.setAccessible(true); //override the access restriction of it being private
        field.set(action, true);
        institution.setConsortiumMember(true);
        action.save();
        assertEquals(APPROVED, institution.getCurrentSignedSenderMta().getStatus());
        action.setObject(institution);
        field.set(action, false);
        institution.setConsortiumMember(false);
        action.save();
        assertEquals(APPROVED, institution.getCurrentSignedSenderMta().getStatus());
        action.setObject(institution);
        field.set(action, true);
        institution.setConsortiumMember(false);
        action.save();
        assertEquals(OUT_OF_DATE, institution.getCurrentSignedSenderMta().getStatus());

        // Test that we can create an Institution with new MTA
        institution = createInstitution(null, "test");
        institution.setConsortiumMember(true);
        institution.getSignedSenderMtas().add(mta);
        mta.setStatus(APPROVED);
        action.setObject(institution);
        action.setSendingMta(true);
        action.getSignedMta().setSendingInstitution(institution);
        action.getSignedMta().setReceivingInstitution(null);
        person = createPerson(2L);
        action.getSignedMta().getSendingInstitution().setMtaContact(person);
        action.save();
        assertNotNull(action.getSignedMta());

        institution = createInstitution(1L, "test");
        institution.setConsortiumMember(true);
        institution.getSignedSenderMtas().add(mta);
        mta.setStatus(APPROVED);
        action.setObject(institution);
        action.setSendingMta(false);
        action.setDocument(new File(ClassLoader.getSystemResource("log4j.properties").toURI()));
        action.setDocumentFileName("log4j.properties");
        action.setDocumentContentType("text/plain");
        action.getSignedMta().setSendingInstitution(null);
        action.getSignedMta().setReceivingInstitution(institution);
        person = createPerson(2L);
        action.getSignedMta().getReceivingInstitution().setMtaContact(person);
        action.save();
        assertNotNull(action.getSignedMta());
     }

    /**
     * Tests the get signed mtas method.
     */
    @Test
    public void testGetSignedMtas() {
        InstitutionAction action = new InstitutionAction(getTestUserService());
        assertNotNull(action.getObject());
        Calendar cal = Calendar.getInstance();
        SignedMaterialTransferAgreement receiver1 = new SignedMaterialTransferAgreement();
        receiver1.setReceivingInstitution(action.getObject());
        receiver1.setUploadTime(cal.getTime());
        action.getObject().getSignedRecipientMtas().add(receiver1);
        cal.add(Calendar.DATE, 1);
        SignedMaterialTransferAgreement sender1 = new SignedMaterialTransferAgreement();
        sender1.setSendingInstitution(action.getObject());
        sender1.setUploadTime(cal.getTime());
        action.getObject().getSignedSenderMtas().add(sender1);
        cal.add(Calendar.DATE, 1);
        SignedMaterialTransferAgreement receiver2 = new SignedMaterialTransferAgreement();
        receiver2.setReceivingInstitution(action.getObject());
        receiver2.setUploadTime(cal.getTime());
        action.getObject().getSignedRecipientMtas().add(receiver2);
        cal.add(Calendar.DATE, 1);
        SignedMaterialTransferAgreement sender2 = new SignedMaterialTransferAgreement();
        sender2.setSendingInstitution(action.getObject());
        sender2.setUploadTime(cal.getTime());
        action.getObject().getSignedSenderMtas().add(sender2);
        SignedMaterialTransferAgreement[] ordered = {sender2, receiver2, sender1, receiver1};
        assertNotNull(action.getSignedMtas());
        assertEquals(ordered.length, action.getSignedMtas().size());
        int i = 0;
        for (SignedMaterialTransferAgreement signedMta : action.getSignedMtas()) {
            assertEquals(ordered[i++], signedMta);
        }
    }

    private Person createPerson(Long id) {
        Address address = new Address();
        address.setCity("city");
        address.setCountry(Country.UNITED_STATES);
        address.setFax("fax");
        address.setId(1L);
        address.setLine1("address1");
        address.setPhone("5555555555");
        address.setPhoneExtension("1234");
        address.setState(State.MARYLAND);
        address.setZip("20878");

        Person person = new Person();
        person.setId(id);
        person.setEmail("noone@nowhere.com");
        person.setFirstName("firstName");
        person.setLastName("lastName");
        person.setAddress(address);
        return person;
    }

    private Institution createInstitution(Long id, String name) {
        Institution institution = new Institution();
        institution.setId(id);
        institution.setName(name);
        institution.setConsortiumMember(true);
        institution.setMtaContact(createPerson(1L));

        InstitutionType type = new InstitutionType();
        type.setId(1L);
        type.setName("institution type");
        institution.setType(type);

        return institution;
    }
}
