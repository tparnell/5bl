<%@ tag body-content="empty" %>
<%@ attribute name="extendableEntity" required="true" type="com.fiveamsolutions.dynamicextensions.ExtendableEntity" %>
<%@ attribute name="fieldDefinition" required="true" type="com.fiveamsolutions.dynamicextensions.AbstractDynamicFieldDefinition" %>
<%@ attribute name="layout" required="true" %>
<%@ attribute name="mode" required="true" %>
<%@ attribute name="propertyPath" %>
<%@ attribute name="wrapperClass" %>
<%@ attribute name="wrapperStyle" %>
<%@ attribute name="displayUnknown" %>
<%@ attribute name="helpUrlBase" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator" %>

<s:set var="dynamicFieldName" value="%{'files[\\'' + #attr.fieldDefinition.fieldName + '\\'].file'}"/>
<fmt:message key="unknown.value" var="unknownValue"/>

    <s:if test="%{#attr.layout == 'rowLayout'}">
        <tr>
            <td class="label" scope="row">
            <s:if test="%{#attr.mode == 'view'}">
                <label>${fieldDefinition.fieldDisplayName}</label>
            </s:if>
            <s:if test="%{#attr.mode == 'edit'}">
                <tissuelocator:helpLabel helpUrlBase="${helpUrlBase}"
                    labelFor="${dynamicFieldName}"
                    fieldDisplayName="${fieldDefinition.fieldDisplayName}"
                    fieldName="${fieldDefinition.fieldName}"
                    required="${!fieldDefinition.nullable || fieldDefinition.nullableExtensionDeciderClass != null}"/>            
            </s:if>
            </td>
            <td class="value">
    </s:if>
    <s:else>
        <div class="${wrapperClass}" style="${wrapperStyle}">
    </s:else>
    <s:if test="%{#attr.mode == 'view'}">
        <s:if test="%{#attr.layout == 'boxLayout'}">
            <label>${fieldDefinition.fieldDisplayName}</label>
        </s:if>
    </s:if>
    <s:if test="%{#attr.mode == 'edit'}">
        <s:if test="%{#attr.layout == 'boxLayout'}">
            <tissuelocator:helpLabel helpUrlBase="${helpUrlBase}"
                labelFor="${dynamicFieldName}"
                fieldDisplayName="${fieldDefinition.fieldDisplayName}"
                fieldName="${fieldDefinition.fieldName}"
                required="${!fieldDefinition.nullable || fieldDefinition.nullableExtensionDeciderClass != null}"/>        
        </s:if>
        <s:file id="%{#dynamicFieldName}"
            name="%{#dynamicFieldName}"
            size="30"
            required="%{!nullable || nullableExtensionDeciderClass != null}"
            requiredposition="right"/>
        <s:fielderror cssClass="fielderror">
            <s:param value="%{#attr.propertyPath + '.' + #attr.fieldDefinition.fieldDisplayName}"/>
        </s:fielderror>
        <s:fielderror cssClass="fielderror">
            <s:param value="%{#attr.fieldDefinition.fieldDisplayName}"/>
        </s:fielderror>
    </s:if>
    <s:set var="fileId" value="%{#attr.extendableEntity.getCustomProperty(#attr.fieldDefinition.fieldName).id}"/>
    <s:set var="fileName" value="%{#attr.extendableEntity.getCustomProperty(#attr.fieldDefinition.fileNameFieldName)}"/>
    <s:set var="contentType" value="%{#attr.extendableEntity.getCustomProperty(#attr.fieldDefinition.contentTypeFieldName)}"/>
    <s:if test="%{#fileId != null}">
        <div class="value">
            <c:url value="/protected/downloadFile.action" var="fileDownloadUrl">
                <c:param name="file.id"><s:property value="%{#fileId}"/></c:param>
                <c:param name="fileName"><s:property value="%{#fileName}"/></c:param>
                <c:param name="contentType"><s:property value="%{#contentType}"/></c:param>
            </c:url>
            <a href="${fileDownloadUrl}" class="file">
                <s:property value="%{#fileName}"/>
            </a>
        </div>
    </s:if>
    <s:elseif test="%{#attr.displayUnknown == 'true'">
        ${unknownValue}
    </s:elseif>
    <s:if test="%{#attr.layout == 'rowLayout'}">
            </td>
        </tr>
    </s:if>  
    <s:else>
        </div>
    </s:else>        
