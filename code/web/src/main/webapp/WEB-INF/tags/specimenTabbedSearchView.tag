<%@ tag body-content="scriptless" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>

<s:if test="%{tabbedSearchAndSectionedCategoriesViewsEnabled}">
    <p class="instructions_2">
        <fmt:message key="specimen.search.tabbed.view.instructions"/>
    </p>
    <div id="tabs" style="display:none;">
        <ul id="tabbytabs" class="tabbytabs">
            <s:iterator value="%{uiSections}" status="rowStatus">
                <s:set var="rowNumber" value="%{#rowStatus.index + 1}"/>
                <s:set var="tab_id" value="%{'tab_' + #rowNumber}"/>                                              
                <s:set var="tab_href" value="%{'#tab_' + #rowNumber + '_content'}"/>
                <li class="tab">
                    <a id="${tab_id}" href="${tab_href}" onClick="selectCurrentTab('${tab_href}');">
                        <s:property value="sectionName"/>
                    </a>                    
                </li>                    
            </s:iterator>
        </ul>
        <s:hidden id="selectedTab" name="selectedTab" />
        <div class="clear"></div>
</s:if>
<jsp:doBody/>
<s:if test="%{tabbedSearchAndSectionedCategoriesViewsEnabled}">
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#tabs').tabs();
            $('#tabs').removeClass('ui-widget ui-widget-content');
            $('#tabbytabs').removeClass('ui-tabs-nav ui-widget-header');
            $('#tabs').show();
            $('a[href=${selectedTab}]').trigger('click');
        });
        
        function selectCurrentTab(currentTab) {
            $('#selectedTab').val(currentTab);
        }
    </script>
</s:if>