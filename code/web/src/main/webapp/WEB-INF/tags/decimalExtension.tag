<%@ tag body-content="empty" %>
<%@ attribute name="extendableEntity" required="true" type="com.fiveamsolutions.dynamicextensions.ExtendableEntity" %>
<%@ attribute name="fieldDefinition" required="true" type="com.fiveamsolutions.dynamicextensions.AbstractDynamicFieldDefinition" %>
<%@ attribute name="layout" required="true" %>
<%@ attribute name="mode" required="true" %>
<%@ attribute name="propertyPath" %>
<%@ attribute name="wrapperClass" %>
<%@ attribute name="wrapperStyle" %>
<%@ attribute name="displayUnknown" %>
<%@ attribute name="helpUrlBase" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator" %>

<s:set var="dynamicFieldName" value="%{#attr.propertyPath + '.customProperties[\\'' + #attr.fieldDefinition.fieldName + '\\']'}" scope="page"/>
<fmt:message key="unknown.value" var="unknownValue"/>

    <s:if test="%{#attr.layout == 'rowLayout'}">
        <tr>
            <td class="label" scope="row">
                <label>
                    <s:property value="%{#attr.fieldDefinition.fieldDisplayName}"/>
                    <s:if test="%{#attr.mode == 'edit'}">
                        <s:if test="%{!nullable || nullableExtensionDeciderClass != null}">
                            <span class="reqd">
                                <fmt:message key="required.indicator"/>
                            </span>
                        </s:if>
                    </s:if>
                </label>
            </td>
            <td class="value">
    </s:if>
    <s:else>
        <div class="${wrapperClass}" style="${wrapperStyle}">
    </s:else>
    <s:if test="%{#attr.mode == 'view'}">
        <s:if test="%{#attr.layout == 'boxLayout'}">
            <label>${fieldDefinition.fieldDisplayName}</label>
            <div class="value">
        </s:if>
        <s:set value="%{#attr.extendableEntity.getCustomProperty(#attr.fieldDefinition.fieldName)}" var="propertyValue" scope="page"/>
        <s:set value="%{#attr.fieldDefinition.integerDigits}" var="integerDigits" scope="page"/>
        <s:set value="%{#attr.fieldDefinition.fractionalDigits}" var="fractionalDigits" scope="page"/>
        <s:if test="%{(#attr.displayUnknown == 'true') && (#attr.propertyValue == null)}">
            ${unknownValue}
        </s:if>
        <s:else>
            <s:if test="%{#attr.fieldDefinition.monetary}">
                <fmt:message key="currency.label"/>
            </s:if>
            <fmt:formatNumber value="${propertyValue}" maxIntegerDigits="${integerDigits}" maxFractionDigits="${fractionalDigits}"/>
        </s:else>
        <s:if test="%{#attr.layout == 'boxLayout'}">
            </div>
        </s:if>
    </s:if>
    <s:if test="%{#attr.mode == 'edit'}">
        <s:if test="%{#attr.layout == 'boxLayout'}">
            <tissuelocator:helpLabel helpUrlBase="${helpUrlBase}" labelFor="${dynamicFieldName}"
                fieldDisplayName="${fieldDefinition.fieldDisplayName}" fieldName="${fieldDefinition.fieldName}"
                required="${!fieldDefinition.nullable || fieldDefinition.nullableExtensionDeciderClass != null}"/>
        </s:if>
        <s:if test="%{#attr.fieldDefinition.monetary}">
            <fmt:message key="currency.label"/>
        </s:if>
        <s:set var="textFieldSize" value="%{#attr.fieldDefinition.integerDigits + #attr.fieldDefinition.fractionalDigits + (#attr.fieldDefinition.integerDigits/3) + 1}"/>
        <s:textfield id="%{#attr.dynamicFieldName}"
            name="%{#attr.dynamicFieldName}"
            value="%{#attr.extendableEntity.getCustomProperty(#attr.fieldDefinition.fieldName)}"
            size="%{#textFieldSize}"
            maxlength="%{#textFieldSize}"
            theme="simple"/>
        <s:fielderror cssClass="fielderror">
            <s:param value="%{#attr.propertyPath + '.' + #attr.fieldDefinition.fieldDisplayName}"/>
        </s:fielderror>
        <s:fielderror cssClass="fielderror">
            <s:param value="%{#attr.fieldDefinition.fieldDisplayName}"/>
        </s:fielderror>
    </s:if>
    <s:if test="%{#attr.layout == 'rowLayout'}">
            </td>
        </tr>
    </s:if>
    <s:else>
        </div>
    </s:else>
