<%@ tag body-content="empty" %>
<%@ attribute name="question" required="true" type="com.fiveamsolutions.tissuelocator.data.support.Question" rtexprvalue="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator" %>

<div class="box pad10">
    <h2 class="formtop noline nobg"><fmt:message key="questionResponse.question.title"/></h2>

    <div id="principal_investigator">

        <h4 class="green_bar"><fmt:message key="questionResponse.investigator.title"/></h4>

        <tissuelocator:viewPersonSingleColumn person="${question.investigator}" showInstitution="true"/>

        <div class="col250">
            <c:url value="/protected/downloadFile.action" var="downloadResumeUrl">
                <c:param name="file.id" value="${question.resume.lob.id}" />
                <c:param name="fileName" value="${question.resume.name}" />
                <c:param name="contentType" value="${question.resume.contentType}" />
            </c:url>
                <b><fmt:message key="question.resume"/></b>
            <a href="${downloadResumeUrl}" class="file">
                ${question.resume.name}
            </a>
        </div>

        <c:if test="${!empty question.protocolDocument}">
            <div class="col250">
                <c:url value="/protected/downloadFile.action" var="downloadProtocolUrl">
                    <c:param name="file.id" value="${question.protocolDocument.lob.id}" />
                    <c:param name="fileName" value="${question.protocolDocument.name}" />
                    <c:param name="contentType" value="${question.protocolDocument.contentType}" />
                </c:url>
                <b><fmt:message key="question.protocolDocument"/></b>
                <a href="${downloadProtocolUrl}" class="file">
                    ${question.protocolDocument.name}
                </a>
            </div>
        </c:if>

        <div class="clear"></div>

        <h4 class="green_bar"><fmt:message key="question.question.title"/></h4>

        <div class="pad10">
            <p style="width:100%; max-width:none;">
                ${question.questionText}
            </p>
        </div>
    </div>
</div>

