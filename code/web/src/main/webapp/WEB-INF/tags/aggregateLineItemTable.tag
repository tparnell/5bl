<%@ tag body-content="empty" %>
<%@ attribute name="requestLineItems" required="true" type="java.util.Collection" rtexprvalue="true" %>
<%@ attribute name="showOrderFulfillmentForms" required="true" rtexprvalue="true" %>
<%@ attribute name="showFinalPriceColumn" required="true" rtexprvalue="true" %>
<%@ attribute name="showReceiptQuality" rtexprvalue="true" %>
<%@ attribute name="cartPrefix" rtexprvalue="true" %>
<%@ attribute name="lineItemPrefix" rtexprvalue="true" %>
<%@ attribute name="order" type="com.fiveamsolutions.tissuelocator.data.Shipment" rtexprvalue="true" %>
<%@ attribute name="emptyKey" rtexprvalue="true" %>
<%@ attribute name="totalKey" rtexprvalue="true" %>
<%@ attribute name="titleKey" rtexprvalue="true" %>
<%@ attribute name="displayVotingResults" rtexprvalue="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/request-1.0" prefix="req" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator" %>

<c:set var="tableCellCount" value="${4}"/>
<req:isUserInRole role="priceViewer">
    <c:if test="${showFinalPriceColumn == 'true'}">
        <c:set var="tableCellCount" value="${tableCellCount + 1}"/>
    </c:if>
</req:isUserInRole>

<c:if test="${showOrderFulfillmentForms == 'true'}">
    <c:set var="tableCellCount" value="${tableCellCount + 1}"/>
</c:if>

<c:if test="${showReceiptQuality == 'true'}">
    <c:set var="tableCellCount" value="${tableCellCount + 1}"/>
</c:if>

<c:if test="${displayVotingResults == 'true'}">
    <c:set var="tableCellCount" value="${tableCellCount + 2}"/>
</c:if>

<tr id="${cartPrefix}_title">
    <td colspan="${tableCellCount}">
        <h3 class="noline" style="padding:0">
            <fmt:message key="${titleKey}"/>
        </h3>
    </td>
</tr>

<tr id="${cartPrefix}_header">
    <th><div><fmt:message key="aggregateCart.institution"/></div></th>
    <th><div><fmt:message key="aggregateCart.quantity"/></div></th>
    <th><div><fmt:message key="aggregateCart.criteria"/></div></th>
    <th><div><fmt:message key="aggregateCart.table.note"/></div></th>

    <req:isUserInRole role="priceViewer">
        <c:if test="${showFinalPriceColumn == 'true'}">
            <th><div><fmt:message key="specimenRequest.finalFee"/></div></th>
        </c:if>
    </req:isUserInRole>

    <c:if test="${showOrderFulfillmentForms == 'true'}">
        <th class="action"><div><fmt:message key="column.action"/></div></th>
    </c:if>

    <c:if test="${showReceiptQuality == 'true'}">
        <th><div><fmt:message key="specimenRequest.receiptQuality"/></div></th>
    </c:if>

    <c:if test="${displayVotingResults == 'true'}">
        <th><div><fmt:message key="specimenRequest.review.columnHeader.vote"/></div></th>
        <th><div><fmt:message key="specimenRequest.review.columnHeader.comment"/></div></th>
    </c:if>
</tr>

<c:if test="${empty requestLineItems}">
    <tr class="empty" id="${cartPrefix}_empty">
        <td colspan="${tableCellCount}">
            <fmt:message key="${emptyKey}"/>
        </td>
    </tr>
</c:if>

<c:forEach items="${requestLineItems}" var="lineItem" varStatus="lineItemStatus">
    <c:set var="rowClass" value=""/>
    <c:if test="${lineItemStatus.count % 2 == 1}">
        <c:set var="rowClass" value="odd"/>
    </c:if>

    <tr class="${rowClass}" id="${cartPrefix}_data${lineItemStatus.index}">
        <td><tissuelocator:institutionName institution="${lineItem.institution}"/></td>
        <td><fmt:formatNumber value="${lineItem.quantity}"/></td>
        <td>${lineItem.criteria}</td>
        <td>${lineItem.note}</td>

        <req:isUserInRole role="priceViewer">
            <c:if test="${showFinalPriceColumn == 'true'}">
                <td class="action" style="white-spqce:nowrap">
                    <c:choose>
                        <c:when test="${showOrderFulfillmentForms == 'true'}">
                            &#36;&nbsp;<s:textfield size="5" theme="simple" value="%{#attr.lineItem.finalPrice}"
                                        name="%{#attr.lineItemPrefix}LineItems[%{#attr.lineItemStatus.index}].finalPrice"
                                        title="%{getText('specimenRequest.finalFee.field')}"/>
                            <s:fielderror cssClass="fielderror">
                                <s:param>${lineItemPrefix}LineItems[${lineItemStatus.index}].finalPrice</s:param>
                            </s:fielderror>
                            <c:if test="${not empty lineItem.finalPrice and showFinalPriceColumn == 'true'}">
                                <c:set var="tablePriceSum" value="${tablePriceSum + lineItem.finalPrice}"/>
                            </c:if>
                        </c:when>
                        <c:when test="${not empty lineItem.finalPrice}">
                            <fmt:formatNumber value="${lineItem.finalPrice}" type="currency" />
                            <c:set var="tablePriceSum" value="${tablePriceSum + lineItem.finalPrice}"/>
                        </c:when>
                        <c:otherwise>-</c:otherwise>
                    </c:choose>
                </td>
            </c:if>
        </req:isUserInRole>

        <c:if test="${showOrderFulfillmentForms == 'true'}">
            <td class="action">
                <c:url var="removeUrl" value="/admin/shipment/removeAggregateLineItem.action">
                    <c:param name="aggregateLineItem" value="${lineItem.id}"/>
                    <c:param name="object.id" value="${object.id}"/>
                </c:url>
                <a href="${removeUrl}" class="btn_fw_action_col" id="${cartPrefix}_remove${lineItemStatus.index}">
                    <fmt:message key="shipment.remove.btn"/>
                </a>
            </td>
        </c:if>

        <c:if test="${showReceiptQuality == 'true'}">
            <td>
                <c:set var="receiptQualityString" value="${''}"/>
                <c:if test="${!empty lineItem.received}">
                    <c:set var="receiptQualityString">
                        ${receiptQualityString}<fmt:message key="specimenRequest.received.${lineItem.received}"/>
                    </c:set>
                </c:if>
                <c:if test="${!empty lineItem.receiptQuality}">
                    <c:if test="${!empty receiptQualityString}">
                        <c:set var="receiptQualityString">${receiptQualityString},&nbsp;</c:set>
                    </c:if>
                    <c:set var="receiptQualityString">
                        ${receiptQualityString}${lineItem.receiptQuality.name}
                    </c:set>
                </c:if>
                <c:if test="${!empty lineItem.disposition}">
                    <c:if test="${!empty receiptQualityString}">
                        <c:set var="receiptQualityString">${receiptQualityString},&nbsp;</c:set>
                    </c:if>
                    <c:set var="receiptQualityString">
                        ${receiptQualityString}<fmt:message key="${lineItem.disposition.resourceKey}"/>
                    </c:set>
                </c:if>
                <c:if test="${!empty lineItem.receiptNote}">
                    <c:if test="${!empty receiptQualityString}">
                        <c:set var="receiptQualityString">${receiptQualityString},&nbsp;</c:set>
                    </c:if>
                    <c:set var="receiptQualityString">
                        ${receiptQualityString}${lineItem.receiptNote}
                    </c:set>
                </c:if>
                ${receiptQualityString}
            </td>
        </c:if>

        <c:if test="${displayVotingResults == 'true'}">
            <c:choose>
                <c:when test="${not empty lineItem.vote.vote}">
                    <s:set name="approvedEnum" value="%{@com.fiveamsolutions.tissuelocator.data.Vote@APPROVE}" />
                    <s:set name="deniedEnum" value="%{@com.fiveamsolutions.tissuelocator.data.Vote@DENY}" />
                    <c:set var="decisionClass" value=""/>
                    <s:if test="%{#approvedEnum == #attr.lineItem.vote.vote}">
                        <c:set var="decisionClass" value="approved"/>
                    </s:if>
                    <s:elseif test="%{#deniedEnum == #attr.lineItem.vote.vote}">
                        <c:set var="decisionClass" value="denied"/>
                    </s:elseif>
                    <td class="${decisionClass}" id="${cartPrefix}_votingResults${lineItemStatus.index}">
                        <fmt:message key="specimenRequest.vote.vote.label"/>
                        <fmt:message key="${lineItem.vote.vote.status.resourceKey}"/>
                    </td>
                    <td>
                        ${lineItem.vote.comment}
                    </td>
                </c:when>
                <c:otherwise>
                    <td colspan="2"></td>
                </c:otherwise>
            </c:choose>
        </c:if>
    </tr>
</c:forEach>

<c:if test="${(!empty order) && (!empty requestLineItems) && (showOrderFulfillmentForms == 'true')}">
    <tr class="graybar" id="${cartPrefix}_footer">
        <td colspan="4" class="alignright">
            <req:isUserInRole role="priceViewer">
                <strong><fmt:message key="${totalKey}" /></strong> &nbsp;
                <c:if test="${showOrderFulfillmentForms == 'true'}">
                    <s:submit value="%{getText('btn.saveOrderPricing')}" name="submit"
                        id="btn_save" cssClass="btn" theme="simple" title="%{getText('btn.saveOrderPricing.button')}"/>
                </c:if>
            </req:isUserInRole>
        </td>
        <c:if test="${showFinalPriceColumn == 'true'}">
            <req:isUserInRole role="priceViewer">
                <td class="action" style="white-space: nowrap">
                    <c:choose>
                        <c:when test="${not empty tablePriceSum}">
                            <strong> <fmt:formatNumber value="${tablePriceSum}" type="currency" /> </strong>
                        </c:when>
                        <c:otherwise>-</c:otherwise>
                    </c:choose>
                </td>
            </req:isUserInRole>
        </c:if>
        <c:if test="${showReceiptQuality == 'true'}">
            <td>&nbsp;</td>
        </c:if>
        <c:if test="${showOrderFulfillmentForms == 'true'}">
            <td>&nbsp;</td>
        </c:if>
    </tr>
</c:if>
<c:set var="finalPriceSum" value="${finalPriceSum + tablePriceSum}" scope="request"/>
