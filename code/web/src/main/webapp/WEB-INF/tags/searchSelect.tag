<%@ tag body-content="empty" %>
<%@ attribute name="selectSearchConfig" type="com.fiveamsolutions.tissuelocator.data.config.search.SimpleSearchFieldConfig" required="true" rtexprvalue="true" %>
<%@ attribute name="optionList" type="java.util.Collection" required="true" rtexprvalue="true"%>
<%@ attribute name="name" required="true" rtexprvalue="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<s:if test="%{#attr.selectSearchConfig.multiSelectCollection}">
    <s:set var="selectFieldName" value="%{'multiSelectCollectionParamMap[\\'' + #attr.name + '\\']'}"/>
</s:if>
<s:elseif test="%{#attr.selectSearchConfig.multiSelect}">
    <s:set var="selectFieldName" value="%{'multiSelectParamMap[\\'' + #attr.name + '\\']'}"/>
</s:elseif>
<s:else>
    <s:set var="selectFieldName" value="%{#attr.name}"/>
</s:else>
<span id="searchConfigSelectContainer${selectSearchConfig.id}" style="display: none">
    <s:select name="%{#selectFieldName}" id="searchConfigSelect%{#attr.selectSearchConfig.id}"
        value="value" theme="simple"
        list="%{#attr.optionList}" listKey="value" listValue="label"
        title="%{getText('search.option.select')}" multiple="%{#attr.selectSearchConfig.multiSelect}"/>
</span>
<fmt:message var="selectText" key="search.emptyOption"/>
<s:if test="%{#attr.selectSearchConfig.multiSelect}">
    <script type="text/javascript">
    $(function(){
      $("#searchConfigSelect${selectSearchConfig.id}").multiselect({
            selectedList: 1,
            minWidth: ${selectSearchConfig.minWidth},
            noneSelectedText: "${selectText}",
            classes: '${selectSearchConfig.cssClasses}',
            position: {
                my: 'left top',
                at: 'left bottom'
            }
        });
    });
    <c:if test="${not empty selectSearchConfig.cssClasses}">
    $(document).ready(function(){
        $('.${selectSearchConfig.cssClasses}').each(function(idx,el){
          el.style.width='';
        });
      });
    </c:if>
    </script>
</s:if>
<s:else>
    <script type="text/javascript">
    $(function(){
        $("#searchConfigSelect${selectSearchConfig.id}").multiselect({
            multiple: false,
            header: false,
            selectedList: 1,
            minWidth: ${selectSearchConfig.minWidth},
            noneSelectedText: "${selectText}",
            classes: '${selectSearchConfig.cssClasses}',
            position: {
                my: 'left top',
                at: 'left bottom'
            }
         });
    });
    </script>
</s:else>
<script type="text/javascript"t>
    $(document).ready(function(){
      $('#searchConfigSelectContainer${selectSearchConfig.id}').show();
  });
</script>