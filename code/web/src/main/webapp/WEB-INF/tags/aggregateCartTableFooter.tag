<%@ tag body-content="empty" %>
<%@ attribute name="order" type="com.fiveamsolutions.tissuelocator.data.Shipment" rtexprvalue="true" %>
<%@ attribute name="finalPriceTotal" rtexprvalue="true" %>
<%@ attribute name="showOrderForms" required="true" rtexprvalue="true" %>
<%@ attribute name="showReceiptQuality" rtexprvalue="true" %>
<%@ attribute name="showFinalPriceColumn" required="true" rtexprvalue="true" %>
<%@ attribute name="minPriceTotal" rtexprvalue="true" %>
<%@ attribute name="maxPriceTotal" rtexprvalue="true" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/request-1.0" prefix="req"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator"%>

<c:if test="${empty finalPriceTotal}">
    <c:set var="finalPriceTotal" value="${0.0}"/>
</c:if>

<c:if test="${!empty order && showFinalPriceColumn == 'true'}">
    <req:isUserInRole role="priceViewer">
        <c:if test="${not empty order.shippingPrice}">
            <c:set var="finalPriceTotal" value="${finalPriceTotal + order.shippingPrice}" />
        </c:if>
        <c:if test="${not empty order.fees}">
            <c:set var="finalPriceTotal" value="${finalPriceTotal + order.fees}" />
        </c:if>
        <tr class="graybar" style="background: #efefef" id="aggregateCartFooterRow1">
            <td class="alignleft" colspan="3">
                <strong><fmt:message key="shipment.othercharges" /></strong>
            </td>
            <td class="alignright">
                <strong><fmt:message key="shipment.shippingPrice" /></strong>
            </td>
            <td class="action" style="white-space: nowrap">
                <c:choose>
                    <c:when test="${showOrderForms == 'true'}">
                        &#36;&nbsp;<s:textfield size="5" name="object.shippingPrice" id="price"
                                    value="%{#attr.order.shippingPrice}" theme="simple"
                                    title="%{getText('shipment.shippingPrice.field')}"/>
                        <s:fielderror cssClass="fielderror">
                            <s:param>object.shippingPrice</s:param>
                        </s:fielderror>
                    </c:when>
                    <c:when test="${not empty order.shippingPrice}">
                        <strong> <fmt:formatNumber value="${order.shippingPrice}" type="currency" /></strong>
                    </c:when>
                    <c:otherwise>-</c:otherwise>
                </c:choose>
            </td>
            <c:if test="${showReceiptQuality == 'true'}">
                <td>&nbsp;</td>
            </c:if>
            <c:if test="${showOrderForms == 'true'}">
                <td>&nbsp;</td>
            </c:if>
        </tr>

        <tr class="graybar" style="background: #efefef" id="aggregateCartFooterRow2">
            <td colspan="3"></td>
            <td class="alignright"><strong><fmt:message key="shipment.fees" /></strong></td>
            <td class="action" style="white-space: nowrap">
                <c:choose>
                    <c:when test="${showOrderForms == 'true'}">
                        &#36;&nbsp;<s:textfield size="5" name="object.fees" id="fees"
                                  value="%{#attr.order.fees}" theme="simple"
                                  title="%{getText('shipment.fees.field')}"/>
                        <s:fielderror cssClass="fielderror">
                          <s:param>object.fees</s:param>
                        </s:fielderror>
                    </c:when>
                    <c:when test="${not empty order.fees}">
                        <strong> <fmt:formatNumber value="${order.fees}" type="currency" /> </strong>
                    </c:when>
                    <c:otherwise>-</c:otherwise>
                </c:choose>
            </td>
            <c:if test="${showReceiptQuality == 'true'}">
                <td>&nbsp;</td>
            </c:if>
            <c:if test="${showOrderForms == 'true'}">
                <td>&nbsp;</td>
            </c:if>
        </tr>

        <tr class="graybar" id="aggregateCartFooterRow3">
            <td class="alignright" colspan="4">
                <req:isUserInRole role="priceViewer">
                    <strong><fmt:message key="shipment.orderTotal" /></strong>
                    <c:if test="${showOrderForms == 'true'}">
                        &nbsp; <s:submit value="%{getText('btn.saveOrderPricing')}" name="submit"
                          id="btn_save" cssClass="btn" theme="simple" title="%{getText('btn.saveOrderPricing.button')}"/>
                    </c:if>
                </req:isUserInRole>
            </td>
            <td class="action" style="white-space: nowrap">
                <c:choose>
                    <c:when test="${not empty finalPriceTotal}">
                        <strong> <fmt:formatNumber value="${finalPriceTotal}" type="currency" /> </strong>
                    </c:when>
                    <c:otherwise>-</c:otherwise>
                </c:choose>
            </td>
            <c:if test="${showReceiptQuality == 'true'}">
                <td>&nbsp;</td>
            </c:if>
            <c:if test="${showOrderForms == 'true'}">
                <td>&nbsp;</td>
            </c:if>
        </tr>
    </req:isUserInRole>
</c:if>
