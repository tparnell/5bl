<%@ tag body-content="scriptless"%>
<%@ attribute name="specimen" required="true" rtexprvalue="true" type="com.fiveamsolutions.tissuelocator.data.Specimen"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/request-1.0" prefix="req"%>

<%--
  1.if user has pre-approval role
  2.if user has post-approval role and the specimen request has been approved
--%>
<c:set var="displaySpecimenSource" value="${false}" />
<req:isUserInRole role="preApprovalSpecimenLocViewer">
    <c:set var="displaySpecimenSource" value="${true}" />
</req:isUserInRole>

<req:isUserInRole role="postApprovalSpecimenLocViewer">
    <c:forEach items="${specimen.lineItems}" var="lineItem">
        <c:forEach items="${lineItem.requests}" var="request">
            <s:if test="%{#attr.request.status eq @com.fiveamsolutions.tissuelocator.data.RequestStatus@APPROVED}">
                <c:set var="displaySpecimenSource" value="${true}" />
            </s:if>
        </c:forEach>
    </c:forEach>
</req:isUserInRole>

<c:if test="${displaySpecimenSource}">
    <jsp:doBody />
</c:if>