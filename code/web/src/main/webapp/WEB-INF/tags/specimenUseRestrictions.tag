<%@ tag body-content="empty" %>
<%@ attribute name="institution" required="true" type="com.fiveamsolutions.tissuelocator.data.Institution" rtexprvalue="true" %>
<%@ attribute name="emptyResourceKey" required="true" rtexprvalue="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:choose>
    <c:when test="${empty institution.specimenUseRestrictions}">
        <p><fmt:message key="${emptyResourceKey}"/></p>
    </c:when>
    <c:otherwise>
        <c:forEach items="${institution.specimenUseRestrictionParts}" var="stringPart">
            <c:if test="${!empty fn:trim(stringPart)}">
                <p>${fn:trim(stringPart)}</p>
            </c:if>
        </c:forEach>
    </c:otherwise>
</c:choose>
