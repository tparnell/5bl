<%@ tag body-content="empty" %>
<%@ attribute name="filterActionBase" required="true" %>
<%@ attribute name="viewAction" required="true" %>
<%@ attribute name="exportEnabled" required="true" %>
<%@ attribute name="dualPersistencePossible" required="true" %>
<%@ attribute name="includeUnsubmittedRequests" required="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/request-1.0" prefix="req" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator" %>

<tissuelocator:messages/>
<c:choose>
    <c:when test="${dualPersistencePossible}">
        <tissuelocator:determinePersistenceType urlPrefix="${filterActionBase}${'/filter'}" urlEnding=".action"/>
        <c:set var="filterFormUrl" value="${outputUrl}"></c:set>
        <c:set var="displayTableUrl" value="${filterActionBase}/listSession.action"></c:set>
        <c:set var="specimenIdUrl" value="${filterActionBase}/${viewAction}Db.action"></c:set>
    </c:when>
    <c:otherwise>
        <c:set var="filterFormUrl" value="${filterActionBase}/filter.action"></c:set>
        <c:set var="displayTableUrl" value="${filterActionBase}/list.action"></c:set>
        <c:set var="specimenIdUrl" value="${filterActionBase}/${viewAction}.action"></c:set>
    </c:otherwise>
</c:choose>

<s:form id="filterForm" action="%{#attr.filterFormUrl}">
    <!--Filters-->
    <div class="roundbox_gray_wrapper">
        <div class="roundbox_gray">
            <div class="float_left">
              <s:checkbox theme="simple" id="actionNeededOnly" name="actionNeededOnly" onclick="submit()"
                      title="%{getText(specimenRequest.review.list.actionOnly)}"/>
              <strong><fmt:message key="specimenRequest.review.list.actionOnly"/></strong>
            </div>

            <div class="float_right">
                <strong><fmt:message key="specimenRequest.review.list.filter"/></strong>
                <s:if test="%{object.requestor != null}">
                    <s:hidden name="object.requestor" value="%{object.requestor.id}"/>
                </s:if>
                <c:choose>
                    <c:when test="${includeUnsubmittedRequests}">
                        <s:set var="availableStatuses"
                            value="%{@com.fiveamsolutions.tissuelocator.data.RequestStatus@values()}"/>
                    </c:when>
                    <c:otherwise>
                        <s:set var="availableStatuses"
                            value="%{@com.fiveamsolutions.tissuelocator.data.RequestStatus@getSubmittedValues()}"/>
                    </c:otherwise>
                </c:choose>
                <s:select name="object.status" id="status"
                        onchange="submit()" theme="simple"
                        list="%{#availableStatuses}" listValue="%{getText(resourceKey)}"
                        headerKey="" headerValue="%{getText('requeststatus.all')}"
                        title="%{getText('requeststatus.status.select')}"/>
            </div>

            <div class="clear"></div>
        </div>
    </div>

    <s:set value="%{pageSize}" name="tablePageSize" scope="page"/>
    <s:set value="%{@com.fiveamsolutions.tissuelocator.data.RequestStatus@PENDING_REVISION}"
        name="pendingRevision" scope="page"/>
    <s:set value="%{@com.fiveamsolutions.tissuelocator.data.RequestStatus@DRAFT}"
        name="draft" scope="page"/>
    <fmt:message key="default.datetime.format" var="datetimeFormat"/>
    <display:table name="objects" requestURI="${displayTableUrl}"
            uid="specimenRequest" pagesize="${tablePageSize}" sort="external" export="${exportEnabled}">
            <tissuelocator:displaytagProperties/>
            <display:setProperty name="export.csv.filename" value="specimenrequests.csv"/>

        <display:column titleKey="specimenRequest.id" sortProperty="ID" sortable="true" media="html">
            <c:choose>
                <c:when test="${specimenRequest.status == draft}">
                    ${specimenRequest.id}
                </c:when>
                <c:otherwise>
                    <c:url var="viewUrl" value="${specimenIdUrl}">
                        <c:param name="object.id" value="${specimenRequest.id}"/>
                        <c:param name="forceReload" value="true"></c:param>
                    </c:url>
                    <a href="${viewUrl}">${specimenRequest.id}</a>
                </c:otherwise>
            </c:choose>
        </display:column>
        <display:column titleKey="specimenRequest.id" sortProperty="ID" sortable="true" media="csv">
            ${specimenRequest.id}
        </display:column>
        <display:column titleKey="specimenRequest.requestor.name" sortable="true" sortProperty="REQUESTOR_FIRSTNAME,REQUESTOR_LASTNAME">
            <tissuelocator:truncateText cssMaxWidth="200px" isTableCell="${true}" value="${specimenRequest.requestor.firstName} ${specimenRequest.requestor.lastName}"
                                maxWordLength="20"/>
        </display:column>
        <display:column titleKey="specimenRequest.requestor.institution.name" property="requestor.institution.name" sortable="true" sortProperty="REQUESTOR_INSTITUTION"/>
        <display:column titleKey="specimenRequest.requestedDate" sortable="true" sortProperty="REQUESTED_DATE">
            <fmt:formatDate pattern="${datetimeFormat}" value="${specimenRequest.requestedDate}"/>
        </display:column>
        <display:column titleKey="specimenRequest.updatedDate" sortable="true" sortProperty="UPDATED_DATE">
            <fmt:formatDate pattern="${datetimeFormat}" value="${specimenRequest.updatedDate}"/>
        </display:column>
        <display:column titleKey="specimenRequest.status" sortable="true" sortProperty="STATUS">
            <fmt:message key="${specimenRequest.status.resourceKey}" />
        </display:column>
        <display:column titleKey="specimenRequest.review.list.actionRequired">
            <c:set var="adminActionRequired" value="${false}"/>
            <req:isUserInRole role="scientificReviewerAssigner">
                <s:if test="%{reviewProcess.userActionAnalyzer.needsReviewerAssignment(#attr.TissueLocatorUser, #attr.specimenRequest)}">
                    <fmt:message key="specimenRequest.review.list.assignReviewer" /><br />
                    <c:set var="adminActionRequired" value="${true}"/>
                </s:if>
            </req:isUserInRole>
            <req:isUserInRole role="leadReviewer">
                <s:if test="%{reviewProcess.userActionAnalyzer.needsLeadReview(#attr.TissueLocatorUser, #attr.specimenRequest)}">
                    <fmt:message key="specimenRequest.review.list.leadReview" /><br />
                    <c:set var="adminActionRequired" value="${true}"/>
                </s:if>
            </req:isUserInRole>
            <req:isUserInRole role="consortiumReviewVoter">
                <s:if test="%{reviewProcess.userActionAnalyzer.needsConsortiumReview(#attr.TissueLocatorUser, #attr.specimenRequest)}">
                    <fmt:message key="specimenRequest.review.list.consortiumReview" /><br />
                    <c:set var="adminActionRequired" value="${true}"/>
                </s:if>
            </req:isUserInRole>
            <req:isUserInRole role="instiutionalReviewVoter">
                <s:if test="%{reviewProcess.userActionAnalyzer.needsInstitutionalReview(#attr.TissueLocatorUser, #attr.specimenRequest)}">
                    <fmt:message key="specimenRequest.review.list.institutionalReview" /><br />
                    <c:set var="adminActionRequired" value="${true}"/>
                </s:if>
            </req:isUserInRole>
            <req:isUserInRole role="lineItemReviewVoter">
                <s:if test="%{reviewProcess.userActionAnalyzer.needsLineItemReview(#attr.TissueLocatorUser, #attr.specimenRequest)}">
                    <fmt:message key="specimenRequest.review.list.lineItemReview" /><br />
                    <c:set var="adminActionRequired" value="${true}"/>
                </s:if>
            </req:isUserInRole>
            <req:isUserInRole role="leadReviewer">
                <s:if test="%{reviewProcess.userActionAnalyzer.needsFinalComment(#attr.TissueLocatorUser, #attr.specimenRequest)}">
                    <fmt:message key="specimenRequest.review.list.finalComment" /><br />
                    <c:set var="adminActionRequired" value="${true}"/>
                </s:if>
            </req:isUserInRole>
            <req:isUserInRole role="reviewDecisionNotifier">
                <s:if test="%{reviewProcess.userActionAnalyzer.needsResearcherNotification(#attr.TissueLocatorUser, #attr.specimenRequest)}">
                    <fmt:message key="specimenRequest.review.list.notifyResearcher" /><br />
                    <c:set var="adminActionRequired" value="${true}"/>
                </s:if>
            </req:isUserInRole>
            <s:set value="%{@com.fiveamsolutions.tissuelocator.data.RequestStatus@PENDING_REVISION}"
                name="pendingRevision" scope="page" />
            <c:if test="${specimenRequest.status == pendingRevision}">
                <fmt:message key="specimenRequest.review.list.revision" /><br />
            </c:if>
            <c:set var="showRecordQuality" value="${false}" />
            <c:forEach items="${specimenRequest.orders}" var="order">
                <s:if test="%{#attr.order.needsQualityUpdate(#attr.TissueLocatorUser)}">
                    <c:set var="showRecordQuality" value="${true}" />
                </s:if>
            </c:forEach>
            <c:if test="${showRecordQuality}">
                <fmt:message key="specimenRequest.review.list.recordQuality" /><br />
            </c:if>
        </display:column>
        <display:column titleKey="column.action" media="html" headerClass="action" class="action">
            <c:if test="${specimenRequest.requestor.id == TissueLocatorUser.id}">
                <c:if test="${specimenRequest.status == pendingRevision || specimenRequest.status == draft}">
                    <c:url var="editRequestUrl" value="/protected/request/updateCartDb.action">
                        <c:param name="object.id" value="${specimenRequest.id}"/>
                    </c:url>
                    <a href="${editRequestUrl}" class="btn_fw_action_col"><fmt:message key="btn.edit" /></a>
                </c:if>
            </c:if>
            <c:if test="${specimenRequest.requestor.id == TissueLocatorUser.id}">
                <c:if test="${specimenRequest.status == draft}">
                    <tissuelocator:determinePersistenceType urlPrefix="/protected/request/delete" urlEnding=".action"/>
                    <c:url var="deleteRequestUrl" value="${outputUrl}">
                        <c:param name="object.id" value="${specimenRequest.id}"/>
                    </c:url>
                    <fmt:message key="specimenRequest.delete.confirm" var="confirmDelete"/>
                    <a href="#" class="btn_fw_action_col" onclick="confirmAndContinue('${confirmDelete}','${deleteRequestUrl}'); return false;"><fmt:message key="btn.delete" /></a>
                </c:if>
            </c:if>
            <c:set var="needsRecordQuality" value="${false}"/>
            <c:forEach items="${specimenRequest.orders}" var="order">
                <s:if test="%{#attr.order.needsQualityUpdate(#attr.TissueLocatorUser)}">
                    <c:set var="needsRecordQuality" value="${true}"/>
                </s:if>
            </c:forEach>
            <c:if test="${needsRecordQuality}">
                <c:url var="recordQualityUrl" value="/protected/request/viewDb.action">
                    <c:param name="object.id" value="${specimenRequest.id}" />
                </c:url>
                <a href="${recordQualityUrl}" class="btn_fw_action_col"><fmt:message key="btn.recordQuality"/></a>
            </c:if>
            <req:isUserInRole role="tissueRequestAdmin">
                <c:if test="${specimenRequest.status != draft && specimenRequest.requestor.id != TissueLocatorUser.id}">
                    <c:url var="editUrl" value="/admin/request/review/input.action">
                        <c:param name="object.id" value="${specimenRequest.id}"/>
                    </c:url>
                    <a href="${editUrl}" class="btn_fw_action_col">
                        <c:choose>
                            <c:when test="${adminActionRequired}">
                                <fmt:message key="btn.review" />
                            </c:when>
                            <c:otherwise>
                                <fmt:message key="btn.view" />
                            </c:otherwise>
                        </c:choose>
                    </a>
                </c:if>
            </req:isUserInRole>
        </display:column>
    </display:table>
</s:form>
