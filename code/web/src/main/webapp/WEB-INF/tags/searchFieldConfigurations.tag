<%@ tag body-content="empty" %>
<%@ attribute name="searchFieldConfigs" required="true" type="java.util.Collection" %>
<%@ attribute name="rowIndex" required="true" type="java.lang.Integer" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/request-1.0" prefix="req" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator" %>

<s:set var="bean" scope="request"/>
<s:set var="configSearchSetType" value="%{searchSetType}"/>
<%-- set base URL for the help action --%>
<s:set var="helpBaseUrl" value="%{'/protected/specimen/info/popup/fieldHelp.action'}"/>
<s:subset source="%{#attr.searchFieldConfigs}" decider="%{searchFieldConfigurationValidationDecider}">
    <s:iterator var="config" status="rowstatus">
    <s:set var="currentIndex" value="%{#rowstatus.index + #attr.rowIndex}" />
    <s:set var="actionName" value="%{'get' + #config.class.simpleName + 'Widget'}"/>
    <tr <s:if test="#currentIndex % 2 != 0">class="odd"</s:if>>
        <s:action namespace="/widget" name="search/%{#actionName}" executeResult="true" ignoreContextParams="true" >
            <s:param name="searchFieldConfig.id" value="%{#config.id}"/>
            <s:param name="searchSetType" value="%{configSearchSetType}"/>
            <s:param name="helpBaseUrl" value="%{helpBaseUrl}"/>
        </s:action>
    </tr>
    </s:iterator>
</s:subset>