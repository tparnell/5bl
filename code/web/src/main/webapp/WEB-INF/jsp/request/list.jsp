<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<html>
<head>
    <title><fmt:message key="specimenRequest.myrequests.title" /></title>
</head>
<body>
    <tissuelocator:requestListTabs isAdmin="false" selectedTab="specimen"
        subtitleKey="specimenRequest.myrequests.subtitle"/>
    <tissuelocator:specimenRequestList filterActionBase="/protected/request" viewAction="view"
        exportEnabled="false" dualPersistencePossible="true"
        includeUnsubmittedRequests="true"/>
</body>
</html>
