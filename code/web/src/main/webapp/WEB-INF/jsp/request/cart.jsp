<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<c:url var="imagesBase" value="/images"/>
<c:url value="/notYetImplemented.jsp" var="notYetImplementedUrl"/>
<html>
    <head>
        <title>
            <fmt:message key="cart.title"/>
        </title>
    </head>
    <body>

        <tissuelocator:determinePersistenceType urlPrefix="/protected/request/updateCart" urlEnding=".action"/>
        <s:form theme="simple" action="%{#attr.request.outputUrl}">

        <p><fmt:message key="cart.message"/></p>
        <c:if test="${!empty specimenRequestError}">
            <div class="toperror">
                <p><fmt:message key="${specimenRequestError}"/></p>
            </div>
        </c:if>
        <tissuelocator:messages breakCount="1" escape="false"/>

        <tissuelocator:externalCommentDisplay specimenRequest="${object}"/>

        <!--Data Table-->
        <c:set var="minPriceTotal" value="${0.0}"/>
        <c:set var="maxPriceTotal" value="${0.0}"/>

        <display:table name="object.lineItems" uid="lineItem" defaultsort="1">
            <tissuelocator:displaytagProperties/>
            <display:setProperty name="basic.msg.empty_list_row">
                <tr class="empty"><td colspan="{0}"><fmt:message key="specimenRequest.noSpecimensSelected"/></td></tr>
            </display:setProperty>

            <fmt:message key="cart.specimen" var="specimenTitle"/>
            <display:column title="<div>${specimenTitle}</div>" class="carttitle" sortProperty="specimen.id">
                <c:url var="viewUrl" value="/protected/specimen/view.action">
                    <c:param name="object.id" value="${lineItem.specimen.id}"/>
                </c:url>
                <a href="${viewUrl}">${lineItem.specimen.id} <req:isUserInRole role="externalIdViewer">(External Id: ${lineItem.specimen.externalId})</req:isUserInRole></a>
                <tissuelocator:codeDisplay code="${lineItem.specimen.pathologicalCharacteristic}"/>
            </display:column>

            <fmt:message key="cart.available" var="availableTitle"/>
            <display:column title="<div>${availableTitle}</div>">
                <c:if test="${!empty lineItem.specimen.availableQuantity}">
                    <fmt:formatNumber value="${lineItem.specimen.availableQuantity}" maxFractionDigits="2"/>
                    <fmt:message key="${lineItem.specimen.availableQuantityUnits.resourceKey}"/>
                </c:if>
            </display:column>

            <fmt:message key="cart.requested" var="requestedTtile"/>
            <display:column title="<div>${requestedTtile}</div>">
                <s:textfield theme="simple" size="8" name="lineItems[%{#attr.lineItem_rowNum - 1}].quantity" value="%{#attr.lineItem.quantity}"
                       title="%{getText('cart.requested.field')}"/>
                <c:choose>
                    <c:when test="${empty lineItem.quantityUnits}">
                        <s:select name="lineItems[%{#attr.lineItem_rowNum - 1}].quantityUnits"
                            value="%{#attr.lineItem.quantityUnits}" theme="simple"
                            list="%{@com.fiveamsolutions.tissuelocator.data.QuantityUnits@values()}"
                            listValue="%{getText(resourceKey)}" title="%{getText('cart.quantityUnits.select')}"/>
                    </c:when>
                    <c:otherwise>
                        <fmt:message key="${lineItem.quantityUnits.resourceKey}" />
                    </c:otherwise>
                </c:choose>
                <s:fielderror cssClass="fielderror">
                    <s:param>lineItems[${lineItem_rowNum - 1}].quantity</s:param>
                    <s:param>lineItems[${lineItem_rowNum - 1}].validQuantity</s:param>
                </s:fielderror>
            </display:column>

            <req:isUserInRole role="priceViewer">
                <fmt:message key="cart.fee" var="feeTitle"/>
                <display:column title="<div>${feeTitle}</div>">
                    <tissuelocator:specimenPrice priceNegotiable="${lineItem.specimen.priceNegotiable}"
                        minimumPrice="${lineItem.specimen.minimumPrice}"
                        maximumPrice="${lineItem.specimen.maximumPrice}"/>
                    <c:if test="${!lineItem.specimen.priceNegotiable}">
                        <c:set var="minPriceTotal" value="${minPriceTotal + lineItem.specimen.minimumPrice}"/>
                        <c:set var="maxPriceTotal" value="${maxPriceTotal + lineItem.specimen.maximumPrice}"/>
                    </c:if>
                </display:column>
            </req:isUserInRole>
            <fmt:message key="cart.notes" var="notesTitle"/>
            <display:column title="<div>${notesTitle}</div>">
                <s:textfield theme="simple" name="lineItems[%{#attr.lineItem_rowNum - 1}].note" value="%{#attr.lineItem.note}"
                       title="%{getText('cart.notes.field')}"/>
                <s:fielderror cssClass="fielderror">
                    <s:param>lineItems[${lineItem_rowNum - 1}].note</s:param>
                </s:fielderror>
            </display:column>

            <fmt:message key="cart.remove" var="removeTitle"/>
            <display:column title="<div>${removeTitle}</div>" class="action" headerClass="action">
                <tissuelocator:determinePersistenceType urlPrefix="/protected/request/removeFromCart" urlEnding=".action"/>
                <c:url value="${outputUrl}" var="removeUrl">
                    <c:param name="selection" value="${lineItem.specimen.id}"/>
                </c:url>
                <a href="${removeUrl}" class="btn_fw_action_col">
                    <fmt:message key="cart.remove"/>
                </a>
            </display:column>

            <display:footer>
                <tr class="graybar">
                    <td colspan="2" class="alignright"></td>
                    <td class="alignright">
                        <req:isUserInRole role="priceViewer">
                            <strong>
                                <fmt:message key="cart.subtotal"/>
                            </strong>
                        </req:isUserInRole>
                    </td>
                    <td>
                        <req:isUserInRole role="priceViewer">
                            <strong>
                                <tissuelocator:specimenPrice
                                    minimumPrice="${minPriceTotal}" maximumPrice="${maxPriceTotal}"/>
                            </strong>
                        </req:isUserInRole>
                    </td>
                    <td colspan="2"></td>
                </tr>
            </display:footer>
        </display:table>

        <div class="clear"><br /></div>

        <s:if test="%{displayProspectiveCollection}">

            <tissuelocator:prospectiveCollectionCart/>

        </s:if>

        <div class="btn_bar">
            <s:submit value="%{getText('cart.request')}" name="submit"
                    id="btn_request" cssClass="btn" theme="simple" title="%{getText('cart.request.button')}"/>
            <s:submit value="%{getText('cart.update')}" name="submit"
                    id="btn_update" cssClass="btn" theme="simple" title="%{getText('cart.update.button')}"/>
            <s:submit value="%{getText('specimenRequest.edit.saveDraft')}" name="submit"
                    id="btn_saveDraft" cssClass="btn" theme="simple" title="%{getText('specimenRequest.edit.saveDraft.button')}"/>
            <tissuelocator:determinePersistenceType urlPrefix="/protected/request/emptyCart" urlEnding=".action" asUrl="true"/>
            <c:url value="/protected/specimen/search/refreshList.action" var="specimenSearchUrl"/>
            <a href="${specimenSearchUrl}" class="btn">
                <fmt:message key="cart.searchMore"/>
            </a>
            <a href="${outputUrl}" class="btn">
                <fmt:message key="cart.empty"/>
            </a>
            <s:set value="%{@com.fiveamsolutions.tissuelocator.data.RequestStatus@DRAFT}"
                name="draft" scope="page"/>
            <c:if test="${object.status == draft}">
                <tissuelocator:determinePersistenceType urlPrefix="/protected/request/delete" urlEnding=".action"/>
                <c:url var="deleteRequestUrl" value="${outputUrl}">
                    <c:param name="object.id" value="${object.id}"/>
                </c:url>
                <fmt:message key="specimenRequest.delete.confirm" var="confirmDelete"/>
                <a href="#" class="btn" onclick="confirmAndContinue('${confirmDelete}','${deleteRequestUrl}'); return false;"><fmt:message key="btn.delete.draft" /></a>
            </c:if>
        </div>
        </s:form>
    </body>
</html>
