<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:url value="/static/overview.jsp" var="overviewUrl"/>
<c:url value="/register/input.action" var="registerUrl"/>
<c:url var="imagesBase" value="/images"/>
<c:url value="/static/howToUse.jsp" var="howToUrl"/>

<div id="search_home">
    <div id="tabbedWelcome_infotabs" class="tabbed">

        <ul id="infotabs" class="infotabs">
            <li class="tab">
                <a href="#welcome" id="tab_welcome" class="active">
                    <fmt:message key="login.tab.welcome"/>
                </a>
            </li>
            <li class="tab">
                <a href="#about_nbstrn" id="tab_about_nbstrn" class="">
                    <fmt:message key="login.tab.about"/>
                </a>
            </li>
            <li class="tab">
                <a href="#how_to_use_site" id="tab_how_to_use_site" class="">
                    <fmt:message key="login.tab.how_to_use_site"/>
                </a>
            </li>
        </ul>

        <div class="clear"></div>

        <div id="tabbox">

            <!--Welcome tab-->
            <div id="welcome" style="">
                <p>
                    <fmt:message key="login.tab.welcome.text"/>
                </p>
                <p>
                    <a href="${overviewUrl}">
                    <fmt:message key="login.link.about"/>
                    </a>
                    &nbsp; | &nbsp;
                    <a href="${registerUrl}">
                    <fmt:message key="login.link.register"/>
                    </a>
                </p>
            </div>
            <!--/Welcome tab-->

            <!--About the NBSTRN tab-->
            <div id="about_nbstrn">
                <p>
                    <fmt:message key="login.tab.about.text"/>
                </p>
                <p>
                    <fmt:message key="login.tab.about.foot"/>
                </p>
            </div>
            <!--/About the NBSTRN tab-->

            <!--how to use this site tab-->
            <div id="how_to_use_site" style="padding: 0; text-align:center;">
	            <a href="${howToUrl}">
	                <img src="${imagesBase}/home-how-to-use-this-site.png" alt="${login.tab.how_to_use_site.long}" id="how_to_use_site_img"/>
	            </a>
            </div>
            <!--/how to use this site tab-->

        </div>

    </div>
</div>
<!--Tabs-->
<script type="text/javascript">
    $(document).ready(function () {
        $('#tabbedWelcome_infotabs').tabs();
        $('#tabbedWelcome_infotabs').removeClass('ui-widget ui-widget-content');
        $('#infotabs').removeClass('ui-tabs-nav ui-widget-header');
    });
</script>
<!--/Tabs-->

