<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<div id="type">
   <h2><fmt:message key="home.browse.state"/></h2>
    <div id="us_map">
        <s:form id="filterForm" action="specimen/search/filter.action" namespace="/protected">
            <s:select id="institution" name="object.externalIdAssigner.id" onchange="submit()" theme="simple"
                headerKey="" headerValue="%{getText('home.browse.state.emptyOption')}"
                list="%{institutions.entrySet()}"
                listKey="%{getValue()}"
                listValue="%{getKey()}"
                title="%{getText('home.browse.institution.select')}"/>
        </s:form>
    </div>

    <c:url value="/static/faq.jsp#faq_states" var="faq_states"/>
    <p style="margin-left:10px;">
        <a href="${faq_states}">&raquo; <fmt:message key="home.browse.state.notListed"/></a><br />
    </p>

</div>