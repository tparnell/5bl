<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>

<s:set var="propName" value="searchFieldConfig.autocompleteConfig.searchFieldName"/>
<s:set var="isObjectProperty" value="%{searchFieldConfig.autocompleteConfig.objectProperty}"/>
<s:if test="%{isObjectProperty}">
 <s:set var="propName" value="%{'object.' + #propName}"/>
</s:if>
<s:set var="autoLabel">${searchFieldConfig.autocompleteConfig.searchFieldDisplayName}</s:set>
<s:set var="autodisabled" value="%{getValue(searchFieldConfig.checkboxConfig.searchFieldName,#attr.request)}"/>
<s:set var="checkboxKeyProperty" value="%{searchFieldConfig.checkboxConfig.searchFieldName + 'Id'}"/>
<s:set var="checkboxKeyValue" value="%{getValue(#checkboxKeyProperty,#attr.request)}"/>
<s:set var="autoKeyProperty" value="%{#attr.propName + '.id'}"/>
<s:set var="autoKeyValue" value="%{getValue(#autoKeyProperty,#attr.request).toString()}"/>
<s:set var="autoValue" value="%{getValue(searchFieldConfig.autocompleteConfig.valueProperty,#attr.request)}"/>
<s:set var="helpBaseUrl" value="searchFieldConfig.helpBaseUrl"/>
<script type="text/javascript">
    $(document).ready(function () {
        $('#<s:property value="searchFieldConfig.checkboxConfig.searchFieldName"/>').click(function() {
            typeAutocompleteAndDisable('<s:property value="searchFieldConfig.autocompleteConfig.searchFieldName"/>',
                    '<s:property value="searchFieldConfig.checkboxConfig.searchFieldName"/>',
                    '<s:property value="searchFieldConfig.checkboxTextValue"/>',
                    '<s:property value="%{#checkboxKeyValue}"/>');
        });
        disableAutocompleteIfChecked('<s:property value="searchFieldConfig.autocompleteConfig.searchFieldName"/>',
                '<s:property value="searchFieldConfig.checkboxConfig.searchFieldName"/>',
                '<s:property value="searchFieldConfig.checkboxTextValue"/>');
    });
    $('form').submit(function () {
        checkAutocompleteAndClear('<s:property value="searchFieldConfig.autocompleteConfig.searchFieldName"/>');
    });
</script>
<%-- If a value carries over from a previous search, it must be manually pushed to the autocomplete widget --%>
<c:if test="${not empty autoValue}">
    <script type="text/javascript">
        $(document).ready(function () {
            pushAutocompleteValue('<s:property value="searchFieldConfig.autocompleteConfig.searchFieldName"/>',
                '<s:property value="%{#attr.autoValue}"/>', '<s:property value="%{#attr.autoKeyValue}"/>');
        });
    </script>
</c:if>


<td scope="col" class="label">
    <tissuelocator:helpLabel helpUrlBase="${helpBaseUrl}"
                             labelFor="${propName}"
                             fieldDisplayName="${autoLabel}"
                             fieldName="${propName}"
                             required="false"/>
</td>
<td scope="col" class="value">
    <div id="search${searchFieldConfig.autocompleteConfig.searchFieldName}" > <!-- For selenium tests to find this control -->
        <s:checkbox theme="simple" id="%{searchFieldConfig.checkboxConfig.searchFieldName}" name="%{searchFieldConfig.checkboxConfig.searchFieldName}"
        cssClass="search_checkbox_autocomplete_composite" />
        <label class="inline" for="${searchFieldConfig.checkboxConfig.searchFieldName}">
            <strong>${searchFieldConfig.checkboxConfig.note}</strong>
        </label>
        <br/>

        <s:set var="autoList" value="%{searchFieldConfig.autocompleteConfig.listUrl}" scope="page"/>
        <s:set var="fieldName" value="%{#propName}" scope="page"/>
        <s:set var="searchFieldName" value="%{searchFieldConfig.autocompleteConfig.valueProperty}" scope="page"/>
        <s:set var="searchFieldId" value="%{searchFieldConfig.autocompleteConfig.searchFieldName}" scope="page"/>
        <tissuelocator:autocompleter fieldName="${fieldName}" fieldId="${searchFieldId}"
            searchFieldName="${searchFieldName}" autocompleteUrl="${autoList}"
            cssClass="${searchFieldConfig.autocompleteConfig.cssClasses}"/>
        <c:if test="${not empty searchFieldConfig.autocompleteConfig.note}">
            <div class="note">${searchFieldConfig.autocompleteConfig.note}</div>
        </c:if>
        <s:if test="%{searchFieldConfig.errorProperty != null}">
            <s:fielderror cssClass="fielderror">
                <s:param value="%{searchFieldConfig.errorProperty}"/>
            </s:fielderror>
        </s:if>
    </div>
</td>
