<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<html>
<head>
    <title><fmt:message key="participant.title" /></title>
</head>
<body>
    <div class="topbtns">
        <a href="<c:url value="/admin/participant/input.action"/>" class="btn">
            <fmt:message key="participant.add"/>
        </a>
    </div>

    <tissuelocator:messages/>

    <s:form id="filterForm" action="/admin/participant/filter.action">
        <!--Filters-->
        <div class="roundbox_gray_wrapper">
            <div class="roundbox_gray">
                <div class="float_left">
                </div>

                <div class="float_right">
                	<label class="inline" for="externalId">
                    	<strong><fmt:message key="participant.list.filter"/></strong>
                    </label>
                    <s:textfield name="object.externalId" label="%{getText('participant.externalId')}" theme="simple" id="externalId" />
                    <s:submit value="%{getText('btn.search')}" name="btn_search" cssClass="btn" theme="simple" title="%{getText('btn.search.button')}"/>
                </div>

                <div class="clear"></div>
            </div>
        </div>

        <s:set value="%{pageSize}" name="tablePageSize" scope="page"/>
        <fmt:message key="default.datetime.format" var="datetimeFormat"/>
        <display:table name="objects" requestURI="/admin/participant/list.action"
                uid="participant" pagesize="${tablePageSize}" sort="external" export="true">
                <tissuelocator:displaytagProperties/>
                <display:setProperty name="export.csv.filename" value="participants.csv"/>

            <display:column titleKey="participant.externalId" sortProperty="EXTERNAL_ID" sortable="true" media="html">
                <c:choose>
                    <c:when test="${TissueLocatorUser.crossInstitution || TissueLocatorUser.institution.id == participant.externalIdAssigner.id}">
                        <c:url var="editUrl" value="/admin/participant/input.action">
                            <c:param name="object.id" value="${participant.id}"/>
                        </c:url>
                        <a href="${editUrl}">${participant.externalId}</a>
                    </c:when>
                    <c:otherwise>
                        ${participant.externalId}
                    </c:otherwise>
                </c:choose>
            </display:column>
            <display:column titleKey="participant.externalId" sortProperty="EXTERNAL_ID" sortable="true" media="csv">
                ${participant.externalId}
            </display:column>
            <display:column titleKey="participant.externalIdAssigner.name" property="externalIdAssigner.name"
                sortProperty="EXTERNAL_ID_ASSIGNER" sortable="true"/>
            <display:column titleKey="participant.gender" sortProperty="GENDER" sortable="true">
                <fmt:message key="${participant.gender.resourceKey}" />
            </display:column>
            <display:column titleKey="participant.ethnicity" sortProperty="ETHNICITY" sortable="true">
                <c:if test="${!empty participant.ethnicity}">
                    <fmt:message key="${participant.ethnicity.resourceKey}" />
                </c:if>
            </display:column>
            <display:column titleKey="participant.races">
                <c:forEach items="${participant.races}" var="race" varStatus="loopStatus">
                    <c:if test="${!loopStatus.first}">, </c:if><fmt:message key="${race.resourceKey}" />
                </c:forEach>
            </display:column>
            <display:column titleKey="column.action" media="html" headerClass="action" class="action">
                <c:if test="${TissueLocatorUser.crossInstitution || TissueLocatorUser.institution.id == participant.externalIdAssigner.id}">
                    <c:url var="editUrl" value="/admin/participant/input.action">
                        <c:param name="object.id" value="${participant.id}"/>
                    </c:url>
                    <a href="${editUrl}" class="btn_fw_action_col"><fmt:message key="btn.edit" /></a>
                </c:if>
            </display:column>
        </display:table>
    </s:form>
</body>
</html>
