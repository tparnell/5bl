<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<c:url value="/admin/participant/list.action" var="listUrl"/>
<c:url var="refreshUrl" value="/admin/participant/input.action">
    <c:param name="object.id" value="${object.id}"/>
    <c:param name="specimenReturned" value="true"/>
</c:url>
<html>
<head>
    <title><fmt:message key="participant.title" /></title>
    <script type="text/javascript">
        function returnRefresh(returnVal) {
            window.location = '${refreshUrl}';
         }
    </script>
</head>
<body onload="setFocusToFirstControl();">
<div class="topbtns">
    <a href="${listUrl}" class="btn">
        <fmt:message key="btn.backToList"/>
    </a>
</div>

<s:form id="participantForm" action="/admin/participant/save.action" onsubmit="">
    <tissuelocator:messages/>

    <s:hidden key="object.id" />

    <h2 class="formtop"><fmt:message key="participant.sectionHeader"/></h2>

    <div class="formcol">
        <s:textfield name="object.externalId" label="%{getText('participant.externalId')}" size="30" maxlength="254" cssStyle="width:20em"
            labelposition="top" labelSeparator="" required="true" requiredposition="right" id="externalId" />
    </div>

    <div class="formcol_wide">
        <c:choose>
            <c:when test="${TissueLocatorUser.crossInstitution}">
                <tissuelocator:autocompleter fieldName="object.externalIdAssigner" fieldId="institution"
                    searchFieldName="institutionSelection" cssStyle="width:27em"
                    autocompleteUrl="protected/json/autocompleteInstitution.action" required="true"
                    labelKey="participant.externalIdAssigner" noteKey="participant.externalIdAssigner.note"/>
            </c:when>
            <c:otherwise>
                <label><s:property value="%{getText('participant.externalIdAssigner')}"/></label>
                ${TissueLocatorUser.institution.name}
                <s:hidden name="institutionSelection" value="%{#attr.TissueLocatorUser.institution.name}"/>
                <s:hidden name="object.externalIdAssigner" id="hiddenInstitution"
                    value="%{#attr.TissueLocatorUser.institution.id}"/>
            </c:otherwise>
        </c:choose>
    </div>

    <div class="clear"><br /></div>

    <div class="formcol">
        <s:select name="object.gender" id="gender"
                        list="%{@com.fiveamsolutions.tissuelocator.data.Gender@values()}"
                        listValue="%{getText(resourceKey)}"
                        labelposition="top"
                        label="%{getText('participant.gender')}" labelSeparator=""/>
    </div>

    <div class="formcol">
        <s:select name="object.ethnicity" id="ethnicity"
                        list="%{@com.fiveamsolutions.tissuelocator.data.Ethnicity@values()}"
                        listValue="%{getText(resourceKey)}"
                        labelposition="top"
                        label="%{getText('participant.ethnicity')}" labelSeparator=""/>
    </div>

    <div class="clear"><br /></div>

    <div class="formcol">
        <s:checkboxlist name="object.races" id="races" cssClass="inline"
                        list="%{@com.fiveamsolutions.tissuelocator.data.Race@values()}"
                        listValue="%{getText(resourceKey)}"
                        labelposition="top"
                        label="%{getText('participant.races')}" labelSeparator=""/>
    </div>

    <c:if test="${(not empty object.id) and (not empty specimens.list)}">
        <div class="formcol">
            <c:url value="/admin/participant/withdrawConsent.action" var="withdrawConsentUrl">
                <c:param name="object.id" value="${object.id}"/>
            </c:url>
            <label for="withdrawConsent"><fmt:message key="participant.withdrawConsent"/></label>
            <p><fmt:message key="participant.withdrawConsent.text"/></p>
            <fmt:message key="participant.withdrawConsent.confirm" var="confirmWithdraw"/>
            <a id="withdrawConsent" href="#" class="btn" onclick="confirmAndContinue('${confirmWithdraw}','${withdrawConsentUrl}'); return false;">
                <fmt:message key="btn.withdrawConsent"/>
            </a>
        </div>
    </c:if>



    <div class="clear"><br /></div>
</s:form>

    <c:if test="${!empty specimens}">
        <h2 class="noline">
            <a href="javascript:;" onclick="toggleVisibility('participant_specimens');" class="toggle">
                <fmt:message key="participant.specimens.title" />
            </a>
        </h2>

        <div id="participant_specimens">
            <s:form id="specimenForm" action="/admin/participant/input.action">
                <div class="roundbox_gray_wrapper">
                    <div class="roundbox_gray">
                        <div class="float_left">
                        </div>
                        <div class="float_right">
                          <label class="inline" for="status">
                              <strong><fmt:message key="participant.specimen.filter"/></strong>
                            </label>
                            <s:select name="exampleSpecimen.status" id="status" value="%{exampleSpecimen.status}"
                                    list="%{@com.fiveamsolutions.tissuelocator.data.SpecimenStatus@values()}" theme="simple"
                                    onchange="submit()" listValue="%{getText(resourceKey)}"
                                    headerKey="" headerValue="%{getText('specimenStatus.all')}" />
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <s:hidden key="object.id"/>
                <s:set value="%{pageSize}" name="tablePageSize" scope="page"/>
                <display:table name="${specimens}" uid="specimen" requestURI="/admin/participant/input.action" sort="external"
                                htmlId="specimens" pagesize="${tablePageSize}">
                    <tissuelocator:displaytagProperties/>

                    <fmt:message key="participant.specimen" var="specimensTitle"/>
                    <display:column title="<div>${specimensTitle}</div>" class="carttitle" sortProperty="EXTERNAL_ID" sortable="true">
                        <c:url var="viewUrl" value="/protected/specimen/view.action">
                            <c:param name="object.id" value="${specimen.id}"/>
                        </c:url>
                        <a href="${viewUrl}">${specimen.externalId}</a>
                        <tissuelocator:codeDisplay code="${specimen.pathologicalCharacteristic}"/>
                    </display:column>
                    <fmt:message key="participant.specimen.status" var="statusTitle"/>
                    <display:column title="<div>${statusTitle}</div>" sortProperty="STATUS" sortable="true">
                        <fmt:message key="${specimen.status.resourceKey}"/>
                    </display:column>
                    <fmt:message key="participant.specimen.details" var="detailsTitle"/>
                    <display:column title="<div>${detailsTitle}</div>">
                        <s:if test="%{hasMoreDetails(#attr.specimen) == true}">
                            <c:url var="detailsUrl" value="/admin/specimen/moreDetails.action">
                                <c:param name="object.id" value="${specimen.id}"/>
                            </c:url>
                            <a href="${detailsUrl}"><fmt:message key="participant.specimen.moreDetails"/></a>
                        </s:if>
                        <s:else>
                            &nbsp;&nbsp;
                        </s:else>
                    </display:column>
                    <fmt:message key="default.date.format" var="dateFormat"/>
                    <fmt:message key="specimen.consentWithdrawnDate" var="withdrawnDateTitle"/>
                    <display:column title="<div>${withdrawnDateTitle}</div>">
                        <c:if test="${not empty specimen.consentWithdrawnDate}" >
                            <fmt:formatDate pattern="${dateFormat}" value="${specimen.consentWithdrawnDate}"/>
                        </c:if>
                    </display:column>
                    <s:set value="%{@com.fiveamsolutions.tissuelocator.data.SpecimenStatus@RETURNED_TO_SOURCE_INSTITUTION}" name="returnedStatus"/>
                    <fmt:message key="specimen.returnedToSourceInstitutionDate" var="returnedDateTitle"/>
                    <display:column title="<div>${returnedDateTitle}</div>">
                        <s:iterator value="%{#attr.specimen.statusTransitionHistory}">
                            <s:if test="%{newStatus.equals(#attr.returnedStatus)}">
                                <s:set var="returnedDate" value="%{transitionDate}"/>
                                <fmt:formatDate pattern="${dateFormat}" value="${returnedDate}"/>
                            </s:if>
                        </s:iterator>
                    </display:column>
                    <fmt:message key="column.action" var="actionTitle"/>
                    <display:column title="<div>${actionTitle}</div>">
                        <c:if test="${specimen.consentWithdrawable}">
                            <c:url value="/admin/participant/specimen/withdrawConsent.action" var="withdrawSpecimenConsentUrl">
                                <c:param name="object.id" value="${specimen.id}"/>
                            </c:url>
                            <a href="${withdrawSpecimenConsentUrl}" class="btn tiny" style="padding-left: 9px"><fmt:message key="btn.withdrawConsent"/></a>
                        </c:if>
                        <c:if test="${specimen.status != returnedStatus && specimen.returnableToSourceInstitution}">
                            <c:url var="loadReturnFormUrl" value="/admin/specimen/popup/loadReturnToSourceForm.action" >
                                <c:param name="object.id" value="${specimen.id}"/>
                            </c:url>

                            <a href="javascript: showPopWin('${loadReturnFormUrl}', 400, 225, returnRefresh);" class="btn tiny"><fmt:message key="btn.returnToSourceInstitution"/></a>
                        </c:if>
                    </display:column>
                </display:table>
            </s:form>
        </div>
        <div class="clear"><br /></div>
    </c:if>

    <div class="btn_bar">
        <s:submit value="%{getText('btn.save')}" name="btn_save" cssClass="btn" theme="simple"
              onclick="$('#participantForm').get(0).submit();" title="%{getText('btn.save.button')}"/>
        &nbsp;&nbsp;&nbsp;|&nbsp;
        <a href="${listUrl}"><fmt:message key="btn.cancel"/></a>
    </div>
</body>
</html>
