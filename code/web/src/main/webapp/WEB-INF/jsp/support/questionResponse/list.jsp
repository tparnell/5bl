<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<html>
<head>
    <title><fmt:message key="specimenRequest.review.list.title" /></title>
</head>
<body>
    <tissuelocator:requestListTabs isAdmin="true" selectedTab="response" subtitleKey="questionResponse.list.title"/>

    <tissuelocator:messages/>

    <s:set name="pendingStatus" value="%{@com.fiveamsolutions.tissuelocator.data.support.SupportRequestStatus@PENDING}" />

    <s:form id="filterForm" namespace="/admin/support/question/response" action="filter.action">
        <!--Filters-->
        <div class="roundbox_gray_wrapper">
            <div class="roundbox_gray">
                <div class="float_left">
                </div>

                <div class="float_right">
                    <label class="inline" for="status">
                      <strong><fmt:message key="questionResponse.list.filter.status"/></strong>
                    </label>
                    <s:select name="object.status" id="status"
                        onchange="submit()" theme="simple"
                        list="%{@com.fiveamsolutions.tissuelocator.data.support.SupportRequestStatus@values()}"
                        listValue="%{getText(administratorResourceKey)}"
                        headerKey="" headerValue="%{getText('supportRequestStatus.all')}" />
                    <s:hidden name="object.institution" value="%{object.institution.id}"/>
                </div>

                <div class="clear"></div>
            </div>
        </div>

        <s:set value="%{pageSize}" name="tablePageSize" scope="page"/>
        <fmt:message key="default.date.format" var="dateFormat"/>
        <display:table name="objects" requestURI="/admin/support/question/response/list.action"
                uid="questionResponse" pagesize="${tablePageSize}" sort="external" export="true">
                <tissuelocator:displaytagProperties/>

            <display:column titleKey="questionResponse.id" sortProperty="ID" sortable="true">
                ${questionResponse.id}
                <c:url var="viewUrl" value="/admin/support/question/response/view.action">
                    <c:param name="object.id" value="${questionResponse.id}"/>
                </c:url>
                <a href="${viewUrl}"><fmt:message key="btn.view"/></a>
            </display:column>
            <display:column titleKey="questionResponse.requestorName"
                sortProperty="REQUESTOR_LAST_NAME,REQUESTOR_FIRST_NAME" sortable="true">
                ${questionResponse.question.requestor.displayName}
            </display:column>
            <display:column titleKey="questionResponse.requestorInstitution"
                property="question.requestor.institution.name" sortProperty="REQUESTOR_INSTITUTION" sortable="true"/>
            <display:column titleKey="questionResponse.createdDate" sortProperty="CREATED_DATE" sortable="true">
                <fmt:formatDate pattern="${dateFormat}" value="${questionResponse.createdDate}"/>
            </display:column>
            <display:column titleKey="questionResponse.lastUpdatedDate" sortProperty="UPDATED_DATE" sortable="true">
                <fmt:formatDate pattern="${dateFormat}" value="${questionResponse.lastUpdatedDate}"/>
            </display:column>
            <display:column titleKey="questionResponse.status" sortProperty="STATUS" sortable="true">
                <fmt:message key="${questionResponse.status.administratorResourceKey}"/>
            </display:column>

            <display:column titleKey="column.actionRequired">
                <s:if test="%{#attr.questionResponse.status == #pendingStatus}">
                    <fmt:message key="questionResponse.review"/>
                </s:if>
            </display:column>

            <display:column titleKey="column.action" headerClass="action" class="action">
                <s:if test="%{#attr.questionResponse.status == #pendingStatus}">
                    <c:url var="reviewUrl" value="/admin/support/question/response/input.action">
                        <c:param name="object.id" value="${questionResponse.id}"/>
                    </c:url>
                    <a href="${reviewUrl}" class="btn_fw_action_col"><fmt:message key="btn.review" /></a>
                </s:if>
            </display:column>
        </display:table>
    </s:form>
</body>
</html>
