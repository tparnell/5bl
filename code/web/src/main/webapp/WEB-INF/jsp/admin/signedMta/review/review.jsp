<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<html>
<head>
    <title><fmt:message key="signedMta.review.title" /></title>
    <script type="text/javascript">
        var mtaId = <s:property value="%{signedMta.id}"/>;
        var originalInstitutionId = <s:property value="%{signedMta.receivingInstitution.id}"/>;
        var url = contextPath + '/admin/signedMta/review/list.action?signedMta.id=' + mtaId + '&correctInstitution=true';

        function correctInstSelected() {
            if ($('#correctInstitution_yes').get(0).checked) {
                if ($('#institutionId').get(0).value != originalInstitutionId) {
                    window.location = url;
                } else {
                    showReplaceOrVerify();
                    hideDiv('institution_list', null);
                    hideDiv('mta_contact', null);
                    resetMtaContact();
                    hideDiv('mta_submit_button', null);
                }
            } else if ($('#correctInstitution_no').get(0).checked) {
                showDiv('institution_list');
                hideDiv('replace_existing', 'replaceExisting');
                hideDiv('mta_certification', 'valid');
                hideDiv('mta_contact', null);
                resetMtaContact();
                hideDiv('mta_submit_button', null);
            }
        }

        function replaceExistingSelected() {
            if ($('#replace_existing').get(0)) {
                if ($('#replaceExisting_yes').get(0).checked) {
                    showDiv('mta_certification');
                    hideDiv('mta_contact', null);
                    resetMtaContact();
                    hideDiv('mta_submit_button', null);
                } else if ($('#replaceExisting_no').get(0).checked) {
                    showDiv('mta_contact');
                    showDiv('mta_submit_button');
                    hideDiv('mta_certification', 'valid');
                }
            }
        }

        function certificationSelected() {
            if ($('#valid_yes').get(0).checked) {
                showDiv('mta_contact');
                showDiv('mta_submit_button');
            } else if ($('#valid_no').get(0).checked) {
                hideDiv('mta_contact', null);
                resetMtaContact();
                showDiv('mta_submit_button');
            }
        }

        function showReplaceOrVerify() {
            if ($('#replace_existing').get(0)) {
                showDiv('replace_existing');
                hideDiv('mta_certification', 'valid');
            } else {
                showDiv('mta_certification');
            }
        }

        function resetMtaContact() {
            var originalFirstName = '<s:property value="%{institution.mtaContact.firstName}"/>';
            var originalLastName = '<s:property value="%{institution.mtaContact.lastName}"/>';
            var originalLine1 = '<s:property value="%{institution.mtaContact.address.line1}"/>';
            var originalLine2 = '<s:property value="%{institution.mtaContact.address.line2}"/>';
            var originalCity = '<s:property value="%{institution.mtaContact.address.city}"/>';
            var originalState = '<s:property value="%{institution.mtaContact.address.state.name()}"/>';
            var originalZip = '<s:property value="%{institution.mtaContact.address.zip}"/>';
            var originalCountry = '<s:property value="%{institution.mtaContact.address.country.name()}"/>';
            var originalEmail = '<s:property value="%{institution.mtaContact.email}"/>';
            var originalPhone = '<s:property value="%{institution.mtaContact.address.phone}"/>';
            var originalPhoneExtension = '<s:property value="%{institution.mtaContact.address.phoneExtension}"/>';
            var originalFax = '<s:property value="%{institution.mtaContact.address.fax}"/>';

            $('#mtaContactFirstName').get(0).value = originalFirstName;
            $('#mtaContactLastName').get(0).value = originalLastName;
            $('#mtaContactLine1').get(0).value = originalLine1;
            $('#mtaContactLine2').get(0).value = originalLine2;
            $('#mtaContactCity').get(0).value = originalCity;
            $('#mtaContactState').get(0).value = originalState;
            $('#mtaContactZip').get(0).value = originalZip;
            $('#mtaContactCountry').get(0).value = originalCountry;
            $('#mtaContactEmail').get(0).value = originalEmail;
            $('#mtaContactPhone').get(0).value = originalPhone;
            $('#mtaContactPhoneExtension').get(0).value = originalPhoneExtension;
            $('#mtaContactFax').get(0).value = originalFax;
        }

        function showDiv(divId) {
            var divToShow = $('#' + divId).get(0);
            if (divToShow != null && divToShow.style.display != '') {
                divToShow.style.display = '';
            }
        }

        function hideDiv(divId, optionBase) {
            var divToHide = $('#' + divId).get(0);
            if (divToHide != null && divToHide.style.display != 'none') {
                divToHide.style.display = 'none';
            }
            var yesOption = $('#' + optionBase + '_yes').get(0);
            if (yesOption != null) {
                yesOption.checked = null;
            }
            var noOption = $('#' + optionBase + '_no').get(0);
            if (noOption != null) {
                noOption.checked = null;
            }
        }
    </script>
</head>
<body>

    <fmt:message key="default.date.format" var="dateFormat"/>
    <br />
    <div class="box">

        <table class="data">
            <tr>
                <th scope="col"><div><fmt:message key="signedMta.list.uploadTime"/></div></th>
                <th scope="col"><div><fmt:message key="signedMta.list.institution"/></div></th>
                <th scope="col"><div><fmt:message key="signedMta.list.uploader.name"/></div></th>
                <th scope="col"><div><fmt:message key="signedMta.list.uploader.email"/></div></th>
                <th scope="col"><div><fmt:message key="signedMta.list.uploader.phone"/></div></th>
                <th scope="col" class="action"></th>
            </tr>
            <tr class="odd">
                <td><s:date name="%{signedMta.uploadTime}" format="%{#attr.dateFormat}"/></td>
                <td><s:property value="%{signedMta.receivingInstitution.name}"/></td>
                <td>
                    <s:property value="%{signedMta.uploader.lastName}"/>,
                    <s:property value="%{signedMta.uploader.firstName}"/>
                </td>
                <td><s:property value="%{signedMta.uploader.email}"/></td>
                <td><s:property value="%{signedMta.uploader.address.phone}"/>
                    <tissuelocator:phoneExtensionDisplay extension="${signedMta.uploader.address.phoneExtension}"/>
                </td>
                <td class="action">
                    <c:url value="/protected/downloadFile.action" var="signedMtaUrl">
                        <c:param name="file.id"><s:property value="%{signedMta.document.lob.id}"/></c:param>
                        <c:param name="fileName"><s:property value="%{signedMta.document.name}"/></c:param>
                        <c:param name="contentType"><s:property value="%{signedMta.document.contentType}"/></c:param>
                    </c:url>
                    <fmt:message key="signedMta.review.downloadTitle" var="downloadTitle"/>
                    <a class="btn_fw_action_col" href="${signedMtaUrl}" title="${downloadTitle}" target="_blank">
                        <fmt:message key="signedMta.review.view"/>
                    </a>
                </td>
            </tr>
        </table>
    </div>

    <h2 class="formtop"><fmt:message key="signedMta.review.subtitle"/></h2>

    <tissuelocator:messages/>

    <div class="box" style="margin-top:20px; padding-left:20px;">
        <s:form id="reviewSignedMtaForm" action="/admin/signedMta/review/filter.action">
            <s:hidden name="signedMta.id"/>
            <s:hidden name="institution.id" id="institutionId"/>

            <div id="none"></div>

            <!-- Institution Radio Buttons -->
            <h3><fmt:message key="signedMta.review.institutionName"/></h3>

            <label for="institution" class="normal">
                <span class="hilite"><s:property value="%{signedMta.receivingInstitution.name}"/></span>
                <fmt:message key="signedMta.review.institutionName.message"/>
                &nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="javascript:;" onclick="toggleVisibility('institutions_list');" class="toggle small">
                    <fmt:message key="signedMta.review.institutionName.referenceList"/>
                </a>
            </label>

            <!--List of Institutions for Comparision-->
            <div id="institutions_list" style="display:none;">
                <s:select name="institutions_existing" id="institutions_existing"
                    disabled="true" multiple="true" size="10"
                    theme="simple" cssStyle="float:right; width:470px;"
                    list="institutions" listValue="name" listKey="id"
                    headerKey="" headerValue="%{getText('signedMta.review.institutionName.referenceList.empty')}"
                    labelposition="top" label="%{getText('institution.billingAddress.state')}" labelSeparator=""
                    title="%{getText('consortiumReview.vote.select')}"/>
                <div class="clear"></div>
            </div>
            <!--/List of Institutions for Comparision-->

            <ul id="inst_valid" class="radio_tabs">
                <li class="tab" style="display:none;"><a href="#none"></a></li>
                <c:set var="checked" value=""/>
                <s:if test="%{correctInstitution}">
                    <c:set var="checked" value="checked"/>
                </s:if>
                <li class="tab" style="margin-left:-20px; width:45em;">
                    <a href="#inst_valid_yes"></a>
                    <input type="radio" name="correctInstitution" id="correctInstitution_yes" value="true"
                        onclick="correctInstSelected()" ${checked}/>
                    <label class="inline" for="correctInstitution_yes" style="font-size: 110%">
                        <fmt:message key="signedMta.review.institutionName.yes"/>
                        <s:property value="%{signedMta.receivingInstitution.name}"/>
                    </label>
                </li>
                <c:set var="checked" value=""/>
                <s:if test="%{correctInstitution != null && !correctInstitution}">
                    <c:set var="checked" value="checked"/>
                </s:if>
                <li class="tab" style="margin-left:-20px; width:45em;">
                    <a href="#inst_not_valid"></a>
                    <input type="radio" name="correctInstitution" id="correctInstitution_no" value="false"
                        onclick="correctInstSelected()" ${checked}/>
                    <label class="inline" for="correctInstitution_no" style="font-size: 110%">
                        <fmt:message key="signedMta.review.institutionName.no"/>
                    </label>
                </li>
            </ul>

            <div class="clear"></div>
            <!-- /Institution Radio Buttons -->

            <c:set var="institutionListStyle" value="display:none"/>
            <s:if test="%{correctInstitution != null && !correctInstitution}">
                <c:set var="institutionListStyle" value=""/>
            </s:if>
            <div id="institution_list" style="${institutionListStyle}">
                <br />

                <!-- Institution List -->
                <div id="mta_info">
                    <div class="box mta_listbox">
                        <h3><fmt:message key="signedMta.review.institutionList.title"/></h3>
                        <tissuelocator:institutionMtaList showRadioButtons="true"
                            listActionBase="admin/signedMta/review"/>
                   </div>
               </div>

                <div class="clear"></div>
                <!-- /Institution List -->
            </div>

            <s:set var="hasExistingMta" value="%{false}"/>
            <s:set name="outOfDateStatus" value="%{@com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreementStatus@OUT_OF_DATE}" />
            <s:set var="instSignedMta" value="%{institution.currentSignedRecipientMta}"/>
            <s:if test="%{#instSignedMta == null || signedMta.id == #instSignedMta.id || #instSignedMta.status == #outOfDateStatus}">
                 <s:hidden name="replaceExisting" value="%{true}"/>
            </s:if>
            <s:else>
                <s:set var="hasExistingMta" value="%{true}"/>
                <c:set var="replaceExistingStyle" value="display:none"/>
                <s:if test="%{correctInstitution != null || replaceExisting != null}">
                    <c:set var="replaceExistingStyle" value=""/>
                </s:if>
                <div id="replace_existing" style="${replaceExistingStyle}">
                    <div class="line"></div>

                    <!-- Replace existing MTA Radio Buttons -->
                    <h3><fmt:message key="signedMta.review.fileHandling"/></h3>
                    <label for="mta_valid" class="normal"><fmt:message key="signedMta.review.fileHandling.message"/></label>

                    <ul id="mta_version_to_use" class="radio_tabs">
                        <li class="tab" style="display:none;"><a href="#none"></a></li>
                        <c:set var="checked" value=""/>
                        <s:if test="%{replaceExisting}">
                            <c:set var="checked" value="checked"/>
                        </s:if>
                        <li class="tab" style="margin-left:-20px; width:45em;">
                            <a href="#mta_version_to_use_yes"></a>
                            <input type="radio" name="replaceExisting" id="replaceExisting_yes" value="true"
                                onclick="replaceExistingSelected()" ${checked}/>
                            <label class="inline" for="replaceExisting_yes" style="font-size: 110%">
                                <fmt:message key="signedMta.review.fileHandling.yes"/>
                            </label>
                        </li>
                        <c:set var="checked" value=""/>
                        <s:if test="%{replaceExisting != null && !replaceExisting}">
                            <c:set var="checked" value="checked"/>
                        </s:if>
                        <li class="tab" style="margin-left:-20px; width:45em;">
                            <a href="#mta_version_to_use_no"></a>
                            <input type="radio" name="replaceExisting" id="replaceExisting_no" value="false"
                                onclick="replaceExistingSelected()" ${checked}/>
                            <label class="inline" for="replaceExisting_no" style="font-size: 110%">
                                <fmt:message key="signedMta.review.fileHandling.no"/>
                            </label>
                        </li>
                    </ul>

                    <div class="clear"></div>
                    <!-- /Replace existing MTA Radio Buttons -->
                </div>
            </s:else>

            <c:set var="validStyle" value="display:none"/>
            <s:if test="%{(correctInstitution != null && !#hasExistingMta) || valid != null}">
                <c:set var="validStyle" value=""/>
            </s:if>
            <div id="mta_certification" style="${validStyle}">
                <div class="line"></div>

                <!-- Verify MTA as Valid Radio Buttons -->
                <h3><fmt:message key="signedMta.review.certification"/></h3>
                <label for="mta_valid" class="normal"><fmt:message key="signedMta.review.certification.message"/></label>

                <ul id="mta_valid" class="radio_tabs">
                    <li class="tab" style="display:none;"><a href="#none"></a></li>
                    <c:set var="checked" value=""/>
                    <s:if test="%{valid}">
                        <c:set var="checked" value="checked"/>
                    </s:if>
                    <li class="tab" style="margin-left:-20px; width:45em;">
                        <a href="#mta_valid_yes"></a>
                        <input type="radio" name="valid" id="valid_yes" value="true"
                            onclick="certificationSelected()" ${checked}/>
                        <label class="inline" for="valid_yes" style="font-size: 110%">
                            <fmt:message key="signedMta.review.certification.yes"/>
                        </label>
                    </li>
                    <c:set var="checked" value=""/>
                    <s:if test="%{valid != null && !valid}">
                        <c:set var="checked" value="checked"/>
                    </s:if>
                    <li class="tab" style="margin-left:-20px; width:45em;">
                        <a href="#mta_valid_no"></a>
                        <input type="radio" name="valid" id="valid_no" value="false"
                            onclick="certificationSelected()" ${checked}/>
                        <label class="inline" for="valid_no" style="font-size: 110%">
                            <fmt:message key="signedMta.review.certification.no"/>
                        </label>
                    </li>
                </ul>

                <div class="clear"></div>
                <!-- /Verify MTA as Valid Radio Buttons -->
            </div>

            <c:set var="contactInfoStyle" value="display:none"/>
            <s:if test="%{(valid == null && replaceExisting != null && !replaceExisting) || (valid != null && valid && (replaceExisting == null || replaceExisting))}">
                <c:set var="contactInfoStyle" value=""/>
            </s:if>
            <div id="mta_contact" style="${contactInfoStyle}">
                <div class="line"></div>

                <!--Verify Contact-->
                <h3><fmt:message key="signedMta.review.mtaContact"/></h3>
                <label for="mta_contact" class="normal"><fmt:message key="signedMta.review.mtaContact.message"/></label>

                <tissuelocator:editPerson copyEnabled="false" propertyPrefix="institution.mtaContact"
                        resourcePrefix="signedMta.review.mtaContact"  idPrefix="mtaContact"
                        countryValue="${institution.mtaContact.address.country}"
                        stateValue="${institution.mtaContact.address.state}"
                        showInstitution="false" showFax="true"/>
                <s:hidden name="institution.mtaContact.organization" value="%{institution.id}"/>
                <div class="clear"><br /></div>
                <div class="line"></div>

            </div>

            <c:set var="submitButtonStyle" value="display:none"/>
            <s:if test="%{valid != null || (replaceExisting != null && !replaceExisting)}">
                <c:set var="submitButtonStyle" value=""/>
            </s:if>
            <div id="mta_submit_button" style="${submitButtonStyle}">
                <div class="clear"><br /></div>

                <div class="btn_bar">
                    <c:url value="/admin/signedMta/review/review.action" var="reviewActionUrl"/>
                    <s:submit value="%{getText('btn.save')}" name="btn_save" cssClass="btn" theme="simple"
                        onclick="$('#reviewSignedMtaForm').get(0).action='%{#attr.reviewActionUrl}'"
                        title="%{getText('btn.save.button')}"
                        />
                    &nbsp;&nbsp;&nbsp;|&nbsp;
                    <c:url var="mtaListUrl" value="/admin/signedMta/list.action"/>
                    <a href="${mtaListUrl}"><fmt:message key="btn.cancel"/></a>
                </div>

                <br />
                <div class="clear"></div>
                <br />
            </div>
        </s:form>
    </div>

    <div class="clear"></div>
    <br />
</body>
</html>
