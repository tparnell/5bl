<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<c:set var="extraBodyTags" scope="request">style="margin:5px 15px"</c:set>
<c:url var="imagesBase" value="/images"/>
<html>
    <head>
        <title>
            <tissuelocator:codeDisplay code="${object.pathologicalCharacteristic}" displayUnknown="true"/>
            <br/>${object.externalId}
        </title>
    </head>
    <body>
        <div class="topbtns">
            <a href="#" onclick="history.back();return false" class="btn">
                <fmt:message key="btn.back"/>
            </a>
        </div>
        <div class="box_wrapper" id="bio-details">
            <s:action namespace="/protected" name="specimen/detailsPanel/view" executeResult="true">
                <s:param name="object.id" value="%{object.id}"/>
            </s:action>
            <div class="box_inner" id="table-format">
                <tissuelocator:specimenReadOnlyView />
            </div>
        </div>
        <div id="rightcol">
            <s:if test="%{!aggregateResults}">
                <div class="roundpanel_210">
                    <h3><fmt:message key="shipment.add.to.order"/></h3>
                    <h4 id="huge">
                        <tissuelocator:specimenPrice priceNegotiable="${object.priceNegotiable}"
                            minimumPrice="${object.minimumPrice}" maximumPrice="${object.maximumPrice}"/>
                    </h4>
                    <s:set value="%{@com.fiveamsolutions.tissuelocator.data.SpecimenStatus@AVAILABLE}"
                        name="available" scope="page"/>
                    <c:if test="${object.status == available}">
                        <c:url var="addToOrderUrl" value="/admin/shipment/popup/addToOrder.action">
                            <c:param name="order" value="${param.orderId}" />
                            <c:param name="selection" value="${object.id}" />
                        </c:url>
                        <fmt:message key="shipment.add.to.order" var="addToCart"/>
                        <a href="${addToOrderUrl}">
                            <img src="${imagesBase}/btn_add_to_order.gif" alt="${addToCart}" id="add_to_cart"/>
                        </a>
                    </c:if>
                </div>
            </s:if>
        </div>
        <div class="clear"><br /></div>
        <div class="btn_bar">
            <a href="${addToOrderUrl}" class="btn">
                ${addToCart}
            </a>
            <a href="javascript: window.parent.hidePopWin();" class="btn tiny">Cancel</a>
        </div>
        <script type="text/javascript">
            $(document).ready(function () {
                $(".data.tabular-layout tr:nth-child(odd)").addClass('odd');
            });
        </script>
    </body>
</html>