<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<html>
<head>
    <title><fmt:message key="shipment.shippedDate.title" /></title>
</head>
<body>
    <req:isUserInRole role="shipmentAdmin">
        <s:form id="shipmentDateForm" action="/admin/shipment/popup/setShipmentDate.action" target="_self">
            <s:hidden key="object.id" />

            <fmt:message key="datepicker.format" var="dateFormat"/>
            <sj:datepicker id="shipmentDate" name="object.shipmentDate"
                value="%{object.shipmentDate == null ? new java.util.Date() : object.shipmentDate}"
                displayFormat="%{#attr.dateFormat}" cssStyle="width:160px"
                labelSeparator="" labelposition="top" label="%{getText('shipment.shipmentDate')}" 
                buttonImageOnly="true" changeMonth="true" changeYear="true"/>

            <div class="clear"><br /></div>

            <button onclick="javascript: $('#shipmentDateForm').get(0).submit();" id="btn_setShipmentDate"><fmt:message key="btn.submit"/></button>
        </s:form>
    </req:isUserInRole>
</body>
</html>
