<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<c:url value="/admin/specimen/list.action" var="listUrl"/>
<c:url value="/notYetImplemented.jsp" var="notYetImplementedUrl"/>
<html>
    <head>
        <title>
            <s:if test="%{object.pathologicalCharacteristic != null}">
                <tissuelocator:codeDisplay code="${object.pathologicalCharacteristic}" displayUnknown="true"/>
            </s:if>
            <s:else>
                <fmt:message key="specimen.view.title"/>
            </s:else>
        </title>
        <script type="text/javascript">
            var text = "<fmt:message key="specimen.search.normal.name"/>";
            var normalCode = "<s:property value='%{normalSampleId}'/>";
        </script>
    </head>
    <body onload="setFocusToFirstControl();">
        <fmt:message key="default.date.format" var="dateFormat"/>
        <s:date name="%{new java.util.Date()}" var="today" format="%{#attr.dateFormat}"/>
        <fmt:message key="datepicker.format" var="datePickerFormat"/>
        <div class="topbtns">
            <a href="${listUrl}" class="btn">
                <fmt:message key="btn.backToList"/>
            </a>
        </div>
        <div class="box_wrapper" id="bio-edit">
            <s:form id="specimenForm" action="/admin/specimen/save.action" onsubmit="enablePriceFields()">
                <tissuelocator:messages/>
                <s:hidden key="object.id" />
                <div class="box_inner" id="table-format">
                    <h2><fmt:message key="specimen.storageInstitution"/></h2>
                    <table class="data tabular-layout">
                        <tr>
                            <td class="label" scope="row">
                                <label>
                                    <fmt:message key="specimen.storageInstitution"/>
                                    <span class="reqd"><fmt:message key="required.indicator"/></span>
                                </label>
                            </td>
                            <td class="value">
                                <c:choose>
                                    <c:when test="${TissueLocatorUser.crossInstitution}">
                                        <s:select id="institution"
                                            name="object.externalIdAssigner"
                                            value="%{object.externalIdAssigner.id}"
                                            list="institutions" listKey="id" listValue="name"
                                            onchange="updateAutocompletes(true)"
                                            required="true" requiredposition="right" tabindex="1"/>
                                        <s:hidden id="institutionSelection"
                                            name="institutionSelection"
                                            value="%{object.externalIdAssigner.name}"/>
                                    </c:when>
                                    <c:otherwise>
                                        <label><s:property value="%{getText('specimen.externalIdAssigner')}"/></label>
                                        ${TissueLocatorUser.institution.name}
                                        <s:hidden id="institutionSelection"
                                            name="institutionSelection"
                                            value="%{#attr.TissueLocatorUser.institution.name}"/>
                                        <s:hidden id="institution"
                                            name="object.externalIdAssigner"
                                            value="%{#attr.TissueLocatorUser.institution.id}"/>
                                    </c:otherwise>
                                </c:choose>
                            </td>
                        </tr>
                    </table>
				    <h2><fmt:message key="specimen.biospecimenCharacteristics"/> - (<span class="reqd"><fmt:message key="required.indicator"/></span><fmt:message key="required.indicator.label"/>)</h2>
				    <table class="data tabular-layout">
                        <tr>
                            <td class="label" scope="row">
                                <label>
                                    <fmt:message key="specimen.externalId"/>
                                    <span class="reqd"><fmt:message key="required.indicator"/></span>
                                </label>
                            </td>
                            <td class="value">
                                <s:textfield id="externalId" size="20"
                                    name="object.externalId"
                                    maxlength="50"
                                    cssStyle="width:12em"
                                    required="true"
                                    tabindex="2"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" scope="row">
                                <label>
                                    <fmt:message key="specimen.pathologicalCharacteristic"/>
                                    <span class="reqd"><fmt:message key="required.indicator"/></span>
                                </label>
                            </td>
                            <td class="value">
                                <s:checkbox id="normalCheckbox"
                                    name="normalSample"
                                    tabindex="3"
                                    theme="simple"
                                    cssStyle="vertical-align:middle;"
                                    onclick="typeAutocompleteAndDisable('pathologicalCharacteristic', 'normalCheckbox', text, normalCode)"/>
                                <label class="inline" for="normalCheckbox">
                                    <fmt:message key="specimen.search.normal.note"/>
                                </label>
                                <s:fielderror fieldName="object.consortiumMember" cssClass="fielderror"/>
                                <div id="diseaseAutocompleteDiv">
                                    <tissuelocator:autocompleter fieldId="pathologicalCharacteristic"
                                        fieldName="object.pathologicalCharacteristic"
                                        searchFieldName="diseaseSelection"
                                        required="true"
                                        autocompleteUrl="json/specimen/search/autocompleteDisease.action"
                                        noteKey="specimen.pathologicalCharacteristic.note"
                                        cssStyle="width:40em; margin:5px 0 0 0"
                                        tabIndex="4"/>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" scope="row">
                                <label>
                                    <fmt:message key="specimen.specimenType"/>
                                    <span class="reqd"><fmt:message key="required.indicator"/></span>
                                </label>
                            </td>
                            <td class="value">
                                 <s:select id="specimenType"
                                    name="object.specimenType"
                                    value="%{object.specimenType.id}"
                                    list="specimenTypes" listKey="id" listValue="name"
                                    required="true"
                                    tabindex="5"/>
                            </td>
                        </tr>
                        <tr>
                            <div class="wwgrp">
                                <td class="label" scope="row">
                                    <label><fmt:message key="specimen.availableQuantity"/></label>
                                </td>
                                <td class="value">
                                    <s:textfield id="availableQuantity"
                                        name="object.availableQuantity"
                                        size="3"
                                        maxlength="10"
                                        theme="simple"
                                        cssStyle="width:4em"
                                        tabindex="6" />
                                    <s:select id="availableQuantityUnits"
                                        name="object.availableQuantityUnits"
                                        value="%{object.availableQuantityUnits}"
                                        theme="simple"
                                        headerKey="" headerValue="%{getText('select.emptyOption')}"
                                        list="%{@com.fiveamsolutions.tissuelocator.data.QuantityUnits@values()}"
                                        listValue="%{getText(resourceKey)}"
                                        title="%{getText('specimen.availableQuantityUnits.select')}"
                                        tabindex="7"/>
                                    <s:fielderror cssClass="fielderror">
                                        <s:param>object.availableQuantity</s:param>
                                        <s:param>object.validQuantity</s:param>
                                    </s:fielderror>
                                </td>
                            </div>
                        </tr>
                        <tr>
                            <div class="wwgrp">
                                <td class="label" scope="row">
                                    <label>
                                        <fmt:message key="specimen.patientAgeAtCollection"/>
                                        <span class="reqd"><fmt:message key="required.indicator"/></span>
                                    </label>
                                </td>
                                <td class="value">
                                    <s:textfield id="age"
                                        name="object.patientAgeAtCollection"
                                        size="7"
                                        maxlength="7"
                                        theme="simple"
                                        cssStyle="width:6em"
                                        tabindex="8" />
                                    <s:select id="ageUnits"
                                        name="object.patientAgeAtCollectionUnits"
                                        value="%{object.patientAgeAtCollectionUnits}"
                                        theme="simple"
                                        list="%{@com.fiveamsolutions.tissuelocator.data.TimeUnits@values()}"
                                        listValue="%{getText(resourceKey)}"
                                        title="%{getText('specimen.patientAgeAtCollectionUnits.select')}"
                                        tabindex="9"/>
                                    <s:fielderror fieldName="object.patientAgeAtCollection" cssClass="fielderror"/>
                                </td>
                            </div>
                        </tr>
                        <tr>
                            <td class="label" scope="row">
                                <label><fmt:message key="specimen.collectionYear"/></label>
                            </td>
                            <td class="value">
                                <s:textfield id="collectionYear"
                                    name="object.collectionYear"
                                    size="5"
                                    maxLength="4"
                                    tabindex="10"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" scope="row">
                                <label>
                                    <fmt:message key="specimen.status"/>
                                    <span class="reqd"><fmt:message key="required.indicator"/></span>
                                </label>
                            </td>
                            <td class="value">
                                <s:select id="specimenStatus"
                                    name="object.status"
                                    value="%{object.status}"
                                    theme="simple"
                                    list="%{legalStatuses}"
                                    listValue="%{getText(resourceKey)}"
                                    required="true"
                                    onchange="toggleStatusTransitionDate()"
                                    tabindex="11"/>
                                <s:fielderror fieldName="object.status" cssClass="fielderror"/>
                            </td>
                        </tr>
                        <c:if test="${object.id != null}">
                            <c:set var="transitionDateStyle" value="display: none"/>
                            <c:if test="${not empty object.statusTransitionDate}">
                                <c:set var="transitionDateStyle" value="display: block"/>
                            </c:if>
                            <div id="statusTransitionDateContainer" style="${transitionDateStyle}">
                                <tr>
                                    <td class="label" scope="row">
                                        <label for="statusTransitionDate" id="statusTransitionDateLabel">
                                            <fmt:message key="specimen.statusTransitionDate">
                                                <fmt:param><fmt:message key="${object.status.resourceKey}"/></fmt:param>
                                            </fmt:message>
                                        </label>
                                    </td>
                                    <td class="value">
                                        <sj:datepicker id="statusTransitionDate" name="object.statusTransitionDate"
                                            displayFormat="%{#attr.datePickerFormat}" cssStyle="width:200px"
                                            buttonImageOnly="true" changeMonth="true" changeYear="true"/>
                                        <div class="clear"></div>
                                        <s:fielderror cssClass="fielderror" fieldName="object.statusTransitionDateValid"/>
                                    </td>
                                </tr>
                            </div>
                        </c:if>
                        <s:set var="uiDynamicFieldCategory" value="%{getUiDynamicFieldCategory('specimen.biospecimenCharacteristics')}"/>
                        <s:if test="%{#uiDynamicFieldCategory != null}">
                            <s:set var="dynamicFieldDefinitions" value="%{getDynamicFieldDefinitionsByCategory(#uiDynamicFieldCategory)}"/>
                            <tissuelocator:editExtensions
                                dynamicFieldDefinitions="${dynamicFieldDefinitions}"
                                layout="rowLayout"
                                extendableEntity="${object}"
                                propertyPath="object"/>
                        </s:if>
                    </table>
                    <req:isUserInRole role="priceViewer">
                        <h2>
                            <fmt:message key="specimen.price"/>
                            <span class="reqd"><fmt:message key="required.indicator"/></span>
                        </h2>
                        <table class="data tabular-layout">
                            <tr>
                                <td class="label" scope="row">
                                    <label><fmt:message key="specimen.priceNegotiable"/></label>
                                </td>
                                <td class="value">
                                    <s:checkbox id="priceNegotiable"
                                        name="object.priceNegotiable"
                                        theme="simple"
                                        onclick="updatePriceFields()"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="label" scope="row">
                                    <label><fmt:message key="specimen.minimumPrice"/></label>
                                </td>
                                <td class="value">
                                    <s:textfield id="minimumPrice"
                                        name="object.minimumPrice"
                                        size="3"
                                        maxlength="10"
                                        cssStyle="width:4em"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="label" scope="row">
                                    <label><fmt:message key="specimen.maximumPrice"/></label>
                                </td>
                                <td class="value">
                                    <s:textfield id="maximumPrice"
                                        name="object.maximumPrice"
                                        size="3"
                                        maxlength="10"
                                        cssStyle="width:4em"/>
                                </td>
                            </tr>
                        </table>
                    </req:isUserInRole>
                    <req:isUserInRole role="priceViewer" value="false">
                        <s:hidden id="minimumPrice" key="object.minimumPrice" />
                        <s:hidden id="maximumPrice" key="object.maximumPrice" />
                        <s:hidden id="priceNegotiable" name="object.priceNegotiable" value="true"/>
                    </req:isUserInRole>
                    <s:if test="%{displayCollectionProtocol || defaultProtocol == null}">
                      <div id="protocolContainer">
                        <h2><fmt:message key="specimen.collectionProtocol"/></h2>
                        <table class="data tabular-layout">
                            <tr id="protocolRow">
                                <td class="label" scope="row">
                                    <label>
                                        <fmt:message key="specimen.protocol"/>
                                        <span class="reqd"><fmt:message key="required.indicator"/></span>
                                    </label>
                                </td>
                                <s:if test="%{defaultProtocol != null}">
                                    <td class="value">
                                        ${defaultProtocol.name}
                                            <s:hidden id="protocolSelection"
                                                name="protocolSelection"
                                                value="%{defaultProtocol.name}"/>
                                            <s:hidden id="protocol"
                                                name="object.protocol"
                                                value="%{object.protocol.id}"/>
                                    </td>
                                </s:if>
                                <s:else>
                                    <td class="value">
                                        <tissuelocator:autocompleter fieldId="protocol"
                                            fieldName="object.protocol"
                                            searchFieldName="protocolSelection"
                                            cssStyle="width:30em; margin:5px 0 0 0"
                                            required="true"
                                            autocompleteUrl="admin/json/specimen/autocompleteProtocol.action"
                                            noteKey="autocompleter.note"/>
                                    </td>
                                </s:else>
                            </tr>
                            <s:set var="uiDynamicFieldCategory" value="%{getUiDynamicFieldCategory('specimen.collectionProtocol')}"/>
                            <s:if test="%{#uiDynamicFieldCategory != null}">
                                <s:set var="dynamicFieldDefinitions" value="%{getDynamicFieldDefinitionsByCategory(#uiDynamicFieldCategory)}"/>
                                <s:if test="%{!#dynamicFieldDefinitions.isEmpty()}">
                                    <tissuelocator:editExtensions
                                        dynamicFieldDefinitions="${dynamicFieldDefinitions}"
                                        layout="rowLayout"
                                        extendableEntity="${object}"
                                        propertyPath="object"/>
                                </s:if>
                            </s:if>
                        </table>
                      </div>
                    </s:if>
                    <s:else>
                        <s:hidden id="protocolSelection"
                            name="protocolSelection"
                            value="%{defaultProtocol.name}"/>
                        <s:hidden id="protocol"
                            name="object.protocol"
                            value="%{object.protocol.id}"/>
                    </s:else>
                    <h2><fmt:message key="specimen.participantHeader"/></h2>
                    <table class="data tabular-layout">
                        <tr>
                            <td class="label" scope="row">
                                <label>
                                    <fmt:message key="specimen.participant"/>
                                    <span class="reqd"><fmt:message key="required.indicator"/></span>
                                </label>
                            </td>
                            <td class="value">
                                <tissuelocator:autocompleter fieldId="participant"
                                    fieldName="object.participant"
                                    searchFieldName="participantSelection"
                                    cssStyle="width:30em; margin:5px 0 0 0"
                                    required="true"
                                    autocompleteUrl="admin/json/specimen/autocompleteParticipant.action"
                                    noteKey="autocompleter.note"/>
                            </td>
                        </tr>
                        <s:if test="%{object.id != null}">
                            <s:set name="yesEnum" value="%{@com.fiveamsolutions.tissuelocator.data.YesNoResponse@YES}"/>
                            <s:set name="noEnum"  value="%{@com.fiveamsolutions.tissuelocator.data.YesNoResponse@NO}" />
                            <tr>
                                <div class="wwgrp">
                                    <td class="label" scope="row">
                                        <label><fmt:message key="specimen.consentWithdrawn"/></label>
                                    </td>
                                    <td class="value">
                                        <s:iterator value="%{#{#yesEnum : true, #noEnum : false}}">
                                          <s:set value="%{key.name()}" var="optionName"/>
                                          <s:set value="%{value}" var="optionValue"/>
                                          <c:set var="checked" value=""/>
                                          <s:if test="%{#optionValue == object.consentWithdrawn}">
                                              <c:set var="checked" value="checked"/>
                                          </s:if>
                                          <c:set var="consentOnclick" value="onclick=\"clearDate('consentWithdrawnDate')\""/>
                                          <s:if test="%{#optionValue == true}">
                                              <c:set var="consentOnclick" value="onclick=\"setDate('consentWithdrawnDate', '${today}')\""/>
                                          </s:if>
                                          <input type="radio" name="object.consentWithdrawn"
                                              id="consentWithdrawn_${optionName}" value="${optionValue}" ${checked} ${consentOnclick}/>
                                          <label class="inline" for="consentWithdrawn_${optionName}">
                                              <s:property value="%{getText(key.resourceKey)}"/>
                                          </label>
                                        </s:iterator>
                                    </td>
                                </div>
                            </tr>
                            <tr>
                                <td class="label" scope="row">
                                    <label><fmt:message key="specimen.consentWithdrawnDate"/></label>
                                </td>
                                <td class="value">
                                    <sj:datepicker id="consentWithdrawnDate" name="object.consentWithdrawnDate"
                                      displayFormat="%{#attr.datePickerFormat}" cssStyle="width:160px"
                                      buttonImageOnly="true" changeMonth="true" changeYear="true"/>
                                    <s:fielderror cssClass="fielderror">
                                          <s:param>object.consentWithdrawnDateValid</s:param>
                                    </s:fielderror>
                                </td>
                            </tr>
                        </s:if>
                        <s:set var="uiDynamicFieldCategory" value="%{getUiDynamicFieldCategory('specimen.participantHeader')}"/>
                        <s:if test="%{#uiDynamicFieldCategory != null}">
                            <s:set var="dynamicFieldDefinitions" value="%{getDynamicFieldDefinitionsByCategory(#uiDynamicFieldCategory)}"/>
                            <s:if test="%{!#dynamicFieldDefinitions.isEmpty()}">
                                <tissuelocator:editExtensions
                                    dynamicFieldDefinitions="${dynamicFieldDefinitions}"
                                    layout="rowLayout"
                                    extendableEntity="${object}"
                                    propertyPath="object"/>
                            </s:if>
                        </s:if>
                    </table>
                    <s:if test="%{!uiDynamicFieldCategories.isEmpty()}">
                        <s:if test="%{tabbedSearchAndSectionedCategoriesViewsEnabled}">
	                        <s:iterator var="uiSection" value="%{uiSections}">
	                            <s:if test="%{!#uiSection.sectionName.equals(getText('specimen.biospecimenCharacteristics'))}">
	                                <h2><s:property value="%{#uiSection.sectionName}"/></h2>
	                                <s:subset source="%{uiDynamicFieldCategories}" decider="%{uiDynamicFieldCategoryDecider}">
	                                    <s:iterator var="uiDynamicFieldCategory">
	                                        <s:set var="dynamicFieldDefinitions" value="%{getDynamicFieldDefinitionsByCategory(#uiDynamicFieldCategory)}"/>
	                                        <s:if test="%{!#dynamicFieldDefinitions.isEmpty()}">
	                                            <table class="data tabular-layout">
	                                                <thead>
	                                                    <tr>
	                                                        <th colspan="2">
	                                                            <div class="padme5">
	                                                                <s:property value="%{#uiDynamicFieldCategory.categoryName}"/>
	                                                            </div>
	                                                        </th>
	                                                    </tr>
	                                                </thead>
	                                            </table>
	                                            <table class="data tabular-layout">
	                                                <tissuelocator:editExtensions
	                                                    dynamicFieldDefinitions="${dynamicFieldDefinitions}"
	                                                    layout="rowLayout"
	                                                    extendableEntity="${object}"
	                                                    propertyPath="object"/>
	                                            </table>
	                                        </s:if>
	                                    </s:iterator>
	                                </s:subset>
	                            </s:if>
							</s:iterator>
						</s:if>
						<s:else>
						    <s:subset source="uiDynamicFieldCategories.values()" decider="uiDynamicFieldCategoryDecider">
                            <s:iterator var="uiDynamicFieldCategory">
                                <s:set var="dynamicFieldDefinitions" value="%{getDynamicFieldDefinitionsByCategory(#uiDynamicFieldCategory)}"/>
                                <s:if test="%{!#dynamicFieldDefinitions.isEmpty()}">
                                    <s:set var="categoryName" value="%{#uiDynamicFieldCategory.categoryName}"/>
                                    <h2>${categoryName}</h2>
                                    <table class="data tabular-layout">
                                        <tissuelocator:editExtensions
                                            dynamicFieldDefinitions="${dynamicFieldDefinitions}"
                                            layout="rowLayout"
                                            extendableEntity="${object}"
                                            propertyPath="object"/>
                                    </table>
                                </s:if>
                            </s:iterator>
                        </s:subset>
						</s:else>
                    </s:if>
                </div>
                <div class="btn_bar">
                    <s:submit value="%{getText('btn.save')}" name="btn_save" cssClass="btn" theme="simple" title="%{getText('btn.save.button')}"/>
                    &nbsp;&nbsp;&nbsp;|&nbsp;
                    <a href="${listUrl}"><fmt:message key="btn.cancel"/></a>
                </div>
            </s:form>
        </div>
        <script>
            var statusResourceMap = {};
        </script>
        <c:forEach items="${legalStatuses}" var="legalStatus">
            <script>
                statusResourceMap['${legalStatus}'] = '<fmt:message key="specimen.statusTransitionDate"><fmt:param><fmt:message key="${legalStatus.resourceKey}"/></fmt:param></fmt:message>';
            </script>
        </c:forEach>
        <script>
            var defaultProtocolMap = {};
        </script>
        <s:iterator value="%{defaultProtocolMap}" var="defaultProtocolEntry">
            <script>
                defaultProtocolMap['${defaultProtocolEntry.key}'] = { name : '${defaultProtocolEntry.value.name}', id : '${defaultProtocolEntry.value.id}' };
            </script>
        </s:iterator>
        <script>
            var updateAutocompletes = function(refresh) {
                <c:if test="${TissueLocatorUser.crossInstitution}">
                    var institutionsList = $('#institution').get(0);
                    $('#institutionSelection').get(0).value = institutionsList.options[institutionsList.options.selectedIndex].text;
                </c:if>
                <c:if test="${defaultProtocol == null}">
                    var instId = $('#institution').get(0).value;
                    if (defaultProtocolMap[instId]) {
                      pushAutocompleteValue('protocol', defaultProtocolMap[instId].name, defaultProtocolMap[instId].id)
                        var instName = $('#institutionSelection').get(0).value;
                        var fullUrl = contextPath + '/admin/json/specimen/autocompleteProtocol.action?institutionSelection=' + instName;
                        $('#protocol').autocomplete('option', 'source', fullUrl);
                        <c:if test="${!displayCollectionProtocol}">
                        if ($('#protocolContainer').is(':visible')) {
                            toggleVisibility('protocolContainer');
                        }
                        </c:if>
                    } else {
                      updateAutocompleteSource('protocol', '/admin/json/specimen/autocompleteProtocol.action', refresh);
                      <c:if test="${!displayCollectionProtocol}">
                          if (!$('#protocolContainer').is(':visible')) {
                            toggleVisibility('protocolContainer');
                          }
                        </c:if>
                    }
                </c:if>
                updateAutocompleteSource('participant', '/admin/json/specimen/autocompleteParticipant.action', refresh);
            }

            var updateAutocompleteSource = function(id, url, refresh) {
                var instName = $('#institutionSelection').get(0).value;
                var fullUrl = contextPath + url + '?institutionSelection=' + instName;
                $('#' + id).autocomplete('option', 'source', fullUrl);
                if (refresh) {
                    $('#' + id).get(0).value = '';
                    $('#' + id + 'Hidden').get(0).value = '';
                }
            }

            var enablePriceFields = function() {
                $('#minimumPrice').get(0).disabled = false;
                $('#maximumPrice').get(0).disabled = false;
            }

            var updatePriceFields = function() {
                var negotiable = $('#priceNegotiable').get(0).checked;
                if (negotiable) {
                    $('#minimumPrice').get(0).value = '';
                    $('#minimumPrice').get(0).disabled = true;
                    $('#maximumPrice').get(0).value = '';
                    $('#maximumPrice').get(0).disabled = true;
                } else {
                    enablePriceFields();
                }
            }

            function toggleStatusTransitionDate() {
              if ($('#statusTransitionDateContainer').get(0)) {
                  var statusElement = $('#specimenStatus').get(0);
                  if (statusElement.value != '${object.previousStatus}') {
                      var transitionDateElement = $('#statusTransitionDateContainer');
                      var dateLabel = $('#statusTransitionDateLabel').get(0);
                      dateLabel.innerHTML = statusResourceMap[statusElement.value];
                      transitionDateElement.show();
                      setDate('statusTransitionDate', '${today}');
                  } else {
                      var transitionDateElement = $('#statusTransitionDateContainer');
                      transitionDateElement.hide();
                      clearDate('statusTransitionDate');
                  }
              }
            }

            $(document).ready(function () {
                updateAutocompletes(false);
                updatePriceFields();
                $(".data.tabular-layout > tbody > tr:odd").addClass('odd');
                disableAutocompleteIfChecked('pathologicalCharacteristic', 'normalCheckbox', text);
            });
        </script>
    </body>
</html>