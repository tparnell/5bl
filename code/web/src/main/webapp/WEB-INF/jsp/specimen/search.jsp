<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<c:url value="/protected/specimen/search/storeSearch.action" var="storeSearchAction"/>
<html>
    <head>
        <title>
            <s:if test="%{simpleSearch}">
                <fmt:message key="specimen.search.simple.title"/>
            </s:if>
            <s:else>
                <fmt:message key="specimen.search.title"/>
            </s:else>
        </title>
        <script type="text/javascript">
            $(document).ready(function () {
                var boxes = [];
                $('#specimen input[type=checkbox]').each(function(index) {
                    if (this.id == 'filterForm_selection') {
                        boxes.push(this);
                    }
                    if (this.id == 'searchResultsForm_selection') {
                        boxes.push(this);
                    }
                });

                disableOnSelection($('#btn_addtocart').get(0), boxes, ['selectall'], false);
            });

            function refreshSpecimenSearchResults(returnVal) {
                // resubmit the form
                $('#filterForm').submit();
            }

            function saveSearch(saveSearchUrl) {
                $("#forwardSavedSpecimenSearchUrl").val(saveSearchUrl);
                $("#filterForm").get(0).action = '${storeSearchAction}';
                $("#filterForm").submit();
            }
        </script>
    </head>
    <body onload="setFocusToFirstControl();">
        <span style="float: right; margin-top: -30px">
            <s:if test="%{savedSpecimenSearch != null}">
                <s:url var="createSavedSpecimenSearchUrl"
                    value="/protected/myAccount/savedSpecimenSearches/popup/createSavedSpecimenSearch.action"
                    includeContext="false">
                    <s:param name="object.id" value="%{savedSpecimenSearch.id}"/>
                </s:url>
                <s:url var="updateSavedSpecimenSearchUrl"
                    value="/protected/myAccount/savedSpecimenSearches/popup/updateSavedSpecimenSearch.action"
                    includeContext="false">
                    <s:param name="object.id" value="%{savedSpecimenSearch.id}"/>
                </s:url>
                <a href="javascript: saveSearch('${updateSavedSpecimenSearchUrl}');">
                    <s:property value="%{'Update ' + savedSpecimenSearch.name}"/>
                </a>
                &nbsp;|&nbsp;
            </s:if>
            <s:else>
                <s:url var="createSavedSpecimenSearchUrl"
                    value="/protected/myAccount/savedSpecimenSearches/popup/createSavedSpecimenSearch.action"
                    includeContext="false"/>
            </s:else>
            <a href="javascript: saveSearch('${createSavedSpecimenSearchUrl}');">
                <fmt:message key="savedSpecimenSearch.saveAs"/>
            </a>
            &nbsp;|&nbsp;
            <s:url var="manageSavedSpecimenSearchesUrl"
                value="/protected/myAccount/savedSpecimenSearches/popup/listSavedSpecimenSearches.action"/>
            <a href="javascript:showPopWin('${manageSavedSpecimenSearchesUrl}', 675, 525, null);">
                <fmt:message key="savedSpecimenSearch.manage"/>
            </a>
        </span>
        <s:form id="filterForm" namespace="/protected" action="specimen/search/filter.action">
            <tissuelocator:specimenTabbedSearchView>
                <div id="table-format" class="roundbox_gray_wrapper">
                    <div class="roundbox_gray">
                        <tissuelocator:specimenSearchForm searchParams="${SpecimenSearchAction_params}" refreshAction="/protected/specimen/search/refreshList.action"/>
                    </div>
                </div>
                <s:set var="hasErrors" value="%{hasErrors()}" scope="page"/>
                <c:if test="${!empty SpecimenSearchAction_params and !startPopupOnLoad and !hasErrors}">
                    <div id="specimen-search-results-header" class="specimen-results-header">
                        <div id="specimen-search-results-title" style="float: left;">
                            <h2 class="noline"><fmt:message key="specimen.search.results.title"/></h2>
                        </div>
                        <div id="specimen-search-results-config" style="float: right;">
                            <s:url namespace="/protected"
                                   action="specimen/popup/configureSearchResultFields.action"
                                   var="configureDisplayedSpecimenResultsUrl"/>
                            <a href="javascript:showPopWin('${configureDisplayedSpecimenResultsUrl}',
                                                            600, 400,
                                                            refreshSpecimenSearchResults);"
                               class="btn">
                                <fmt:message key="specimen.search.results.configure.displayed.fields.button"/>
                            </a>
                        </div>
                    </div>
                    <tissuelocator:specimenTable showEditButtons="false" showCheckboxes="true" viewAction="/protected/specimen/view.action"
                        formId="filterForm" checkboxProperty="selection" listAction="/protected/specimen/search/filter.action"
                        exportEnabled="true" showStatus="false" showFee="true" showInstitution="false"
                        columnFilteringEnabled="true"/>
                    <div class="clear"><br /></div>
                    <div class="btn_bar">
                        <tissuelocator:determinePersistenceType urlPrefix="/protected/request/addSearchResultsToCart" urlEnding=".action" asUrl="true"/>
                        <s:submit value="%{getText('specimen.search.addToCart')}" title="%{getText('specimen.search.addToCart.button')}"
                            name="btn_addtocart" id="btn_addtocart" cssClass="btn" theme="simple"
                            onclick="$('#filterForm').get(0).action = '%{#attr.request.outputUrl}'"/>
                    </div>
                </c:if>
            </tissuelocator:specimenTabbedSearchView>
            <s:hidden id="forwardSavedSpecimenSearchUrl" name="forwardSavedSpecimenSearchUrl"/>
        </s:form>
        <c:if test="${startPopupOnLoad}">
            <s:url var="savedSpecimenSearchUrl" value="%{#parameters['forwardSavedSpecimenSearchUrl']}"/>
            <script type="text/javascript">
                $(document).ready(function () {
                    showPopWin('${savedSpecimenSearchUrl}', 600, 400, null);
                });
            </script>
        </c:if>
    </body>
</html>
