<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<c:set var="skipHeader" value="true" scope="request"/>
<html>
<head>
    <title><fmt:message key="savedSpecimenSearch.manage" /></title>
    <script>
    function closePopupWindow() {
        window.parent.hidePopWin(false);
        window.parent.location.replace(window.parent.location.href);
    }
    </script>
</head>
<body>
    <tissuelocator:messages/>
    <c:set var="tablePageSize" scope="page" value="${param['pageSize']}" />
    <c:if test="${tablePageSize == null}">
        <c:set var="tablePageSize" scope="page" value="10"/>
    </c:if>
    <s:set var="pageSize" value="%{#attr.tablePageSize}"/>
    <s:form namespace="/protected/myAccount/savedSpecimenSearches" action="popup/listSavedSpecimenSearches" target="_self">
        <display:table name="${savedSpecimenSearches}"
            requestURI="/protected/myAccount/savedSpecimenSearches/popup/listSavedSpecimenSearches.action"
            pagesize="${tablePageSize}"
            export="false"
            sort="list"
            defaultsort="0"
            uid="currentSavedSpecimenSearch">
            <tissuelocator:displaytagProperties />
            <display:column sortProperty="name" titleKey="savedSpecimenSearch.name" sortable="true">
                <c:url var="loadSavedSpecimenSearchUrl"
                    value="/protected/myAccount/savedSpecimenSearches/loadSavedSpecimenSearch.action">
                    <c:param name="object.id" value="${currentSavedSpecimenSearch.id}"/>
                </c:url>
                <a href="${loadSavedSpecimenSearchUrl}" target="_top">${currentSavedSpecimenSearch.name}</a>
            </display:column>
            <display:column property="description" titleKey="savedSpecimenSearch.description" sortable="true"/>
            <display:column titleKey="column.action" headerClass="action" class="action" style="width: 100px">
                <c:url var="deleteSavedSpecimenSearchUrl"
                    value="/protected/myAccount/savedSpecimenSearches/popup/deleteSavedSpecimenSearch.action">
                    <c:param name="object.id" value="${currentSavedSpecimenSearch.id}"/>
                </c:url>
                <a href="${deleteSavedSpecimenSearchUrl}" target="_self" class="btn">
                    <fmt:message key="btn.delete"/>
                </a>
            </display:column>
        </display:table>
        <div class="btn_bar">            
            <a href="#"
                onclick="closePopupWindow();return false;" 
                class="btn"
                style="float:left; padding:0px 8px;">
                <s:text name="btn.close"/>
            </a>
        </div>
    </s:form>
</body>
</html>
