<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<div id="details_panel_top_wrapper">
    <div id="details_panel_top">
        <div class="details_panel_col">
            <label id="details_panel_column1_label">${column1Label}</label>
            <div class="big_value" id="details_panel_column1_value">
                <tissuelocator:specimenDetailsPanelColumn columnLabel="${column1Label}" columnValue="${column1Value}"/>
            </div>
        </div>
        <div class="details_panel_col"> 
            <label id="details_panel_column2_label">${column2Label}</label>
            <div class="big_value" id="details_panel_column2_value">
                <tissuelocator:specimenDetailsPanelColumn columnLabel="${column2Label}" columnValue="${column2Value}"/>
            </div>
        </div>
        <div class="details_panel_col">
            <label id="details_panel_column3_label">${column3Label}</label>
            <div class="big_value" id="details_panel_column3_value">
                <tissuelocator:specimenDetailsPanelColumn columnLabel="${column3Label}" columnValue="${column3Value}"/>        
            </div>
        </div>
        <div class="details_panel_col noborder"> 
            <label id="details_panel_column4_label">${column4Label}</label>
            <div class="big_value" id="details_panel_column4_value">
                <tissuelocator:specimenDetailsPanelColumn columnLabel="${column4Label}" columnValue="${column4Value}"/>        
            </div>
        </div>
        <div class="clear"></div>          
    </div>
</div>