function setFocusToFirstControl() {
    for (var f=0; f < document.forms.length; f++) {
        for(var i=0; i < document.forms[f].length; i++) {
            var elt = document.forms[f][i];
            if (elt.type != "hidden" && elt.disabled != true && elt.id != 'enableEnterSubmit') {
                try {
                    elt.focus();
                    return;
                } catch(er) {
                }
            }
        }
    }
}

function selectAllCheckboxes(formId, selectAllId) {
    var selectAllValue = $('#' + selectAllId).get(0).checked;
    var formInputs = $('#' + formId).get(0).elements;
    for (var i = 0; i < formInputs.length; i++) {
        if (formInputs[i].type == 'checkbox') {
            formInputs[i].checked = selectAllValue;
        }
    }
}


var highlightOnFocus = function(){
  if(document.forms){ //find all forms
    var forms = document.forms; //build array of all forms
    for(y=0;y<forms.length;y++){
      var formEls = document.forms[y].elements; //find all elements within each form
      for(g=0;g<formEls.length;g++){
        if(formEls[g].type=="text" || formEls[g].type=="select" || formEls[g].type=="textarea" || formEls[g].type=="password"){
          formEls[g].onfocus = function(){ //handle event when form field is focused on
            if(!this.className.match(/focus/gi))
              this.className += " focus";
            else if(!this.className)
              this.className += "focus";
          }
          formEls[g].onblur = function(){ //handle event when form field is no longer focused
            if(this.className.match(/focus/gi))
              this.className = this.className.replace(/focus/gi,"");
          }
        }
      }
    }
  }
}

function confirmAndContinue(confirmText, url) {
  var proceed = confirm(confirmText);
  if (proceed) {
      window.location.href = url;
  }
}

function setDate(datePickerId, date) {
    $('#' + datePickerId).get(0).value = date;
}

function clearDate(datePickerId) {
    $('#' + datePickerId).get(0).value = '';
}

/**
 * Adds an unchangeable entry into an autocomplete field.
 * @param autocompleteId the ID of the autocomplete field
 * @param checkboxId the ID of the checkbox
 * @param text the text to be placed in the autocomplete field
 * @param normalCode the key/code to be associated with the autocomplete field
 */
function typeAutocompleteAndDisable(autocompleteId, checkboxId, text, code) {
    var autoCompleter = $('#' + autocompleteId).get(0);
    var hiddenField= $('#' + autocompleteId + 'Hidden').get(0);

    if ($('#' + checkboxId).get(0).checked) {
        autoCompleter.value = text;
        hiddenField.value = code;
        disableAutocomplete(autocompleteId, text);
    } else {
        autoCompleter.value = '';
        hiddenField.value = '';
        $('#' + autocompleteId).autocomplete("enable");
        autoCompleter.disabled = false;
        var autoHiddenField = $('#' + autocompleteId + 'AutoHidden');
        if (autoHiddenField) {
          autoHiddenField.remove();
        }
    }
}

/**
 * Disable an autocompleter
 * @param autocompleteId the ID of the autocomplete field
 * @param text the text to be placed in the hidden field replacing the autocomplete field
 */
function disableAutocomplete(autocompleteId, text) {
    var autoCompleter = $('#' + autocompleteId).get(0);
    $('#' + autocompleteId).autocomplete("disable");
    autoCompleter.disabled = true;
    $('<input>').attr({
        type: 'hidden',
        id: autocompleteId + 'AutoHidden',
        name: autoCompleter.name,
        value : text
    }).appendTo('form');
}

/**
 * Disable an autocompleter if the associated checkbox is checked.
 * @param autocompleteId the ID of the autocomplete field
 * @param checkboxId the ID of the checkbox
 * @param text the text to be placed in the hidden field replacing the autocomplete field
 */
function disableAutocompleteIfChecked(autocompleteId, checkboxId, text) {
    if ($('#' + checkboxId).get(0).checked) {
        disableAutocomplete(autocompleteId, text);
    }
}


function pushAutocompleteValue(autocompleteId, text, code) {
    $('#' + autocompleteId).get(0).value = text;
    $('#' + autocompleteId + 'Hidden').get(0).value = code;
}

/**
 * Checks to see if an autocomplete field is empty,
 * and clears it of all remaining keys/values if so.
 * @param autocompleteId the ID of the autocomplete field.
 */
function checkAutocompleteAndClear(autocompleteId) {
    var autoCompleter = $('#' + autocompleteId).get(0);
    if (autoCompleter.value == null || autoCompleter.value == ""){
        $('#' + autocompleteId + 'Hidden').get(0).value = '';
    }
}

/**
 * Registers an element to toggle between enabled/disabled depending on whether one
 * or more of the indicated check boxes are checked. The target element will
 * be disabled or enabled based on the value of disableOnSelection. Additional elements
 * can be specified to listen for a click event.
 * @param disableElement Element to be disabled/enabled.
 * @param primaryElements Checkbox element(s) to be monitored.
 * @param listenerIds List of ids of additional elements for which a click will be listened.
 * @param disableOnSelection Should the element be disabled on selection? If true, it will be disabled on selection,
 * if false, it will be disabled if nothing is selected.
 */
function disableOnSelection(disableElement, primaryElements, listenerIds, disableOnSelection) {
    checkForDisableSelection(disableElement, primaryElements, disableOnSelection);
    var checkboxListener = function() {
      if (this.checked) {
          disableElement.disabled = disableOnSelection;
      } else {
          checkForDisableSelection(disableElement, primaryElements, disableOnSelection);
      }
    };
    for (i=0; i < primaryElements.length; i++) {
      $(primaryElements[i]).click(checkboxListener);
    }
    if (listenerIds) {
      for (i=0; i < listenerIds.length; i++) {
        if ($('#' + listenerIds[i]).get(0)) {
              $('#' + listenerIds[i]).click(function() {
                checkForDisableSelection(disableElement, primaryElements, disableOnSelection);
              });
        }
      }
    }
}

/**
 * Disables or enables an element based on the selection status of the
 * checkbox elements.
 * @param disableElement Element to be disabled/enabled.
 * @param checkElements Checkbox element(s) determining the enabled/disabled status.
 * @param disableOnSelection Should the element be disabled on selection? If true, it will be disabled on selection,
 * if false, it will be disabled if nothing is selected.
 */
function checkForDisableSelection(disableElement, checkElements, disableOnSelection) {
  if (disableElement) {
        for (i = 0; i < checkElements.length; i++) {
            if (checkElements[i].checked) {
                disableElement.disabled = disableOnSelection;
                return;
            }
        }
        disableElement.disabled = !disableOnSelection;
  }
}

function checkTextForDisable(disableElement, institutionAmountFieldMap) {
  if (disableElement) {
    for (var key in institutionAmountFieldMap) {
      var textElement = institutionAmountFieldMap[key];
      if (textElement && textElement.value && parseInt(textElement.value) != 0) {
        disableElement.disabled = false;
        return;
      }
    }
    disableElement.disabled = true;
  }
}



window.onload = highlightOnFocus;

/*******************************************************************************
 * Yes/No radio button tab functionality.
 ******************************************************************************/

/***
 * Creates yes/no radio tab functionality for the UL element with the given ID.
 * tabContainerId - ID of the div containing the tabs and the tab content.
 * tabListId - ID of the UL element containing radio button tabs.
 * tabPrefix - Prefix of the LI element IDs containing radio button tabs (to which YES/NO will be appended).
 * linkPrefix - Prefix of the div IDs to which radio buttons will link (to which YES/NO will be appended).
 * inconsistentFields - Mapping of LI ID to element IDs for which values will be cleared if the radio button is checked.
 */
function createYesNoRadioTab(tabContainerId, tabListId, tabPrefix, linkPrefix, inconsistentFields) {
    if ($('#' + tabContainerId).get(0)) {
        var yesNoOptions = ['YES','NO'];
        var tabLinks = {};
        for (i=0; i < yesNoOptions.length; i++) {
            tabLinks[tabPrefix + yesNoOptions[i]] = linkPrefix + yesNoOptions[i];
        }
        createRadioTab(tabContainerId, tabListId, tabLinks, inconsistentFields)
    }
}

/**
 * Creates radio tab functionality for the UL element with the given ID.
 * tabContainerId - ID of the div containing the tabs and the tab content.
 * tabListId - ID of the UL element containing radio button tabs.
 * @param tabLinkMap Map of LI element IDs to the ID of the div that should be displayed when the tab is selected
 * @param inconsistentFieldMap Mapping of LI ID to element IDs for which values will be cleared if the radio button
 * is checked.
 * @param inconsistentMultiSelectFieldMap Mapping of LI ID to element IDs of multiselect fields for which values
 * will be cleared if the radio button is checked.
 */
function createRadioTab(tabContainerId, tabListId, tabLinkMap, inconsistentFieldMap, inconsistentMultiSelectFieldMap) {
    if ($('#' + tabContainerId).get(0)) {
        initializeRadioTabs(tabContainerId,tabListId, tabLinkMap);
        if (inconsistentFieldMap) {
            clearRadioTabsOnClick(inconsistentFieldMap);
        }
        if (inconsistentMultiSelectFieldMap) {
            clearMultiSelectsOnClick(inconsistentMultiSelectFieldMap);
        }
    }
}

// Enable show/hide radio buttons
function initializeRadioTabs(tabContainerId, tabListId, tabLinkMap) {
    $('#' + tabContainerId).tabs();
    $('#' + tabContainerId).removeClass('ui-widget ui-widget-content');
    $('#' + tabListId).removeClass('ui-tabs-nav ui-widget-header');

    for (var key in tabLinkMap) {
      if ($('#' + key).length > 0) {
        $('#' + key).click(function(){
                $('#' + tabContainerId).tabs('select', tabLinkMap[this.id]);
            });
            if ($('#' + key).get(0).checked) {
                $('#' + tabContainerId).tabs('select', tabLinkMap[key]);
            }
      }
    }
}

// Clear form fields inconsistent with radio tab selections
function clearRadioTabsOnClick(clearFieldMap) {
    for (var key in clearFieldMap) {
      if ($('#' + key).length > 0) {
        $('#' + key).click(function() {
                for (i = 0; i < clearFieldMap[this.id].length; i++) {
                    document.getElementById(clearFieldMap[this.id][i]).value = '';
                }
            });
      }
    }
}

//Clear multiselect form fields inconsistent with radio tab selections
function clearMultiSelectsOnClick(clearFieldMap) {
    for (var key in clearFieldMap) {
      if ($('#' + key).length > 0) {
        $('#' + key).click(function() {
                for (i = 0; i < clearFieldMap[this.id].length; i++) {
                    $('#' + clearFieldMap[this.id][i]).multiselect('uncheckAll');
                }
            });
      }
    }
}

function removeFields(fieldsToRemove) {
    for (i = 0; i < fieldsToRemove.length; i++) {
        var element = $('#' + fieldsToRemove[i]).get(0);
        element.parentNode.removeChild(element);
    }
}

/**
 * Updates the input field value for the total number of specimens requested based on
 * the values in individual institution input fields.
 * @param institutionField The updated institution input field.
 * @param fieldCountMap Mapping of input field id to amount available for that field.
 * @param fieldByInstitutionMap Mapping of institution id to the request amount input field for the institution.
 * @param errorMessages Mapping of error types to corresponding error messages.
 */
function updateInstitutionRequest(institutionField, fieldCountMap, fieldByInstitutionMap, errorMessages) {
  if (institutionField.value) {
    var amountAvailable = fieldCountMap[institutionField.id];
    var amountRequested = parseInt(institutionField.value);
    if (isNaN(institutionField.value) || (institutionField.value.indexOf('.') > -1) || (amountRequested < 0)) {
      handleError('INVALID_TYPE',errorMessages,null);
      institutionField.value = "";
    } else if (amountRequested > amountAvailable) {
      handleError('MAX',errorMessages,[String(amountRequested), String(amountAvailable)]);
      institutionField.value = amountAvailable;
    }
  }
}

/**
 * Performs argument substitution on an error message and creates
 * an alert with the resulting message. Substitutions are performed on
 * strings of the format '{index}'.
 * @param errorType The type of error that occured.
 * @param errorMessages Mapping of error types to corresponding messages.
 * @param errorArgs List of replacement arguments, to be substituted by index.
 */
function handleError(errorType, errorMessages, errorArgs) {
  var msg = errorMessages[errorType];
  if (errorArgs) {
    for (var i = 0; i < errorArgs.length; i++) {
      var sub = '{' + i + '}';
      msg = msg.replace(sub,errorArgs[i]);
    }
  }
  alert(msg);
}

/**
 * toggle visibility of a div
 * @param divId the id of the div
 */
function toggleVisibility(divId) {
    $('#' + divId).toggle('blind', {}, 500);
}

function toggleFade(divId) {
    $('#' + divId).toggle('fade', {}, 500);
}

/*******************************************************************************
 * Different CSS depending on OS (mac/pc)- � Dynamic Drive
 * (www.dynamicdrive.com) This notice must stay intact for use Visit
 * http://www.dynamicdrive.com/ for full source code
 ******************************************************************************/

var csstype="external" //Specify type of CSS to use. "Inline" or "external"

var mac_externalcss = contextPath + '/styles/mac.css' //if "external", specify Mac css file here
var pc_externalcss = contextPath + '/styles/pc.css'   //if "external", specify PC/default css file here

///////No need to edit beyond here////////////

var ietest=navigator.appName.indexOf("Internet Explorer") != -1;
if (csstype=="inline") {
    document.write('<style type="text/css">');
    if (ietest) {
        document.write(mac_css);
    } else {
        document.write(pc_css);
    }
    document.write('</style>');
} else if (csstype=="external") {
    document.write('<link rel="stylesheet" type="text/css" href="'+ (ietest? pc_externalcss : mac_externalcss) +'">');
}


/**
 * Session timeout warning popup
 */
var pageLoadTime = new Date().getTime();
var lastActivityTime = null;
var sessionWarningPopupTimeout = null;
function showSessionWarningPopup() {
    if (lastActivityTime != null && lastActivityTime > pageLoadTime) {
        var timeDifference = lastActivityTime - pageLoadTime
        $.get(contextPath + '/ajax/resetSession.action');
        pageLoadTime = new Date().getTime();
        lastActivityTime = null;
        sessionWarningPopupTimeout = setTimeout('showSessionWarningPopup()', timeDifference);
    } else {
        showPopWin(contextPath + '/popup/sessionWarning.action', 500, 130, null);
        startSessionWarningTimer();
    }
}

function startSessionWarningTimer() {
    pageLoadTime = new Date().getTime();
    lastActivityTime = null;
    if (sessionWarningPopupTimeout != null) {
        clearTimeout(sessionWarningPopupTimeout);
    }
    var timeout = ((sessionTimeout * 60) - sessionWarningTime - 10) * 1000;
    sessionWarningPopupTimeout = setTimeout('showSessionWarningPopup()', timeout);
}

function resetSession() {
    $.get(contextPath + '/ajax/resetSession.action');
    startSessionWarningTimer();
    var popupFrame = document.getElementById('popupFrame');
    if (popupFrame && popupFrame.src.indexOf(contextPath + '/popup/sessionWarning.action') != -1) {
        hidePopWin(false);
    }
}

var timeLeft = sessionWarningTime;
function countdown() {
    document.getElementById('sessionWarningTime').innerHTML = getTimeLeftString(timeLeft);
    timeLeft--;
    if (timeLeft == 0) {
        window.top.location= contextPath + '/login/logout.action';
    } else {
        setTimeout('countdown()', 1000);
    }
}

function getTimeLeftString(remainingTime) {
    var minutesLeft = Math.floor(remainingTime / 60);
    var minutesLeftString = getTimeLeftStringPart(minutesLeft, sessionWarningMinute, sessionWarningMinutes);
    var secondsLeft = remainingTime % 60;
    var secondsLeftString = getTimeLeftStringPart(secondsLeft, sessionWarningSecond, sessionWarningSeconds);
    return $.trim(minutesLeftString + ' ' + secondsLeftString);
}

function getTimeLeftStringPart(timeLeftPart, singularLabel, pluralLabel) {
    if (timeLeftPart == 0) {
        return '';
    } else if (timeLeftPart == 1) {
        return '1 ' + singularLabel;
    } else {
        return timeLeftPart + ' ' + pluralLabel;
    }
}

function monitorActivity() {
    lastActivityTime = new Date().getTime();
}
