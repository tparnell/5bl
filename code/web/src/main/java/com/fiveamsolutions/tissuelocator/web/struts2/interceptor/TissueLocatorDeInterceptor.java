/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.web.struts2.interceptor;

import java.util.Map;

import com.fiveamsolutions.dynamicextensions.hibernate.DynamicExtensionsConfigurator;
import com.fiveamsolutions.dynamicextensions.struts2.converter.DynamicExtensionsTypeConverter;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

/**
 * Struts interceptor for converting dynamic extension fields populated by the params interceptor.
 * This interceptor should appear after every params interceptor in the interceptor stacks.
 * @author ddasgupta
 *
 */
public class TissueLocatorDeInterceptor extends AbstractInterceptor {

    private static final long serialVersionUID = -462864510362229601L;

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("PMD.SignatureDeclareThrowsException")
    public String intercept(ActionInvocation actionInvocation) throws Exception {
        DynamicExtensionsConfigurator configurator = TissueLocatorHibernateUtil.getDynamicExtensionConfigurator();
        Map<String, Object> conversionErrors = actionInvocation.getInvocationContext().getConversionErrors();
        new DynamicExtensionsTypeConverter(configurator, "com.fiveamsolutions.nci.commons.data.persistent",
                conversionErrors, TissueLocatorRegistry.getServiceLocator().getGenericService())
                .convertObject(actionInvocation.getAction());
        return actionInvocation.invoke();
    }
}
