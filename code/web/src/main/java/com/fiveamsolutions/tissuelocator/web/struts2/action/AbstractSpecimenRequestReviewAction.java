/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.struts2.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.validation.SkipValidation;

import com.fiveamsolutions.nci.commons.util.UsernameHolder;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.RequestStatus;
import com.fiveamsolutions.tissuelocator.data.ReviewComment;
import com.fiveamsolutions.tissuelocator.data.Shipment;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestReviewVote;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.Vote;
import com.fiveamsolutions.tissuelocator.service.InstitutionServiceLocal;
import com.fiveamsolutions.tissuelocator.service.SpecimenRequestServiceLocal;
import com.fiveamsolutions.tissuelocator.service.SpecimenRequestVoteEndDateCalculator;
import com.fiveamsolutions.tissuelocator.service.config.category.UiDynamicFieldCategoryServiceLocal;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceLocal;
import com.fiveamsolutions.tissuelocator.service.user.TissueLocatorUserServiceLocal;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.fiveamsolutions.tissuelocator.web.TissueLocatorContextParameterHelper;
import com.google.inject.Inject;
import com.opensymphony.xwork2.validator.annotations.CustomValidator;
import com.opensymphony.xwork2.validator.annotations.RequiredFieldValidator;
import com.opensymphony.xwork2.validator.annotations.ValidationParameter;
import com.opensymphony.xwork2.validator.annotations.Validations;

/**
 * @author smiller
 *
 */
public abstract class AbstractSpecimenRequestReviewAction extends AbstractSpecimenRequestAction {

    private static final long serialVersionUID = 1L;
    private static final String COMMENT_SAVED_PARAM = "commentSaved";

    private ReviewComment comment = new ReviewComment();
    private SpecimenRequestReviewVote consortiumReview;
    private SpecimenRequestReviewVote institutionalReview;
    private final Map<Institution, List<SpecimenRequestLineItem>> lineItemsByInstitution
        = new HashMap<Institution, List<SpecimenRequestLineItem>>();
    private Institution institution = new Institution();
    private final TissueLocatorUserServiceLocal userService;

    /**
     * Default constructor.
     */
    public AbstractSpecimenRequestReviewAction() {
        // Default constructor is unsupported, guice build using the other constructor.
        // this needs to be here so xml validation by struts passes.
        throw new UnsupportedOperationException();
    }

    /**
     * Default constructor.
     *
     * @param userService user service
     * @param appSettingService the application setting service
     * @param uiDynamicFieldCategoryService category service.
     */
    @Inject
    public AbstractSpecimenRequestReviewAction(TissueLocatorUserServiceLocal userService,
            ApplicationSettingServiceLocal appSettingService, 
            UiDynamicFieldCategoryServiceLocal uiDynamicFieldCategoryService) {
        super(appSettingService, uiDynamicFieldCategoryService);
        this.userService = userService;
    }

    /**
     * @return the userService
     */
    public TissueLocatorUserServiceLocal getUserService() {
        return userService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void prepare() {
        super.prepare();
        TissueLocatorUser user = getUserService().getByUsername(UsernameHolder.getUser());
        getComment().setUser(user);
        getComment().setDate(new Date());

        if (StringUtils.isNotBlank(ServletActionContext.getRequest().getParameter(COMMENT_SAVED_PARAM))) {
            addActionMessage(getText("specimenRequest.review.comments.success"));
        }
    }

    /**
     * delegate to the right review process.
     * @return string forward.
     */
    @SkipValidation
    public String delegate() {
        return getReviewProcess().getNamespace();
    }

    /**
     * Loads the comment form.
     * @return string forward.
     */
    @SkipValidation
    public String loadCommentForm() {
        return INPUT;
    }

    /**
     * View the read only summary of the request.
     * @return the struts forward
     */
    @SkipValidation
    public String viewSummary() {
        return "summary";
    }

    /**
     * add the vote to the set and save the object.
     * @return the struts forward.
     */
    @Validations(
            customValidators =
                @CustomValidator(type = "hibernate", fieldName = "consortiumReview",
                    parameters = { @ValidationParameter(name = "resourceKeyBase", value = "consortiumReview") }),
            requiredFields = @RequiredFieldValidator(fieldName = "consortiumReview.vote",
                    message = "Vote is required.")
    )
    public String saveConsortiumReview()  {
        TissueLocatorUser user = getUserService().getByUsername(UsernameHolder.getUser());
        getConsortiumReview().setDate(new Date());
        getConsortiumReview().setUser(user);
        getService().savePersistentObject(getObject());
        addActionMessage(getText("specimenRequest.review.consortium.success"));
        return INPUT;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected SpecimenRequestServiceLocal getService() {
        return TissueLocatorRegistry.getServiceLocator().getSpecimenRequestService();
    }

    /**
     * @return the comment
     */
    public ReviewComment getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(ReviewComment comment) {
        this.comment = comment;
    }

    /**
     * @return the consortiumReview
     */
    public SpecimenRequestReviewVote getConsortiumReview() {
        return consortiumReview;
    }

    /**
     * @param consortiumReview the consortiumReview to set
     */
    public void setConsortiumReview(SpecimenRequestReviewVote consortiumReview) {
        this.consortiumReview = consortiumReview;
    }

    /**
     * Get the voting end date.
     *
     * @return the date
     */
    public Date getVotingEndDate() {
        SpecimenRequestVoteEndDateCalculator calculator = new SpecimenRequestVoteEndDateCalculator();
        return calculator.getVotingPeriodEndDate(getObject().getUpdatedDate(), TissueLocatorContextParameterHelper
                .getVotingPeriod(ServletActionContext.getServletContext()));
    }

    /**
     * Load the institutional review for the current user.
     * @param user Current user.
     */
    protected void loadInstitutionalReview(TissueLocatorUser user) {
        for (SpecimenRequestReviewVote curVote : getObject().getInstitutionalReviews()) {
            if (user.getInstitution().equals(curVote.getInstitution())) {
                setInstitutionalReview(curVote);
            }
        }
    }

    /**
     * @return the institutionalReview
     */
    public SpecimenRequestReviewVote getInstitutionalReview() {
        return institutionalReview;
    }

    /**
     * @param institutionalReview the institutionalReview to set
     */
    public void setInstitutionalReview(SpecimenRequestReviewVote institutionalReview) {
        this.institutionalReview = institutionalReview;
    }

    /**
     * Populates a map of line items by institution.
     */
    protected void populateLineItemsMaps() {
        List<SpecimenRequestLineItem> lineItems = new ArrayList<SpecimenRequestLineItem>();
        lineItems.addAll(getObject().getLineItems());

        for (Shipment shipment : getObject().getOrders()) {
            lineItems.addAll(shipment.getLineItems());
        }

        for (SpecimenRequestLineItem li : lineItems) {
            Institution i = li.getSpecimen().getExternalIdAssigner();
            List<SpecimenRequestLineItem> lis = lineItemsByInstitution.get(i);
            if (lis == null) {
                lis = new ArrayList<SpecimenRequestLineItem>();
            }
            lis.add(li);
            lineItemsByInstitution.put(i, lis);
        }
    }

    /**
     * View the line items for a specific institution.
     * @return the struts forward
     */
    @SkipValidation
    public String viewLineItems() {
        InstitutionServiceLocal isl = TissueLocatorRegistry.getServiceLocator().getInstitutionService();
        setInstitution(isl.getPersistentObject(Institution.class, getInstitution().getId()));
        return "viewLineItems";
    }

    /**
     * @return the institution to display line items for
     */
    public Institution getInstitution() {
        return institution;
    }

    /**
     * @param inst the institution to set
     */
    public void setInstitution(Institution inst) {
        institution = inst;
    }

    /**
     * Gets the line items by specified institution.
     * @param inst The institution to get line items for
     * @return the line items
     */
    public List<SpecimenRequestLineItem> getLineItemsByInstitution(Institution inst) {
        return lineItemsByInstitution.get(inst);
    }

    /**
     * get the allowable votes for an instiutional review.
     * @return the votes
     */
    public List<Vote> getInstiutionalReviewAllowableVotes() {
        List<Vote> votes = new ArrayList<Vote>();
        votes.add(Vote.APPROVE);
        votes.add(Vote.DENY);
        return votes;
    }

    /**
     * @return the id of the object that will be redirected to
     */
    public Long getObjectId() {
        return getObject().getId();
    }

    /**
     * @return the status of the object that will be redirected to
     */
    public RequestStatus getObjectStatus() {
        return getObject().getStatus();
    }
}
