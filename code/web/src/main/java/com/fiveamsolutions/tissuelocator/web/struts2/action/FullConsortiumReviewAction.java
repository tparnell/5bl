/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.struts2.action;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.mail.MessagingException;
import javax.servlet.ServletContext;

import org.apache.struts2.ServletActionContext;

import com.fiveamsolutions.nci.commons.data.security.AbstractUser;
import com.fiveamsolutions.nci.commons.util.UsernameHolder;
import com.fiveamsolutions.tissuelocator.data.Role;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestReviewVote;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.service.FullConsortiumReviewServiceLocal;
import com.fiveamsolutions.tissuelocator.service.config.category.UiDynamicFieldCategoryServiceLocal;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceLocal;
import com.fiveamsolutions.tissuelocator.service.user.TissueLocatorUserServiceLocal;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.fiveamsolutions.tissuelocator.web.TissueLocatorContextParameterHelper;
import com.google.inject.Inject;
import com.opensymphony.xwork2.validator.annotations.CustomValidator;
import com.opensymphony.xwork2.validator.annotations.RequiredFieldValidator;
import com.opensymphony.xwork2.validator.annotations.ValidationParameter;
import com.opensymphony.xwork2.validator.annotations.Validations;

/**
 * @author smiller
 *
 */
public class FullConsortiumReviewAction extends AbstractSpecimenRequestReviewAction {

    private static final long serialVersionUID = 1L;

    private final Set<AbstractUser> reviewers = new HashSet<AbstractUser>();


    /**
     * Default constructor.
     */
    public FullConsortiumReviewAction() {
        // Default constructor is unsupported, guice build using the other constructor.
        // this needs to be here so xml validation by struts passes.
        throw new UnsupportedOperationException();
    }

    /**
     * Default constructor.
     *
     * @param userService user service
     * @param appSettingService the application setting service
     * @param uiDynamicFieldCategoryService category service.
     */
    @Inject
    public FullConsortiumReviewAction(TissueLocatorUserServiceLocal userService,
            ApplicationSettingServiceLocal appSettingService, 
            UiDynamicFieldCategoryServiceLocal uiDynamicFieldCategoryService) {
        super(userService, appSettingService, uiDynamicFieldCategoryService);
        setObject(new SpecimenRequest());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void prepare() {
        super.prepare();
        TissueLocatorUser user = getUserService().getByUsername(UsernameHolder.getUser());

        // load the correct consortium review
        for (SpecimenRequestReviewVote curVote : getObject().getConsortiumReviews()) {
            if (user.getInstitution().equals(curVote.getInstitution())) {
                setConsortiumReview(curVote);
            }
        }

        // load the correct institutional review
        loadInstitutionalReview(user);

        setAvailableReviewers(user);
        populateLineItemsMaps();
    }

    private void setAvailableReviewers(TissueLocatorUser loggedInUser) {
        // get the consortium reviewers for the current user's institution
        Set<AbstractUser> users = getUserService().getUsersInRole(Role.CONSORTIUM_REVIEW_VOTER.getName());
        // Make sure that the requestor cannot be assigned as a reviewer
        if (users != null) {
            users.remove(getObject().getRequestor());
        }
        for (AbstractUser reviewer : users) {
            if  (((TissueLocatorUser) reviewer).getInstitution().equals(loggedInUser.getInstitution())) {
                getReviewers().add(reviewer);
            }
        }
    }

    /**
     * assign a scientific reviewer.
     * @return the struts forward.
     * @throws MessagingException on error
     */
    @Validations(requiredFields = @RequiredFieldValidator(fieldName = "consortiumReview.user",
                    message = "User is required."))
    public String assignScientificReviewer() throws MessagingException {
        ServletContext sc = ServletActionContext.getServletContext();
        int votingPeriod = TissueLocatorContextParameterHelper.getVotingPeriod(sc);
        int reviewPeriod = TissueLocatorContextParameterHelper.getReviewPeriod(sc);
        getReviewService().assignRequest(getObject(), getConsortiumReview().getUser(), votingPeriod, reviewPeriod);
        addActionMessage(getText("specimenRequest.review.assigner.success"));
        return INPUT;
    }

    /**
     * add the vote to the set and save the object.
     * @return the struts forward.
     */
    @Validations(
            customValidators =
                @CustomValidator(type = "hibernate", fieldName = "institutionalReview",
                    parameters = { @ValidationParameter(name = "resourceKeyBase", value = "institutionalReview") }),
            requiredFields = @RequiredFieldValidator(fieldName = "institutionalReview.vote",
                    message = "Vote is required.")
    )
    public String saveInstitutionalReview()  {
        TissueLocatorUser user = getUserService().getByUsername(UsernameHolder.getUser());
        getInstitutionalReview().setDate(new Date());
        getInstitutionalReview().setUser(user);
        getReviewService().savePersistentObject(getObject());
        addActionMessage(getText("specimenRequest.review.institutional.success"));
        return INPUT;
    }

    /**
     * Adds the comment.
     * @return string forward.
     * @throws MessagingException on error
     */
    @Validations(
            customValidators =
                @CustomValidator(type = "hibernate", fieldName = "comment",
                    parameters = { @ValidationParameter(name = "resourceKeyBase", value = "comment") })
    )
    public String addComment() throws MessagingException {
        getComment().setDate(new Date());
        getObject().getComments().add(getComment());
        ServletContext sc = ServletActionContext.getServletContext();
        int votingPeriod = TissueLocatorContextParameterHelper.getVotingPeriod(sc);
        getReviewService().addComment(getObject(), votingPeriod);
        return SUCCESS;
    }

    /**
     * Finalizes the vote.
     * @return string forward.
     * @throws MessagingException on error
     */
    @Validations(requiredFields = @RequiredFieldValidator(fieldName = "object.externalComment", message = "",
            key = "specimenRequest.review.finalComment.required"))
    public String finalizeVote() throws MessagingException {
        getReviewService().finalizeVote(getObject(), true);
        addActionMessage(getText("specimenRequest.review.finalComment.success"));
        return list();
    }

    /**
     * @return the reviewers
     */
    public Set<AbstractUser> getReviewers() {
        return reviewers;
    }

    private FullConsortiumReviewServiceLocal getReviewService() {
        return TissueLocatorRegistry.getServiceLocator().getFullConsortiumReviewService();
    }
}
