/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.web.struts2.action.mta;

import java.util.List;

import org.apache.commons.lang.BooleanUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.fiveamsolutions.nci.commons.data.search.PageSortParams;
import com.fiveamsolutions.nci.commons.search.SearchCriteria;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreement;
import com.fiveamsolutions.tissuelocator.service.InstitutionServiceLocal;
import com.fiveamsolutions.tissuelocator.service.SignedMaterialTransferAgreementServiceLocal;
import com.fiveamsolutions.tissuelocator.service.search.InstitutionSortCriterion;
import com.fiveamsolutions.tissuelocator.service.search.TissueLocatorAnnotatedBeanSearchCriteria;
import com.fiveamsolutions.tissuelocator.service.user.TissueLocatorUserServiceLocal;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.google.inject.Inject;
import com.opensymphony.xwork2.validator.annotations.CustomValidator;
import com.opensymphony.xwork2.validator.annotations.RequiredFieldValidator;
import com.opensymphony.xwork2.validator.annotations.ValidationParameter;
import com.opensymphony.xwork2.validator.annotations.Validations;

/**
 * @author ddasgupta
 *
 */
@Namespace("/admin/signedMta/review")
@Results(value = {
                @Result(name = "success", location = "review.jsp"),
                @Result(name = "input", location = "review.jsp"),
                @Result(name = "reviewSuccess", type = "redirect", location = "/admin/signedMta/list.action")
        }
)
public class MtaReviewAction extends MtaSubmissionAction {

    private static final long serialVersionUID = -7388929855145339760L;

    private Boolean correctInstitution;
    private Boolean valid;
    private Boolean replaceExisting;
    private Institution institution = new Institution();
    private List<Institution> institutions;

    /**
     * Injected constructor.
     * 
     * @param userService user service
     */
    @Inject
    public MtaReviewAction(TissueLocatorUserServiceLocal userService) {
        super(userService);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void prepare() {
        super.prepare();
        if (getSignedMta().getId() != null) {
            SignedMaterialTransferAgreementServiceLocal service =
                TissueLocatorRegistry.getServiceLocator().getSignedMaterialTransferAgreementService();
            setSignedMta(service.getPersistentObject(SignedMaterialTransferAgreement.class, getSignedMta().getId()));
        }

        InstitutionServiceLocal service = TissueLocatorRegistry.getServiceLocator().getInstitutionService();
        if (getInstitution().getId() != null) {
            setInstitution(service.getPersistentObject(Institution.class, getInstitution().getId()));
        } else {
            setInstitution(getSignedMta().getReceivingInstitution());
        }

        SearchCriteria<Institution> sc = new TissueLocatorAnnotatedBeanSearchCriteria<Institution>(new Institution());
        PageSortParams<Institution> psp =
            new PageSortParams<Institution>(service.count(sc), 0, InstitutionSortCriterion.NAME, false);
        setInstitutions(service.search(sc, psp));

    }

    /**
     * review the signed MTA.
     * @return an action forward
     */
    @Validations(
            customValidators = {
                    @CustomValidator(type = "hibernate", fieldName = "institution.mtaContact", parameters = {
                            @ValidationParameter(name = "resourceKeyBase", value = "signedMta.review.mtaContact"),
                            @ValidationParameter(name = "conditionalExpression",
                                    value = "(valid != null && valid) "
                                    + " || (replaceExisting != null && !replaceExisting)") })
            },
            requiredFields = {
                    @RequiredFieldValidator(fieldName = "correctInstitution", message = "",
                            key = "signedMta.review.correctInstitution.required"),
                    @RequiredFieldValidator(fieldName = "institution", message = "",
                            key = "signedMta.review.institution.required")
            }
    )
    @Action("review")
    public String review() {
        if (BooleanUtils.isFalse(getValid()) && BooleanUtils.isNotFalse(getReplaceExisting())
                && getInstitution().getMtaContact().getId() == null) {
            getInstitution().setMtaContact(null);
        }
        TissueLocatorRegistry.getServiceLocator().getSignedMaterialTransferAgreementService()
            .reviewSignedMta(getSignedMta(), getInstitution(), BooleanUtils.toBoolean(getValid()),
                    BooleanUtils.toBoolean(getReplaceExisting()));
        addActionMessage(getText("signedMta.review.success"));
        return "reviewSuccess";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected SearchCriteria<Institution> getSearchCriteria() {
        return new TissueLocatorAnnotatedBeanSearchCriteria<Institution>(getObject());
    }

    /**
     * @return the correctInstitution
     */
    public Boolean getCorrectInstitution() {
        return correctInstitution;
    }

    /**
     * @param correctInstitution the correctInstitution to set
     */
    public void setCorrectInstitution(Boolean correctInstitution) {
        this.correctInstitution = correctInstitution;
    }

    /**
     * @return the valid
     */
    public Boolean getValid() {
        return valid;
    }

    /**
     * @param valid the valid to set
     */
    public void setValid(Boolean valid) {
        this.valid = valid;
    }

    /**
     * @return the replaceExisting
     */
    public Boolean getReplaceExisting() {
        return replaceExisting;
    }

    /**
     * @param replaceExisting the replaceExisting to set
     */
    public void setReplaceExisting(Boolean replaceExisting) {
        this.replaceExisting = replaceExisting;
    }

    /**
     * @return the institution
     */
    public Institution getInstitution() {
        return institution;
    }

    /**
     * @param institution the institution to set
     */
    public void setInstitution(Institution institution) {
        this.institution = institution;
    }

    /**
     * @return the institutions
     */
    public List<Institution> getInstitutions() {
        return institutions;
    }

    /**
     * @param institutions the institutions to set
     */
    public void setInstitutions(List<Institution> institutions) {
        this.institutions = institutions;
    }
}
