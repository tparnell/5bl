/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.struts2.action;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.struts2.util.SubsetIteratorFilter.Decider;

import com.fiveamsolutions.nci.commons.data.search.GroupByCriteria;
import com.fiveamsolutions.nci.commons.data.search.PageSortParams;
import com.fiveamsolutions.nci.commons.search.SearchCriteria;
import com.fiveamsolutions.nci.commons.web.displaytag.PaginatedList;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.Participant;
import com.fiveamsolutions.tissuelocator.data.SavedSpecimenSearch;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.SpecimenStatus;
import com.fiveamsolutions.tissuelocator.data.config.category.SearchResultFieldDisplaySetting;
import com.fiveamsolutions.tissuelocator.data.config.category.UiSearchFieldCategory;
import com.fiveamsolutions.tissuelocator.data.config.search.AbstractSearchFieldConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.SearchSetType;
import com.fiveamsolutions.tissuelocator.data.config.search.SelectConfig;
import com.fiveamsolutions.tissuelocator.service.config.category.SearchResultDisplaySettingsServiceLocal;
import com.fiveamsolutions.tissuelocator.service.config.category.UiDynamicFieldCategoryServiceLocal;
import com.fiveamsolutions.tissuelocator.service.config.category.UiSearchFieldCategoryServiceLocal;
import com.fiveamsolutions.tissuelocator.service.config.category.UiSectionServiceLocal;
import com.fiveamsolutions.tissuelocator.service.config.search.SearchFieldConfigServiceLocal;
import com.fiveamsolutions.tissuelocator.service.search.AggregateSpecimenSearchCriteria;
import com.fiveamsolutions.tissuelocator.service.search.RangeSearchCondition;
import com.fiveamsolutions.tissuelocator.service.search.RangeSearchParameterAccessor;
import com.fiveamsolutions.tissuelocator.service.search.SavedSpecimenSearchServiceLocal;
import com.fiveamsolutions.tissuelocator.service.search.SpecimenGroupCriterion;
import com.fiveamsolutions.tissuelocator.service.search.SpecimenSearchServiceLocal;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceLocal;
import com.fiveamsolutions.tissuelocator.service.user.TissueLocatorUserServiceLocal;
import com.fiveamsolutions.tissuelocator.web.TissueLocatorRequestHelper;
import com.google.inject.Inject;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.util.CreateIfNull;

/**
 * @author ddasgupta
 *
 */
@SuppressWarnings({ "PMD.TooManyFields", "PMD.TooManyMethods", "PMD.ExcessiveClassLength" })
public class SpecimenSearchAction extends SpecimenAction implements RangeSearchParameterAccessor, SessionAware {

    private static final long serialVersionUID = 8847328312185625857L;
    private static final String SEPARATOR = "; ";
    private final SpecimenSearchActionHelper specimenSearchActionHelper;
    private final SearchResultDisplayConfigHelper resultDisplayConfigHelper;
    private SavedSpecimenSearchServiceLocal savedSpecimenSearchService;
    private UiSearchFieldCategoryServiceLocal uiSearchFieldCategoryService;
    private SearchFieldConfigServiceLocal searchFieldConfigService;
    private List<SpecimenGroupCriterion> groupByCriterias;

    private SearchSetType searchSetType = SearchSetType.ADVANCED;
    private boolean simpleSearchEnabled;
    private Collection<Institution> institutionsBelowMinimumResultsCount = new ArrayList<Institution>();
    private boolean displayUsageRestrictions;
    private boolean displayRequestProcessSteps;

    private PaginatedList<SpecimenGroupedByInstitution> aggregateList;
    
    private List<UiSearchFieldCategory> uiSearchFieldCategories;
    private boolean startPopupOnLoad = false;

    /**
     * default constructor.
     */
    public SpecimenSearchAction() {
        // Default constructor is unsupported, guice build using the other constructor.
        // this needs to be here so xml validation by struts passes.
        throw new UnsupportedOperationException();
    }

    /**
     * Default constructor.
     *
     * @param specimenSearchService the search service.
     * @param userService the user service
     * @param appSettingService the application setting service
     * @param savedSpecimenSearchService the saved specimen search service
     * @param uiSearchFieldCategoryService search field category service.
     * @param searchResultDisplaySettingService search result display setting service.
     * @param uiSectionService ui section service.
     * @param searchFieldConfigService search field config service.
     * @param uiDynamicFieldCategoryService category service.
     */
    @Inject
    @SuppressWarnings("PMD.ExcessiveParameterList")
    //CHECKSTYLE:OFF - too many parameters
    public SpecimenSearchAction(SpecimenSearchServiceLocal specimenSearchService,
            TissueLocatorUserServiceLocal userService,
            ApplicationSettingServiceLocal appSettingService,
            SavedSpecimenSearchServiceLocal savedSpecimenSearchService,
            UiSearchFieldCategoryServiceLocal uiSearchFieldCategoryService,
            SearchResultDisplaySettingsServiceLocal searchResultDisplaySettingService,
            UiSectionServiceLocal uiSectionService,
            SearchFieldConfigServiceLocal searchFieldConfigService, UiDynamicFieldCategoryServiceLocal uiDynamicFieldCategoryService) {
    //CHECKSTYLE:ON
        super(specimenSearchService, userService, appSettingService, uiDynamicFieldCategoryService, uiSectionService);
        this.savedSpecimenSearchService = savedSpecimenSearchService;
        this.uiSearchFieldCategoryService = uiSearchFieldCategoryService;
        this.searchFieldConfigService = searchFieldConfigService;
        specimenSearchActionHelper =
            new SpecimenSearchActionHelper(getDynamicFieldDefinitions(),
                SpecimenSearchAction.class.getSimpleName());
        resultDisplayConfigHelper =
            new SearchResultDisplayConfigHelper(userService, uiSearchFieldCategoryService,
                    searchResultDisplaySettingService);
    }

    /**
     * Default constructor.
     *
     * @param specimenSearchService the search service.
     * @param sessionParamDiscriminator the discriminator to use for session params.
     * @param userService the user service
     * @param appSettingService the application setting service
     * @param savedSpecimenSearchService the saved specimen search service
     * @param uiSearchFieldCategoryService search field category service.
     * @param searchResultDisplaySettingService searh result display setting service.
     * @param uiSectionService ui section service.
     * @param searchFieldConfigService search field config service.
     * @param uiDynamicFieldCategoryService category service.
     */
    @SuppressWarnings("PMD.ExcessiveParameterList")
    //CHECKSTYLE:OFF - too many parameters
    public SpecimenSearchAction(SpecimenSearchServiceLocal specimenSearchService,
            String sessionParamDiscriminator,
            TissueLocatorUserServiceLocal userService,
            ApplicationSettingServiceLocal appSettingService,
            SavedSpecimenSearchServiceLocal savedSpecimenSearchService,
            UiSearchFieldCategoryServiceLocal uiSearchFieldCategoryService,
            SearchResultDisplaySettingsServiceLocal searchResultDisplaySettingService,
            UiSectionServiceLocal uiSectionService,
            SearchFieldConfigServiceLocal searchFieldConfigService, UiDynamicFieldCategoryServiceLocal uiDynamicFieldCategoryService) {
    //CHECKSTYLE:ON
        super(specimenSearchService, userService, appSettingService, uiDynamicFieldCategoryService, uiSectionService);
        this.savedSpecimenSearchService = savedSpecimenSearchService;
        this.uiSearchFieldCategoryService = uiSearchFieldCategoryService;
        this.searchFieldConfigService = searchFieldConfigService;
        specimenSearchActionHelper =
            new SpecimenSearchActionHelper(getDynamicFieldDefinitions(),
                sessionParamDiscriminator);
        resultDisplayConfigHelper =
            new SearchResultDisplayConfigHelper(userService, uiSearchFieldCategoryService,
                    searchResultDisplaySettingService);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected SearchCriteria<Specimen> getSearchCriteria() {
        return specimenSearchActionHelper.getSearchCriteria(getObject());
    }

    /**
     *
     * @author bhumphrey
     *
     */
    public static class SpecimenGroupedByInstitution implements Serializable {
        /**
         *
         */
        private static final long serialVersionUID = 1L;
        private final Institution institution;
        private final Long count;

        /**
         * parameterized constructor.
         *
         * @param institution the institution
         * @param count the count
         */
        public SpecimenGroupedByInstitution(Institution institution, Long count) {
            this.institution = institution;
            this.count = count;
        }

        /**
         * @return the institution
         */
        public Institution getInstitution() {
            return institution;
        }

        /**
         * @return the count
         */
        public Long getCount() {
            return count;
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSession(Map<String, Object> s) {
        resultDisplayConfigHelper.setSession(s);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String filter() {
        specimenSearchActionHelper.storeSearchParamsInSession(getObject());
        specimenSearchActionHelper.setAggregateResults(isAggregateResults());
        getObject().setStatus(SpecimenStatus.AVAILABLE);
        setDiseaseProperties();
        initSearchConfigValues();
        if (isAggregateResults()) {
            final SpecimenSearchServiceLocal service = getSearchService();
            final AggregateSpecimenSearchCriteria searchCriteria =
                (AggregateSpecimenSearchCriteria) getSearchCriteria();
            PageSortParams<Specimen> pageSortParams = setupPageSortParams(searchCriteria, service, aggregateList);
            final Map<Institution, Long> aggregateResultMap = service.aggregate(searchCriteria, pageSortParams,
                    getGroupByCriteria());
            List<SpecimenGroupedByInstitution> list = new ArrayList<SpecimenGroupedByInstitution>();
            if (aggregateResultMap != null) {
                final Set<Entry<Institution, Long>> entrySet = aggregateResultMap.entrySet();
                for (Entry<Institution, Long> entry : entrySet) {
                    list.add(new SpecimenGroupedByInstitution(entry.getKey(), entry.getValue()));
                }
            }
            Collections.sort(list, getComparator());
            aggregateList.setList(list);
            aggregateList.setFullListSize(list.size());
            setMinimumCountResults();
            return "successGroup";
        } else {
            return super.filter();
        }
    }

    private Comparator<SpecimenGroupedByInstitution> getComparator() {
        return new Comparator<SpecimenGroupedByInstitution>() {
            @Override
            public int compare(SpecimenGroupedByInstitution i1, SpecimenGroupedByInstitution i2) {
                return i1.getInstitution().getName().compareTo(i2.getInstitution().getName());
            }
        };
    }

    private void setMinimumCountResults() {
        if (getMinimumAggregateResultsDisplayed() > 0) {
            AggregateSpecimenSearchCriteria searchCriteria = (AggregateSpecimenSearchCriteria) getSearchCriteria();
            searchCriteria.setMaximumCountThreshold(getMinimumAggregateResultsDisplayed() - 1);
            Map<Institution, Long> belowCountMap = getSearchService().aggregate(searchCriteria, null,
                    getGroupByCriteria());
            if (belowCountMap != null) {
                setInstitutionsBelowMinimumResultsCount(belowCountMap.keySet());
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PaginatedList<?> getObjects() {
        if (isAggregateResults()) {
            return aggregateList;
        } else {
            return super.getObjects();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getInputResult() {
        if (isAggregateResults()) {
            return "inputGroup";
        } else {
            return super.getInputResult();
        }
    }

    /**
     * {@inheritDoc}
     */
    protected List<? extends GroupByCriteria<Specimen>> getGroupByCriteria() {
        if (isAggregateResults()) {
            return groupByCriterias;
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void setPaginatedSearchHelper(PageSortParams<Specimen> pageSortParams,
            SearchCriteria<Specimen> criteria, int count) {
        if (!isAggregateResults()) {
            super.setPaginatedSearchHelper(pageSortParams, criteria, count);
        }
    }

    /**
     * Initiates a search with no session params retained.
     *
     * @return struts forward.
     */
    public String refreshList() {
        specimenSearchActionHelper.removeSearchParamsFromSession();
        return list();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String list() {
        setDiseaseProperties();

        if (specimenSearchActionHelper.loadSearchParamsFromSession()) {
            initSearchConfigValues();
            getObject().setStatus(SpecimenStatus.AVAILABLE);
            return super.list();
        }
        initSearchConfigValues();
        return isAggregateResults() ? "successGroup" : Action.SUCCESS;
    }

    private void setDiseaseProperties() {
        if (getObject().getPathologicalCharacteristic() != null) {
            setDiseaseSelection(getObject().getPathologicalCharacteristic().getName());
            if (getObject().getPathologicalCharacteristic().getId() != null) {
                setNormalSample(ObjectUtils.equals(getObject().getPathologicalCharacteristic().getId().toString(),
                        getNormalSampleId()));
            }

        }
    }

    /**
     * just store the search, don't execute it.
     * @return the struts return
     */
    public String storeSearch() {
        String attributeName = getClass().getSimpleName() + EXAMPLE_SUFFIX;
        ServletActionContext.getRequest().getSession().setAttribute(attributeName, getObject());
        specimenSearchActionHelper.storeSearchParamsInSession(getObject());
        startPopupOnLoad = true;
        return isAggregateResults() ? "successGroup" : Action.SUCCESS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void prepare() {
        super.prepare();
        Specimen s = new Specimen();
        s.setParticipant(new Participant());
        setObject(s);

        String specimenGrouping = getAppSettingService().getSpecimenGrouping();
        if (StringUtils.isNotEmpty(specimenGrouping)) {
            setGroupByCriteria(specimenGrouping);
        }

        aggregateList = new PaginatedList<SpecimenGroupedByInstitution>();
        setSimpleSearchEnabled(getAppSettingService().isSimpleSearchEnabled());
        setMinimumAggregateResultsDisplayed(getAppSettingService().getMinimumAggregateResultsDisplayed());
        setDisplayUsageRestrictions(getAppSettingService().isDisplayUsageRestrictions());
        setDisplayRequestProcessSteps(getAppSettingService().isDisplayRequestProcessSteps());
        specimenSearchActionHelper.initSearchConfigs(getSearchSetType(), searchFieldConfigService);
        initializeUiSearchFieldCategories();
        resultDisplayConfigHelper.initializeCategoriesList(getSearchSetType());
    }

    private void initSearchConfigValues() {
        Map<AbstractSearchFieldConfig, Object> configValueMap = new HashMap<AbstractSearchFieldConfig, Object>();
        for (AbstractSearchFieldConfig config : getSearchConfigs()) {
            configValueMap.putAll(getSearchConfigValues(config));
        }
        TissueLocatorRequestHelper.setSearchConfigValueMap(ServletActionContext.getRequest(), configValueMap);
    }

    private Map<AbstractSearchFieldConfig, Object> getSearchConfigValues(AbstractSearchFieldConfig config) {
        if (config instanceof SelectConfig) {
            SelectConfig selectConfig = (SelectConfig) config;
            if (selectConfig.isMultiSelectCollection()) {
                return config.getValues(this, "multiSelectCollectionParamMap");
            } else if (selectConfig.isMultiSelect()) {
                return config.getValues(this, "multiSelectParamMap");
            }
        }
        return config.getValues(this, null);
    }

    /**
     * @param specimenGrouping a string with enum names separated by commas
     */
    private void setGroupByCriteria(String specimenGrouping) {
        groupByCriterias = new ArrayList<SpecimenGroupCriterion>();
        for (String property : specimenGrouping.split(",")) {
            groupByCriterias.add(SpecimenGroupCriterion.valueOf(property));
        }
    }

    /**
     * @return the minimumAvailableQuantity
     */
    public BigDecimal getMinimumAvailableQuantity() {
        return specimenSearchActionHelper.getMinimumAvailableQuantity();
    }

    /**
     * @param minimumAvailableQuantity the minimumAvailableQuantity to set
     */
    public void setMinimumAvailableQuantity(BigDecimal minimumAvailableQuantity) {
        specimenSearchActionHelper.setMinimumAvailableQuantity(minimumAvailableQuantity);
    }

    /**
     * @return the searchConfigs
     */
    public Collection<AbstractSearchFieldConfig> getSearchConfigs() {
        return specimenSearchActionHelper.getSearchConfigs();
    }

    /**
     * @return the searchSetType
     */
    public SearchSetType getSearchSetType() {
        if (searchSetType == null) {
            searchSetType = SearchSetType.ADVANCED;
        }
        return searchSetType;
    }

    /**
     * @param searchSetType the searchSetType to set
     */
    public void setSearchSetType(SearchSetType searchSetType) {
        this.searchSetType = searchSetType;
    }

    /**
     * @return the Whether simple search is enabled.
     */
    public boolean isSimpleSearchEnabled() {
        return simpleSearchEnabled;
    }

    /**
     * @param simpleSearchEnabled the simpleSearchEnabled to set
     */
    public void setSimpleSearchEnabled(boolean simpleSearchEnabled) {
        this.simpleSearchEnabled = simpleSearchEnabled;
    }

    /**
     * @return the minimumAggregateResultsDisplayed
     */
    public int getMinimumAggregateResultsDisplayed() {
        return specimenSearchActionHelper.getMinimumAggregateResultsDisplayed();
    }

    /**
     * @param minimumAggregateResultsDisplayed the minimumAggregateResultsDisplayed to set
     */
    public void setMinimumAggregateResultsDisplayed(int minimumAggregateResultsDisplayed) {
        specimenSearchActionHelper.setMinimumAggregateResultsDisplayed(minimumAggregateResultsDisplayed);
    }

    /**
     * @return the institutionsBelowMinimumResultsCount
     */
    public Collection<Institution> getInstitutionsBelowMinimumResultsCount() {
        return institutionsBelowMinimumResultsCount;
    }

    /**
     * @param institutionsBelowMinimumResultsCount the institutionsBelowMinimumResultsCount to set
     */
    public void setInstitutionsBelowMinimumResultsCount(Collection<Institution> institutionsBelowMinimumResultsCount) {
        this.institutionsBelowMinimumResultsCount = institutionsBelowMinimumResultsCount;
    }

    /**
     * @return the displayUsageRestrictions
     */
    public boolean isDisplayUsageRestrictions() {
        return displayUsageRestrictions;
    }

    /**
     * @param displayUsageRestrictions the displayUsageRestrictions to set
     */
    public void setDisplayUsageRestrictions(boolean displayUsageRestrictions) {
        this.displayUsageRestrictions = displayUsageRestrictions;
    }

    /**
     * @return the displayRequestProcessSteps
     */
    public boolean isDisplayRequestProcessSteps() {
        return displayRequestProcessSteps;
    }

    /**
     * @param displayRequestProcessSteps the displayRequestProcessSteps to set
     */
    public void setDisplayRequestProcessSteps(boolean displayRequestProcessSteps) {
        this.displayRequestProcessSteps = displayRequestProcessSteps;
    }

    /**
     * @return Whether the current search is a prospective collection search.
     */
    public boolean isSimpleSearch() {
        return getSearchSetType().equals(SearchSetType.SIMPLE);
    }

    /**
     * @param simpleSearch Whether the current search is a prospective collection search.
     */
    public void setSimpleSearch(boolean simpleSearch) {
        setSearchSetType(simpleSearch ? SearchSetType.SIMPLE : SearchSetType.ADVANCED);
    }

    /**
     * @return the startPopupOnLoad
     */
    public boolean isStartPopupOnLoad() {
        return startPopupOnLoad;
    }

    /**
     * @return a read only version of the search criteria
     */
    public String getCriteriaString() {
        return getCriteriaString(true);
    }

    /**
     * @return a read only version of the filtered search criteria
     */
    public String getFilteredCriteriaString() {
        return getCriteriaString(false);
    }

    private String getCriteriaString(boolean includeFilteredConfigs) {
        List<String> criteriaList = new ArrayList<String>();
        for (AbstractSearchFieldConfig config : getSearchConfigs()) {
            if (includeFilteredConfigs || !config.isFilterCriteriaString()) {
                String criteria = getCriteriaString(config);
                if (StringUtils.isNotBlank(criteria)) {
                    criteriaList.add(criteria);
                }
            }
        }
        String defaultString = getText("specimen.search.noCriteria");
        return StringUtils.defaultIfEmpty(StringUtils.join(criteriaList, SEPARATOR), defaultString);
    }

    private String getCriteriaString(AbstractSearchFieldConfig config) {
        if (config instanceof SelectConfig) {
            SelectConfig selectConfig = (SelectConfig) config;
            if (selectConfig.isMultiSelectCollection()) {
                return config.getCriteriaString(this, "multiSelectCollectionParamMap");
            } else if (selectConfig.isMultiSelect()) {
                return config.getCriteriaString(this, "multiSelectParamMap");
            }
        }
        return config.getCriteriaString(this, null);
    }

    /**
     * @return the multiSelectParamMap
     */
    public Map<String, Collection<Object>> getMultiSelectParamMap() {
        return specimenSearchActionHelper.getMultiSelectParamMap(getObject());
    }

    /**
     * @param multiSelectParamMap the multiSelectParamMap to set
     */
    public void setMultiSelectParamMap(Map<String, Collection<Object>> multiSelectParamMap) {
        specimenSearchActionHelper.setMultiSelectParamMap(multiSelectParamMap);
    }


    /**
     * @return the multiSelectCollectionParamMap
     */
    public Map<String, Collection<Object>> getMultiSelectCollectionParamMap() {
        return specimenSearchActionHelper.getMultiSelectCollectionParamMap(getObject());
    }

    /**
     * @param multiSelectCollectionParamMap the multiSelectCollectionParamMap to set
     */
    public void setMultiSelectCollectionParamMap(
            Map<String, Collection<Object>> multiSelectCollectionParamMap) {
        specimenSearchActionHelper.setMultiSelectCollectionParamMap(multiSelectCollectionParamMap);
    }


    /**
     * Gets the map of range search parameters.
     *
     * @return a map of range search parameters.
     */
    @Override
    @CreateIfNull(value = false)
    public Map<String, Object> getRangeSearchParameters() {
        return specimenSearchActionHelper.getRangeSearchParameters();
    }

    /**
     * Sets the map of range search parameters.
     *
     * @param rangeSearchParameters a map of range search parameters.
     */
    @Override
    public void setRangeSearchParameters(Map<String, Object> rangeSearchParameters) {
        specimenSearchActionHelper.setRangeSearchParameters(rangeSearchParameters);
    }

    /**
     * Validate range search parameters.
     */
    @Override
    public void validate() {
        for (AbstractSearchFieldConfig config : getSearchConfigs()) {
            if (StringUtils.isNotBlank(config.getSearchConditionClass())) {
                validateRangeSearchParameters(config.getSearchFieldName(), config.getSearchFieldDisplayName());
            }
        }
        if (hasErrors()) {
            initSearchConfigValues();
        }
    }

    private void validateRangeSearchParameters(String propertyName, String displayName) {
        String minPropertyName = RangeSearchCondition.RANGE_MIN_PREFIX + propertyName;
        String maxPropertyName = RangeSearchCondition.RANGE_MAX_PREFIX + propertyName;
        Object minPropertyValue = getRangeSearchParameters().get(minPropertyName);
        Object maxPropertyValue = getRangeSearchParameters().get(maxPropertyName);
        validateRangeSearchValue(minPropertyName, minPropertyValue, "Minimum " + displayName);
        validateRangeSearchValue(maxPropertyName, maxPropertyValue, "Maximum " + displayName);
    }

    private void validateRangeSearchValue(String parameterName, Object parameterValue, String displayName) {
        Object value = parameterValue instanceof String[] ? ((String[]) parameterValue)[0] : parameterValue;
        if (value != null && !NumberUtils.isNumber(value.toString())) {
            String errorMessage = getText("invalid.fieldvalue.rangeSearchParameters");
            addFieldError(parameterName, String.format(errorMessage, displayName));
        }
    }

    /**
     * Returns all search result field display settings.
     * @return all search result field display settings.
     */
    public List<SearchResultFieldDisplaySetting> getSearchResultFieldDisplaySettings() {
        return resultDisplayConfigHelper.getSearchResultFieldDisplaySettings();
    }

    /**
     * Returns a list of search field user interface categories.
     *
     * @return list of search field user interface categories
     */
    public List<UiSearchFieldCategory> getUiSearchFieldCategories() {
        return uiSearchFieldCategories;
    }

    private void initializeUiSearchFieldCategories() {
        uiSearchFieldCategories = uiSearchFieldCategoryService
            .getUiSearchFieldCategories(getObjectType(), getSearchSetType());
    }

    /**
     * Returns the search field configurations associated with the given category.
     * @param category Search field category.
     * @return The search field configurations associated with the given category.
     */
    public List<AbstractSearchFieldConfig> getSearchFieldConfigsForCategory(UiSearchFieldCategory category) {
        return specimenSearchActionHelper.getSearchFieldConfigs(category);
    }

    /**
     * Determines if a given search field configuration is valid and should be included in the
     * resulting subset iterator.
     *
     * @return the search field configuration validation decider
     */
    public Decider getSearchFieldConfigurationValidationDecider() {
        return new Decider() {
            @Override
            public boolean decide(Object element) {
                AbstractSearchFieldConfig configuration = (AbstractSearchFieldConfig) element;
                return configuration.getRequiredRole() == null
                    || ServletActionContext.getRequest().isUserInRole(configuration.getRequiredRole().getName());
            }
        };
    }

    /**
     * Gets the HTML anchor of the selected tab in the tabbed search view.
     *
     * @return HTML anchor or an empty string if no tab is selected
     */
    public String getSelectedTab() {
        return specimenSearchActionHelper.getSelectedTab();
    }

    /**
     * Sets the HTML anchor of the selected tab in the tabbed search view.
     *
     * @param selectedTab HTML anchor of the selected tab
     */
    public void setSelectedTab(String selectedTab) {
        specimenSearchActionHelper.setSelectedTab(selectedTab);
    }

    /**
     * Gets the saved specimen search.
     *
     * @return a saved specimen search
     */
    public SavedSpecimenSearch getSavedSpecimenSearch() {
        Long id = specimenSearchActionHelper.getSavedSpecimenSearchIdFromSession();
        if (id == null) {
            return null;
        }
        return savedSpecimenSearchService.getSavedSpecimenSearchAndData(id);
    }

}