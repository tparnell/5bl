/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.struts2.action;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.ServletActionContext;

import com.fiveamsolutions.nci.commons.search.SearchCriteria;
import com.fiveamsolutions.nci.commons.web.displaytag.PaginatedList;
import com.fiveamsolutions.nci.commons.web.displaytag.SortablePaginatedList;
import com.fiveamsolutions.tissuelocator.data.AggregateSpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.FundingStatus;
import com.fiveamsolutions.tissuelocator.data.IrbApprovalStatus;
import com.fiveamsolutions.tissuelocator.data.ReviewProcess;
import com.fiveamsolutions.tissuelocator.data.Role;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.service.SpecimenRequestServiceLocal;
import com.fiveamsolutions.tissuelocator.service.config.category.UiDynamicFieldCategoryServiceLocal;
import com.fiveamsolutions.tissuelocator.service.search.SpecimenRequestSearchCriteria;
import com.fiveamsolutions.tissuelocator.service.search.SpecimenRequestSortCriterion;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceLocal;
import com.fiveamsolutions.tissuelocator.util.AggregateLineItemSeparator;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.fiveamsolutions.tissuelocator.web.TissueLocatorSessionHelper;
import com.fiveamsolutions.tissuelocator.web.util.ExtendableEntitySection;
import com.google.inject.Inject;

/**
 * @author aevansel
 */
@SuppressWarnings({ "PMD.AvoidDuplicateLiterals", "PMD.TooManyMethods", "PMD.TooManyFields",
    "PMD.ExcessiveClassLength" })
public abstract class AbstractSpecimenRequestAction extends
        AbstractDynamicExtensionAction<SpecimenRequest, SpecimenRequestSortCriterion> {

    private static final long serialVersionUID = 1L;
    private static final String ATTRIBUTE_SUFFIX = "_params";
    private boolean actionNeededOnly;
    private boolean displayPILegalField;
    private boolean displayProspectiveCollection;
    private boolean displayAggregateSearchResults;
    private boolean displayRequestReviewVotes;
    private boolean displayFundingStatusPending;
    private boolean mtaCertificationRequired;
    private boolean irbApprovalRequired;
    private FundingStatus minimumFundingRequired;
    private boolean protocolDocumentRequired;
    private boolean displayShipmentBillingRecipient;
    private String submit;
    private SpecimenRequestLineItem[] lineItems = new SpecimenRequestLineItem[0];
    private AggregateSpecimenRequestLineItem[] generalLineItemsArray = new AggregateSpecimenRequestLineItem[0];
    private AggregateSpecimenRequestLineItem[] specificLineItemsArray = new AggregateSpecimenRequestLineItem[0];
    private Set<AggregateSpecimenRequestLineItem> generalLineItems =
        new LinkedHashSet<AggregateSpecimenRequestLineItem>();
    private Set<AggregateSpecimenRequestLineItem> specificLineItems =
        new LinkedHashSet<AggregateSpecimenRequestLineItem>();
    private final Collection<AggregateSpecimenRequestLineItem> orderSpecificLineItems =
        new ArrayList<AggregateSpecimenRequestLineItem>();
    private final Collection<AggregateSpecimenRequestLineItem> orderGeneralLineItems =
        new ArrayList<AggregateSpecimenRequestLineItem>();
    private final Collection<AggregateSpecimenRequestLineItem> reviewedSpecificLineItems =
        new ArrayList<AggregateSpecimenRequestLineItem>();
    private final Collection<AggregateSpecimenRequestLineItem> reviewedGeneralLineItems =
        new ArrayList<AggregateSpecimenRequestLineItem>();
    private final Collection<AggregateSpecimenRequestLineItem> unreviewedSpecificLineItems =
        new ArrayList<AggregateSpecimenRequestLineItem>();
    private final Collection<AggregateSpecimenRequestLineItem> unreviewedGeneralLineItems =
        new ArrayList<AggregateSpecimenRequestLineItem>();
    private ReviewProcess reviewProcess;
    private final ApplicationSettingServiceLocal appSettingService;

    /**
     * Default constructor.
     */
    public AbstractSpecimenRequestAction() {
        throw new UnsupportedOperationException();
    }

    /**
     * Default constructor.
     * @param appSettingService the application setting service
     * @param uiDynamicFieldCategoryService category service.
     */
    @Inject
    public AbstractSpecimenRequestAction(ApplicationSettingServiceLocal appSettingService, 
            UiDynamicFieldCategoryServiceLocal uiDynamicFieldCategoryService) {
        super(uiDynamicFieldCategoryService);
        this.appSettingService = appSettingService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String list() {
        String attributeName = getClass().getSimpleName() + ATTRIBUTE_SUFFIX;
        HttpSession session = ServletActionContext.getRequest().getSession();
        Boolean actionOnly = (Boolean) session.getAttribute(attributeName);
        if (actionOnly != null) {
            setActionNeededOnly(actionOnly.booleanValue());
        }
        return super.list();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String filter() {
        String attributeName = getClass().getSimpleName() + ATTRIBUTE_SUFFIX;
        HttpSession session = ServletActionContext.getRequest().getSession();
        session.setAttribute(attributeName, Boolean.valueOf(isActionNeededOnly()));
        return super.filter();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected SearchCriteria<SpecimenRequest> getSearchCriteria() {
        HttpSession session = ServletActionContext.getRequest().getSession();
        TissueLocatorUser user = TissueLocatorSessionHelper.getLoggedInUser(session);
        return new SpecimenRequestSearchCriteria(getObject(),
                includeCondition(Role.SCIENTIFIC_REVIEWER_ASSIGNER, true),
                includeCondition(Role.LEAD_REVIEWER, true),
                includeCondition(Role.CONSORTIUM_REVIEW_VOTER, true),
                includeCondition(Role.INSTITUTIONAL_REVIEW_VOTER, true),
                includeCondition(Role.LINE_ITEM_REVIEW_VOTER, true),
                includeCondition(Role.LEAD_REVIEWER, true),
                includeCondition(Role.REVIEW_DECISION_NOTIFIER, true),
                includeCondition(Role.TISSUE_LOCATOR_USER, true),
                includeCondition(Role.LINE_ITEM_REVIEW_VOTER, false),
                user, getReviewProcess());
    }

    private boolean includeCondition(Role role, boolean actionNeeded) {
        return isActionNeededOnly() == actionNeeded && ServletActionContext.getRequest().isUserInRole(role.getName());
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public void prepare() {
        super.prepare();
        initLineItems();
        setDisplayPILegalField(appSettingService.isDisplayPiLegalFields());
        setDisplayProspectiveCollection(appSettingService.isDisplayProspectiveCollection());
        setDisplayAggregateSearchResults(StringUtils.isNotBlank(appSettingService.getSpecimenGrouping()));
        setDisplayRequestReviewVotes(appSettingService.isDisplayRequestReviewVotes());
        setReviewProcess(appSettingService.getReviewProcess());
        setMtaCertificationRequired(appSettingService.isMtaCertificationRequired());
        setMinimumFundingRequired(appSettingService.getMinimunRequiredFundingStatus());
        setIrbApprovalRequired(appSettingService.isIrbApprovalRequired());
        setProtocolDocumentRequired(appSettingService.isProtocolDocumentRequired());
        setDisplayFundingStatusPending(appSettingService.isDisplayFundingStatusPending());
        setDisplayShipmentBillingRecipient(appSettingService.isDisplayShipmentBillingRecipient());
    }

    /**
     * @return the actionNeededOnly
     */
    public boolean isActionNeededOnly() {
        return actionNeededOnly;
    }

    /**
     * @param actionNeededOnly the actionNeededOnly to set
     */
    public void setActionNeededOnly(boolean actionNeededOnly) {
        this.actionNeededOnly = actionNeededOnly;
    }

    /**
     * @return the displayPILegalField
     */
    public boolean isDisplayPILegalField() {
        return displayPILegalField;
    }

    /**
     * @param displayPILegalField the displayPILegalField to set
     */
    public void setDisplayPILegalField(boolean displayPILegalField) {
        this.displayPILegalField = displayPILegalField;
    }

    /**
     * @return the displayProspectiveCollection
     */
    public boolean isDisplayProspectiveCollection() {
        return displayProspectiveCollection;
    }

    /**
     * @param displayProspectiveCollection the displayProspectiveCollection to set
     */
    public void setDisplayProspectiveCollection(boolean displayProspectiveCollection) {
        this.displayProspectiveCollection = displayProspectiveCollection;
    }

    /**
     * @return the mtaCertificationRequired
     */
    protected boolean isMtaCertificationRequired() {
        return mtaCertificationRequired;
    }

    /**
     * @param mtaCertificationRequired the mtaCertificationRequired to set
     */
    protected void setMtaCertificationRequired(boolean mtaCertificationRequired) {
        this.mtaCertificationRequired = mtaCertificationRequired;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PaginatedList<SpecimenRequest> getPaginatedList() {
        return new SortablePaginatedList<SpecimenRequest, SpecimenRequestSortCriterion>(1,
                SpecimenRequestSortCriterion.ID.name(), SpecimenRequestSortCriterion.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected SpecimenRequestServiceLocal getService() {
        return TissueLocatorRegistry.getServiceLocator().getSpecimenRequestService();
    }

    /**
     * @return the submit
     */
    public String getSubmit() {
        return submit;
    }

    /**
     * @param submit the submit to set
     */
    public void setSubmit(String submit) {
        this.submit = submit;
    }

   /**
    * Initialize aggregate and regular line items.
    */
    protected void initLineItems() {
        if (getObject().getLineItems() != null) {
            lineItems = new SpecimenRequestLineItem[getObject().getLineItems().size()];
            getObject().getLineItems().toArray(lineItems);
        }
        if (getObject().getAggregateLineItems() != null) {
            generateSeparateArrays();
        }
    }

    /**
     * @return the lineItems
     */
    @SuppressWarnings("PMD.MethodReturnsInternalArray")
    public SpecimenRequestLineItem[] getLineItems() {
        return lineItems;
    }

    /**
     * @param lineItems the lineItems to set
     */
    @SuppressWarnings("PMD.ArrayIsStoredDirectly")
    public void setLineItems(SpecimenRequestLineItem[] lineItems) {
        this.lineItems = lineItems;
    }

    /**
     * @param generalLineSet the generalLineSet to set
     */
    public void setGeneralLineSet(Set<AggregateSpecimenRequestLineItem> generalLineSet) {
        generalLineItems = generalLineSet;
    }

    /**
     * @return the generalLineItems
     */
    public Set<AggregateSpecimenRequestLineItem> getGeneralLineItems() {
        return new LinkedHashSet<AggregateSpecimenRequestLineItem>(generalLineItems);
    }

    /**
     * @return the specificLineItems
     */
    public Set<AggregateSpecimenRequestLineItem> getSpecificLineItems() {
        return specificLineItems;
    }

    /**
     * @param specificLineSet the specificLineItems to set
     */
    public void setSpecificLineSet(Set<AggregateSpecimenRequestLineItem> specificLineSet) {
        specificLineItems = specificLineSet;
    }


    /**
     * @return the displayAggregateSearchResults
     */
    public boolean isDisplayAggregateSearchResults() {
        return displayAggregateSearchResults;
    }

    /**
     * @param displayAggregateSearchResults the displayAggregateSearchResults to set
     */
    public void setDisplayAggregateSearchResults(boolean displayAggregateSearchResults) {
        this.displayAggregateSearchResults = displayAggregateSearchResults;
    }

    /**
     * @return the displayRequestReviewVotes
     */
    public boolean isDisplayRequestReviewVotes() {
        return displayRequestReviewVotes;
    }

    /**
     * @param displayRequestReviewVotes the displayRequestReviewVotes to set
     */
    public void setDisplayRequestReviewVotes(boolean displayRequestReviewVotes) {
        this.displayRequestReviewVotes = displayRequestReviewVotes;
    }

    /**
     * @return the irbApprovalRequired
     */
    public boolean isIrbApprovalRequired() {
        return irbApprovalRequired;
    }

    /**
     * @param irbApprovalRequired the irbApprovalRequired to set
     */
    public void setIrbApprovalRequired(boolean irbApprovalRequired) {
        this.irbApprovalRequired = irbApprovalRequired;
    }

    /**
     * Confirm that user does indeed want to submit a specimen request.
     *
     * @return - confirmation.
     */
    protected String confirm() {
        if (!IrbApprovalStatus.APPROVED.equals(getObject().getStudy().getIrbApprovalStatus())) {
            getObject().getStudy().setIrbApprovalNumber(null);
            getObject().getStudy().setIrbApprovalExpirationDate(null);
            getObject().getStudy().setIrbApprovalLetter(null);
        }
        if (!IrbApprovalStatus.EXEMPT.equals(getObject().getStudy().getIrbApprovalStatus())) {
            getObject().getStudy().setIrbExemptionLetter(null);
        }
        if (isMtaCertificationRequired()) {
            return "certifyMta";
        }
        return "confirm";
    }

    /**
     * @return the reviewProcess
     */
    public ReviewProcess getReviewProcess() {
        return reviewProcess;
    }

    /**
     * @param reviewProcess the reviewProcess to set
     */
    public void setReviewProcess(ReviewProcess reviewProcess) {
        this.reviewProcess = reviewProcess;
    }

    /**
     * Generate General Population array, Specific Selection Array,
     * and sort the aggregateLineItems array.
     */
    protected void generateSeparateArrays() {
        generalLineItems.clear();
        specificLineItems.clear();
        AggregateLineItemSeparator separator = new AggregateLineItemSeparator(
                getObject().getAggregateLineItems(), getAppSettingService());
        generalLineItems.addAll(separator.getGeneralLineItems());
        specificLineItems.addAll(separator.getSpecificLineItems());
        generalLineItemsArray = new AggregateSpecimenRequestLineItem[generalLineItems
                .size()];
        specificLineItemsArray = new AggregateSpecimenRequestLineItem[specificLineItems
                .size()];
        generalLineItems.toArray(generalLineItemsArray);
        specificLineItems.toArray(specificLineItemsArray);

        separator = new AggregateLineItemSeparator(getObject().getOrderAggregateLineItems(), getAppSettingService());
        getOrderSpecificLineItems().clear();
        getOrderSpecificLineItems().addAll(separator.getSpecificLineItems());
        getOrderGeneralLineItems().clear();
        getOrderGeneralLineItems().addAll(separator.getGeneralLineItems());

        separator = new AggregateLineItemSeparator(getObject().getReviewedAggregateLineItems(),
                getAppSettingService());
        getReviewedSpecificLineItems().clear();
        getReviewedSpecificLineItems().addAll(separator.getSpecificLineItems());
        getReviewedGeneralLineItems().clear();
        getReviewedGeneralLineItems().addAll(separator.getGeneralLineItems());

        separator = new AggregateLineItemSeparator(getObject().getUnreviewedAggregateLineItems(),
                getAppSettingService());
        getUnreviewedSpecificLineItems().clear();
        getUnreviewedSpecificLineItems().addAll(separator.getSpecificLineItems());
        getUnreviewedGeneralLineItems().clear();
        getUnreviewedGeneralLineItems().addAll(separator.getGeneralLineItems());
    }

    /**
     * @return the generalLineItemsArray
     */
    @SuppressWarnings("PMD.MethodReturnsInternalArray")
    public AggregateSpecimenRequestLineItem[] getGeneralLineItemsArray() {
        return generalLineItemsArray;
    }

    /**
     * @param generalLineItemsArray the generalLineItemsArray to set
     */
    @SuppressWarnings("PMD.ArrayIsStoredDirectly")
    public void setGeneralLineItemsArray(
            AggregateSpecimenRequestLineItem[] generalLineItemsArray) {
        this.generalLineItemsArray = generalLineItemsArray;
    }

    /**
     * @return the specificLineItemsArray
     */
    @SuppressWarnings("PMD.MethodReturnsInternalArray")
    public AggregateSpecimenRequestLineItem[] getSpecificLineItemsArray() {
        return specificLineItemsArray;
    }

    /**
     * @param specificLineItemsArray the specificLineItemsArray to set
     */
    @SuppressWarnings("PMD.ArrayIsStoredDirectly")
    public void setSpecificLineItemsArray(
            AggregateSpecimenRequestLineItem[] specificLineItemsArray) {
        this.specificLineItemsArray = specificLineItemsArray;
    }

    /**
     * @return the orderSpecificLineItems
     */
    public Collection<AggregateSpecimenRequestLineItem> getOrderSpecificLineItems() {
        return orderSpecificLineItems;
    }

    /**
     * @return the orderGeneralLineItems
     */
    public Collection<AggregateSpecimenRequestLineItem> getOrderGeneralLineItems() {
        return orderGeneralLineItems;
    }

    /**
     * @return the reviewedSpecificLineItems
     */
    public Collection<AggregateSpecimenRequestLineItem> getReviewedSpecificLineItems() {
        return reviewedSpecificLineItems;
    }

    /**
     * @return the reviewedGeneralLineItems
     */
    public Collection<AggregateSpecimenRequestLineItem> getReviewedGeneralLineItems() {
        return reviewedGeneralLineItems;
    }

    /**
     * @return the unreviewedSpecificLineItems
     */
    public Collection<AggregateSpecimenRequestLineItem> getUnreviewedSpecificLineItems() {
        return unreviewedSpecificLineItems;
    }

    /**
     * @return the unreviewedGeneralLineItems
     */
    public Collection<AggregateSpecimenRequestLineItem> getUnreviewedGeneralLineItems() {
        return unreviewedGeneralLineItems;
    }

    /**
     * @param minimumFundingRequired the minimumFundingRequired to set
     */
    public void setMinimumFundingRequired(FundingStatus minimumFundingRequired) {
        this.minimumFundingRequired = minimumFundingRequired;
    }

    /**
     * @return the minimumFundingRequired
     */
    public FundingStatus getMinimumFundingRequired() {
        return minimumFundingRequired;
    }

    /**
     * @return the protocolDocumentRequired
     */
    public boolean isProtocolDocumentRequired() {
        return protocolDocumentRequired;
    }

    /**
     * @param protocolDocumentRequired the protocolDocumentRequired to set
     */
    public void setProtocolDocumentRequired(boolean protocolDocumentRequired) {
        this.protocolDocumentRequired = protocolDocumentRequired;
    }

    /**
     * @return the displayFundingStatusPending
     */
    public boolean isDisplayFundingStatusPending() {
        return displayFundingStatusPending;
    }

    /**
     * @param displayFundingStatusPending the displayFundingStatusPending to set
     */
    public void setDisplayFundingStatusPending(boolean displayFundingStatusPending) {
        this.displayFundingStatusPending = displayFundingStatusPending;
    }

    /**
     * @return the displayShipmentBillingRecipient
     */
    public boolean isDisplayShipmentBillingRecipient() {
        return displayShipmentBillingRecipient;
    }

    /**
     * @param displayShipmentBillingRecipient the displayShipmentBillingRecipient to set
     */
    public void setDisplayShipmentBillingRecipient(
            boolean displayShipmentBillingRecipient) {
        this.displayShipmentBillingRecipient = displayShipmentBillingRecipient;
    }

    /**
     * @return the appSettingService
     */
    public ApplicationSettingServiceLocal getAppSettingService() {
        return appSettingService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Collection<SpecimenRequestHeaderKey> getAllSections() {
        return Arrays.asList(SpecimenRequestHeaderKey.values());
    }

    /**
     * Keys of the specimen header properties defined in the application resources.
     */
    enum SpecimenRequestHeaderKey implements ExtendableEntitySection {

        /**
         * Currently Funded header key.
         */
        CURRENTLY_FUNDED("specimenRequest.study.fundingStatus");

        private final String resourceKey;

        /**
         * Creates a specimen header key.
         *
         * @param resourceKey the resource key value
         */
        private SpecimenRequestHeaderKey(String resourceKey) {
            this.resourceKey = resourceKey;
        }

        /**
         * Returns the value of the resource key.
         *
         * @return the resource key
         */
        public String getResourceKey() {
            return resourceKey;
        }
    }
}
