/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.web.struts2.action;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.struts2.ServletActionContext;

import com.fiveamsolutions.tissuelocator.data.config.search.AbstractSearchFieldConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.SearchSetType;
import com.fiveamsolutions.tissuelocator.service.config.search.SearchConfig;
import com.fiveamsolutions.tissuelocator.service.config.search.SearchFieldConfigServiceLocal;
import com.google.inject.Inject;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;

/**
 * @author zmelnick
 *
 */
public class SimpleSearchAction extends ActionSupport implements Preparable {

    private static final long serialVersionUID = 8781350700250491989L;

    private final Collection<AbstractSearchFieldConfig> searchConfigs = new ArrayList<AbstractSearchFieldConfig>();
    private SearchFieldConfigServiceLocal searchFieldConfigService;

    /**
     * default constructor.
     */
    public SimpleSearchAction() {
        // Default constructor is unsupported, guice build using the other constructor.
        // this needs to be here so xml validation by struts passes.
        throw new UnsupportedOperationException();
    }
    
    /**
     * 
     * @param searchFieldConfigService Search field config service.
     */
    @Inject
    public SimpleSearchAction(SearchFieldConfigServiceLocal searchFieldConfigService) {
        this.searchFieldConfigService = searchFieldConfigService;
    }
    
    /**
     * {@inheritDoc}
     */
    public void prepare() {
           initSimpleSearchConfigs();
    }

    private void initSimpleSearchConfigs() {
        SearchConfig simpleSearchConfig = new SearchConfig(SearchSetType.WIDGET, searchFieldConfigService);
        for (AbstractSearchFieldConfig config : simpleSearchConfig.getSearchFieldConfigs()) {
            if (isSearchFieldValid(config)) {
                searchConfigs.add(config);
            }

        }
    }

    private boolean isSearchFieldValid(AbstractSearchFieldConfig config) {
        return (config.getRequiredRole() == null || ServletActionContext.getRequest().isUserInRole(
                config.getRequiredRole().getName()));
    }

    /**
     * @return the searchConfigs
     */
    public Collection<AbstractSearchFieldConfig> getSimpleSearchConfigs() {
        return searchConfigs;
    }

}
