/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.struts2.action;

import com.fiveamsolutions.tissuelocator.data.Shipment;
import com.fiveamsolutions.tissuelocator.service.config.category.SearchResultDisplaySettingsServiceLocal;
import com.fiveamsolutions.tissuelocator.service.config.category.UiDynamicFieldCategoryServiceLocal;
import com.fiveamsolutions.tissuelocator.service.config.category.UiSearchFieldCategoryServiceLocal;
import com.fiveamsolutions.tissuelocator.service.config.category.UiSectionServiceLocal;
import com.fiveamsolutions.tissuelocator.service.config.search.SearchFieldConfigServiceLocal;
import com.fiveamsolutions.tissuelocator.service.search.SpecimenSearchServiceLocal;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceLocal;
import com.fiveamsolutions.tissuelocator.service.user.TissueLocatorUserServiceLocal;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.google.inject.Inject;

/**
 * @author smiller
 *
 */
public class SpecimenSearchPopupAction extends SpecimenSearchAction {

    private static final long serialVersionUID = 1L;
    private Shipment order = new Shipment();

    /**
     * default constructor.
     */
    public SpecimenSearchPopupAction() {
        // Default constructor is unsupported, guice build using the other constructor.
        // this needs to be here so xml validation by struts passes.
        throw new UnsupportedOperationException();
    }

    /**
     * constructor.
     * @param specimenSearchService the search service.
     * @param userService the user service
     * @param appSettingService the application setting service
     * @param uiSearchFieldCategoryService search field category service.
     * @param uiSectionService ui section service.
     * @param searchResultDisplaySettingService search result display setting service.
     * @param searchFieldConfigService search field config service.
     * @param uiDynamicFieldCategoryService category service.
     */
    @Inject
    @SuppressWarnings("PMD.ExcessiveParameterList")
    //CHECKSTYLE:OFF - too many parameters
    public SpecimenSearchPopupAction(SpecimenSearchServiceLocal specimenSearchService,
            TissueLocatorUserServiceLocal userService, ApplicationSettingServiceLocal appSettingService, 
            UiSearchFieldCategoryServiceLocal uiSearchFieldCategoryService, 
            UiSectionServiceLocal uiSectionService, 
            SearchResultDisplaySettingsServiceLocal searchResultDisplaySettingService, 
            SearchFieldConfigServiceLocal searchFieldConfigService, 
            UiDynamicFieldCategoryServiceLocal uiDynamicFieldCategoryService) {
   //CHECKSTYLE:ON
        super(specimenSearchService,
                SpecimenSearchPopupAction.class.getSimpleName(), userService, appSettingService, null, 
                uiSearchFieldCategoryService, searchResultDisplaySettingService, 
                uiSectionService, searchFieldConfigService, uiDynamicFieldCategoryService);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void prepare() {
        super.prepare();
        if (getOrder().getId() != null) {
            setOrder(TissueLocatorRegistry.getServiceLocator().getShipmentService().
                    getPersistentObject(Shipment.class, getOrder().getId()));
        }
    }

    /**
     * @return the order
     */
    public Shipment getOrder() {
        return order;
    }

    /**
     * @param order the order to set
     */
    public void setOrder(Shipment order) {
        this.order = order;
    }
}
