/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.web.struts2.action.searchfield;

import ognl.Ognl;
import ognl.OgnlException;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;

import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.config.help.HelpConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.AbstractSearchFieldConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.SearchSetType;
import com.fiveamsolutions.tissuelocator.service.config.search.SearchConfig;
import com.fiveamsolutions.tissuelocator.service.config.search.SearchFieldConfigServiceLocal;
import com.google.inject.Inject;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;

/**
 * Base class for processing search field configurations.
 * @author gvaughn
 * @param <T> The type of the configuration object.
 */
public abstract class AbstractSearchFieldWidgetAction<T extends AbstractSearchFieldConfig> extends ActionSupport
    implements Preparable {

    private static final long serialVersionUID = -5558806642066827447L;
    private static final Logger LOG = Logger.getLogger(AbstractSearchFieldWidgetAction.class);
    private static final String DEFAULT_HELP_CONTEXT = Specimen.class.getName();

    private T searchFieldConfig;
    private SearchSetType searchSetType = SearchSetType.ADVANCED;
    private String helpBaseUrl;
    private HelpConfig helpConfig;
    private SearchFieldConfigServiceLocal searchFieldConfigService;
    
    /**
     * Default Constructor.
     */
    @SuppressWarnings("PMD.AvoidThrowingRawExceptionTypes")
    public AbstractSearchFieldWidgetAction() {
        // Default constructor is unsupported, guice build using the other constructor.
        // this needs to be here so xml validation by struts passes.
        throw new UnsupportedOperationException();
    }
    
    /**
     * 
     * @param searchFieldConfigService search field config service.
     */
    @Inject
    @SuppressWarnings("PMD.AvoidThrowingRawExceptionTypes")
    public AbstractSearchFieldWidgetAction(SearchFieldConfigServiceLocal searchFieldConfigService) {
        this.searchFieldConfigService = searchFieldConfigService;
        try {
            this.searchFieldConfig = getConfigType().newInstance();
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
    
    /**
     * {@inheritDoc}
     */
    public void prepare() {
        SearchConfig searchConfig = new SearchConfig(getSearchSetType(), searchFieldConfigService);
        if (getSearchFieldConfig() != null
                && getSearchFieldConfig().getId() != null) {
            setSearchFieldConfig(searchConfig.getConfig(getConfigType(), getSearchFieldConfig().getId()));
            helpConfig = new HelpConfig(DEFAULT_HELP_CONTEXT);
        }
    }

    /**
     * Entry action.
     * @return a struts forward.
     */
    @SkipValidation
    public String input() {
        return Action.INPUT;
    }

    /**
     * Performs any necessary setup for a search field widget display.
     * Subclasses should override as necessary.
     * @return struts forward.
     */
    @SkipValidation
    public String getWidget() {
        return Action.SUCCESS;
    }

    /**
     * @return the search field configuration manages by this action.
     */
    public T getSearchFieldConfig() {
        return searchFieldConfig;
    }

    /**
     * @param searchFieldConfig the search field configuration manages by this action.
     */
    public void setSearchFieldConfig(T searchFieldConfig) {
        this.searchFieldConfig = searchFieldConfig;
    }
    
    /**
     * @return the searchSetType
     */
    public SearchSetType getSearchSetType() {
        if (searchSetType == null) {
            searchSetType = SearchSetType.ADVANCED;
        }
        return searchSetType;
    }

    /**
     * @param searchSetType the searchSetType to set
     */
    public void setSearchSetType(SearchSetType searchSetType) {
        this.searchSetType = searchSetType;
    }

    /**
     * Gets the base URL for retrieving the help text.
     * 
     * @return the helpBaseUrl
     */
    public String getHelpBaseUrl() {
        return helpBaseUrl;
    }

    /**
     * @param helpBaseUrl the helpBaseUrl to set
     */
    public void setHelpBaseUrl(String helpBaseUrl) {
        this.helpBaseUrl = helpBaseUrl;
    }
    
    /**
     * @return the helpConfig
     */
    public HelpConfig getHelpConfig() {
        return helpConfig;
    }

    /**
     * The configuration type corresponding to this action.
     * @return The configuration type corresponding to this action.
     */
    protected abstract Class<T> getConfigType();

    /**
     * Returns a field value from the given object using the given OGNL expression.
     * @param expr OGNL expression used to retrieve the value.
     * @param object Object from which the value will be retrieved.
     * @return The value retrieved from the object, or null if there is no value.
     */
    public Object getValue(String expr, Object object) {
        Object retVal = null;
        try {
            retVal = Ognl.getValue(expr, object);
        } catch (OgnlException e) {
            //exceptions are thrown for null values and can be ignored
            LOG.debug(e);
        }
        return retVal;
    }
}
