/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.struts2.action;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.mail.MessagingException;
import javax.servlet.ServletContext;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.validation.SkipValidation;

import com.fiveamsolutions.nci.commons.data.security.AbstractUser;
import com.fiveamsolutions.nci.commons.util.UsernameHolder;
import com.fiveamsolutions.tissuelocator.data.RequestStatus;
import com.fiveamsolutions.tissuelocator.data.Role;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestReviewVote;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.Vote;
import com.fiveamsolutions.tissuelocator.service.FixedConsortiumReviewServiceLocal;
import com.fiveamsolutions.tissuelocator.service.config.category.UiDynamicFieldCategoryServiceLocal;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceLocal;
import com.fiveamsolutions.tissuelocator.service.user.TissueLocatorUserServiceLocal;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.fiveamsolutions.tissuelocator.web.TissueLocatorContextParameterHelper;
import com.google.inject.Inject;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.validator.annotations.CustomValidator;
import com.opensymphony.xwork2.validator.annotations.RequiredFieldValidator;
import com.opensymphony.xwork2.validator.annotations.ValidationParameter;
import com.opensymphony.xwork2.validator.annotations.Validations;

/**
 * @author smiller
 *
 */
public class FixedConsortiumReviewAction extends AbstractSpecimenRequestReviewAction {

    private static final long serialVersionUID = 1L;

    private final Set<AbstractUser> reviewers = new HashSet<AbstractUser>();
    private SpecimenRequestReviewVote[] consortiumReviews;
    private Vote finalVote;
    private final TissueLocatorUserServiceLocal userService;

    /**
     * Default constructor.
     */
    public FixedConsortiumReviewAction() {
        // Default constructor is unsupported, guice build using the other constructor.
        // this needs to be here so xml validation by struts passes.
        throw new UnsupportedOperationException();
    }

    /**
     * Default constructor.
     * @param userService user service.
     * @param appSettingService the application setting service
     * @param uiDynamicFieldCategoryService category service.
     */
    @Inject
    public FixedConsortiumReviewAction(TissueLocatorUserServiceLocal userService,
            ApplicationSettingServiceLocal appSettingService, 
            UiDynamicFieldCategoryServiceLocal uiDynamicFieldCategoryService) {
        super(userService, appSettingService, uiDynamicFieldCategoryService);
        this.userService = userService;
        setObject(new SpecimenRequest());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void prepare() {
        super.prepare();
        Set<AbstractUser> users = userService.getUsersInRole(Role.CONSORTIUM_REVIEW_VOTER.getName());
        // Make sure that the requestor cannot be assigned as a reviewer
        if (users != null) {
            users.remove(getObject().getRequestor());
        }
        getReviewers().addAll(users);

        setConsortiumReviews(new SpecimenRequestReviewVote[getObject().getConsortiumReviews().size()]);
        getObject().getConsortiumReviews().toArray(consortiumReviews);

        TissueLocatorUser user = userService.getByUsername(UsernameHolder.getUser());

        // load the correct consortium review
        for (SpecimenRequestReviewVote vote : getObject().getConsortiumReviews()) {
            if (user.equals(vote.getUser())) {
                setConsortiumReview(vote);
            }
        }

        if (getObject().getFinalVote() != null) {
            setFinalVote(RequestStatus.getVote(getObject().getFinalVote()));
        }
    }

    /**
     * assign a scientific reviewer.
     * @return the struts forward.
     * @throws MessagingException on error
     */
    @Validations(
            customValidators =
                @CustomValidator(type = "hibernate", fieldName = "object",
                    parameters = { @ValidationParameter(name = "resourceKeyBase", value = "specimenRequest") })
    )
    public String assignScientificReviewer() throws MessagingException {
        ServletContext sc = ServletActionContext.getServletContext();
        int votingPeriod = TissueLocatorContextParameterHelper.getVotingPeriod(sc);
        getReviewService().assignRequest(getObject(), votingPeriod);
        addActionMessage(getText("specimenRequest.review.assigner.success"));
        return INPUT;
    }

    /**
     * decline a scientific review.
     * @return the struts forward.
     * @throws MessagingException on error
     */
    public String declineReview() throws MessagingException {
        ServletContext sc = ServletActionContext.getServletContext();
        int votingPeriod = TissueLocatorContextParameterHelper.getVotingPeriod(sc);
        int reviewerAssignmentPeriod = TissueLocatorContextParameterHelper.getReviewerAssignmentPeriod(sc);
        getReviewService().declineReview(getObject(), getConsortiumReview(), votingPeriod, reviewerAssignmentPeriod);
        addActionMessage(getText("specimenRequest.review.decline.success"));
        return list();
    }

    /**
     * Adds the comment.
     * @return string forward.
     * @throws MessagingException on error
     */
    @Validations(
            customValidators =
                @CustomValidator(type = "hibernate", fieldName = "comment",
                    parameters = { @ValidationParameter(name = "resourceKeyBase", value = "comment") })
    )
    public String addComment() throws MessagingException {
        getComment().setDate(new Date());
        getObject().getComments().add(getComment());
        getReviewService().savePersistentObject(getObject());
        return SUCCESS;
    }

    /**
     * Finalizes the vote.
     * @return string forward.
     * @throws MessagingException on error
     */
    @Validations(
            requiredFields = {
                    @RequiredFieldValidator(fieldName = "finalVote", message = "",
                            key = "specimenRequest.review.finalVote.required"),
                    @RequiredFieldValidator(fieldName = "object.externalComment", message = "",
                            key = "specimenRequest.review.finalComment.required")
            }
    )
    public String finalizeVote() throws MessagingException {
        getObject().setFinalVote(getFinalVote().getStatus());
        getReviewService().saveFinalVote(getObject());
        addActionMessage(getText("specimenRequest.review.finalComment.success"));
        return Action.INPUT;
    }

    /**
     * Notifies a request's requestor of the final decision.
     * @return the struts forward
     * @throws MessagingException on error
     */
    @SkipValidation
    public String notifyResearcherOfDecision() throws MessagingException {
        getReviewService().finalizeVote(getObject(), false);
        addActionMessage(getText("specimenRequest.review.notifyResearcher.success"));
        return list();
    }

    /**
     * @return the reviewers
     */
    public Set<AbstractUser> getReviewers() {
        return reviewers;
    }

    /**
     * @return the consortiumReviews
     */
    @SuppressWarnings("PMD.MethodReturnsInternalArray")
    public SpecimenRequestReviewVote[] getConsortiumReviews() {
        return consortiumReviews;
    }

    /**
     * @param consortiumReviews the consortiumReviews to set
     */
    @SuppressWarnings("PMD.ArrayIsStoredDirectly")
    public void setConsortiumReviews(SpecimenRequestReviewVote[] consortiumReviews) {
        this.consortiumReviews = consortiumReviews;
    }

    /**
     * @return the finalVote
     */
    public Vote getFinalVote() {
        return finalVote;
    }

    /**
     * @param finalVote the finalVote to set
     */
    public void setFinalVote(Vote finalVote) {
        this.finalVote = finalVote;
    }

    private FixedConsortiumReviewServiceLocal getReviewService() {
        return TissueLocatorRegistry.getServiceLocator().getFixedConsortiumReviewService();
    }
}
