/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.web.util;

import org.apache.commons.lang.StringUtils;

import com.fiveamsolutions.tissuelocator.service.SpecimenServiceLocal;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;


/**
 * @author ddasgupta
 *
 */
public enum HomePageWidget {

    /**
     * the browse by institution widget.
     */
    INSTITUTION("institution", new WidgetDataRetriever() {

        public Object getData() {
            SpecimenServiceLocal specimenService = TissueLocatorRegistry.getServiceLocator().getSpecimenService();
            return specimenService.getInstitutions();
        }

    }),

    /**
     * the browse by specimen type widget.
     */
    TYPE("type", new WidgetDataRetriever() {

        public Object getData() {
            SpecimenServiceLocal specimenService = TissueLocatorRegistry.getServiceLocator().getSpecimenService();
            return specimenService.getCountsByClass();
        }

    }),

    /**
     * the browse by disease widget without counts.
     */
    DISEASE_NO_COUNT("diseaseNoCount", new WidgetDataRetriever() {

        public Object getData() {
            SpecimenServiceLocal specimenService = TissueLocatorRegistry.getServiceLocator().getSpecimenService();
            return specimenService.getPathologicalCharacteristics();
        }

    }),

    /**
     * the browse by disease widget with counts.
     */
    DISEASE_WITH_COUNT("diseaseWithCount", new WidgetDataRetriever() {

        public Object getData() {
            SpecimenServiceLocal specimenService = TissueLocatorRegistry.getServiceLocator().getSpecimenService();
            return specimenService.getCountsByPathologicalCharacteristic();
        }

    }),

    /**
     * the simple search widget.
     */
    SIMPLE_SEARCH("simpleSearch", new WidgetDataRetriever() {

        public Object getData() {
            return null;
        }

    }),

    /**
     * the welcome widget.
     */
    WELCOME("welcome", new WidgetDataRetriever() {

        public Object getData() {
            return null;
        }

    }),

    /**
     * the tabbed welcome widget.
     */
    TABBED_WELCOME("tabbedWelcome", new WidgetDataRetriever() {

        public Object getData() {
            return null;
        }

    }),

    /**
     * the no search widget.
     */
    NO_SIMPLE_SEARCH("noSimpleSearch", new WidgetDataRetriever() {

        public Object getData() {
            return null;
        }

    });

    /**
     * Returns the data needed by a home page widget.
     * @author ddasgupta
     *
     */
    public interface WidgetDataRetriever {

        /**
         * @return the data needed by a home page widget
         */
        Object getData();

    }

    private static final String SUFFIX = "_data";

    private String name;
    private WidgetDataRetriever dataRetriever;

    HomePageWidget(String name, WidgetDataRetriever dataRetriever) {
        this.name = name;
        this.dataRetriever = dataRetriever;
    }

    /**
     * @return the data needed by the home page widget
     */
    public Object getData() {
        return dataRetriever.getData();
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }


    /**
     * @return the servlet context attribute name
     */
    public String getAttributeName() {
        return getName() + SUFFIX;
    }

    /**
     * Get a home page widget by its name in the application settings.
     * @param searchName the name of the widget
     * @return the home page widget with the given name
     */
    public static HomePageWidget getByName(String searchName) {
        for (HomePageWidget widget : values()) {
            if (StringUtils.equalsIgnoreCase(widget.name, searchName)) {
                return widget;
            }
        }
        throw new IllegalArgumentException("HomePageWidget with name " + searchName + " does not exist");
    }
}
