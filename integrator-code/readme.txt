This product includes software developed by 5AM and the National Cancer Institute.

THIS SOFTWARE IS PROVIDED "AS IS," AND ANY EXPRESSED OR IMPLIED WARRANTIES, (INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, 
NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE) ARE DISCLAIMED. IN NO EVENT SHALL THE NATIONAL CANCER INSTITUTE, 5AM SOLUTIONS, INC. 
OR THEIR AFFILIATES BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
THE POSSIBILITY OF SUCH DAMAGE.

Development Environment:
========================
The development environment for the integration application is generally the same as the core application.
However, there is one additional JBoss configuration that must be done:

Update the definition of the topContextComparator bean in $JBOSS_HOME/server/default/conf/bootstrap/deployers.xml
to ensure that the core app is deployed before the integration app.

      <!-- use legacy ordering -->
       <bean name="topContextComparator">
         <constructor factoryClass="org.jboss.system.deployers.LegacyDeploymentContextComparator" factoryMethod="getInstance"/>
         <property name="suffixOrder">
           <map keyClass="java.lang.String" valueClass="java.lang.Integer">
             <entry>
               <key>tissuelocator.ear</key>
               <value>610</value>
             </entry>
             <entry>
               <key>tissuelocator-integrator.ear</key>
               <value>620</value>
             </entry>
           </map>
         </property>
       </bean>
       
Running Selenium test for integrator to test flat-file import.  By default, SpecimenImportTest will use a test sFTP server setup on ci.  Currently,
the client is setup to use an FTP server.  To change the properties of the FTP server, add the following properties to your profiles.xml:
<selenium.ftp.site>192.168.140.104</selenium.ftp.site>
<selenium.ftp.user>testuser</selenium.ftp.user>
<selenium.ftp.password>5ams0lutions</selenium.ftp.password>
<selenium.ftp.root>/files/specimen/</selenium.ftp.root>
