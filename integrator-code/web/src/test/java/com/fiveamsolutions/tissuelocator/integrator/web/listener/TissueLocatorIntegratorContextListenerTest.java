/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.web.listener;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.fiveamsolutions.tissuelocator.integrator.service.adapter.AdapterLauncher;
import com.fiveamsolutions.tissuelocator.integrator.web.test.AbstractTissueLocatorIntegratorWebTest;

/**
 * @author smiller
 *
 */
public class TissueLocatorIntegratorContextListenerTest extends AbstractTissueLocatorIntegratorWebTest {

    /**
     * test the context listener.
     */
    @Test
    public void testContextListener() {
        TissueLocatorIntegratorContextListener listener = new TissueLocatorIntegratorContextListener();
        MockAdapterLauncher launcher = new MockAdapterLauncher();
        TissueLocatorIntegratorContextListener.setAdapterLauncher(launcher);

        listener = new TissueLocatorIntegratorContextListener();
        assertEquals(0, launcher.getLaunchCount());

        listener = new TissueLocatorIntegratorContextListener();
        listener.contextInitialized(null);
        assertEquals(1, launcher.getLaunchCount());

        listener = new TissueLocatorIntegratorContextListener();
        listener.contextDestroyed(null);
        assertEquals(0, launcher.getLaunchCount());
    }

    /**
     * mock adapter launcher.
     * @author bpickeral
     */
    class MockAdapterLauncher extends AdapterLauncher {

        private int launchCount = 0;

        /**
         * {@inheritDoc}
         */
        @Override
        public int launchAdapterThreads() {
            launchCount++;
            return 1;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void stopAdapterThreads() {
            launchCount = 0;
        }

        /**
         * @return the launchCount
         */
        public int getLaunchCount() {
            return launchCount;
        }
    }
}
