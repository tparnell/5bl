/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.web.test;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.tissuelocator.integrator.service.CaTissueGridService;
import com.fiveamsolutions.tissuelocator.integrator.service.DataExportConfigServiceLocal;
import com.fiveamsolutions.tissuelocator.integrator.service.GenericServiceLocal;
import com.fiveamsolutions.tissuelocator.integrator.service.ImportBatchServiceLocal;
import com.fiveamsolutions.tissuelocator.integrator.service.TissueBankIntegrationConfigServiceLocal;
import com.fiveamsolutions.tissuelocator.integrator.service.TranslationServiceLocal;
import com.fiveamsolutions.tissuelocator.integrator.service.locator.ServiceLocator;
import com.fiveamsolutions.tissuelocator.service.CodeServiceRemote;
import com.fiveamsolutions.tissuelocator.service.CollectionProtocolServiceRemote;
import com.fiveamsolutions.tissuelocator.service.InstitutionServiceRemote;
import com.fiveamsolutions.tissuelocator.service.ParticipantServiceRemote;
import com.fiveamsolutions.tissuelocator.service.SpecimenServiceRemote;
import com.fiveamsolutions.tissuelocator.service.extension.DynamicExtensionsServiceRemote;

/**
 * @author smiller
 *
 */
public class MockServiceLocator implements ServiceLocator {

    private MockTissueBankIntegrationConfigService configService = new MockTissueBankIntegrationConfigService();

    /**
     * {@inheritDoc}
     */
    public GenericServiceLocal<PersistentObject> getGenericService() {
        return new GenericServiceStub<PersistentObject>();
    }

    /**
     * {@inheritDoc}
     */
    public TissueBankIntegrationConfigServiceLocal getTissueBankIntegrationConfigService() {
        return configService;
    }

    /**
     * {@inheritDoc}
     */
    public SpecimenServiceRemote getSpecimenService() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public ParticipantServiceRemote getParticipantService() {
        return null;
    }
    
    /**
     * {@inheritDoc}
     */
    public InstitutionServiceRemote getInstitutionService() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public CollectionProtocolServiceRemote getCollectionProtocolService() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public ImportBatchServiceLocal getImportBatchService() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public CaTissueGridService getCaTissueGridService() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public CodeServiceRemote getCodeService() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public TranslationServiceLocal getTranslationService() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public DataExportConfigServiceLocal getDataExportConfigService() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public DynamicExtensionsServiceRemote getDynamicExtensionsService() {
        return null;
    }

}