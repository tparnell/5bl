/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.persister.collection.AbstractCollectionPersister;
import org.hibernate.persister.entity.EntityPersister;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.junit.After;
import org.junit.Before;
import org.jvnet.mock_javamail.Mailbox;

import com.fiveamsolutions.tissuelocator.integrator.util.TissueLocatorIntegratorHibernateUtil;
import com.fiveamsolutions.tissuelocator.integrator.util.TissueLocatorIntegratorRegistry;

/**
 * @author smiller
 *
 */
public abstract class AbstractHibernateTestCase {
    private static final Logger LOG = Logger.getLogger(AbstractHibernateTestCase.class);
    private static SchemaExport schemaExporter;

    /**
     * the transaction.
     */
    private Transaction transaction;

    /**
     * Method that runs before the test to clean up the db and start the transaction.
     */
    @SuppressWarnings("unchecked")
    @Before
    public final void setUp() {
        // clean up the hibernate second level cache between runs
        SessionFactory sf = TissueLocatorIntegratorHibernateUtil.getCurrentSession().getSessionFactory();
        Map<?, EntityPersister> classMetadata = sf.getAllClassMetadata();
        for (EntityPersister ep : classMetadata.values()) {
            if (ep.hasCache()) {
                sf.evictEntity(ep.getCacheAccessStrategy().getRegion().getName());
            }
        }

        Map<?, AbstractCollectionPersister> collMetadata = sf.getAllCollectionMetadata();
        for (AbstractCollectionPersister acp : collMetadata.values()) {
            if (acp.hasCache()) {
                sf.evictCollection(acp.getCacheAccessStrategy().getRegion().getName());
            }
        }
        transaction = TissueLocatorIntegratorHibernateUtil.getHibernateHelper().beginTransaction();
        TissueLocatorIntegratorRegistry.getInstance().setServiceLocator(new TestServiceLocator());
    }

    /**
     * method to clean up after test is done.
     */
    @After
    public final void tearDown() {
        try {
            transaction.commit();
        } catch (Exception e) {
            TissueLocatorIntegratorHibernateUtil.getHibernateHelper().rollbackTransaction(transaction);
        }
    }

    /**
     * method to init the db if needed.
     * @throws SQLException on error
     */
    @Before
    @SuppressWarnings({"unchecked", "deprecation" })
    public final void initDbIfNeeded() throws SQLException {
        // First drop the audit sequence (& associated table, see
        // http://opensource.atlassian.com/projects/hibernate/browse/HHH-2472)
        Transaction tx = TissueLocatorIntegratorHibernateUtil.getHibernateHelper().beginTransaction();
        Statement s = TissueLocatorIntegratorHibernateUtil.getCurrentSession().connection().createStatement();
        try {
            s.execute("drop sequence AUDIT_ID_SEQ");
            s.execute("drop table if exists dual_AUDIT_ID_SEQ");
        } catch (SQLException e) {
            // expected
        }
        tx.commit();

        tx = TissueLocatorIntegratorHibernateUtil.getHibernateHelper().beginTransaction();
        List<Long> counts = TissueLocatorIntegratorHibernateUtil.getCurrentSession().createQuery(
                "select count(*) from " + Object.class.getName()).list();
        s = TissueLocatorIntegratorHibernateUtil.getCurrentSession().connection().createStatement();
        s.execute("create sequence AUDIT_ID_SEQ");
        s.execute("create table dual_AUDIT_ID_SEQ(test boolean)");
        for (Long count : counts) {
            if (count > 0) {
                LOG.debug("DB contains data, dropping and readding.");
                if (schemaExporter == null) {
                    schemaExporter = new
                        SchemaExport(TissueLocatorIntegratorHibernateUtil.getHibernateHelper().getConfiguration());
                }
                schemaExporter.drop(false, true);
                schemaExporter.create(false, true);
                break;
            }
        }
        tx.commit();
    }

    /**
     * verify an email.
     * @param recipient the recipient of the email
     * @param subjectContent the content for the subject
     * @param content the expected content
     * @throws MessagingException on error
     * @throws IOException on error
     */
    protected void testEmail(String recipient, String subjectContent, String... content)
        throws MessagingException, IOException {
        Mailbox inbox = Mailbox.get(recipient);
        assertNotNull(inbox);
        MimeMessage message = (MimeMessage) inbox.get(0);
        assertNotNull(message);
        testEmailPart(message.getSubject(), subjectContent);
        MimeMultipart mmp = (MimeMultipart) message.getContent();
        testEmailPart(mmp.getBodyPart(0).getContent().toString(), content);
        testEmailPart(mmp.getBodyPart(1).getContent().toString(), content);
        inbox.remove(message);
    }

    private void testEmailPart(String emailPart, String... content) {
        assertTrue(StringUtils.isNotBlank(emailPart));
        if (!emailPart.contains("N/A")) {
            for (String contentPart : content) {
                assertTrue(emailPart.toLowerCase().contains(contentPart.toLowerCase()));
            }
        }
        assertFalse(emailPart.contains("{"));
        assertFalse(emailPart.contains("}"));
    }
}
