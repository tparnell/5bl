/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.services.adapter;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.fiveamsolutions.tissuelocator.integrator.data.CaTissueIntegrationConfig;
import com.fiveamsolutions.tissuelocator.integrator.data.CbmDataExportConfig;
import com.fiveamsolutions.tissuelocator.integrator.data.FileImportIntegrationConfig;
import com.fiveamsolutions.tissuelocator.integrator.service.DataExportConfigServiceLocal;
import com.fiveamsolutions.tissuelocator.integrator.service.TissueBankIntegrationConfigServiceLocal;
import com.fiveamsolutions.tissuelocator.integrator.service.adapter.AdapterLauncher;
import com.fiveamsolutions.tissuelocator.integrator.services.TissueBankIntegrationHelper;
import com.fiveamsolutions.tissuelocator.integrator.test.AbstractHibernateTestCase;
import com.fiveamsolutions.tissuelocator.integrator.test.TestProperties;
import com.fiveamsolutions.tissuelocator.integrator.util.TissueLocatorIntegratorRegistry;

/**
 * @author smiller
 *
 */
public class AdapterLauncherTest extends AbstractHibernateTestCase {

    private static final int INTERVAL = 600000;

    /**
     * test the adapter.
     */
    @Test
    public void testAdapterLauncher() {
        AdapterLauncher launcher = AdapterLauncher.getInstance();
        assertEquals(0, launcher.launchAdapterThreads());
        // Assert no Exceptions occur when calling stop with no threads started.
        launcher.stopAdapterThreads();

        TissueBankIntegrationConfigServiceLocal service =
            TissueLocatorIntegratorRegistry.getServiceLocator().getTissueBankIntegrationConfigService();
        FileImportIntegrationConfig config = TissueBankIntegrationHelper.createConfig(1L, INTERVAL);
        service.savePersistentObject(config);

        CaTissueIntegrationConfig config2 = new CaTissueIntegrationConfig();
        config2.setGridServiceUrl("http://localhost:8081/wsrf/services/cagrid/CaTissueSuite");
        config2.setInstitutionId(2L);
        config2.setStartTime(TissueBankIntegrationHelper.getNonCurrentTime());
        config2.setNotificationEmail("notification@email.com");
        config2.setNotificationInterval(1);
        service.savePersistentObject(config2);

        DataExportConfigServiceLocal exportConfigService =
            TissueLocatorIntegratorRegistry.getServiceLocator().getDataExportConfigService();
        CbmDataExportConfig cbmConfig = new CbmDataExportConfig();
        cbmConfig.setBatchSize(1);
        cbmConfig.setInterval(1);
        cbmConfig.setNotificationEmail("test@example.com");
        cbmConfig.setStartTime(TissueBankIntegrationHelper.getNonCurrentTime());
        cbmConfig.setCodeSet("CBM 1.0");
        cbmConfig.setDbDriverClass(TestProperties.getCbmDriver());
        cbmConfig.setDbUrl(TestProperties.getCbmUrl());
        cbmConfig.setDbUsername(TestProperties.getCbmUsername());
        cbmConfig.setDbPassword(TestProperties.getCbmPassworString());
        exportConfigService.savePersistentObject(cbmConfig);

        assertEquals(2, launcher.launchAdapterThreads());
        launcher.stopAdapterThreads();
    }
}
