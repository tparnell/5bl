/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.integrator.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

/**
 * @author gvaughn
 *
 */
public class ThreadsyncedCacheTest {

    /**
     * Tests the addition of objects to all caches.
     */
    @Test
    public void testAddToAllCaches() {
        ThreadsyncedCache<Object> cache = new ThreadsyncedCache<Object>();
        Object cacheId1 = new Object();
        String key1 = "key1";
        Object val1 = new Object();
        
        assertNull(cache.getFromCache(cacheId1, key1));
        cache.addToAllCaches(cacheId1, key1, val1);
        assertEquals(val1, cache.getFromCache(cacheId1, key1));
        
        Object cacheId2 = new Object();
        String key2 = "key2";
        Object val2 = new Object();
        assertNull(cache.getFromCache(cacheId2, key2));
        assertNull(cache.getFromCache(cacheId1, key2));
        cache.addToAllCaches(cacheId2, key2, val2);
        assertEquals(val1, cache.getFromCache(cacheId1, key1));
        assertEquals(val2, cache.getFromCache(cacheId1, key2));
        assertEquals(val2, cache.getFromCache(cacheId2, key2));
        
        cache.clearCache(cacheId2);
        assertEquals(val1, cache.getFromCache(cacheId1, key1));
        assertEquals(val2, cache.getFromCache(cacheId1, key2));
        assertNull(cache.getFromCache(cacheId2, key2));
        
        cache.clearCache(cacheId1);
        assertNull(cache.getFromCache(cacheId1, key1));
        assertNull(cache.getFromCache(cacheId1, key2));
        assertNull(cache.getFromCache(cacheId2, key2));       
    }
    
    /**
     * Tests the addition of objects to a single cache.
     */
    @Test
    public void testAddToCache() {
        ThreadsyncedCache<Object> cache = new ThreadsyncedCache<Object>();
        Object cacheId1 = new Object();
        String key1 = "key1";
        Object val1 = new Object();
        
        assertNull(cache.getFromCache(cacheId1, key1));
        cache.addToCache(cacheId1, key1, val1);
        assertEquals(val1, cache.getFromCache(cacheId1, key1));
        
        Object cacheId2 = new Object();
        String key2 = "key2";
        Object val2 = new Object();
        assertNull(cache.getFromCache(cacheId2, key2));
        assertNull(cache.getFromCache(cacheId1, key2));
        cache.addToCache(cacheId2, key2, val2);
        assertNull(cache.getFromCache(cacheId1, key2));
        assertEquals(val2, cache.getFromCache(cacheId2, key2));
        
        cache.clearCache(cacheId2);
        assertEquals(val1, cache.getFromCache(cacheId1, key1));
        assertNull(cache.getFromCache(cacheId2, key2));
        
        cache.clearCache(cacheId1);
        assertNull(cache.getFromCache(cacheId1, key1));
        assertNull(cache.getFromCache(cacheId2, key2));       
    }
}
