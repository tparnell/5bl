/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.services.adapter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.text.ParseException;

import com.fiveamsolutions.tissuelocator.data.CollectionProtocol;
import com.fiveamsolutions.tissuelocator.data.Contact;
import com.fiveamsolutions.tissuelocator.data.Ethnicity;
import com.fiveamsolutions.tissuelocator.data.Gender;
import com.fiveamsolutions.tissuelocator.data.Participant;
import com.fiveamsolutions.tissuelocator.data.QuantityUnits;
import com.fiveamsolutions.tissuelocator.data.Race;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.SpecimenStatus;
import com.fiveamsolutions.tissuelocator.data.TimeUnits;
import com.fiveamsolutions.tissuelocator.data.code.AdditionalPathologicFinding;
import com.fiveamsolutions.tissuelocator.data.code.Code;
import com.fiveamsolutions.tissuelocator.data.code.SpecimenType;

/**
 * @author bpickeral
 *
 */
public final class SpecimenValidator {

    /**
     * 
     */
    private static final int YEAR = 2010;
    /**
     * Instance of this class used for testing data SpecimenHelper created.
     */
    public static final SpecimenValidator INSTANCE = new SpecimenValidator();

    /**
     * Validate Specimen object.
     * @param specimen Specimen to validate
     */
    public void checkSpecimen(Specimen specimen) {
        assertEquals(SpecimenHelper.EXTERNAL_ID, specimen.getExternalId());
        assertEquals(SpecimenHelper.AGE, specimen.getPatientAgeAtCollection());
        assertEquals(TimeUnits.YEARS, specimen.getPatientAgeAtCollectionUnits());
        assertEquals(SpecimenHelper.QUANTITY, specimen.getAvailableQuantity());
        assertEquals(QuantityUnits.MG, specimen.getAvailableQuantityUnits());
        checkCode(specimen.getPathologicalCharacteristic(), AdditionalPathologicFinding.class);
        checkCode(specimen.getSpecimenType(), SpecimenType.class);
        assertEquals(SpecimenHelper.MIN_PRICE, specimen.getMinimumPrice());
        assertEquals(SpecimenHelper.MAX_PRICE, specimen.getMaximumPrice());
        assertFalse(specimen.isPriceNegotiable());
        assertEquals(SpecimenStatus.UNAVAILABLE, specimen.getStatus());
        assertEquals(SpecimenStatus.AVAILABLE, specimen.getPreviousStatus());
        checkParticipant(specimen.getParticipant());
        checkProtocol(specimen.getProtocol());
        assertEquals(YEAR, specimen.getCollectionYear().intValue());
        checkCustomProperties(specimen);
    }
    
    /**
     * Validate Specimen imported via csv.
     * @param specimen Specimen to validate
     * @param checkCustomProperties whether custom property values should be checked
     */
    public void checkCsvSpecimen(Specimen specimen, boolean checkCustomProperties) {
        assertEquals(SpecimenHelper.EXTERNAL_ID, specimen.getExternalId());
        assertEquals(SpecimenHelper.AGE, specimen.getPatientAgeAtCollection());
        assertEquals(TimeUnits.YEARS, specimen.getPatientAgeAtCollectionUnits());
        assertEquals(SpecimenHelper.QUANTITY, specimen.getAvailableQuantity());
        assertEquals(QuantityUnits.MG, specimen.getAvailableQuantityUnits());
        checkCode(specimen.getPathologicalCharacteristic(), AdditionalPathologicFinding.class);
        checkCode(specimen.getSpecimenType(), SpecimenType.class);
        assertEquals(SpecimenHelper.MIN_PRICE, specimen.getMinimumPrice());
        assertEquals(SpecimenHelper.MAX_PRICE, specimen.getMaximumPrice());
        assertFalse(specimen.isPriceNegotiable());
        assertEquals(SpecimenStatus.UNAVAILABLE, specimen.getStatus());
        checkParticipant(specimen.getParticipant());
        assertEquals(SpecimenHelper.NAME, specimen.getProtocol().getName());
        assertEquals(YEAR, specimen.getCollectionYear().intValue());
        if (checkCustomProperties) {
            checkCustomProperties(specimen);
        }    
    }

    private <T extends Code> void checkCode(Code code, Class<T> c) {
        assertTrue(code.getActive());
        assertEquals(c.getSimpleName() + "-active", code.getName());
    }

    private void checkContact(Contact contact) {
        assertEquals(SpecimenHelper.CONTACT_FIRST, contact.getFirstName());
        assertEquals(SpecimenHelper.CONTACT_LAST, contact.getLastName());
        assertEquals(SpecimenHelper.CONTACT_EMAIL, contact.getEmail());
        assertEquals(SpecimenHelper.CONTACT_PHONE, contact.getPhone());
    }

    private void checkParticipant(Participant participant) {
        assertEquals(Ethnicity.HISPANIC_OR_LATINO, participant.getEthnicity());
        assertEquals(Race.BLACK_OR_AFRICAN_AMERICAN, participant.getRaces().get(0));
        assertEquals(SpecimenHelper.EXTERNAL_ID, participant.getExternalId());
        assertEquals(Gender.FEMALE, participant.getGender());
    }

    private void checkProtocol(CollectionProtocol protocol) {
        assertEquals(SpecimenHelper.NAME, protocol.getName());
        checkContact(protocol.getContact());
        try {
            assertEquals(SpecimenHelper.FORMAT.parse(SpecimenHelper.DATE), protocol.getEndDate());
            assertEquals(SpecimenHelper.FORMAT.parse(SpecimenHelper.DATE), protocol.getStartDate());
        } catch (ParseException pe) {
            fail("Error parsing date");
        }
    }

    private void checkCustomProperties(Specimen specimen) {
        assertEquals(SpecimenHelper.COLLECTION_TYPE, specimen.getCustomProperty("collectionType"));
        assertEquals(SpecimenHelper.ANATOMIC_SOURCE, specimen.getCustomProperty("anatomicSource"));
        assertEquals(SpecimenHelper.PRESERVATION_TYPE, specimen.getCustomProperty("preservationType"));
        assertEquals(SpecimenHelper.TIME_LAPSE_TO_PROCESSING.toString(),
                specimen.getCustomProperty("timeLapseToProcessing"));
        assertEquals(SpecimenHelper.SPECIMEN_DENSITY.toString(), specimen.getCustomProperty("specimenDensity"));
        assertEquals(SpecimenHelper.STORAGE_TEMPERATURE.toString(), specimen.getCustomProperty("storageTemperature"));
    }

}
