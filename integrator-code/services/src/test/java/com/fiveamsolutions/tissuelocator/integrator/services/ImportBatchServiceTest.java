/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.services;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.junit.Before;
import org.junit.Test;

import com.fiveamsolutions.tissuelocator.integrator.data.AbstractTissueBankIntegrationConfig;
import com.fiveamsolutions.tissuelocator.integrator.data.FileImportBatch;
import com.fiveamsolutions.tissuelocator.integrator.data.ImportBatch;
import com.fiveamsolutions.tissuelocator.integrator.data.jaxb.SpecimenList;
import com.fiveamsolutions.tissuelocator.integrator.service.ImportBatchServiceLocal;
import com.fiveamsolutions.tissuelocator.integrator.service.TissueBankIntegrationConfigServiceLocal;
import com.fiveamsolutions.tissuelocator.integrator.services.adapter.SpecimenHelper;
import com.fiveamsolutions.tissuelocator.integrator.test.AbstractHibernateTestCase;
import com.fiveamsolutions.tissuelocator.integrator.util.TissueLocatorIntegratorRegistry;

/**
 * @author bpickeral
 *
 */
public class ImportBatchServiceTest extends AbstractHibernateTestCase {
    private AbstractTissueBankIntegrationConfig config;
    private TissueBankIntegrationConfigServiceLocal tissueBankService =
        TissueLocatorIntegratorRegistry.getServiceLocator().getTissueBankIntegrationConfigService();
    private ImportBatchServiceLocal importBatchService = TissueLocatorIntegratorRegistry.getServiceLocator()
    .getImportBatchService();

    private static final String SYNC_DATE1 = "01/01/2010";
    private static final String SYNC_DATE2 = "01/05/2010";
    private static final String SYNC_DATE3 = "01/03/2010";

    /**
     * Before method.
     */
    @Before
    public void before() {
        config = TissueBankIntegrationHelper.createConfig(1L, 1, new Date());
        Long configId = tissueBankService.savePersistentObject(config);
        config = tissueBankService.getPersistentObject(AbstractTissueBankIntegrationConfig.class, configId);
    }

    /**
     * Test creating and retrieving an ImportBatch object.
     */
    @Test
    public void testCreateAndRetrieve() {
        ImportBatch importBatch = TissueBankIntegrationHelper.createBatch(config);
        Long id = importBatchService.savePersistentObject(importBatch);

        ImportBatch retrievedBatch = importBatchService.getPersistentObject(ImportBatch.class, id);
        assertTrue(EqualsBuilder.reflectionEquals(importBatch, retrievedBatch));
    }

    /**
     * Test creating and retrieving a FileImportBatch object.
     * @throws Exception if Exception occurs
     */
    @Test
    public void testCreateAndRetrieveFile() throws Exception {
        SpecimenList specimenList = new SpecimenList();
        specimenList.getSpecimens().add(SpecimenHelper.INSTANCE.createSpecimen());
        FileImportBatch fileBatch = createImportBatch(SpecimenHelper.FORMAT.parse(SYNC_DATE1));

        Long id = importBatchService.savePersistentObject((ImportBatch) fileBatch);

        ImportBatch retrievedBatch = importBatchService.getPersistentObject(ImportBatch.class, id);
        assertTrue(EqualsBuilder.reflectionEquals(fileBatch, retrievedBatch));
    }

    private FileImportBatch createImportBatch(Date endDate) throws ParseException {
        FileImportBatch fileBatch = new FileImportBatch();
        fileBatch.setStartTime(new Date());
        fileBatch.setEndTime(endDate);
        fileBatch.setConfig(config);
        fileBatch.setFileName("fakefile.xml");
        return fileBatch;
    }

    /**
     * Tests getLastSynchronizationDate returns the latest end date.
     * @throws ParseException if error occurs when parsing date
     */
    @Test
    public void testStoreBatches() throws ParseException {
        assertTrue(importBatchService.getAll(ImportBatch.class).isEmpty());
        List<ImportBatch> batches = new ArrayList<ImportBatch>();
        batches.add(createImportBatch(SpecimenHelper.FORMAT.parse(SYNC_DATE1)));
        batches.add(createImportBatch(SpecimenHelper.FORMAT.parse(SYNC_DATE2)));
        batches.add(createImportBatch(SpecimenHelper.FORMAT.parse(SYNC_DATE3)));
        importBatchService.storeBatches(batches);
        assertFalse(importBatchService.getAll(ImportBatch.class).isEmpty());
        assertEquals(batches.size(), importBatchService.getAll(ImportBatch.class).size());
    }
}
