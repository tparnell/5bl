ALTER TABLE file_import_integration_config ADD COLUMN username varchar(24);
ALTER TABLE file_import_integration_config ADD COLUMN password varchar(24);
ALTER TABLE file_import_integration_config ADD COLUMN rootDirectory varchar(254);

create table file_import_batch (lob bytea not null, import_batch_id int8 not null, primary key (import_batch_id));
create table import_batch (id int8 not null, endTime timestamp not null, failures int4 not null, imports int4 not null, startTime timestamp not null, file_import_integration_config_id int8 not null, primary key (id));
create index file_import_integration_config_idx on import_batch (file_import_integration_config_id);
alter table file_import_batch add constraint FKC985DC03502433C6 foreign key (import_batch_id) references import_batch;
alter table import_batch add constraint file_import_integration_config_fk foreign key (file_import_integration_config_id) references integration_config;