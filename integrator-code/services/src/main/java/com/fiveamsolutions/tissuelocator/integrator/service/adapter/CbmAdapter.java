/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.service.adapter;

import gov.nih.nci.cbm.domain.logicalmodel.SpecimenCollectionSummary;

import java.sql.SQLException;
import java.util.List;

import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.integrator.cbm.service.CbmDataTranslator;
import com.fiveamsolutions.tissuelocator.integrator.cbm.service.CbmPersistenceService;
import com.fiveamsolutions.tissuelocator.integrator.data.AbstractDataExportConfig;
import com.fiveamsolutions.tissuelocator.integrator.data.CbmDataExportConfig;
import com.fiveamsolutions.tissuelocator.integrator.util.NotificationHelper;
import com.fiveamsolutions.tissuelocator.integrator.util.TissueLocatorIntegratorRegistry;
import com.fiveamsolutions.tissuelocator.service.SpecimenServiceRemote;

/**
 * @author smiller
 */
public class CbmAdapter implements DataExportAdapter {

    private final SpecimenServiceRemote specimenService = TissueLocatorIntegratorRegistry.getServiceLocator()
            .getSpecimenService();

    private CbmPersistenceService cbmService;

    /**
     * {@inheritDoc}
     */
    public synchronized int launch(AbstractDataExportConfig config) {
        CbmDataExportConfig cbmConfig = (CbmDataExportConfig) config;
        long lastId = -1;
        int curPage = 0;
        int importCount = 0;
        
        try {
            initService(cbmConfig);
            cbmService.deleteAllData();

            List<Specimen> batch = specimenService.getAvailableSpecimens(curPage, cbmConfig.getBatchSize());
            CbmDataTranslator translator = new CbmDataTranslator(config.getCodeSet());
            while (!batch.isEmpty()) {
                for (Specimen source : batch) {
                    if (source.getId().longValue() > lastId) {
                        SpecimenCollectionSummary dest = translator.translateSpecimen(source);
                        cbmService.createOrUpdate(dest);
                        importCount++;
                        lastId = source.getId();
                    }
                }
                curPage++;
                batch = specimenService.getAvailableSpecimens(curPage, cbmConfig.getBatchSize());
            }
        } catch (ClassNotFoundException e) { 
            NotificationHelper.getInstance().sendErrorNotification("Unable to load cbm db driver.",
                    config.getNotificationEmail(), e);
        } catch (SQLException e) { 
            NotificationHelper.getInstance().sendErrorNotification("Error while exporting to CBM.",
                    config.getNotificationEmail(), e);
        } finally {
            tearDownSerivce(config);
        }
        return importCount;
    }

    private void initService(CbmDataExportConfig cbmConfig) throws ClassNotFoundException, SQLException {
        cbmService = createCbmPersistenceService(cbmConfig);
        cbmService.connect();
        cbmService.beginTransaction();
    }

    /**
     * create the service.
     * @param cbmConfig the config.
     * @return the service
     */
    public CbmPersistenceService createCbmPersistenceService(CbmDataExportConfig cbmConfig) {
        return new CbmPersistenceService(cbmConfig);
    }

    private void tearDownSerivce(AbstractDataExportConfig config) {
        if (cbmService != null) {
            try {
                cbmService.commitTransaction();
                cbmService.disconnect();
            } catch (SQLException e) { 
                NotificationHelper.getInstance().sendErrorNotification(
                        "Error while committing and disconnecting from CBM.",
                        config.getNotificationEmail(), e);
            } 
            
            cbmService = null;
        }
    }
}
