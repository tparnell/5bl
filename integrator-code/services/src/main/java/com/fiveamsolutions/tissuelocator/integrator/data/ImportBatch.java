/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.data;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.Index;
import org.hibernate.validator.NotNull;

import com.fiveamsolutions.nci.commons.audit.Auditable;
import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;

/**
 * @author bpickeral
 *
 */
@Entity
@Table(name = "import_batch")
@Inheritance(strategy = InheritanceType.JOINED)
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ImportBatch", propOrder = {
    "imports",
    "failures",
    "startTime",
    "endTime"
})
@XmlRootElement(name = "importBatch")
public class ImportBatch implements PersistentObject, Auditable {
    private static final long serialVersionUID = 1L;

    @XmlTransient
    private Long id;
    @XmlTransient
    private AbstractTissueBankIntegrationConfig config;
    private Date startTime;
    private Date endTime;
    private Integer imports = 0;
    private Integer failures = 0;

    /**
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the config
     */
    @ManyToOne
    @JoinColumn(name = "file_import_integration_config_id")
    @ForeignKey(name = "file_import_integration_config_fk")
    @Index(name = "file_import_integration_config_idx")
    @NotNull
    public AbstractTissueBankIntegrationConfig getConfig() {
        return config;
    }
    /**
     * @param config the config to set
     */
    public void setConfig(AbstractTissueBankIntegrationConfig config) {
        this.config = config;
    }
    /**
     * @return the startTime
     */
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    public Date getStartTime() {
        return startTime;
    }
    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }
    /**
     * @return the endTime
     */
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    public Date getEndTime() {
        return endTime;
    }
    /**
     * @param endTime the endTime to set
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
    /**
     * @return the imports
     */
    @NotNull
    public Integer getImports() {
        return imports;
    }
    /**
     * @param imports the imports to set
     */
    public void setImports(Integer imports) {
        this.imports = imports;
    }
    /**
     * @return the failures
     */
    @NotNull
    public Integer getFailures() {
        return failures;
    }
    /**
     * @param failures the failures to set
     */
    public void setFailures(Integer failures) {
        this.failures = failures;
    }
    /**
     * Increment the number of imports by 1.
     */
    @Transient
    public synchronized void incrementImports() {
        imports++;
    }
    /**
     * Increment the number of failures by 1.
     */
    @Transient
    public synchronized void incrementFailures() {
        failures++;
    }
}
