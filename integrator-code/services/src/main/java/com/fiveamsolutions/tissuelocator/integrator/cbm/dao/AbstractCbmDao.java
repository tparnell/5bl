/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.cbm.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author smiller
 *
 */
public class AbstractCbmDao {
    private final Connection connection;
    
    /**
     * Create the DAO.
     * @param connection the connection.
     */
    public AbstractCbmDao(Connection connection) {
        this.connection = connection;
    }

    /**
     * @return the connection
     */
    public Connection getConnection() {
        return connection;
    }
    
    /**
     * Executes an update statement.
     * @param sql the sql
     * @param params the params
     * @return the number of rows impacted
     * @throws SQLException on error
     */
    protected int executeUpdate(String sql, Object[] params) throws SQLException {
        PreparedStatement s = null;
        try {
            s = connection.prepareStatement(sql);
            processParams(params, s);
            return s.executeUpdate();
        } finally {
            if (s != null) {
                s.close();
            }
        }
    }
    
    
    /**
     * Execute a prepared query.
     * @param sql the sql to execute
     * @param params the params to the query
     * @return the list of results.
     * @throws SQLException on error.
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    protected List executePreparedQuery(String sql, Object[] params) throws SQLException {
        List results = new ArrayList();
        PreparedStatement s = null;
        ResultSet r = null;
        try {
            s = connection.prepareStatement(sql);
            processParams(params, s);
            r = s.executeQuery();
            while (r.next()) {
                results.add(r.getObject(1));
            }
            return results;
        } finally {
            if (r != null) {
                r.close();
            }
            if (s != null) {
                s.close();
            }
        }
    }

    private void processParams(Object[] params, PreparedStatement s) throws SQLException {
        for (int i = 1; i <= params.length; i++) {
            Object param = params[i - 1];
            if (param instanceof Long) {
                s.setLong(i, (Long) param);
            } else if (param instanceof Integer) {
                s.setInt(i, (Integer) param);
            } else if (param instanceof Date) {
                s.setDate(i, new java.sql.Date(((Date) param).getTime()));
            } else {
                s.setString(i, (String) param);
            }
        }
    }
}
