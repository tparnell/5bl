/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import com.fiveamsolutions.tissuelocator.integrator.data.ImportBatch;
import com.fiveamsolutions.tissuelocator.integrator.data.jaxb.SpecimenList;

/**
 * @author ddasgupta
 *
 */
@WebService(name = "CaTissueService")
public interface CaTissueSpecimenService {

    /**
     * Process a specimen extracted from the caTissue database.
     * This includes using the caTissue grid service to complete population of the specimen.
     * Assumptions:
     * <ul>
     *   <li> The identifier of the specimen in the caTissue database is in the id property of the incoming specimen.
     *   <li> The type of the specimen is in the specimen class property of the specimen type property of the specimen.
     * </ul>
     * @param specimens the specimens to process
     * @param locationId the id of the location of the specimens
     * @return an indication of success
     */
    @WebMethod(operationName = "processSpecimens")
    @WebResult(name = "processSpecimensResult", targetNamespace = "", header = false)
    @RequestWrapper(localName = "processSpecimens",
            className = "com.fiveamsolutions.tissuelocator.integrator.service.ProcessSpecimens")
    @ResponseWrapper(localName = "processSpecimensResponse",
            className = "com.fiveamsolutions.tissuelocator.integrator.service.ProcessSpecimensResponse")
    ImportBatch processSpecimens(
            @WebParam(name = "specimens", targetNamespace = "") SpecimenList specimens,
            @WebParam(name = "locationId", targetNamespace = "") Long locationId);
}
