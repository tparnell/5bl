/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;

import com.fiveamsolutions.tissuelocator.data.code.Code;
import com.fiveamsolutions.tissuelocator.integrator.data.AbstractTissueBankIntegrationConfig;
import com.fiveamsolutions.tissuelocator.integrator.data.Translation;
import com.fiveamsolutions.tissuelocator.integrator.service.adapter.SpecimenColumn;
import com.fiveamsolutions.tissuelocator.integrator.util.NotificationHelper;
import com.fiveamsolutions.tissuelocator.integrator.util.TissueLocatorIntegratorHibernateUtil;
import com.fiveamsolutions.tissuelocator.integrator.util.TissueLocatorIntegratorRegistry;
import com.fiveamsolutions.tissuelocator.service.CodeServiceRemote;

/**
 * @author ddasgupta
 *
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class TranslationServiceBean extends GenericServiceBean<Translation>
    implements TranslationServiceLocal {

    private static final String TRANSLATION_QUERY = "from " + Translation.class.getName()
        + " where lower(sourceValue) = :sourceValue "
        + " and codeSetName = :codeSetName and codeType = :codeType";

    /**
     * {@inheritDoc}
     */
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public <T extends Code> T getCode(String sourceValue, Long locationId, Class<T> codeType,
            Map<String, Map<String, Map<String, String>>> translationMap) {
        AbstractTissueBankIntegrationConfig config = getConfigByLocation(locationId);
        String translatedValue = translateCode(config, sourceValue, codeType, translationMap);
        if (StringUtils.isNotBlank(translatedValue)) {
            CodeServiceRemote codeService = TissueLocatorIntegratorRegistry.getServiceLocator().getCodeService();
            T code = codeService.getCode(translatedValue, codeType);
            if (code == null) {
                NotificationHelper.getInstance().sendUnmappedCodeNotification(config, sourceValue, translatedValue,
                        codeType);
            }
            return code;
        }
        return null;
    }

    private AbstractTissueBankIntegrationConfig getConfigByLocation(Long locationId) {
        TissueBankIntegrationConfigServiceLocal configService =
            TissueLocatorIntegratorRegistry.getServiceLocator().getTissueBankIntegrationConfigService();
        return configService.getByLocationId(locationId);
    }

    /**
     * {@inheritDoc}
     */
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public <T extends Enum> T getEnum(String sourceValue, Long locationId, Class<T> codeType,
            Map<String, Map<String, Map<String, String>>> translationMap) {
        AbstractTissueBankIntegrationConfig config = getConfigByLocation(locationId);
        String translatedValue = translateCode(config, sourceValue, codeType, translationMap);
        if (StringUtils.isNotBlank(translatedValue)) {
            T code = null;
            try {
                code = (T) Enum.valueOf(codeType, translatedValue);
            } catch (IllegalArgumentException e) {
                NotificationHelper.getInstance().sendUnmappedEnumNotification(config, sourceValue, translatedValue,
                        codeType);
            }
            return code;
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public String getCustomProperty(String clientValue, AbstractTissueBankIntegrationConfig config,
            String customPropertyName,
            Map<String, Map<String, Map<String, String>>> translationMap) {
        return translateValue(config, clientValue, customPropertyName, translationMap);
    }

    /**
     * {@inheritDoc}
     */
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public String getSpecimenColumn(String clientValue,
            Long locationId,
            Map<String, Map<String, Map<String, String>>> translationMap) {
        AbstractTissueBankIntegrationConfig config = getConfigByLocation(locationId);
        return translateValue(config, clientValue, SpecimenColumn.class.getSimpleName(), translationMap);
    }

    /**
     * {@inheritDoc}
     */
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Translation getTranslation(String sourceValue, String codeSetName, String codeType) {
        // Search the code sets in order for a translation and return the first one you find
        Query q = TissueLocatorIntegratorHibernateUtil.getCurrentSession().createQuery(TRANSLATION_QUERY);
        q.setParameter("sourceValue", StringUtils.lowerCase(sourceValue));
        q.setParameter("codeType", codeType);
        q.setParameter("codeSetName", codeSetName);
        return (Translation) q.uniqueResult();
    }

    private <T> String translateCode(AbstractTissueBankIntegrationConfig config,
            String sourceValue, Class<T> codeType, Map<String, Map<String, Map<String, String>>> translationMap) {
        return translateValue(config, sourceValue, codeType.getSimpleName(), translationMap);
    }
    
    private String translateValue(AbstractTissueBankIntegrationConfig config,
            String sourceValue, String valueType, Map<String, Map<String, Map<String, String>>> translationMap) {
        //Search the code sets in order for a translation and return the first one you find
        for (String codeSet : config.getCodeSets()) {
            Map<String, Map<String, String>> codeSetMap = translationMap.get(codeSet);
            if (codeSetMap == null) {
                continue;
            }
            Map<String, String> codeTypeMap = codeSetMap.get(valueType);
            if (codeTypeMap == null) {
                continue;
            }
            String translatedValue = codeTypeMap.get(StringUtils.lowerCase(sourceValue));
            if (translatedValue == null) {
                continue;
            }
            return translatedValue;
        }

        // if no translation found, use the original value
        return sourceValue;
    }

    /**
     * {@inheritDoc}
     */
    public Map<String, Map<String, Map<String, String>>> getTranslationMap(Long locationId) {
        List<Translation> translations = getTranslations(locationId);
        Map<String, Map<String, Map<String, String>>> translationMap =
            new HashMap<String, Map<String, Map<String, String>>>();
        for (Translation translation : translations) {
            Map<String, Map<String, String>> codeSetMap = translationMap.get(translation.getCodeSetName());
            if (codeSetMap == null) {
                codeSetMap = new HashMap<String, Map<String, String>>();
                translationMap.put(translation.getCodeSetName(), codeSetMap);
            }
            Map<String, String> codeTypeMap = codeSetMap.get(translation.getCodeType());
            if (codeTypeMap == null) {
                codeTypeMap = new HashMap<String, String>();
                codeSetMap.put(translation.getCodeType(), codeTypeMap);
            }
            codeTypeMap.put(StringUtils.lowerCase(translation.getSourceValue()), translation.getDestinationValue());
        }
        return translationMap;
    }

    @SuppressWarnings("unchecked")
    private List<Translation> getTranslations(Long locationId) {
        AbstractTissueBankIntegrationConfig config = getConfigByLocation(locationId);
        String queryString = "from " + Translation.class.getName()
        + " where codeSetName in (:codeSetNames)";
        Query q = TissueLocatorIntegratorHibernateUtil.getCurrentSession().createQuery(queryString);
        q.setParameterList("codeSetNames", config.getCodeSets());
        return q.list();
    }
}
