/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.integrator.service.adapter;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.csvreader.CsvReader;
import com.fiveamsolutions.dynamicextensions.AbstractDynamicFieldDefinition;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.integrator.data.AbstractTissueBankIntegrationConfig;
import com.fiveamsolutions.tissuelocator.integrator.data.FileImportBatch;
import com.fiveamsolutions.tissuelocator.integrator.data.FileImportIntegrationConfig;
import com.fiveamsolutions.tissuelocator.integrator.service.TranslationServiceLocal;
import com.fiveamsolutions.tissuelocator.integrator.service.adapter.CsvParseException.InvalidCsvValue;
import com.fiveamsolutions.tissuelocator.integrator.util.TissueLocatorIntegratorRegistry;

/**
 * Converts a CSV file to domain objects.
 * @author gvaughn
 *
 */
public class CsvImportFileConverter implements ImportFileConverter {

    private CsvReader csvReader;
    private final List<String> csvHeaders = new ArrayList<String>();
    private final List<String> ignoredColumns = new ArrayList<String>();
    private TranslationServiceLocal translationService;
    private Collection<AbstractDynamicFieldDefinition> dynamicFieldDefinitions;
    private final Map<String, String> caseSensitiveCustomPropertyNameMap = new HashMap<String, String>();

    /**
     * {@inheritDoc}
     */
    @Override
    public void init(FileImportBatch batch, FtpFileImportHelper ftpHelper, Map<String, 
            Map<String, Map<String, String>>> translationMap)
            throws FileParseException {
        csvReader = new CsvReader(new InputStreamReader(ftpHelper.downloadFile(batch)),
                ((FileImportIntegrationConfig) batch.getConfig()).getDelimiterCharacter());
        translationService = TissueLocatorIntegratorRegistry.getServiceLocator().getTranslationService();
        dynamicFieldDefinitions = TissueLocatorIntegratorRegistry.getServiceLocator()
            .getDynamicExtensionsService().getDynamicFieldDefinitions(Specimen.class.getName());
        initCaseSensitiveCustomPropertyNames();
        
        try {
            if (!csvReader.readHeaders()) {
                throw new FileParseException("Could not read file headers.");
            }
            for (String header : csvReader.getHeaders()) {
                csvHeaders.add(StringUtils.lowerCase(translateHeader(header.trim(), 
                        batch.getConfig(), translationMap)));
            }
            for (String ignoredColumn : ((FileImportIntegrationConfig) batch.getConfig()).getIgnoredColumns()) {
                ignoredColumns.add(StringUtils.lowerCase(ignoredColumn.trim()));
            }
            validateColumnHeaders();
        } catch (IOException e) {
            throw new FileParseException("Exception initializing csv reader.", e);
        }

    }

    private String translateHeader(String header, AbstractTissueBankIntegrationConfig config, 
            Map<String, Map<String, Map<String, String>>> translationMap) {
        return translationService.getSpecimenColumn(header, config.getInstitutionId(), translationMap);
    }
    
    private void initCaseSensitiveCustomPropertyNames() {
        for (AbstractDynamicFieldDefinition fieldDef : dynamicFieldDefinitions) {
            caseSensitiveCustomPropertyNameMap.put(StringUtils.lowerCase(fieldDef.getFieldName().trim()), 
                    fieldDef.getFieldName());
        }
    }
    
    private String getCaseSensitiveCustomPropertyName(String header) {
        if (caseSensitiveCustomPropertyNameMap.containsKey(header)) {
            return caseSensitiveCustomPropertyNameMap.get(header);
        }
        return header;
    }
    
    /**
     * validate that all column headers are valid core fields or dynamic extensions.
     */
    private void validateColumnHeaders() throws FileParseException {
        List<String> headers = new ArrayList<String>(csvHeaders);
        headers.removeAll(ignoredColumns);

        for (SpecimenColumn column : SpecimenColumn.values()) {
            headers.remove(StringUtils.lowerCase(column.getColumnHeader().trim()));
        }

        if (!headers.isEmpty()) {
            // assume remaining columns are custom properties
            validateDeColumnHeaders(headers);
        }
    }

    private void validateDeColumnHeaders(List<String> headers) throws FileParseException {
        Set<String> deNames = new HashSet<String>();        
        for (AbstractDynamicFieldDefinition fieldDef : dynamicFieldDefinitions) {
            deNames.add(StringUtils.lowerCase(fieldDef.getFieldName().trim()));
        }
        List<String> invalidHeaders = new ArrayList<String>();
        for (String header : headers) {
            if (!deNames.contains(header)) {
                invalidHeaders.add(header);
            }
        }
        if (!invalidHeaders.isEmpty()) {
            throw new InvalidCsvHeaderException(invalidHeaders);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Specimen getNext() throws FileParseException {
        Specimen specimen = null;
        try {
            if (csvReader.readRecord()) {
                specimen = getNextSpecimen();
            } else {
                csvReader.close();
            }
        } catch (IOException e) {
            throw new FileParseException("Exception reading csv file.", e);
        }

        return specimen;
    }

    private Specimen getNextSpecimen() throws IOException, CsvParseException {
        Specimen specimen = new Specimen();
        List<InvalidCsvValue> invalidValues = new ArrayList<InvalidCsvValue>();

        List<String> headers = new ArrayList<String>(csvHeaders);
        headers.removeAll(ignoredColumns);

        for (SpecimenColumn column : SpecimenColumn.values()) {
            setSpecimenValue(specimen, column, invalidValues, headers);
        }
        if (!invalidValues.isEmpty()) {
            throw new CsvParseException(invalidValues, csvReader.getCurrentRecord() + 2);
        }
        // assume remaining columns are custom properties
        if (!headers.isEmpty()) {
            specimen.setCustomProperties(new HashMap<String, Object>());
            for (String header : headers) {
                int index = csvHeaders.indexOf(header);
                specimen.setCustomProperty(getCaseSensitiveCustomPropertyName(header),
                        csvReader.get(index));
            }
        }
        return specimen;
    }

    private void setSpecimenValue(Specimen specimen, SpecimenColumn column,
            List<InvalidCsvValue> invalidValues, List<String> headers) throws IOException {
        String lowerHeader = StringUtils.lowerCase(column.getColumnHeader().trim());
        if (headers.contains(lowerHeader)) {
            try {
                column.setValue(specimen, csvReader.get(csvHeaders.indexOf(lowerHeader)));
                headers.remove(lowerHeader);
            } catch (Exception e) {
                invalidValues.add(new InvalidCsvValue(csvReader.getHeader(csvHeaders.indexOf(lowerHeader)),
                        csvReader.get(csvHeaders.indexOf(lowerHeader)), e));
            }
        }
    }

}
