/**
 * CBMService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2RC2 Apr 28, 2006 (12:42:00 EDT) WSDL2Java emitter.
 */

package org.cagrid.cbm.stubs.service;

public interface CBMService extends javax.xml.rpc.Service {
    public java.lang.String getCBMPortTypePortAddress();

    public org.cagrid.cbm.stubs.CBMPortType getCBMPortTypePort() throws javax.xml.rpc.ServiceException;

    public org.cagrid.cbm.stubs.CBMPortType getCBMPortTypePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
